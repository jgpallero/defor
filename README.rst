.. -*- coding: utf-8 -*-

.. Este fichero está formateado con reStructuredText para que se vea en HTML en
.. https://bitbucket.org/jgpallero/defor/. Se ha procurado que siga siendo
.. legible en formato de texto plano.

********************************************************************************
FICHERO README DE 'DEFOR, PROGRAMAS PARA EL CÁLCULO DE DEFORMACIONES'
********************************************************************************

En este repositorio se almacena el paquete DEFOR, para la extracción de datos de
ficheros SINEX y el dibujo de series temporales. Este paquete se seguirá
manteniendo únicamente para subsanar errores; en ningún caso se añadirán nuevas
funcionalidades.

Para contactar con al autor:

- José Luis García Pallero, jgpallero@gmail.com, jlg.pallero@upm.es

**Nota**

En toda la documentacion del paquete se omiten deliberadamente las tildes para
que no haya problemas de visualización de los ficheros de texto en diferentes
sistemas operativos.
