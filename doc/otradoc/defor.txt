/**
\mainpage DEFOR, PROGRAMAS PARA EL CÁLCULO DE DEFORMACIONES

\author
\arg José Luis García Pallero, jgpallero@gmail.com, jlg.pallero@upm.es
\date 07 de julio de 2008
\note En toda la documentación del paquete se omiten las tildes para que no haya
      problemas de visualización de los ficheros de texto en diferentes sistemas
      operativos

El objetivo de esta biblioteca y sus programas asociados es facilitar el cálculo
de deformaciones, a partir de coordenadas cartesianas tridimensionales
geocéntricas, de una serie de puntos medidos en distintos instantes de tiempo.

Como productos finales del proceso se obtendrán ficheros con los valores
numéricos de las deformaciones calculados y resultados intermedios, así como un
script con las órdenes necesarias para la representación gráfica con GMT
(Generic Mapping Tools) de la solucion obtenida.
*/
