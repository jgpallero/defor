/**
\addtogroup geodesia
\ingroup transf
\ingroup deformacion
\ingroup dibgmt
@{
\file geodesia.c
\brief Definicion de funciones para calculos geodesicos.

En este fichero se definen varias funciones para el tratamiento de problemas
geodesicos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 16 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<math.h>
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double RadioPrimVert(double lat,
                     double a,
                     double f)
{
    //primera excentrididad al cuadrado
    double e2=2.0*f-f*f;
    //calculamos
    return a/sqrt(1.0-e2*sin(lat)*sin(lat));
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double LonArcMer(double lat,
                 double a,
                 double f)
{
    //semieje menor del elipsoide
    double b=a*(1.0-f);
    //potencias de la primera excentricidad del elipsoide
    double e2=0.0,e4=0.0,e6=0.0,e8=0.0,e10=0.0;
    //seno y coseno de la latitud y sus potencias
    double s=0.0,s3=0.0,s5=0.0,s7=0.0,s9=0.0,c=0.0;
    //sumandos de la formula
    double g1=0.0,g2=0.0,g3=0.0,g4=0.0,g5=0.0,g6=0.0;
    //longitud del arco de meridiano
    double larc=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la primera excentricidad del elipsoide y sus potencias
    e2 = (a*a-b*b)/(a*a);
    e4 = e2*e2;
    e6 = e4*e2;
    e8 = e6*e2;
    e10 = e8*e2;
    //seno y coseno de la latitud y sus potencias
    s = sin(fabs(lat));
    s3 = s*s*s;
    s5 = s3*s*s;
    s7 = s5*s*s;
    s9 = s7*s*s;
    c = cos(fabs(lat));
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //sumandos para el calculo
    g1 = fabs(lat);
    g2 = (3.0/2.0)*e2*(-0.5*s*c+0.5*fabs(lat));
    g3 = (15.0/8.0)*e4*(-0.25*s3*c-(3.0/8.0)*s*c+(3.0/8.0)*fabs(lat));
    g4 = (35.0/16.0)*e6*
         (-(1.0/6.0)*s5*c-(5.0/24.0)*s3*c-(5.0/16.0)*s*c+(5.0/16.0)*fabs(lat));
    g5 = (315.0/128.0)*e8*(-(1.0/8.0)*s7*c-(7.0/48.0)*s5*c-(35.0/192.0)*s3*c-
                           (35.0/128.0)*s*c+(35.0/128.0)*fabs(lat));
    g6 = (693.0/256.0)*e10*(-0.1*s9*c-(9.0/80.0)*s7*c-(21.0/160.0)*s5*c-
                            (21.0/128.0)*s3*c-(63.0/256.0)*s*c+(63.0/256.0)*
                            fabs(lat));
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculo de la longitud del arco de meridiano
    larc = a*(1.0-e2)*(g1+g2+g3+g4+g5+g6);
    //comprobamos si estamos en el hemisferio sur
    if(lat<0.0)
    {
        larc *= -1.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return larc;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double ArcMerLat(double arcMer,
                 double a,
                 double f)
{
    //semieje menor del elipsoide
    double b=a*(1.0-f);
    //latitud geodesica y sus aproximaciones
    double lat=0.0,lat0=0.0,lat1=0.0;
    //primera excentricidad del elipsoide y sus potencias
    double e2=0.0,e4=0.0,e6=0.0,e8=0.0,e10=0.0;
    //seno y coseno de la latitud y sus potencias
    double s=0.0,s3=0.0,s5=0.0,s7=0.0,s9=0.0,c=0.0;
    //sumandos de la formula
    double g2=0.0,g3=0.0,g4=0.0,g5=0.0,g6=0.0;
    //termino de error
    double error=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la primera excentricidad del elipsoide y sus potencias
    e2 = (a*a-b*b)/(a*a);
    e4 = e2*e2;
    e6 = e4*e2;
    e8 = e6*e2;
    e10 = e8*e2;
    //inicializamos un termino de error
    error = 1.0;
    //primera aproximacion para la latitud
    lat0 = fabs(arcMer)/(a*(1.0-e2));
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //proceso iterativo
    while(error>=DIFLAT)
    {
        //seno y coseno de lat0 y potencias
        s = sin(lat0);
        s3 = s*s*s;
        s5 = s3*s*s;
        s7 = s5*s*s;
        s9 = s7*s*s;
        c = cos(lat0);
        //calculamos los terminos g
        g2 = (3.0/2.0)*e2*(-0.5*s*c+0.5*lat0);
        g3 = (15.0/8.0)*e4*(-0.25*s3*c-(3.0/8.0)*s*c+(3.0/8.0)*lat0);
        g4 = (35.0/16.0)*e6*(-(1.0/6.0)*s5*c-(5.0/24.0)*s3*c-(5.0/16.0)*s*c+
                             (5.0/16.0)*lat0);
        g5 = (315.0/128.0)*e8*(-(1.0/8.0)*s7*c-(7.0/48.0)*s5*c-
                               (35.0/192.0)*s3*c-(35.0/128.0)*s*c+
                               (35.0/128.0)*lat0);
        g6 = (693.0/256.0)*e10*(-0.1*s9*c-(9.0/80.0)*s7*c-(21.0/160.0)*s5*c-
                                (21.0/128.0)*s3*c-(63.0/256.0)*s*c+
                                (63.0/256.0)*lat0);
        //calculamos una mejor aproximacion de la latitud
        lat1 = (fabs(arcMer)/(a*(1.0-e2)))-(g2+g3+g4+g5+g6);
        //calculamos el error
        error = fabs(lat1-lat0);
        //asignamos la nueva aproximacion a lat0
        lat0 = lat1;
    }
    //valor de salida
    lat = lat1;
    //comprobamos si estamos en el hemisferio sur
    if(arcMer<0.0)
    {
        lat *= -1.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return lat;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void GeoTri(double a,
            double f,
            double lat,
            double lon,
            double h,
            double* x,
            double* y,
            double* z)
{
    //primera excentricidad al cuadrado
    double e2=2.0*f-f*f;
    //radio de curvatura del primer vertical
    double nu=RadioPrimVert(lat,a,f);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //transformamos
    *x = (nu+h)*cos(lat)*cos(lon);
    *y = (nu+h)*cos(lat)*sin(lon);
    *z = (nu*(1.0-e2)+h)*sin(lat);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TriGeo(double a,
            double f,
            double x,
            double y,
            double z,
            double* lat,
            double* lon,
            double* h)
{
    //primera excentricidad al cuadrado
    double e2=2.0*f-f*f;
    //radio de curvatura del primer vertical
    double nu=0.0;
    //diferencia de latitudes
    double dif=0.0;
    //distancia en el plano XY
    double distXY=sqrt(x*x+y*y);
    //aproximacion a la latitud
    double lat0=0.0;
    //variable auxiliar
    double aux=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la longitud
    *lon = atan2(y,x);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos una primera aproximacion a la latitud
    lat0 = atan(z/((1.0-e2)*distXY));
    //comenzamos el proceso iterativo
    while(1)
    {
        //radio de curvatura del primer vertical
        nu = RadioPrimVert(lat0,a,f);
        //altura elipsoidal
        if(fabs(lat0)<PI/4.0)
        {
            *h = distXY/cos(lat0)-nu;
        }
        else
        {
            *h = z/sin(lat0)-(1.0-e2)*nu;
        }
        //siguiente aproximacion de la latitud
        aux = 1.0-e2*nu/(nu+(*h));
        *lat = atan(z/(distXY*aux));
        //diferencia en latitud
        dif = fabs(*lat-lat0);
        //comprobamos si hemos alcanzado la precision deseada
        if(dif<DIFLAT)
        {
            break;
        }
        else
        {
            lat0 = *lat;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void GeoTriVC(double a,
              double f,
              double lat,
              double lon,
              double h,
              double varlat,
              double varlatlon,
              double varlath,
              double varlon,
              double varlonh,
              double varh,
              double* x,
              double* y,
              double* z,
              double* varx,
              double* varxy,
              double* varxz,
              double* vary,
              double* varyz,
              double* varz)
{
    //primera excentricidad al cuadrado
    double e2=2.0*f-f*f;
    //radio de curvatura del primer vertical
    double nu=RadioPrimVert(lat,a,f);
    //razones trigonometricas auxiliares
    double sLat=sin(lat),cLat=cos(lat),sLon=sin(lon),cLon=cos(lon);
    //matrices auxiliares
    double j[3][3],jvc[3][3];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    GeoTri(a,f,lat,lon,h,x,y,z);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos la matriz jacobiana
    j[0][0] = -(nu+h)*sLat*cLon;
    j[0][1] = -(nu+h)*cLat*sLon;
    j[0][2] = cLat*cLon;
    j[1][0] = -(nu+h)*sLat*sLon;
    j[1][1] = (nu+h)*cLat*cLon;
    j[1][2] = cLat*sLon;
    j[2][0] = (nu*(1.0-e2)+h)*cLat;
    j[2][1] = 0.0;
    j[2][2] = sLat;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza
    jvc[0][0] = j[0][0]*varlat+j[0][1]*varlatlon+j[0][2]*varlath;
    jvc[0][1] = j[0][0]*varlatlon+j[0][1]*varlon+j[0][2]*varlonh;
    jvc[0][2] = j[0][0]*varlath+j[0][1]*varlonh+j[0][2]*varh;
    jvc[1][0] = j[1][0]*varlat+j[1][1]*varlatlon+j[1][2]*varlath;
    jvc[1][1] = j[1][0]*varlatlon+j[1][1]*varlon+j[1][2]*varlonh;
    jvc[1][2] = j[1][0]*varlath+j[1][1]*varlonh+j[1][2]*varh;
    jvc[2][0] = j[2][0]*varlat+j[2][1]*varlatlon+j[2][2]*varlath;
    jvc[2][1] = j[2][0]*varlatlon+j[2][1]*varlon+j[2][2]*varlonh;
    jvc[2][2] = j[2][0]*varlath+j[2][1]*varlonh+j[2][2]*varh;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varx = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1]+jvc[0][2]*j[0][2];
    *varxy = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1]+jvc[0][2]*j[1][2];
    *varxz = jvc[0][0]*j[2][0]+jvc[0][1]*j[2][1]+jvc[0][2]*j[2][2];
    *vary = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1]+jvc[1][2]*j[1][2];
    *varyz = jvc[1][0]*j[2][0]+jvc[1][1]*j[2][1]+jvc[1][2]*j[2][2];
    *varz = jvc[2][0]*j[2][0]+jvc[2][1]*j[2][1]+jvc[2][2]*j[2][2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TriGeoVC(double a,
              double f,
              double x,
              double y,
              double z,
              double varx,
              double varxy,
              double varxz,
              double vary,
              double varyz,
              double varz,
              double* lat,
              double* lon,
              double* h,
              double* varlat,
              double* varlatlon,
              double* varlath,
              double* varlon,
              double* varlonh,
              double* varh)
{
    //primera excentricidad al cuadrado
    double e2=2.0*f-f*f;
    //radio de curvatura del primer vertical
    double nu=0.0;
    //variables auxiliares
    double factor1=0.0,factor2=0.0;
    double distXY=sqrt(x*x+y*y);
    //matrices auxiliares
    double j[3][3],jvc[3][3];
    //variable para realizar una derivada auxiliar
    double dLatH=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    TriGeo(a,f,x,y,z,lat,lon,h);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos algunas partes constantes para las derivadas parciales
    nu = RadioPrimVert(*lat,a,f);
    factor1 = (nu+(*h))/(*h+nu*(1.0-e2));
    factor2 = z*factor1/distXY;
    //primero calculamos las derivadas de la latitud considerando h constante
    j[0][0] = -x*z*factor1/((1.0+pow(factor2,2.0))*pow(distXY,3.0));
    j[0][1] = -y*z*factor1/((1.0+pow(factor2,2.0))*pow(distXY,3.0));
    j[0][2] = factor1/(distXY*(1.0+pow(factor2,2.0)));
    //las derivadas de la longitud se calculan a la primera
    j[1][0] = -y/pow(distXY,2.0);
    j[1][1] = x/pow(distXY,2.0);
    j[1][2] = 0.0;
    //derivada de la latitud con respecto a la altitud
    dLatH = -z*nu*e2/(distXY*pow(*h+nu*(1.0-e2),2.0)*(1.0+pow(factor2,2.0)));
    //calculamos las derivadas de la altitud
    if(fabs(*lat)<PI/4.0)
    {
        j[2][0] = (x*cos(*lat)/distXY+sin(*lat)*distXY*j[0][0])/
                  pow(cos(*lat),2.0);
        j[2][1] = (y*cos(*lat)/distXY+sin(*lat)*distXY*j[0][1])/
                  pow(cos(*lat),2.0);
        j[2][2] = sin(*lat)*distXY*j[0][2]/pow(cos(*lat),2.0);
    }
    else
    {
        j[2][0] = -z*cos(*lat)*j[0][0]/pow(sin(*lat),2.0);
        j[2][1] = -z*cos(*lat)*j[0][1]/pow(sin(*lat),2.0);
        j[2][2] = (sin(*lat)-z*cos(*lat)*j[0][2])/pow(sin(*lat),2.0);
    }
    //recalculamos las derivadas de la latitud teniendo en cuenta la altitud
    j[0][0] = j[0][0]+dLatH*j[2][0];
    j[0][1] = j[0][1]+dLatH*j[2][1];
    j[0][2] = j[0][2]+dLatH*j[2][2];
    //recalculamos las derivadas de la altitud
    if(fabs(*lat)<PI/4.0)
    {
        j[2][0] = (x*cos(*lat)/distXY+sin(*lat)*distXY*j[0][0])/
                pow(cos(*lat),2.0);
        j[2][1] = (y*cos(*lat)/distXY+sin(*lat)*distXY*j[0][1])/
                pow(cos(*lat),2.0);
        j[2][2] = sin(*lat)*distXY*j[0][2]/pow(cos(*lat),2.0);
    }
    else
    {
        j[2][0] = -z*cos(*lat)*j[0][0]/pow(sin(*lat),2.0);
        j[2][1] = -z*cos(*lat)*j[0][1]/pow(sin(*lat),2.0);
        j[2][2] = (sin(*lat)-z*cos(*lat)*j[0][2])/pow(sin(*lat),2.0);
    }
    //producto de la matriz jacobiana por la matriz de varianza-covarianza
    jvc[0][0] = j[0][0]*varx+j[0][1]*varxy+j[0][2]*varxz;
    jvc[0][1] = j[0][0]*varxy+j[0][1]*vary+j[0][2]*varyz;
    jvc[0][2] = j[0][0]*varxz+j[0][1]*varyz+j[0][2]*varz;
    jvc[1][0] = j[1][0]*varx+j[1][1]*varxy+j[1][2]*varxz;
    jvc[1][1] = j[1][0]*varxy+j[1][1]*vary+j[1][2]*varyz;
    jvc[1][2] = j[1][0]*varxz+j[1][1]*varyz+j[1][2]*varz;
    jvc[2][0] = j[2][0]*varx+j[2][1]*varxy+j[2][2]*varxz;
    jvc[2][1] = j[2][0]*varxy+j[2][1]*vary+j[2][2]*varyz;
    jvc[2][2] = j[2][0]*varxz+j[2][1]*varyz+j[2][2]*varz;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varlat = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1]+jvc[0][2]*j[0][2];
    *varlatlon = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1]+jvc[0][2]*j[1][2];
    *varlath = jvc[0][0]*j[2][0]+jvc[0][1]*j[2][1]+jvc[0][2]*j[2][2];
    *varlon = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1]+jvc[1][2]*j[1][2];
    *varlonh = jvc[1][0]*j[2][0]+jvc[1][1]*j[2][1]+jvc[1][2]*j[2][2];
    *varh = jvc[2][0]*j[2][0]+jvc[2][1]*j[2][1]+jvc[2][2]*j[2][2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void EnuTri(double lat,
            double lon,
            double e,
            double n,
            double u,
            double* x,
            double* y,
            double* z)
{
    //variables auxiliares
    double sLat=sin(lat),cLat=cos(lat),sLon=sin(lon),cLon=cos(lon);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    *x = -e*sLon-n*sLat*cLon+u*cLat*cLon;
    *y =  e*cLon-n*sLat*sLon+u*cLat*sLon;
    *z =  n*cLat+u*sLat;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TriEnu(double lat,
            double lon,
            double x,
            double y,
            double z,
            double* e,
            double* n,
            double* u)
{
    //variables auxiliares
    double sLat=sin(lat),cLat=cos(lat),sLon=sin(lon),cLon=cos(lon);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    *e = -x*sLon+y*cLon;
    *n = -x*sLat*cLon-y*sLat*sLon+z*cLat;
    *u =  x*cLat*cLon+y*cLat*sLon+z*sLat;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void EnuTriVC(double lat,
              double lon,
              double e,
              double n,
              double u,
              double vare,
              double varen,
              double vareu,
              double varn,
              double varnu,
              double varu,
              double* x,
              double* y,
              double* z,
              double* varx,
              double* varxy,
              double* varxz,
              double* vary,
              double* varyz,
              double* varz)
{
    //variables auxiliares
    double sLat=sin(lat),cLat=cos(lat),sLon=sin(lon),cLon=cos(lon);
    //matrices auxiliares
    double j[3][3],jvc[3][3];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    EnuTri(lat,lon,e,n,u,x,y,z);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos la matriz jacobiana, que es la misma que la de rotacion utilizada
    //para la transformacion de coordenadas
    j[0][0] = -sLon;
    j[0][1] = -sLat*cLon;
    j[0][2] = cLat*cLon;
    j[1][0] = cLon;
    j[1][1] = -sLat*sLon;
    j[1][2] = cLat*sLon;
    j[2][0] = 0.0;
    j[2][1] = cLat;
    j[2][2] = sLat;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza del
    //vector en coordenadas cartesianas topocentricas
    jvc[0][0] = j[0][0]*vare+j[0][1]*varen+j[0][2]*vareu;
    jvc[0][1] = j[0][0]*varen+j[0][1]*varn+j[0][2]*varnu;
    jvc[0][2] = j[0][0]*vareu+j[0][1]*varnu+j[0][2]*varu;
    jvc[1][0] = j[1][0]*vare+j[1][1]*varen+j[1][2]*vareu;
    jvc[1][1] = j[1][0]*varen+j[1][1]*varn+j[1][2]*varnu;
    jvc[1][2] = j[1][0]*vareu+j[1][1]*varnu+j[1][2]*varu;
    jvc[2][0] = j[2][0]*vare+j[2][1]*varen+j[2][2]*vareu;
    jvc[2][1] = j[2][0]*varen+j[2][1]*varn+j[2][2]*varnu;
    jvc[2][2] = j[2][0]*vareu+j[2][1]*varnu+j[2][2]*varu;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varx = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1]+jvc[0][2]*j[0][2];
    *varxy = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1]+jvc[0][2]*j[1][2];
    *varxz = jvc[0][0]*j[2][0]+jvc[0][1]*j[2][1]+jvc[0][2]*j[2][2];
    *vary = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1]+jvc[1][2]*j[1][2];
    *varyz = jvc[1][0]*j[2][0]+jvc[1][1]*j[2][1]+jvc[1][2]*j[2][2];
    *varz = jvc[2][0]*j[2][0]+jvc[2][1]*j[2][1]+jvc[2][2]*j[2][2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TriEnuVC(double lat,
              double lon,
              double x,
              double y,
              double z,
              double varx,
              double varxy,
              double varxz,
              double vary,
              double varyz,
              double varz,
              double* e,
              double* n,
              double* u,
              double* vare,
              double* varen,
              double* vareu,
              double* varn,
              double* varnu,
              double* varu)
{
    //variables auxiliares
    double sLat=sin(lat),cLat=cos(lat),sLon=sin(lon),cLon=cos(lon);
    //matrices auxiliares
    double j[3][3],jvc[3][3];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    TriEnu(lat,lon,x,y,z,e,n,u);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos la matriz jacobiana, que es la misma que la de rotacion utilizada
    //para la transformacion de coordenadas
    j[0][0] = -sLon;
    j[0][1] = cLon;
    j[0][2] = 0.0;
    j[1][0] = -sLat*cLon;
    j[1][1] = -sLat*sLon;
    j[1][2] = cLat;
    j[2][0] = cLat*cLon;
    j[2][1] = cLat*sLon;
    j[2][2] = sLat;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza del
    //vector en coordenadas cartesianas geocentricas
    jvc[0][0] = j[0][0]*varx+j[0][1]*varxy+j[0][2]*varxz;
    jvc[0][1] = j[0][0]*varxy+j[0][1]*vary+j[0][2]*varyz;
    jvc[0][2] = j[0][0]*varxz+j[0][1]*varyz+j[0][2]*varz;
    jvc[1][0] = j[1][0]*varx+j[1][1]*varxy+j[1][2]*varxz;
    jvc[1][1] = j[1][0]*varxy+j[1][1]*vary+j[1][2]*varyz;
    jvc[1][2] = j[1][0]*varxz+j[1][1]*varyz+j[1][2]*varz;
    jvc[2][0] = j[2][0]*varx+j[2][1]*varxy+j[2][2]*varxz;
    jvc[2][1] = j[2][0]*varxy+j[2][1]*vary+j[2][2]*varyz;
    jvc[2][2] = j[2][0]*varxz+j[2][1]*varyz+j[2][2]*varz;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *vare = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1]+jvc[0][2]*j[0][2];
    *varen = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1]+jvc[0][2]*j[1][2];
    *vareu = jvc[0][0]*j[2][0]+jvc[0][1]*j[2][1]+jvc[0][2]*j[2][2];
    *varn = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1]+jvc[1][2]*j[1][2];
    *varnu = jvc[1][0]*j[2][0]+jvc[1][1]*j[2][1]+jvc[1][2]*j[2][2];
    *varu = jvc[2][0]*j[2][0]+jvc[2][1]*j[2][1]+jvc[2][2]*j[2][2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int HusoUtm(double lon)
{
    //variable auxiliar
    double aux=0.0;
    //huso calculado
    int huso=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos el numero bloques de 6 grados desde el antimeridiano de
    //Greenwich, que es donde empiezan a contar los husos UTM
    aux = (lon+PI)/(DR*6.0);
    //redondeamos hacia arriba para obtener el número de huso
    huso = (int)ceil(aux);
    //un punto pertenece al huso anterior aunque se meta 0.000001 segundos
    //(0.03 mm) en el siguiente huso
    if((aux-(double)huso+1.0)<(DR*0.000001/3600.0))
    {
        huso--;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return huso;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double MeridianoCentralUtm(int huso)

{
    //anchura del huso UTM
    double dlon=6.0*DR;
    //longitud del meridiano central del huso 1
    double lon=-177.0*DR;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos
    lon += (huso-1.0)*dlon;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return lon;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void GeoUtm(double lat,
            double lon,
            double a,
            double f,
            double* x,
            double* y,
            int* huso)
{
    //traslacion en X y coeficiente de anamorfosos en el meridiano central
    double tx=500000.0,k0=0.9996;
    //traslacion en Y, por si el punto es del hemisferio sur
    double ty=0.0;
    //meridiano central de huso de trabajo
    double lonmcent=0.0;
    //semieje menor del elipsoide
    double b=a*(1.0-f);
    //segunda excentricidad del elipsoide
    double eprima=sqrt((a*a-b*b)/(b*b));
    //variable eta y sus potencias
    double eta=0.0,eta2=0.0,eta4=0.0,eta6=0.0,eta8=0.0;
    //tangente de la latitud y sus potencias
    double t=0.0,t2=0.0,t4=0.0;
    //coseno de la latitud y sus potencias
    double c=0.0,c2=0.0,c3=0.0,c4=0.0,c5=0.0,c6=0.0;
    //incremento de longitud y sus potencias
    double dl=0.0,dl2=0.0,dl3=0.0,dl4=0.0,dl5=0.0,dl6=0.0;
    //longitud del arco de meridiano desde el ecuador hasta el punto de trabajo
    double beta=0.0;
    //sumandos para las funciones de las coordenadas
    double s1=0.0,s2=0.0,s3=0.0;
    //radio de curvatura del primer vertical
    double nu=RadioPrimVert(lat,a,f);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el punto esta en el hemisferio sur
    if(lat<0.0)
    {
        //asignamos la traslacion en Y
        ty = 10000000.0;
        //pasamos la latitud a valor absoluto
        lat = fabs(lat);
    }
    //calculamos el huso UTM
    *huso = HusoUtm(lon);
    //calculamos el meridiano central de huso
    lonmcent = MeridianoCentralUtm(*huso);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //variable eta y sus potencias
    eta = eprima*cos(lat);
    eta2 = eta*eta;
    eta4 = eta2*eta2;
    eta6 = eta4*eta2;
    eta8 = eta6*eta2;
    //tangente de la latitud y sus potencias
    t = tan(lat);
    t2 = t*t;
    t4 = t2*t2;
    //coseno de la latitud y sus potencias
    c = cos(lat);
    c2 = c*c;
    c3 = c2*c;
    c4 = c3*c;
    c5 = c4*c;
    c6 = c5*c;
    //incremento de longitud y sus potencias
    dl = lon-lonmcent;
    dl2 = dl*dl;
    dl3 = dl2*dl;
    dl4 = dl3*dl;
    dl5 = dl4*dl;
    dl6 = dl5*dl;
    //longitud del arco de meridiano desde el ecuador hasta el punto de trabajo
    beta = LonArcMer(lat,a,f);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //sumandos para la x
    s1 = dl*nu*c;
    s2 = (dl3/6.0)*nu*c3*(1.0-t2+eta2);
    s3 = (dl5/120.0)*nu*c5*(5.0-18.0*t2+t4+14.0*eta2-58.0*eta2*t2+13.0*eta4-
                            64.0*eta4*t2+4.0*eta6-24.0*eta6*t2);
    //coordenada x
    *x = tx+k0*(s1+s2+s3);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //sumandos para la y
    s1 = 0.5*dl2*nu*t*c2;
    s2 = (dl4/24.0)*nu*t*c4*(5.0-t2+9.0*eta2+4.0*eta4);
    s3 = (dl6/720.0)*nu*t*c6*(61.0-58.0*t2+t4+270.0*eta2-330.0*eta2*t2+
                              445.0*eta4-680.0*eta4*t2+324.0*eta6-600.0*eta6*t2+
                              88.0*eta8-192.0*eta8*t2);
    //coordenada y
    *y = k0*(beta+s1+s2+s3);
    //si el punto pertenece al hemisferios sur, anyadimos la traslacion en Y
    if(ty!=0.0)
    {
        *y = ty-(*y);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void UtmGeo(double x,
            double y,
            int huso,
            int hemisferio,
            double a,
            double f,
            double* lat,
            double* lon)
{
    //traslacion en X y coeficiente de anamorfosos en el meridiano central
    double tx=500000.0,k0=0.9996;
    //traslacion en Y, por si el punto es del hemisferio sur
    double ty=10000000.0;
    //meridiano central de huso de trabajo
    double lonmcent=MeridianoCentralUtm(huso);
    //semieje menor del elipsoide
    double b=a*(1.0-f);
    //segunda excentricidad del elipsoide
    double eprima=sqrt((a*a-b*b)/(b*b));
    //latitud a partir de la coordenada Y
    double lat0=0.0;
    //variable eta y sus potencias
    double eta=0.0,eta2=0.0,eta4=0.0,eta6=0.0,eta8=0.0,eta10=0.0;
    //tangente de la latitud y sus potencias
    double t=0.0,t2=0.0,t4=0.0;
    //coseno de la latitud
    double c=0.0;
    //potencias de x
    double x2=0.0,x3=0.0,x4=0.0,x5=0.0,x6=0.0;
    //sumandos para las funciones de las coordenadas
    double s1=0.0,s2=0.0,s3=0.0;
    //radio de curvatura del primer vertical y sus potencias
    double nu=0.0,nu2=0.0,nu3=0.0,nu4=0.0,nu5=0.0,nu6=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //coordenadas sin traslacion ni factor de escala
    x = (x-tx)/k0;
    if(hemisferio!=0)
    {
        y /= k0;
    }
    else
    {
        y = (y-ty)/k0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //latitud aproximada
    lat0 = ArcMerLat(y,a,f);
    //variable eta y sus potencias
    eta = eprima*cos(lat0);
    eta2 = eta*eta;
    eta4 = eta2*eta2;
    eta6 = eta4*eta2;
    eta8 = eta6*eta8;
    //tangente de la latitud y sus potencias
    t = tan(lat0);
    t2 = t*t;
    t4 = t2*t2;
    //coseno de la latitud
    c = cos(lat0);
    //radio de curvatura del primer vertical y sus potencias
    nu = RadioPrimVert(lat0,a,f);
    nu2 = nu*nu;
    nu3 = nu2*nu;
    nu4 = nu3*nu;
    nu5 = nu4*nu;
    nu6 = nu5*nu;
    //potencias de la coordenada X
    x2 = x*x;
    x3 = x2*x;
    x4 = x3*x;
    x5 = x4*x;
    x6 = x5*x;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //sumandos para la longitud
    s1 = (x/(nu*c));
    s2 = (-x3/(6.0*nu3*c))*(1.0+2.0*t2+eta2);
    s3 = (x5/(120.0*nu5*c))*(5.0+28.0*t2+24.0*t4+6.0*eta2+8.0*eta2*t2-3.0*eta4+
                             4.0*eta4*t2-4.0*eta6+24.0*eta6*t2);
    //longitud geodesica
    *lon = lonmcent+s1+s2+s3;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //sumandos para la latitud
    s1 = (-x2*t/(2.0*nu2))*(1.0+eta2);
    s2 = (x4*t/(24.0*nu4))*
         (5.0+3.0*t2+6.0*eta2-6.0*eta2*t2-3.0*eta4-9.0*eta4*t2);
    s3 = (-x6*t/(720.0*nu6))*(61.0+90.0*t2+45.0*t4+107.0*eta2-162.0*eta2*t2-
                              45.0*eta2*t4+43.0*eta4-318.0*eta4*t2+
                              135.0*eta4*t4+97.0*eta6+18.0*eta6*t2+
                              225.0*eta6*t4+188.0*eta8-108.0*eta8*t2+88.0*eta10-
                              192.0*eta10*t2);
    //latitud geodesica
    *lat = lat0+s1+s2+s3;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void GeoUtmVC(double lat,
              double lon,
              double varLat,
              double varLatLon,
              double varLon,
              double a,
              double f,
              double* x,
              double* y,
              double* varx,
              double* varxy,
              double* vary,
              int* huso)
{
    //matrices auxiliares
    double j[2][2],jvc[2][2];
    //variables auxiliares
    double x1=0.0,y1=0.0;
    int huso1=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    GeoUtm(lat,lon,a,f,x,y,huso);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos la matriz jacobiana
    GeoUtm(lat+DIFLAT,lon,a,f,&x1,&y1,&huso1);
    j[0][0] = (x1-(*x))/DIFLAT;
    j[1][0] = (y1-(*y))/DIFLAT;
    GeoUtm(lat,lon+DIFLAT,a,f,&x1,&y1,&huso1);
    j[0][1] = (x1-(*x))/DIFLAT;
    j[1][1] = (y1-(*y))/DIFLAT;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza de
    //las coordenadas geodesicas
    jvc[0][0] = j[0][0]*varLat+j[0][1]*varLatLon;
    jvc[0][1] = j[0][0]*varLatLon+j[0][1]*varLon;
    jvc[1][0] = j[1][0]*varLat+j[1][1]*varLatLon;
    jvc[1][1] = j[1][0]*varLatLon+j[1][1]*varLon;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varx = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1];
    *varxy = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1];
    *vary = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void UtmGeoVC(double x,
              double y,
              double varx,
              double varxy,
              double vary,
              int huso,
              int hemisferio,
              double a,
              double f,
              double* lat,
              double* lon,
              double* varLat,
              double* varLatLon,
              double* varLon)
{
    //matrices auxiliares
    double j[2][2],jvc[2][2];
    //variables auxiliares
    double lat1=0.0,lon1=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    UtmGeo(x,y,huso,hemisferio,a,f,lat,lon);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos la matriz jacobiana
    UtmGeo(x+DIFUTM,y,huso,hemisferio,a,f,&lat1,&lon1);
    j[0][0] = (lat1-(*lat))/DIFUTM;
    j[1][0] = (lon1-(*lon))/DIFUTM;
    UtmGeo(x,y+DIFUTM,huso,hemisferio,a,f,&lat1,&lon1);
    j[0][1] = (lat1-(*lat))/DIFUTM;
    j[1][1] = (lon1-(*lon))/DIFUTM;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza de
    //las coordenadas geodesicas
    jvc[0][0] = j[0][0]*varx+j[0][1]*varxy;
    jvc[0][1] = j[0][0]*varxy+j[0][1]*vary;
    jvc[1][0] = j[1][0]*varx+j[1][1]*varxy;
    jvc[1][1] = j[1][0]*varxy+j[1][1]*vary;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varLat = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1];
    *varLatLon = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1];
    *varLon = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void RestaEpocas(double x1,
                 double y1,
                 double z1,
                 double x2,
                 double y2,
                 double z2,
                 double* dx,
                 double* dy,
                 double* dz)
{
    //calculamos el movimiento
    *dx = x2-x1;
    *dy = y2-y1;
    *dz = z2-z1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void RestaEpocasVC(double x1,
                   double y1,
                   double z1,
                   double varx1,
                   double varx1y1,
                   double varx1z1,
                   double vary1,
                   double vary1z1,
                   double varz1,
                   double x2,
                   double y2,
                   double z2,
                   double varx2,
                   double varx2y2,
                   double varx2z2,
                   double vary2,
                   double vary2z2,
                   double varz2,
                   double* dx,
                   double* dy,
                   double* dz,
                   double* vardx,
                   double* vardxdy,
                   double* vardxdz,
                   double* vardy,
                   double* vardydz,
                   double* vardz)
{
    //calculamos el movimiento
    RestaEpocas(x1,y1,z1,x2,y2,z2,dx,dy,dz);
    //calculamos la propagacion de errores
    *vardx = varx1+varx2;
    *vardxdy = varx1y1+varx2y2;
    *vardxdz = varx1z1+varx2z2;
    *vardy = vary1+vary2;
    *vardydz = vary1z1+vary2z2;
    *vardz = varz1+varz2;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ElipseErrorPlano(double varx,
                      double varxy,
                      double vary,
                      double *a,
                      double *b,
                      double *angulo)
{
    //variable auxiliar
    double aux=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //termino auxiliar
    aux = sqrt(pow(varx-vary,2.0)+4.0*pow(varxy,2.0));
    //semieje mayor
    *a = sqrt((varx+vary+aux)/2.0);
    //semieje menor
    *b = sqrt((varx+vary-aux)/2.0);
    //angulo del semieje mayor
    *angulo = atan2(2.0*varxy,varx-vary)/2.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double AnguloMateAcimut(double angulo)
{
    //realizamos la conversion dependiendo del cuadrante en que se encuentre el
    //angulo pasado
    if((angulo>=0.0)&&(angulo<=PI/2.0))
    {
        //el angulo esta en el primer cuadrante trigonometrico
        return PI/2.0-angulo;
    }
    else if(angulo>PI/2.0)
    {
        //el angulo esta en el segundo cuadrante trigonometrico
        return 5.0*PI/2.0-angulo;
    }
    else
    {
        //el angulo esta en el tercer o cuarto cuadrante trigonometrico
        return PI/2.0+fabs(angulo);
    }
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TranSistRef(double t0,
                 double tx,
                 double ty,
                 double tz,
                 double d,
                 double rx,
                 double ry,
                 double rz,
                 double vtx,
                 double vty,
                 double vtz,
                 double vd,
                 double vrx,
                 double vry,
                 double vrz,
                 double t1,
                 double x1,
                 double y1,
                 double z1,
                 double* x2,
                 double* y2,
                 double* z2)
{
    //aplicamos las velocidades a las traslaciones
    tx = tx+vtx*(t1-t0);
    ty = ty+vty*(t1-t0);
    tz = tz+vtz*(t1-t0);
    //aplicamos las velocidades al factor de escala
    d = d+vd*(t1-t0);
    //aplicamos las velocidades a las rotaciones
    rx = rx+vrx*(t1-t0);
    ry = ry+vry*(t1-t0);
    rz = rz+vrz*(t1-t0);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos las nuevas coordenadas
    *x2 = x1+tx+x1*d-y1*rz+z1*ry;
    *y2 = y1+ty+x1*rz+y1*d-z1*rx;
    *z2 = z1+tz-x1*ry+y1*rx+z1*d;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TranSistRefVC(double t0,
                   double tx,
                   double ty,
                   double tz,
                   double d,
                   double rx,
                   double ry,
                   double rz,
                   double vtx,
                   double vty,
                   double vtz,
                   double vd,
                   double vrx,
                   double vry,
                   double vrz,
                   double t1,
                   double x1,
                   double y1,
                   double z1,
                   double varx1,
                   double varx1y1,
                   double varx1z1,
                   double vary1,
                   double vary1z1,
                   double varz1,
                   double* x2,
                   double* y2,
                   double* z2,
                   double* varx2,
                   double* varx2y2,
                   double* varx2z2,
                   double* vary2,
                   double* vary2z2,
                   double* varz2)
{
    //matrices auxiliares
    double j[3][3],jvc[3][3];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la transformacion
    TranSistRef(t0,tx,ty,tz,d,rx,ry,rz,
                vtx,vty,vtz,vd,vrx,vry,vrz,
                t1,x1,y1,z1,
                x2,y2,z2);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aplicamos las velocidades a las traslaciones
    tx = tx+vtx*(t1-t0);
    ty = ty+vty*(t1-t0);
    tz = tz+vtz*(t1-t0);
    //aplicamos las velocidades al factor de escala
    d = d+vd*(t1-t0);
    //aplicamos las velocidades a las rotaciones
    rx = rx+vrx*(t1-t0);
    ry = ry+vry*(t1-t0);
    rz = rz+vrz*(t1-t0);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //rellenamos la matriz jacobiana, es un desperdicio de memoria pero
    //clarifica el codigo
    j[0][0] = 1.0+d;
    j[0][1] = -rz;
    j[0][2] = ry;
    j[1][0] = rz;
    j[1][1] = 1.0+d;
    j[1][2] = -rx;
    j[2][0] = -ry;
    j[2][1] = rx;
    j[2][2] = 1.0+d;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza del
    //punto original
    jvc[0][0] = j[0][0]*varx1+j[0][1]*varx1y1+j[0][2]*varx1z1;
    jvc[0][1] = j[0][0]*varx1y1+j[0][1]*vary1+j[0][2]*vary1z1;
    jvc[0][2] = j[0][0]*varx1z1+j[0][1]*vary1z1+j[0][2]*varz1;
    jvc[1][0] = j[1][0]*varx1+j[1][1]*varx1y1+j[1][2]*varx1z1;
    jvc[1][1] = j[1][0]*varx1y1+j[1][1]*vary1+j[1][2]*vary1z1;
    jvc[1][2] = j[1][0]*varx1z1+j[1][1]*vary1z1+j[1][2]*varz1;
    jvc[2][0] = j[2][0]*varx1+j[2][1]*varx1y1+j[2][2]*varx1z1;
    jvc[2][1] = j[2][0]*varx1y1+j[2][1]*vary1+j[2][2]*vary1z1;
    jvc[2][2] = j[2][0]*varx1z1+j[2][1]*vary1z1+j[2][2]*varz1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varx2 = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1]+jvc[0][2]*j[0][2];
    *varx2y2 = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1]+jvc[0][2]*j[1][2];
    *varx2z2 = jvc[0][0]*j[2][0]+jvc[0][1]*j[2][1]+jvc[0][2]*j[2][2];
    *vary2 = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1]+jvc[1][2]*j[1][2];
    *vary2z2 = jvc[1][0]*j[2][0]+jvc[1][1]*j[2][1]+jvc[1][2]*j[2][2];
    *varz2 = jvc[2][0]*j[2][0]+jvc[2][1]*j[2][1]+jvc[2][2]*j[2][2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void AplicaVelocidad(double x1,
                     double y1,
                     double z1,
                     double vx,
                     double vy,
                     double vz,
                     double fecha1,
                     double fecha2,
                     double* x2,
                     double* y2,
                     double* z2)
{
    //calculamos las nuevas coordenadas
    *x2 = (fecha2-fecha1)*vx+x1;
    *y2 = (fecha2-fecha1)*vy+y1;
    *z2 = (fecha2-fecha1)*vz+z1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void AplicaVelocidadVC(double x1,
                       double y1,
                       double z1,
                       double varx1,
                       double varx1y1,
                       double varx1z1,
                       double vary1,
                       double vary1z1,
                       double varz1,
                       double vx,
                       double vy,
                       double vz,
                       double varvx,
                       double varvxvy,
                       double varvxvz,
                       double varvy,
                       double varvyvz,
                       double varvz,
                       double fecha1,
                       double fecha2,
                       double* x2,
                       double* y2,
                       double* z2,
                       double* varx2,
                       double* varx2y2,
                       double* varx2z2,
                       double* vary2,
                       double* vary2z2,
                       double* varz2)
{
    //matrices auxiliares
    double j[3][6],jvc[3][6];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aplicamos las velocidades
    AplicaVelocidad(x1,y1,z1,vx,vy,vz,fecha1,fecha2,x2,y2,z2);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //rellenamos la matriz jacobiana, es un desperdicio de memoria pero
    //clarifica el codigo
    j[0][0] = 1.0;
    j[0][1] = 0.0;
    j[0][2] = 0.0;
    j[0][3] = fecha2-fecha1;
    j[0][4] = 0.0;
    j[0][5] = 0.0;
    j[1][0] = 0.0;
    j[1][1] = 1.0;
    j[1][2] = 0.0;
    j[1][3] = 0.0;
    j[1][4] = fecha2-fecha1;
    j[1][5] = 0.0;
    j[2][0] = 0.0;
    j[2][1] = 0.0;
    j[2][2] = 1.0;
    j[2][3] = 0.0;
    j[2][4] = 0.0;
    j[2][5] = fecha2-fecha1;
    //producto de la matriz jacobiana por la matriz de varianza-covarianza
    //hay operaciones que sobran, pero se ponen para clarificar el codigo
    jvc[0][0] = j[0][0]*varx1+j[0][1]*varx1y1+j[0][2]*varx1z1+
                j[0][3]*0.0+j[0][4]*0.0+j[0][5]*0.0;
    jvc[0][1] = j[0][0]*varx1y1+j[0][1]*vary1+j[0][2]*vary1z1+
                j[0][3]*0.0+j[0][4]*0.0+j[0][5]*0.0;
    jvc[0][2] = j[0][0]*varx1z1+j[0][1]*vary1z1+j[0][2]*varz1+
                j[0][3]*0.0+j[0][4]*0.0+j[0][5]*0.0;
    jvc[0][3] = j[0][0]*0.0+j[0][1]*0.0+j[0][2]*0.0+
                j[0][3]*varvx+j[0][4]*varvxvy+j[0][5]*varvxvz;
    jvc[0][4] = j[0][0]*0.0+j[0][1]*0.0+j[0][2]*0.0+
                j[0][3]*varvxvy+j[0][4]*varvy+j[0][5]*varvyvz;
    jvc[0][5] = j[0][0]*0.0+j[0][1]*0.0+j[0][2]*0.0+
                j[0][3]*varvxvz+j[0][4]*varvyvz+j[0][5]*varvz;
    jvc[1][0] = j[1][0]*varx1+j[1][1]*varx1y1+j[1][2]*varx1z1+
                j[1][3]*0.0+j[1][4]*0.0+j[1][5]*0.0;
    jvc[1][1] = j[1][0]*varx1y1+j[1][1]*vary1+j[1][2]*vary1z1+
                j[1][3]*0.0+j[1][4]*0.0+j[1][5]*0.0;
    jvc[1][2] = j[1][0]*varx1z1+j[1][1]*vary1z1+j[1][2]*varz1+
                j[1][3]*0.0+j[1][4]*0.0+j[1][5]*0.0;
    jvc[1][3] = j[1][0]*0.0+j[1][1]*0.0+j[1][2]*0.0+
                j[1][3]*varvx+j[1][4]*varvxvy+j[1][5]*varvxvz;
    jvc[1][4] = j[1][0]*0.0+j[1][1]*0.0+j[1][2]*0.0+
                j[1][3]*varvxvy+j[1][4]*varvy+j[1][5]*varvyvz;
    jvc[1][5] = j[1][0]*0.0+j[1][1]*0.0+j[1][2]*0.0+
                j[1][3]*varvxvz+j[1][4]*varvyvz+j[1][5]*varvz;
    jvc[2][0] = j[2][0]*varx1+j[2][1]*varx1y1+j[2][2]*varx1z1+
                j[2][3]*0.0+j[2][4]*0.0+j[2][5]*0.0;
    jvc[2][1] = j[2][0]*varx1y1+j[2][1]*vary1+j[2][2]*vary1z1+
                j[2][3]*0.0+j[2][4]*0.0+j[2][5]*0.0;
    jvc[2][2] = j[2][0]*varx1z1+j[2][1]*vary1z1+j[2][2]*varz1+
                j[2][3]*0.0+j[2][4]*0.0+j[2][5]*0.0;
    jvc[2][3] = j[2][0]*0.0+j[2][1]*0.0+j[2][2]*0.0+
                j[2][3]*varvx+j[2][4]*varvxvy+j[2][5]*varvxvz;
    jvc[2][4] = j[2][0]*0.0+j[2][1]*0.0+j[2][2]*0.0+
                j[2][3]*varvxvy+j[2][4]*varvy+j[2][5]*varvyvz;
    jvc[2][5] = j[2][0]*0.0+j[2][1]*0.0+j[2][2]*0.0+
                j[2][3]*varvxvz+j[2][4]*varvyvz+j[2][5]*varvz;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la propagacion de errores
    *varx2 = jvc[0][0]*j[0][0]+jvc[0][1]*j[0][1]+jvc[0][2]*j[0][2]+
             jvc[0][3]*j[0][3]+jvc[0][4]*j[0][4]+jvc[0][5]*j[0][5];
    *varx2y2 = jvc[0][0]*j[1][0]+jvc[0][1]*j[1][1]+jvc[0][2]*j[1][2]+
               jvc[0][3]*j[1][3]+jvc[0][4]*j[1][4]+jvc[0][5]*j[1][5];
    *varx2z2 = jvc[0][0]*j[2][0]+jvc[0][1]*j[2][1]+jvc[0][2]*j[2][2]+
               jvc[0][3]*j[2][3]+jvc[0][4]*j[2][4]+jvc[0][5]*j[2][5];
    *vary2 = jvc[1][0]*j[1][0]+jvc[1][1]*j[1][1]+jvc[1][2]*j[1][2]+
             jvc[1][3]*j[1][3]+jvc[1][4]*j[1][4]+jvc[1][5]*j[1][5];
    *vary2z2 = jvc[1][0]*j[2][0]+jvc[1][1]*j[2][1]+jvc[1][2]*j[2][2]+
               jvc[1][3]*j[2][3]+jvc[1][4]*j[2][4]+jvc[1][5]*j[2][5];
    *varz2 = jvc[2][0]*j[2][0]+jvc[2][1]*j[2][1]+jvc[2][2]*j[2][2]+
             jvc[2][3]*j[2][3]+jvc[2][4]*j[2][4]+jvc[2][5]*j[2][5];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
