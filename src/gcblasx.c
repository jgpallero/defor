/* -*- coding: utf-8 -*- */
/**
\ingroup gcblas
@{
\file gcblasx.c
\brief Definición de la función de reporte de error de \p GCBLAS.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2014-2015, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"gcblas.h"
/******************************************************************************/
/******************************************************************************/
void cblas_xerbla(gblas_int p,
                  const char *rout,
                  const char *form,
                  ...)
{
    //puntero a la lista de argumentos variable
    //inicializarlo a NULL da error en el iBook con Debian y gcc 4.3.3
    va_list argVar;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el puntero argVar con el último elemento conocido
    va_start(argVar,form);
    //imprimimos la información referente al parámetro que ha fallado
    if(p)
    {
        //información general
        fprintf(stderr,"\n"
  "**********\n"
  "**********ERROR EN CBLAS: EL PARÁMETRO %d DE LA FUNCIÓN '%s' ES INCORRECTO\n"
  "**********EL PROGRAMA FINALIZARÁ MEDIANTE LA LLAMADA A exit(EXIT_FAILURE)\n"
  "**********\n\n",(int)p,rout);
    }
    //imprimimos los valores opcionales
    vfprintf(stderr,form,argVar);
    //retorno normal de la función
    va_end(argVar);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //detenemos la ejecución del programa
    exit(EXIT_FAILURE);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la fnción
    return;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
