/* -*- coding: utf-8 -*- */
/**
\ingroup gcblas
@{
\file auxgcblas.h
\brief Declaración datos y funciones auxiliares para el trabajo con \p GCBLAS y
       otras implementaciones de CBLAS.

Este fichero contiene una macro llamada \ref GCBLAS_INFO_STDERR, que escribe en
la salida de error \em stderr un mensaje con el nombre de la función de BLAS que
ha sido invocada. Para que el mensaje se muestre hay que pasar en la compilación
del fichero involucrado la variable del preprocesador
\em ESCRIBE_INFO_BLAS_STDERR. En \p gcc, las variables para el preprocesador se
pasan como \em -DXXX, donde \em XXX es la variable a introducir.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de junio de 2009
\version 1.0
\copyright
Copyright (c) 2009-2015, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
//definimos CBLAS_H para que no se incluya ningún otro cblas.h (ATLAS, etc.)
#ifndef _AUXGCBLAS_H_
#define _AUXGCBLAS_H_
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include"tdblas.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\typedef blasint
\brief Nombre del tipo correspondiente a las dimensiones e incrementos de
       índices en la implementación de \p BLAS en \p GotoBLAS2 y su heredera
       \p OpenBLAS).
\brief Este tipo no se usa en \p GEOC. Sólo se ha añadido para que este fichero
       funcione con las implementaciones \p GotoBLAS2 y \p OpenBLAS.
\date 13 de marzo de 2010: Creación del tipo.
*/
typedef gblas_int blasint;
/******************************************************************************/
/******************************************************************************/
/**
\def CBLAS_INDEX
\brief Tipo de dato correspondiente al índice devuelto por la función
\ref cblas_idamax.
\note Dependiendo de la implementación de CBLAS, puede devolver \p size_t (como
      en \p GotoBLAS2 y \p OpenBLAS) o \p int (como en \p ATLAS).
\date 06 de julio de 2009: Creación de la constante.
*/
#define CBLAS_INDEX size_t
/******************************************************************************/
/******************************************************************************/
/**
\def OPENBLAS_SEQUENTIAL
\brief Identificador de tipo de compilación secuencial en \p OpenBLAS.
\date 15 de enero de 2014: Creación de la constante.
*/
#define OPENBLAS_SEQUENTIAL  0
/******************************************************************************/
/******************************************************************************/
/**
\def OPENBLAS_THREAD
\brief Identificador de tipo de compilación multihilo clásica en \p OpenBLAS.
\date 15 de enero de 2014: Creación de la constante.
*/
#define OPENBLAS_THREAD  1
/******************************************************************************/
/******************************************************************************/
/**
\def OPENBLAS_OPENMP
\brief Identificador de tipo de compilación multihilo con el modelo de \p OpenMP
       en \p OpenBLAS.
\date 15 de enero de 2014: Creación de la constante.
*/
#define OPENBLAS_OPENMP  2
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_INFO_STDERR
\brief Macro para escribir un mensaje en \p stderr.
\param[in] mens1 Primera parte del texto a escribir en pantalla.
\param[in] mens2 Segunda parte del texto a escribir en pantalla.
\note \em mens1 y \em mens2 se escriben en pantalla uno a continuación del otro.
      Al final se añase un salto de línea y retorno de carro.
\note Esta macro sólo escribe los mensajes en \p stderr si en el momento de la
      compilación se pasa la macro \p ESCRIBE_INFO_BLAS_STDERR.
\date 21 de agosto de 2014: Creación de la constante.
*/
#if defined(ESCRIBE_INFO_BLAS_STDERR)
#define GCBLAS_INFO_STDERR(mens1,mens2) fprintf(stderr,"%s'%s'\n",mens1,mens2);
#else
#define GCBLAS_INFO_STDERR(mens1,mens2)
#endif
/******************************************************************************/
/******************************************************************************/
//sólo definimos los tipos enumerados si la constante simbólica
//CBLAS_ENUM_DEFINED_H no está definida
//esta constante se usa internamente en ATLAS, lo cual puede dar error de
//compilación por redefinición de los tipos enumerados
#ifndef CBLAS_ENUM_DEFINED_H
#define CBLAS_ENUM_DEFINED_H
/**
\enum CBLAS_ORDER
\brief Indicador de almacenamiento matricial.
\date 06 de julio de 2009: Creación del tipo.
*/
enum CBLAS_ORDER
{
    /** \brief Indicador de almacenamiento como ROW MAJOR ORDER. */
    CblasRowMajor=101,
    /** \brief Indicador de almacenamiento como COLUMN MAJOR ORDER. */
    CblasColMajor=102
};
/******************************************************************************/
/******************************************************************************/
/**
\enum CBLAS_TRANSPOSE
\brief Indicador de matriz traspuesta.
\date 06 de julio de 2009: Creación del tipo.
*/
enum CBLAS_TRANSPOSE
{
    /** \brief Indicador de matriz no traspuesta. */
    CblasNoTrans=111,
    /** \brief Indicador de matriz traspuesta. */
    CblasTrans=112,
    /** \brief Indicador de matriz conjugada traspuesta. */
    CblasConjTrans=113,
    /** \brief Indicador de matriz conjugada de \p ATLAS. */
    AtlasConj=114
};
/******************************************************************************/
/******************************************************************************/
/**
\enum CBLAS_UPLO
\brief Indicador de matriz triangular superior o inferior.
\date 06 de julio de 2009: Creación del tipo.
*/
enum CBLAS_UPLO
{
    /** \brief Indicador de matriz triangular superior. */
    CblasUpper=121,
    /** \brief Indicador de matriz triangular inferior. */
    CblasLower=122
};
/******************************************************************************/
/******************************************************************************/
/**
\enum CBLAS_DIAG
\brief Indicador del contenido de la diagonal de una matriz triangular.
\date 06 de julio de 2009: Creación del tipo.
*/
enum CBLAS_DIAG
{
    /** \brief Indicador de diagonal con algún elemento distinto de 1. */
    CblasNonUnit=131,
    /** \brief Indicador de diagonal cuyos elementos son 1. */
    CblasUnit=132
};
/******************************************************************************/
/******************************************************************************/
/**
\enum CBLAS_SIDE
\brief Indicador de la posición de una matriz en una multiplicación.
\date 06 de julio de 2009: Creación del tipo.
*/
enum CBLAS_SIDE
{
    /** \brief Indicador de que la matriz está a la izquierda. */
    CblasLeft=141,
    /** \brief Indicador de que la matriz está a la derecha. */
    CblasRight=142
};
#endif
/******************************************************************************/
/******************************************************************************/
/**
\brief Declaración de la función de \p OpenBLAS para fijar el número de hilos de
       tiempo de ejecución.
*/
void openblas_set_num_threads(int num_threads);
/******************************************************************************/
/******************************************************************************/
/**
\brief Declaración de la función de \p OpenBLAS para fijar el número de hilos de
       tiempo de ejecución.
\note Ésta es la antigua función de \p GotoBLAS2.
*/
void goto_set_num_threads(int num_threads);
/******************************************************************************/
/******************************************************************************/
/**
\brief Declaración de la función de \p OpenBLAS extraer la información de la
       configuración en tiempo de ejecución.
*/
char* openblas_get_config(void);
/******************************************************************************/
/******************************************************************************/
/* Get the parallelization type which is used by OpenBLAS */
/**
\brief Declaración de la función de \p OpenBLAS para extraer el tipo de
       paralelización usada en tiempo de ejecución.
*/
int openblas_get_parallel(void);
/******************************************************************************/
/******************************************************************************/
/**
\brief Declaración de una función que está en el fichero de cabecera \p cblas.h
       de \p ATLAS, supongo que relacionada con el tratamiento de errores.
*/
int cblas_errprn(int ierr, int info, char *form, ...);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
