/**
\addtogroup puntos
\ingroup deformacion
@{
\file posptos.c
\brief Definicion de la estructura y funciones para la extraccion de datos de
       posicion de ficheros de base de datos de puntos.

En este fichero se definen la estructura y todas las funciones necesarias para
la extraccion de datos de posicion de los bloques de puntos contenidos en un
fichero de base de datos de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"errores.h"
#include"general.h"
#include"paramptos.h"
#include"bloqptos.h"
#include"posptos.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void InicializaPosPtos(posptos* posicion)
{
    //inicializamos el contador de puntos a 0
    posicion->nPuntos = 0;
    //inicializamos los miembros que son punteros a NULL
    posicion->puntos = NULL;
    posicion->sistRef = NULL;
    posicion->epocas = NULL;
    posicion->pos = NULL;
    posicion->vcpos = NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int AsignaMemoriaPosPtos(posptos* posicion,
                         int elementos)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de elementos es correcto
    if(elementos<=0)
    {
        //escribimos el mensaje de error
        MensajeErrorNELEMMENORCERO((char*)__func__);
        //salimos de la funcion
        return ERRNELEMMENORCERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aumentamos el contador de puntos
    posicion->nPuntos += elementos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las filas a las matrices de datos y de puntos
    posicion->puntos=(char**)realloc(posicion->puntos,
                                       (size_t)posicion->nPuntos*sizeof(char*));
    posicion->sistRef=(char**)realloc(posicion->sistRef,
                                       (size_t)posicion->nPuntos*sizeof(char*));
    posicion->epocas=(double*)realloc(posicion->epocas,
                                      (size_t)posicion->nPuntos*sizeof(double));
    posicion->pos=(double**)realloc(posicion->pos,
                                     (size_t)posicion->nPuntos*sizeof(double*));
    posicion->vcpos=(double**)realloc(posicion->vcpos,
                                     (size_t)posicion->nPuntos*sizeof(double*));
    //controlamos los posibles errores en la asignacion de memoria
    if((posicion->puntos==NULL)||(posicion->sistRef==NULL)||
       (posicion->epocas==NULL)||(posicion->pos==NULL)||(posicion->vcpos==NULL))
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las columnas a las matrices de datos y de puntos
    for(i=0;i<elementos;i++)
    {
        posicion->puntos[posicion->nPuntos-i-1]=(char*)malloc(LONMAXLINPTOS+1);
        posicion->sistRef[posicion->nPuntos-i-1]=(char*)malloc(LONMAXLINPTOS+1);
        posicion->pos[posicion->nPuntos-i-1]=(double*)malloc(3*sizeof(double));
        posicion->vcpos[posicion->nPuntos-i-1]=
                                              (double*)malloc(6*sizeof(double));
        //controlamos los posibles errores en la asignacion de memoria
        if((posicion->puntos[posicion->nPuntos-i-1]==NULL)||
           (posicion->sistRef[posicion->nPuntos-i-1]==NULL)||
           (posicion->pos[posicion->nPuntos-i-1]==NULL)||
           (posicion->vcpos[posicion->nPuntos-i-1]==NULL))
        {
            //escribimos el mensaje de error
            MensajeErrorNOMEMORIA((char*)__func__);
            //salimos de la funcion
            return ERRNOMEMORIA;
        }
        //vamos inicializando a 0 el vector de epocas
        posicion->epocas[posicion->nPuntos-i-1] = 0.0;
        //vamos inicializando a 0 la matriz de coordenadas
        posicion->pos[posicion->nPuntos-i-1][0] = 0.0;
        posicion->pos[posicion->nPuntos-i-1][1] = 0.0;
        posicion->pos[posicion->nPuntos-i-1][2] = 0.0;
        //vamos inicializando a 0 la matriz de varianza-covarianza
        posicion->vcpos[posicion->nPuntos-i-1][0] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][1] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][2] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][3] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][4] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][5] = 0.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LiberaMemoriaPosPtos(posptos* posicion)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //verificamos si hemos pasado una estructura vacia
    if(!posicion->nPuntos)
    {
        //salimos de la funcion
        return;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada a las matrices y a los array de cadenas de
    //caracteres
    for(i=0;i<posicion->nPuntos;i++)
    {
        free(posicion->puntos[i]);
        free(posicion->sistRef[i]);
        free(posicion->pos[i]);
        free(posicion->vcpos[i]);
    }
    free(posicion->puntos);
    free(posicion->sistRef);
    free(posicion->epocas);
    free(posicion->pos);
    free(posicion->vcpos);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el contador de puntos a 0
    posicion->nPuntos = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeBloquePosPtos(FILE* idFichero,
                     int extraeVarCov,
                     long int posFich,
                     int lineaIni,
                     int lineaFin,
                     posptos* posicion)
{
    //codigo de error
    int cError=ERRNOERROR;
    //indice para recorrer bucle
    int i=0;
    //longitud de cada linea
    int lonLinea=LONMAXLINPTOS;
    //linea leida
    char lineaLeida[lonLinea+2];
    //numero de lineas con datos
    int nLineas=0;
    //numero de elementos leidos
    int nElementos=0;
    //cadena de formato de lectura
    char formato[lonLinea+2];
    //codigo de la estacion leida
    char codEstacion[lonLinea+2];
    //codigo del sistema de referencia leido
    char sistRef[lonLinea+2];
    //epoca de la incognita leida
    double epoca=0.0;
    //valor de las coordenadas leidas
    double x=0.0,y=0.0,z=0.0;
    //valor de los elementos de la matriz varianza-covarianza
    double vx=0.0,vxy=0.0,vxz=0.0,vy=0.0,vyz=0.0,vz=0.0;
    //posicion inicial en el fichero
    long int posOriginal=ftell(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //LECTURA DE POSICIONES Y PARAMETROS DE ERROR
    //nos colocamos al comienzo de la linea que contiene el bloque de puntos
    fseek(idFichero,posFich,SEEK_SET);
    //leemos la linea que contiene la cabecera del bloque
    fgets(lineaLeida,lonLinea+2,idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //numero de lineas con datos en el bloque
    nLineas = lineaFin-lineaIni-1;
    //vamos recorriendo todas las lineas
    for(i=lineaIni+1;i<lineaIni+nLineas+1;i++)
    {
        //leemos la siguiente linea del fichero
        fgets(lineaLeida,lonLinea+2,idFichero);
        //trabajamos con ella si no es un comentario
        if(!ExisteSubcadena(lineaLeida,POSINICOMENTPTOS,CADCOMENTPTOS))
        {
            //comprobamos si es una linea identificadora de posicion
            if(ExisteSubcadena(lineaLeida,POSINIIDPUNTOPTOS,CADIDPUNTOPTOS))
            {
                //creamos la cadena de formato de lectura
                sprintf(formato,"%s%s%%s%s%%s%s%%lf%s%%lf%s%%lf%s%%lf",
                        CADIDPUNTOPTOS,SEPCADIDPUNTOINIPTOPTOS,
                        SEPCAMPOSPUNTOPTOS,SEPCAMPOSPUNTOPTOS,
                        SEPCAMPOSPUNTOPTOS,SEPCAMPOSPUNTOPTOS,
                        SEPCAMPOSPUNTOPTOS);
                //leemos los elementos necesarios
                nElementos = sscanf(lineaLeida,formato,
                                    codEstacion,sistRef,
                                    &epoca,
                                    &x,&y,&z);
                //comprobamos si se han leido todos los elementos
                if(nElementos!=6)
                {
                    //escribimos el mensaje en la salida de error
                    MensajeErrorNELEMLEIDOSFICH((char*)__func__,i);
                    //salimos de la funcion
                    return ERRNELEMLEIDOSFICH;
                }
                //asignamos memoria para un nuevo punto
                cError = AsignaMemoriaPosPtos(posicion,1);
                //comprobamos el codigo de error devuelto
                if(cError!=ERRNOERROR)
                {
                    //lanzamos el mensaje de error
                    MensajeErrorPropagado((char*)__func__,
                                          "AsignaMemoriaPosPtos()");
                    //salimos de la funcion
                    return cError;
                }
                //asignamos los codigos de la estacion y del sistema de
                //referencia, la epoca y las coordenadas
                strcpy(posicion->puntos[posicion->nPuntos-1],codEstacion);
                strcpy(posicion->sistRef[posicion->nPuntos-1],sistRef);
                posicion->epocas[posicion->nPuntos-1] = epoca;
                posicion->pos[posicion->nPuntos-1][0] = x;
                posicion->pos[posicion->nPuntos-1][1] = y;
                posicion->pos[posicion->nPuntos-1][2] = z;
            }
            else
            {
                //extraemos la matriz de varianza-covarianza si se ha solicitado
                if(extraeVarCov)
                {
                    //creamos la cadena de formato de lectura
                    sprintf(formato,"%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                            SEPCAMPOSPUNTOPTOS,SEPCAMPOSPUNTOPTOS,
                            SEPCAMPOSPUNTOPTOS,SEPCAMPOSPUNTOPTOS,
                            SEPCAMPOSPUNTOPTOS,SEPCAMPOSPUNTOPTOS);
                    //leemos los elementos necesarios
                    nElementos = sscanf(lineaLeida,formato,
                                        &vx,&vxy,&vxz,&vy,&vyz,&vz);
                    //comprobamos si se han leido todos los elementos
                    if(nElementos!=6)
                    {
                        //escribimos el mensaje en la salida de error
                        MensajeErrorNELEMLEIDOSFICH((char*)__func__,i);
                        //salimos de la funcion
                        return ERRNELEMLEIDOSFICH;
                    }
                    //la matriz leida se considera perteneciente al ultimo punto
                    //creado, por lo que hay que comprobar si existe alguno
                    //si no existe ninguno, la linea leida no se tiene en cuenta
                    if(posicion->nPuntos)
                    {
                        //asignamos los valores
                        posicion->vcpos[posicion->nPuntos-1][0] = vx;
                        posicion->vcpos[posicion->nPuntos-1][1] = vxy;
                        posicion->vcpos[posicion->nPuntos-1][2] = vxz;
                        posicion->vcpos[posicion->nPuntos-1][3] = vy;
                        posicion->vcpos[posicion->nPuntos-1][4] = vyz;
                        posicion->vcpos[posicion->nPuntos-1][5] = vz;
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el puntero de L/E en su posicion original en el fichero
    fseek(idFichero,posOriginal,SEEK_SET);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeePosFicheroPuntos(char fichero[],
                        char bloque[],
                        int extraeVarCov,
                        posptos* posicion)
{
    //codigo de error
    int cError=ERRNOERROR;
    //puntero al fichero de trabajo
    FILE* idFichero=NULL;
    //estructura bloqptos
    bloqptos bloques;
    //posicion del bloque de trabajo
    int posBLOQUE=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura bloqptos
    InicializaBloqPtos(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //abrimos el fichero de trabajo para leer
    if((idFichero=fopen(fichero,"rb"))==NULL)
    {
        //lanzamos un mensaje de error
        MensajeErrorABRIRFICHERO((char*)__func__,fichero);
        //salimos de la funcion
        return ERRABRIRFICHERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los bloques del fichero
    cError = LeeBloquesBloqPtos(idFichero,
                                CADINIBLOQPTOS,CADFINBLOQPTOS,
                                &bloques);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,"LeeBloquesBloqPtos()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqPtos(&bloques);
        //salimos de la funcion
        return cError;
    }
    //buscamos la posicion del bloque de trabajo
    posBLOQUE = PosicionBloqueBloqPtos(&bloques,bloque);
    if(posBLOQUE==IDNOBLOQUEPTOS)
    {
        //lanzamos un mensaje de error
        MensajeErrorNOBLOQUEFICH((char*)__func__,bloque,fichero);
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqPtos(&bloques);
        //salimos de la funcion
        return ERRNOBLOQUEFICH;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos el bloque elegido
    cError = LeeBloquePosPtos(idFichero,extraeVarCov,
                              bloques.posicion[posBLOQUE][0],
                              bloques.lineas[posBLOQUE][0],
                              bloques.lineas[posBLOQUE][1],
                              posicion);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,"LeeBloquePosPtos()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqPtos(&bloques);
        //salimos de la funcion
        return cError;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //cerramos el fichero de trabajo
    fclose(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaBloqPtos(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
