/**
\addtogroup deformacion
@{
\file snxptos.h
\brief Declaracion de las funciones necesarias para la copia de los datos de
       posicion de una estructura snx en una estructura ptos.

\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _SNXPTOS_H_
#define _SNXPTOS_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include"possnx.h"
#include"posptos.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Copia de los datos de posicion de una estructura possnx en una estructura
       posptos.
\param[in] sinex Estructura possnx.
\param[in] sistRef Codigo del sistema de referencia de los puntos de la
                   estructura possnx.
\param[out] puntos Estructura posptos.
\return Codigo de error.
\note En el caso de que la estructura possnx contenga matrices de correlacion,
      estas se convierten a matrices varianza-covarianza cuando son copiadas a
      la estructura posptos.
\note La estructura posptos pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por las estructura pasadas no es
      liberada.
*/
int CopiaPosSnxEnPosPtos(possnx* sinex,
                         char sistRef[],
                         posptos* puntos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
