/**
\addtogroup errores
\ingroup sinex
\ingroup puntos
\ingroup transf
\ingroup deformacion
@{
\file errores.h
\brief Definicion de funciones para el tratamiento de errores.

En este fichero se definen varias funciones para el tratamiento de errores.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 29 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include"errores.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorNELEMMENORCERO(char funcion[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            El argumento 'elementos' pasado es <= 0\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorNOMEMORIA(char funcion[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            Espacio en memoria insuficiente\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorNELEMLEIDOSFICH(char funcion[],
                                 int numeroLinea)
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            Ha habido un error en la lectura de la\n");
    fprintf(stderr,"            linea numero %d de un fichero\n",numeroLinea);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorCLASMATNOVALIDA(char funcion[],
                                 char clMat[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            La clase de matriz leida (%s)\n",clMat);
    fprintf(stderr,"            no es valida\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorTIPOMATNOVALIDO(char funcion[],
                                 char tipoMat[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            El tipo de matriz leido (%s) no es valido\n",
            tipoMat);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorABRIRFICHERO(char funcion[],
                              char fichero[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            Error al abrir el fichero\n");
    fprintf(stderr,"            %s\n",fichero);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorNOBLOQUEFICH(char funcion[],
                              char bloque[],
                              char fichero[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            No existe bloque %s en el fichero\n",bloque);
    fprintf(stderr,"            %s\n",fichero);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorNOTRANSF(char funcion[],
                          char sr1[],
                          char sr2[],
                          char bloque[],
                          char fichero[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            No existe transformacion entre los sistemas\n");
    fprintf(stderr,"            %s -> %s\n",sr1,sr2);
    fprintf(stderr,"            en el bloque %s del fichero\n",bloque);
    fprintf(stderr,"            %s\n",fichero);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorERRNOPTOSSTEMP(char funcion[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            No hay puntos para calcular series ");
    fprintf(stderr,"temporales\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorERRESCFICH(char funcion[],
                            char fichero[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            Error al escribir en el fichero\n");
    fprintf(stderr,"            %s\n",fichero);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorERRCHOL(char funcion[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcion);
    fprintf(stderr,"            Ha habido un error en la descomposición de\n");
    fprintf(stderr,"            Cholesky de una matriz\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorPropagado(char funcAct[],
                           char funcOrig[])
{
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En la funcion %s\n",funcAct);
    fprintf(stderr,"            Ha ocurrido un error al ejecutar la funcion\n");
    fprintf(stderr,"            %s\n",funcOrig);
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MensajeErrorPrograma(int cError,
                          char programa[])
{
    //mensaje de error
    char mensaje[500];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //buscamos el mensaje de error adecuado
    if(cError==ERRNOERROR)
    {
        strcpy(mensaje,"Todo ha ido bien");
    }
    else if(cError==ERRNELEMMENORCERO)
    {
        strcpy(mensaje,"Numero de elementos <= 0");
    }
    else if(cError==ERRNOMEMORIA)
    {
        strcpy(mensaje,"Error en la asignacion dinamica de memoria");
    }
    else if(cError==ERRNELEMLEIDOSFICH)
    {
        strcpy(mensaje,"Numero incorrecto de elementos leidos de fichero");
    }
    else if(cError==ERRCLASMATNOVALIDA)
    {
        strcpy(mensaje,"Clase de matriz no valida");
    }
    else if(cError==ERRTIPOMATNOVALIDO)
    {
        strcpy(mensaje,"Tipo de matriz no valido");
    }
    else if(cError==ERRABRIRFICHERO)
    {
        strcpy(mensaje,"Error en la apertura de fichero");
    }
    else if(cError==ERRNOBLOQUEFICH)
    {
        strcpy(mensaje,"Error en la lectura de un bloque de fichero");
    }
    else if(cError==ERRNOTRANSF)
    {
        strcpy(mensaje,
               "Error en la transformacion entre sistemas de referencia");
    }
    else if(cError==ERRNOPTOSSTEMP)
    {
        strcpy(mensaje,"Error en el calculo de series temporales");
    }
    else if(cError==ERRESCFICH)
    {
        strcpy(mensaje,"Error en la escritura de fichero");
    }
    else
    {
        strcpy(mensaje,"Tipo de error desconocido");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos el mensaje en la salida de error
    fprintf(stderr,"\n");
    fprintf(stderr,"*****ERROR: En el programa %s\n",programa);
    fprintf(stderr,"            %s\n",mensaje);
    fprintf(stderr,"            El programa terminara\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
