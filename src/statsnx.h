/**
\addtogroup sinex
@{
\file statsnx.h
\brief Declaracion de la estructura y las funciones para la extraccion de
       parametros estadisticos del ajuste de ficheros SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _STATSNX_H_
#define _STATSNX_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include"paramsnx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\struct statsnx
\brief Estructura contenedora de los parametros estadisticos del ajuste de un
       fichero SINEX.

En esta estructura se almacenan los parametros estadisticos referentes a la
solucion contenida en un fichero SINEX.
*/
typedef struct
{
    /** \brief Indicador de existencia del campo "numero de observaciones". */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int enobs;
    /** \brief Indicador de existencia del campo "numero de incognitas". */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int enunk;
    /** \brief Indicador de existencia del campo "intervalo de muestreo". */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int esint;
    /** \brief Indicador de existencia del campo "sumatorio del cuadrado de los
               residuos". */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int essr;
    /** \brief Indicador de existencia del campo "sigma de la medida de fase".
    */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int epms;
    /** \brief Indicador de existencia del campo "sigma de la medida de codigo".
    */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int ecms;
    /** \brief Indicador de existencia del campo "grados de libertad". */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int enfree;
    /** \brief Indicador de existencia del campo "estimacion de la varianza del
    observable de peso unidad a posteriori". */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int evarfact;
    /** \brief Indicador de existencia del campo "WEIGHTED SQUARE SUM OF O-C".
    */
    /**
        - 0: No existe.
        - 1: Si existe.
    */
    int ewss;
    /** \brief Valor del parametro "numero de observaciones". */
    int nobs;
    /** \brief Valor del parametro "numero de incognitas". */
    int nunk;
    /** \brief Valor del parametro "intervalo de muestreo". */
    double sint;
    /** \brief Valor del parametro "sumatorio del cuadrado de los residuos". */
    double ssr;
    /** \brief Valor del parametro "sigma de la medida de fase". */
    double pms;
    /** \brief Valor del parametro "sigma de la medida de codigo". */
    double cms;
    /** \brief Valor del parametro "grados de libertad". */
    int nfree;
    /** \brief Valor del parametro "estimacion de la varianza del observable de
               peso unidad a posteriori". */
    /**
    \note Si este parametro no existe, el valor de statsnx::varfact vale 1.0.
    */
    double varfact;
    /** \brief Valor del parametro "WEIGHTED SQUARE SUM OF O-C". */
    double wss;
}statsnx;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Inicializa una estructura statsnx.

Pone el campo statsnx::varfact de la estructura a 1.0 e inicializa a 0 todos los
indicadores de existencia.
\param[in] estadisticas Puntero a una estructura statsnx.
 */
void InicializaStatSnx(statsnx* estadisticas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Funcion para leer las estadisticas de la solucion de un fichero SINEX.
\param[in] idFichero Identificador de fichero abierto para leer.
\param[in] posFichSOLSTAT Direccion de memoria del inicio de la linea de
                          comienzo del bloque SOLUTION/STATISTICS, del cual se
                          extraeran las estadisticas de la solucion contenida en
                          el fichero SINEX.
\param[in] lineaIniSOLSTAT Numero de linea del fichero en la que se encuentra la
                           linea de comienzo del bloque SOLUTION/STATISTICS.
\param[in] lineaFinSOLSTAT Numero de linea del fichero en la que se encuentra la
                           linea de final del bloque SOLUTION/STATISTICS.
\param[out] estadisticas Puntero a una estructura statsnx. Se almacenaran todos
                         los datos referentes a las estadisticas de la solucion
                         contenida en el fichero SINEX.
\note La estuctura statsnx pasada no hace falta que este inicializada.
\note Todos los campos en la estructura de existencia de parametros estadisticos
      se inicializan a 0 en el caso de que no existan en el fichero.
\note El campo en la estructura del parametro correspondiente a la estimacion de
      la varianza del observable de peso unidad a posteriori se devuelve
      inicializado a 1.0 si el parametro no existe en el fichero SINEX.
\note Esta funcion no comprueba si los parametros de posicion y lineas de
      fichero son validos, por lo que es necesario que se le pasen valores
      correctos.
\note El puntero de L/E es devuelto a su posicion original en el fichero tras la
      ejecucion de esta funcion.
*/
void LeeEstadisticasStatSnx(FILE* idFichero,
                            long int posFichSOLSTAT,
                            int lineaIniSOLSTAT,
                            int lineaFinSOLSTAT,
                            statsnx* estadisticas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee la solucion estimada almacenada en un fichero SINEX.
\param[in] fichero Ruta completa del fichero de trabajo.
\param[in] imprimeVarfact Indicador de impresion de la estimacion de la varianza
                          del observable de peso unidad a posteriori si se ha
                          pedido y hay un error. Dos posibles valores:
                          - 0: No se imprime en la salida de error.
                          - 0: Si se imprime en la salida de error.
\param[out] estadisticas Puntero a una estructura statsnx. Se almacenaran todos
                         los datos referentes a las estadisticas de la solucion
                         contenida en el fichero SINEX.
\return Codigo de error.
\note La estuctura statsnx pasada no hace falta que este inicializada.
\note Todos los campos en la estructura de existencia de parametros estadisticos
      se inicializan a 0 en el caso de que no existan en el fichero.
\note El campo en la estructura del parametro correspondiente a la estimacion de
      la varianza del observable de peso unidad a posteriori se devuelve
      inicializado a 1.0 si el parametro no existe en el fichero SINEX.
*/
int LeeEstadisticasFicheroSinex(char fichero[],
                                int imprimeVarfact,
                                statsnx* estadisticas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
