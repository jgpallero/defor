/* -*- coding: utf-8 -*- */
/**
\defgroup gcblas Módulo \p GCBLAS
\ingroup gclapack algebra
\brief En este módulo se reúnen las funciones de \p GCBLAS para tipos de dato
       \p double y \p float. La interfaz es la estándar de CBLAS.
@{
\file gcblas.h
\brief Declaración de las funciones de \p GCBLAS para tipos de dato \p double y
       \p float. También se incluyen las declaraciones de las funciones para el
       resto de tipos de datos con el fin de que este fichero se pueda utilizar
       con otras implementaciones de CBLAS.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de junio de 2009
\version 1.0
\copyright
Copyright (c) 2009-2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
//definimos CBLAS_H para que no se incluya ningún otro cblas.h (ATLAS, etc.)
#ifndef CBLAS_H
#define CBLAS_H
/******************************************************************************/
/******************************************************************************/
#include<stdarg.h>
#include<stdio.h>
#include<stdlib.h>
#include"tdblas.h"
#include"auxgcblas.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\brief Lanza un mensaje de error y detiene el programa en ejecución.
\param[in] p Posición en la lista de argumentos de entrada del argumento que ha
           hecho que la función correspondiente produzca un error.
\param[in] rout Nombre de la función donde se ha producido el error.
\param[in] form Cadena de formato para la impresión de los argumentos
           opcionales.
\param[in] ... Argumentos opcionales, separados por comas y acordes con la
           cadena de formato pasada en el argumento \em form.
\note Esta función detiene la ejecución del programa en curso mediante la
      lamada a <p>exit(EXIT_FAILURE);</p>
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_xerbla(gblas_int p,
                  const char *rout,
                  const char *form,
                  ...);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación \f$y=\alpha x+y\f$, donde \f$\alpha\f$ es un escalar
       y \f$x\f$ e \f$y\f$ son vectores.
\param[in] N Número de elementos de los vectores.
\param[in] alpha Un escalar.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Al término de la ejecución de la función almacena el
               vector resultado.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si la variable \em N es menor o igual a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de los vectores de
      trabajo son congruentes con la longitudes indicadas.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_daxpy(const gblas_int N,
                 const double alpha,
                 const double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la posición del elemento de mayor valor absoluto en un vector.
\param[in] N Número de elementos del vector de trabajo.
\param[in] X Puntero a la dirección de memoria donde comienza el vector.
\param[in] incX Posiciones de separación entre los elementos del vector.
\return Posición del elemento de mayor valor absoluto en el vector de trabajo.
        <b>Las posición devuelta sigue la convención del lenguaje C, es decir,
        comienza en 0.</b>
\note Si las variables \em N y/o \em incX son menores o iguales a 0, esta
      función devuelve el valor 0.
\note Si hay varios elementos de máximo valor absoluto iguales, la función
      extrae la posición del primero de ellos.
\note Esta función no comprueba internamente si el tamaño del vector de trabajo
      es congruente con la longitud indicada.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
CBLAS_INDEX cblas_idamax(const gblas_int N,
                         const double* X,
                         const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la suma de los valores absolutos de los elementos de un vector.
\param[in] N Número de elementos del vector de trabajo.
\param[in] X Puntero a la dirección de memoria donde comienza el vector.
\param[in] incX Posiciones de separación entre los elementos del vector.
\return Suma de los valores absolutos de los elementos del vector de trabajo.
\note Si las variables \em N y/o \em incX son menores o iguales a 0, esta
      función devuelve el valor 0.0.
\note Esta función no comprueba internamente si el tamaño del vector de trabajo
      es congruente con la longitud indicada.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
double cblas_dasum(const gblas_int N,
                   const double* X,
                   const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Copia el contenido de un vector \em X en un vector \em Y.
\param[in] N Número de elementos de los vectores.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[out] Y Puntero a la dirección de memoria donde comienza el segundo
            vector. Al término de la ejecución de la función almacena los
            elementos del primer vector de trabajo.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si la variable \em N es menor o igual a 0, no se realiza la copia.
\note Esta función no comprueba internamente si los tamaños de los vectores de
      trabajo son congruentes con la longitudes indicadas.
\note Las posiciones de los vectores a copiar no deben solaparse.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
\date 13 de agosto de 2010: Utilización de la función de C99 memcpy() para
                            realizar la copia cuando \em incX=incY=1 para
                            mejorar la velocidad de ejecución.
*/
void cblas_dcopy(const gblas_int N,
                 const double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la norma euclídea de un vector.
\param[in] N Número de elementos del vector de trabajo.
\param[in] X Puntero a la dirección de memoria donde comienza el vector.
\param[in] incX Posiciones de separación entre los elementos del vector.
\return Norma euclídea del vector de trabajo.
\note Si las variables \em N y/o \em incX son menores o iguales a 0, esta
      función devuelve el valor 0.0.
\note Esta función no comprueba internamente si el tamaño del vector de trabajo
      es congruente con la longitud indicada.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
double cblas_dnrm2(const gblas_int N,
                   const double* X,
                   const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Aplica una rotación en el plano.

Aplica una rotación en el plano a cada par de elementos \f$(x,y)\f$ de la forma:

\f$
\left(
\begin{array}{c}
x_i\\
y_i
\end{array}
\right)=
\left(
\begin{array}{cc}
 c  & s\\
-s & c
\end{array}
\right)
\left(
\begin{array}{c}
x_i\\
y_i
\end{array}
\right),
\f$

para \f$i=1,\cdots,N\f$ y donde \f$s\f$ y \f$c\f$ son el seno y el coseno,
respectivamente, del ángulo de rotación \f$\varphi\f$ (en sentido
trigonométrico).
\param[in] N Número de elementos de los vectores.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector que
               almacena la coordenada X de cada par a rotar. Al término de la
               ejecución de la función almacena la coordenada X rotada.
\param[in] incX Posiciones de separación entre los elementos del vector de
           coordenadas X. Si es un número negativo, el vector se recorre desde
           el final hasta el principio.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el vector que
               almacena la coordenada Y de cada par a rotar. Al término de la
               ejecución de la función almacena la coordenada Y rotada.
\param[in] incY Posiciones de separación entre los elementos del vector de
           coordenadas Y. Si es un número negativo, el vector se recorre desde
           el final hasta el principio.
\param[in] c Coseno del ángulo de rotación \f$\varphi\f$ (en sentido
           trigonométrico).
\param[in] s Seno del ángulo de rotación \f$\varphi\f$ (en sentido
           trigonométrico).
\note Si la variable \em N es menor o igual a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de los vectores de
      trabajo son congruentes con la longitudes indicadas.
\note Esta función no comprueba internamente si el seno y el coseno pasados
      están en el dominio correcto ni si pertenecen al mismo ángulo.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_drot(const gblas_int N,
                double* X,
                const gblas_int incX,
                double* Y,
                const gblas_int incY,
                const double c,
                const double s);
/******************************************************************************/
/******************************************************************************/
/**
\brief Genera una rotación de Givens en el plano.

La rotación generada es

\f$
\left( \begin{array}{cc} c & s\\ -s & c \end{array} \right),
\f$

donde \f$c^2+s^2=1\f$, tal que hace 0 la segunda componente del vector
\f$(a,b)^T\f$.

La cantidad \f$r=\pm\sqrt{a^2+b^2}\f$ sobreescribe el argumento \em a. El
argumento \em b se sobreescribe con un valor \f$z\f$ que permite recuperar
\f$c\f$ y \f$s\f$ mediante el siguiente algoritmo:

\f$
Si\,z=1,\,c=0.0,\,s=1.0,
\f$

\f$
si\,|z|<1,\,c=\sqrt{1.0-z^2},\,s=z,
\f$

\f$
si\,|z|>1,\,c=\frac{1.0}{z},\,s=\sqrt{1.0-c^2}.
\f$
\param[in,out] a Primera componente del vector a rotar. Al término de la
               ejecución de la función almacena el valor \em r.
\param[in,out] b Segunda componente del vector a rotar. Al término de la
               ejecución de la función almacena el valor \em z.
\param[out] c Coseno del ángulo de rotación.
\param[out] s Seno del ángulo de rotación.
\note Esta función pertenece al nivel 1 de BLAS.
\date 02 de enero de 2011: Creación de la función.
\todo Esta función todavía no está probada.
*/
void cblas_drotg(double* a,
                 double* b,
                 double* c,
                 double* s);
/******************************************************************************/
/******************************************************************************/
/**
\brief Aplica un factor de escala \f$\alpha\f$ a un vector.
\param[in] N Número de elementos de los vectores.
\param[in] alpha Factor de escala.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector. Al
               término de la ejecución de la función almacena el vector
               resultado.
\param[in] incX Posiciones de separación entre los elementos del vector.
\note Si las variables \em N y/o \em incX son menores o iguales a 0, esta
      función no realiza ninguna operación.
\note Esta función no comprueba internamente si el tamaño del vector de trabajo
      es congruente con la longitud indicada.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_dscal(const gblas_int N,
                 const double alpha,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Intercambia los elementos de dos vectores.
\param[in] N Número de elementos de los vectores.
\param[in,out] X Puntero a la dirección de memoria donde comienza el primer
               vector. Al término de la ejecución de la función almacena los
               elementos del segundo vector de trabajo.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Al término de la ejecución de la función almacena los
               elementos del primer vector de trabajo.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si la variable \em N es menor o igual a 0, los vectores de trabajo no
      cambian.
\note Esta función no comprueba internamente si los tamaños de los vectores de
      trabajo son congruentes con la longitudes indicadas.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_dswap(const gblas_int N,
                 double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula el producto escalar de dos vectores.
\param[in] N Número de elementos de los vectores.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] Y Puntero a la dirección de memoria donde comienza el segundo vector.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\return Producto escalar de los vectores de trabajo.
\note Si la variable \em N es menor o igual a 0, esta función devuelve el valor
      0.0.
\note Esta función no comprueba internamente si los tamaños de los vectores de
      trabajo son congruentes con las longitudes indicadas.
\note Esta función pertenece al nivel 1 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
double cblas_ddot(const gblas_int N,
                  const double* X,
                  const gblas_int incX,
                  const double* Y,
                  const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Aplica una rotación de Gives modificada en el plano.

Aplica una rotación de Givens modificada en el plano a cada par de elementos
\f$(x,y)\f$. Según el valor de \f$P[0]\f$, la matriz \f$h\f$ puede tener las
siguientes expresiones:

Si \f$P[0]=-1.0\f$:

\f$
h=
\left(
\begin{array}{cc}
h00 & h01\\
h10 & h11
\end{array}
\right)
\f$

Si \f$P[0]=0.0\f$:

\f$
h=
\left(
\begin{array}{cc}
1.0 & h01\\
h10 & 1.0
\end{array}
\right)
\f$

Si \f$P[0]=1.0\f$:

\f$
h=
\left(
\begin{array}{cc}
h00 & 1.0\\
-1.0 & h11
\end{array}
\right)
\f$

Si \f$P[0]=-2.0\f$:

\f$
h=
\left(
\begin{array}{cc}
1.0 & 0.0\\
0.0 & 1.0
\end{array}
\right)
\f$

Los valores \f$h00, h10, h01, h11\f$ se almacenan en \f$P[1],P[2],P[3],P[4]\f$,
respectivamente (los valores -1.0, 0.0 y 1.0 no se almacenan en \em P)
\param[in] N Número de elementos de los vectores.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector que
               almacena la coordenada X de cada par a rotar. Al término de la
               ejecución de la función almacena la coordenada X rotada.
\param[in] incX Posiciones de separación entre los elementos del vector de
           coordenadas X. Si es un número negativo, el vector se recorre desde
           el final hasta el principio.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el vector que
               almacena la coordenada Y de cada par a rotar. Al término de la
               ejecución de la función almacena la coordenada Y rotada.
\param[in] incY Posiciones de separación entre los elementos del vector de
           coordenadas Y. Si es un número negativo, el vector se recorre desde
           el final hasta el principio.
\param[in,out] P Vector de 5 elementos. En \em P[0] se almacena el valor que
               dará lugar a los diferentes tipos de transformación. En \em P[1],
               \em P[2], \em P[3] y \em P[4] se almacenan las componentes
               \em h00, \em h10, \em h01 y \em h11 de la matriz de rotación
               (aquéllas con valor igual a -1.0, 0.0 y/o 1.0 no se almacenan).
\note Esta función no comprueba internamente si los tamaños de los vectores de
      trabajo son congruentes con la longitudes indicadas.
\note Esta función pertenece al nivel 1 de BLAS.
\date 02 de enero de 2011: Creación de la función.
*/
void cblas_drotm(const gblas_int N,
                 double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY,
                 const double* P);
/******************************************************************************/
/******************************************************************************/
/**
\brief Genera una matriz de rotación de Givens modificada en el plano, que hace
       0 la segunda componente del vector
       \f$(\sqrt{d1}\cdot b1,\sqrt{d2}\cdot b2)^T\f$.

Según el valor de \f$P[0]\f$, la matriz \f$h\f$ puede tener las siguientes
expresiones:

Si \f$P[0]=-1.0\f$:

\f$
h=
\left(
\begin{array}{cc}
h00 & h01\\
h10 & h11
\end{array}
\right)
\f$

Si \f$P[0]=0.0\f$:

\f$
h=
\left(
\begin{array}{cc}
1.0 & h01\\
h10 & 1.0
\end{array}
\right)
\f$

Si \f$P[0]=1.0\f$:

\f$
h=
\left(
\begin{array}{cc}
h00 & 1.0\\
-1.0 & h11
\end{array}
\right)
\f$

Si \f$P[0]=-2.0\f$:

\f$
h=
\left(
\begin{array}{cc}
1.0 & 0.0\\
0.0 & 1.0
\end{array}
\right)
\f$

Los valores \f$h00, h10, h01, h11\f$ se almacenan en \f$P[1],P[2],P[3],P[4]\f$,
respectivamente (los valores -1.0, 0.0 y 1.0 no se almacenan en \em P)
\param[in,out] d1 Componente de la primera componente del vector a rotar. Al
               término de la ejecución de la función almacena el valor afectado
               por la transformación.
\param[in,out] d2 Componente de la segunda componente del vector a rotar. Al
               término de la ejecución de la función almacena el valor afectado
               por la transformación.
\param[in,out] b1 Componente de la primera componente del vector a rotar. Al
               término de la ejecución de la función almacena el valor afectado
               por la transformación.
\param[in] b2 Componente de la segunda componente del vector a rotar.
\param[in,out] P Vector de 5 elementos. En la entrada, en \em P[0] se almacena
               el valor que dará lugar a los diferentes tipos de transformación.
               Al término de la ejecución de la función, en \em P[1], \em P[2],
               \em P[3] y \em P[4] se almacenan las componentes \em h00,
               \em h10, \em h01 y \em h11 de la matriz de rotación (aquéllas con
               valor igual a -1.0, 0.0 y/o 1.0 no se almacenan).
\note Esta función pertenece al nivel 1 de BLAS.
\date 02 de enero de 2011: Creación de la función.
\todo Esta función todavía no está probada.
*/
void cblas_drotmg(double* d1,
                  double* d2,
                  double* b1,
                  const double b2,
                  double* P);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$y=\alpha Ax+\beta y\f$ o
       \f$y=\alpha A^Tx+\beta y\f$, donde \f$\alpha\f$ y \f$\beta\f$ son
       escalares, \f$x\f$ e \f$y\f$ vectores y \f$A\f$ es una matriz
       \f$M\times N\f$. Se considera \f$A\f$ como una matriz completa.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] TransA Identificador de matriz traspuesta referido a \em A. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] M Número de filas de la matriz \em A.
\param[in] N Número de columnas de la matriz \em A.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
           Este vector ha de tener \em M elementos si la matriz \em A es
           traspuesta y \em N elementos si no lo es.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Este vector ha de tener \em N elementos si la matriz
               \em A es traspuesta y \em M elementos si no lo es. Al término de
               la ejecución de la función almacena el vector resultado.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si las variables \em M y/o \em N son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de los vectores y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em M o \em N sean menores que 0.
      - Que \em lda sea menor que máx(1,N), si el tipo de almacenamiento es ROW
        MAJOR ORDER o menor que máx(1,M), si el almacenamiento es COLUMN MAJOR
        ORDER.
      - Que \em incX o \em incY sean iguales a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_dgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$y=\alpha Ax+\beta y\f$ o
       \f$y=\alpha A^Tx+\beta y\f$, donde \f$\alpha\f$ y \f$\beta\f$ son
       escalares, \f$x\f$ e \f$y\f$ vectores y \f$A\f$ es una matriz
       banda de tamaño \f$M\times N\f$, almacenada en formato empaquetado, con
       \em KL subdiagonales y \em KU superdiagonales.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] TransA Identificador de matriz traspuesta referido a \em A. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] M Número de filas de la matriz \em A.
\param[in] N Número de columnas de la matriz \em A.
\param[in] KL Número de subdiagonales de la matriz \em A.
\param[in] KU Número de superdiagonales de la matriz \em A.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           empaquetada de trabajo. Si el tipo de almacenamiento es
           COLUMN MAJOR ORDER, \em lda es el número de filas. Si el tipo de
           almacenamiento es ROW MAJOR ORDER, \em lda es el número de columnas.
           Sea cual sea el tipo de almacenamiento utilizado, \em lda ha de ser
           mayor o igual a \em KL + \em KU + 1.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
           Este vector ha de tener \em M elementos si la matriz \em A es
           traspuesta y \em N elementos si no lo es.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Este vector ha de tener \em N elementos si la matriz
               \em A es traspuesta y \em M elementos si no lo es. Al término de
               la ejecución de la función almacena el vector resultado.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si las variables \em M y/o \em N son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de los vectores y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em M, \em N, \em KL o \em KU sean menores que 0.
      - Que \em lda sea menor que máx(1,KL+KU+1), sea cual sea el tipo de
        almacenamiento matricial.
      - Que \em incX o \em incY sean iguales a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 19 de septiembre de 2009: Creación de la función.
*/
void cblas_dgbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,
                 const gblas_int N,
                 const gblas_int KL,
                 const gblas_int KU,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$y=\alpha Ax+\beta y\f$, donde \f$\alpha\f$ y
       \f$\beta\f$ son escalares, \f$x\f$ e \f$y\f$ son vectores de \f$N\f$
       elementos y \f$A\f$ es una matriz simétrica de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
           Este vector ha de tener \em N elementos.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Este vector ha de tener \em N elementos. Al término de la
               ejecución de la función almacena el vector resultado.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños de los vectores y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que \em lda sea menor que máx(1,N), sea cual sea el tipo de
        almacenamiento matricial.
      - Que \em incX o \em incY sean iguales a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 09 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dsymv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$y=\alpha Ax+\beta y\f$, donde \f$\alpha\f$ y
       \f$\beta\f$ son escalares, \f$x\f$ e \f$y\f$ son vectores de \f$N\f$
       elementos y \f$A\f$ es una matriz simétrica banda, con \em K sub y
       superdiagonales, de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] K Número de subdiagonales o superdiagonales (dependiendo del
           parámetro \em Uplo) de la matriz \em A.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           empaquetada de trabajo. Si el tipo de almacenamiento es
           COLUMN MAJOR ORDER, \em lda es el número de filas. Si el tipo de
           almacenamiento es ROW MAJOR ORDER, \em lda es el número de columnas.
           Sea cual sea el tipo de almacenamiento utilizado, \em lda de ser
           mayor o igual a \em K+1.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
           Este vector ha de tener \em N elementos.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Este vector ha de tener \em N elementos. Al término de la
               ejecución de la función almacena el vector resultado.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños de los vectores y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N y/o \em K sean menores que 0.
      - Que \em lda sea menor que máx(1,K+1), sea cual sea el tipo de
        almacenamiento matricial.
      - Que \em incX o \em incY sean iguales a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 11 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dsbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const gblas_int K,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$y=\alpha Ax+\beta y\f$, donde \f$\alpha\f$ y
       \f$\beta\f$ son escalares, \f$x\f$ e \f$y\f$ son vectores de \f$N\f$
       elementos y \f$A\f$ es una matriz simétrica banda, almacenada en formato
       empaquetado, de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em Ap. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em Ap (filas==columnas).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] Ap Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
           Este vector ha de tener \em N elementos.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] Y Puntero a la dirección de memoria donde comienza el segundo
               vector. Este vector ha de tener \em N elementos. Al término de la
               ejecución de la función almacena el vector resultado.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños de los vectores y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menore que 0.
      - Que \em incX o \em incY sean iguales a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 11 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dspmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* Ap,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$x=Ax\f$ o \f$x=A^Tx\f$, donde \f$x\f$
       es un vector de \f$N\f$ elementos y \f$A\f$ es una matriz
       \f$N\times N\f$, triangular superior o inferior.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de matriz triangular superior o inferior, referido
           a \em A. Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta, referido a \em A. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector de
               trabajo. Al término de la ejecución de la función almacena el
               vector resultado.
\param[in] incX Posiciones de separación entre los elementos del vector de
           trabajo. Si es un número negativo, el vector se recorre desde el
           final hasta el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y el vector de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N sea menor que 0.
      - Que \em lda sea menor que máx(1,N).
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 03 de agosto de 2009: Creación de la función.
*/
void cblas_dtrmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$x=Ax\f$ o \f$x=A^Tx\f$, donde \f$x\f$
       es un vector de \f$N\f$ elementos y \f$A\f$ es una matriz
       \f$N\times N\f$, triangular banda superior o inferior, almacenada en
       formato empaquetado, con \em K superdiagonales o subdiagonales.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de matriz triangular superior o inferior, referido
           a \em A. Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta, referido a \em A. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] K Número de subdiagonales o superdiagonales (dependiendo del
           parámetro \em Uplo) de la matriz \em A.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           empaquetada de trabajo. Si el tipo de almacenamiento es
           COLUMN MAJOR ORDER, \em lda es el número de filas. Si el tipo de
           almacenamiento es ROW MAJOR ORDER, \em lda es el número de columnas.
           Sea cual sea el tipo de almacenamiento utilizado, \em lda de ser
           mayor o igual a \em K+1.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector de
               trabajo. Al término de la ejecución de la función almacena el
               vector resultado.
\param[in] incX Posiciones de separación entre los elementos del vector de
           trabajo. Si es un número negativo, el vector se recorre desde el
           final hasta el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y el vector de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N o \em K sean menores que 0.
      - Que \em lda sea menor que máx(1,K+1), sea cual sea el tipo de
        almacenamiento matricial.
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 26 de septiembre de 2009: Creación de la función.
*/
void cblas_dtbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const gblas_int K,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$x=Ax\f$ o \f$x=A^Tx\f$, donde \f$x\f$
       es un vector de \f$N\f$ elementos y \f$A\f$ es una matriz
       \f$N\times N\f$, triangular superior o inferior, almacenada en formato
       empaquetado.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de matriz triangular superior o inferior, referido
           a \em A. Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta, referido a \em A. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] Ap Puntero a la dirección de memoria donde comienza la matriz A.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector de
               trabajo. Al término de la ejecución de la función almacena el
               vector resultado.
\param[in] incX Posiciones de separación entre los elementos del vector de
           trabajo. Si es un número negativo, el vector se recorre desde el
           final hasta el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y el vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N sea menor que 0.
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 03 de agosto de 2009: Creación de la función.
*/
void cblas_dtpmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* Ap,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Resuelve el sistema de ecuaciones \f$Ax=b\f$ o \f$A^Tx=b\f$, donde
       \f$x\f$ y \f$b\f$ son vectores de \f$N\f$ elementos y \f$A\f$ es una
       matriz \f$N\times N\f$, triangular superior o inferior.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de matriz triangular superior o inferior, referido
           a \em A. Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta, referido a \em A. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector de
               trabajo. Al inicio de la función, esta variable almacena el
               vector \f$b\f$. Al término de la ejecución de la función,
               almacena el vector resultado.
\param[in] incX Posiciones de separación entre los elementos del vector de
           trabajo. Si es un número negativo, el vector se recorre desde el
           final hasta el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no realiza ninguna comprobación acerca de la posible
      singularidad en la resolución del sistema.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y el vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N sea menor que 0.
      - Que \em lda sea menor que máx(1,N).
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 09 de agosto de 2009: Creación de la función.
*/
void cblas_dtrsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Resuelve el sistema de ecuaciones \f$Ax=b\f$ o \f$A^Tx=b\f$, donde
       \f$x\f$ y \f$b\f$ son vectores de \f$N\f$ elementos y \f$A\f$ es una
       matriz \f$N\times N\f$, triangular banda, superior o inferior, con
       \f$K+1\f$ diagonales.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de matriz triangular superior o inferior, referido
           a \em A. Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta, referido a \em A. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] K Diagonales de la matriz \em A. Si en el parámetro \em Uplo se
           indica que la matriz es triangular superior, este argumento almacena
           el número de superdiagonales, mientras que si se indica que \em A es
           triangilar inferior, es el número de subdiagonales.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz A.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           empaquetada de trabajo. Si el tipo de almacenamiento es
           COLUMN MAJOR ORDER, \em lda es el número de filas. Si el tipo de
           almacenamiento es ROW MAJOR ORDER, \em lda es el número de columnas.
           Sea cual sea el tipo de almacenamiento utilizado, \em lda de ser
           mayor o igual a \em K+1.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector de
               trabajo. Al inicio de la función, esta variable almacena el
               vector \f$b\f$. Al término de la ejecución de la función,
               almacena el vector resultado.
\param[in] incX Posiciones de separación entre los elementos del vector de
           trabajo. Si es un número negativo, el vector se recorre desde el
           final hasta el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no realiza ninguna comprobación acerca de la posible
      singularidad en la resolución del sistema.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y el vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N sea menor que 0.
      - Que \em K sea menor que 0.
      - Que \em lda sea menor que \em K+1.
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 06 de marzo de 2010: Creación de la función.
*/
void cblas_dtbsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const gblas_int K,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Resuelve el sistema de ecuaciones \f$Ax=b\f$ o \f$A^Tx=b\f$, donde
       \f$x\f$ y \f$b\f$ son vectores de \f$N\f$ elementos y \f$A\f$ es una
       matriz \f$N\times N\f$, triangular superior o inferior, almacenada en
       formato empaquetado.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de matriz triangular superior o inferior, referido
           a \em A. Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta, referido a \em A. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] Ap Puntero a la dirección de memoria donde comienza la matriz A.
\param[in,out] X Puntero a la dirección de memoria donde comienza el vector de
               trabajo. Al inicio de la función, esta variable almacena el
               vector \f$b\f$. Al término de la ejecución de la función,
               almacena el vector resultado.
\param[in] incX Posiciones de separación entre los elementos del vector de
           trabajo. Si es un número negativo, el vector se recorre desde el
           final hasta el principio.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no realiza ninguna comprobación acerca de la posible
      singularidad en la resolución del sistema.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y el vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N sea menor que 0.
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 05 de agosto de 2009: Creación de la función.
*/
void cblas_dtpsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* Ap,
                 double* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación \f$A=\alpha xy^T+A\f$, donde \f$\alpha\f$ es un
       escalar, \f$x\f$ un vector de \f$M\f$ elementos, \f$y\f$ un vector de
       \f$N\f$ elementos y \f$A\f$ una matriz de \f$M\times N\f$. Se considera
       \f$A\f$ como una matriz completa.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] M Número de filas de la matriz \em A (elementos del vector \em X).
\param[in] N Número de columnas de la matriz \em A (elementos del vector \em Y).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] Y Puntero a la dirección de memoria donde comienza el segundo vector.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in,out] A Puntero a la dirección de memoria donde comienza la matriz A,
               de tamaño \f$M\times N\f$. Al término de la ejecución de la
               función almacena la matriz resultado.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\note Si las variables \em M y/o \em N son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de los vectores y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em M o \em N sean menores que 0.
      - Que \em incX o \em incY sean iguales a 0.
      - Que \em lda sea menor que máx(1,N), si el tipo de almacenamiento es ROW
        MAJOR ORDER o menor que máx(1,M), si el almacenamiento es COLUMN MAJOR
        ORDER.
\note Esta función pertenece al nivel 2 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_dger(const enum CBLAS_ORDER Order,
                const gblas_int M,
                const gblas_int N,
                const double alpha,
                const double* X,
                const gblas_int incX,
                const double* Y,
                const gblas_int incY,
                double* A,
                const gblas_int lda);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$A=\alpha xx^T+A\f$, donde \f$\alpha\f$ es un
       escalar, \f$x\f$ es un vector de \f$N\f$ elementos y \f$A\f$ es una
       matriz simétrica de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] X Puntero a la dirección de memoria donde comienza el vector. Este
           vector ha de tener \em N elementos.
\param[in] incX Posiciones de separación entre los elementos del vector. Si es
           un número negativo, el vector se recorre desde el final hasta el
           principio.
\param[in,out] A Puntero a la dirección de memoria donde comienza la matriz A.
               Al término de la ejecución de la función almacena la matriz
               resultado en la parte triangular referida en \em Uplo.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que \em incX sea igual a 0.
      - Que \em lda sea menor que máx(1,N), sea cual sea el tipo de
        almacenamiento matricial.
\note Esta función pertenece al nivel 2 de BLAS.
\date 12 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dsyr(const enum CBLAS_ORDER Order,
                const enum CBLAS_UPLO Uplo,
                const gblas_int N,
                const double alpha,
                const double* X,
                const gblas_int incX,
                double* A,
                const gblas_int lda);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$A=\alpha xx^T+A\f$, donde \f$\alpha\f$ es un
       escalar, \f$x\f$ es un vector de \f$N\f$ elementos y \f$A\f$ es una
       matriz simétrica, almacenada en formato empaquetado, de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] X Puntero a la dirección de memoria donde comienza el vector. Este
           vector ha de tener \em N elementos.
\param[in] incX Posiciones de separación entre los elementos del vector. Si es
           un número negativo, el vector se recorre desde el final hasta el
           principio.
\param[in,out] Ap Puntero a la dirección de memoria donde comienza la matriz A.
               Al término de la ejecución de la función almacena la matriz
               resultado.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que \em incX sea igual a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 12 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dspr(const enum CBLAS_ORDER Order,
                const enum CBLAS_UPLO Uplo,
                const gblas_int N,
                const double alpha,
                const double* X,
                const gblas_int incX,
                double* Ap);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$A=\alpha xy^T+\alpha yx^T+A\f$, donde
       \f$\alpha\f$ es un escalar, \f$x\f$ e \f$y\f$ son vectores de \f$N\f$
       elementos y \f$A\f$ es una matriz simétrica de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] Y Puntero a la dirección de memoria donde comienza el segundo vector.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in,out] A Puntero a la dirección de memoria donde comienza la matriz A.
               Al término de la ejecución de la función almacena la matriz
               resultado en la parte triangular referida en \em Uplo.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           de trabajo. Si el almacenamiento utilizado es ROW MAJOR ORDER,
           \em lda es el número de columnas totales de la matriz almacenada en
           memoria. Si el almacenamiento es COLUMN MAJOR ORDER, \em lda es el
           número total de filas. Esto permite el uso de submatrices. Por
           ejemplo, si en memoria se almacena una matriz de \f$4\times5\f$ en
           ROW MAJOR ORDER y sólo queremos utilizar las 3 primeras filas
           (empezando por arriba) y las 3 primeras columnas (empezando por la
           izquierda) los parámetros serán: \em M=3, \em N=3 y \em lda=5,
           mientras que si el almacenamiento es COLUMN MAJOR ORDER los
           parámetros correspondientes son: \em M=3, \em N=3 y \em lda=4.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que \em incX y/o \em incY sean iguales a 0.
      - Que \em lda sea menor que máx(1,N), sea cual sea el tipo de
        almacenamiento matricial.
\note Esta función pertenece al nivel 2 de BLAS.
\date 12 de marzo de 2010: Creación de la función.
\date 11 de agosto de 2010: Corregido bug que daba resultados erróneos por
                            inicializar una variable auxiliar referida al vector
                            \em Y con una variable de posición pensada para el
                            vector \em X.
\todo Esta función no está probada.
*/
void cblas_dsyr2(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* X,
                 const gblas_int incX,
                 const double* Y,
                 const gblas_int incY,
                 double* A,
                 const gblas_int lda);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza la operación: \f$A=\alpha xy^T+\alpha yx^T+A\f$, donde
       \f$\alpha\f$ es un escalar, \f$x\f$ e \f$y\f$ son vectores de \f$N\f$
       elementos y \f$A\f$ es una matriz simétrica, almacenada en formato
       empaquetado, de \f$N\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado en
           la matriz \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] X Puntero a la dirección de memoria donde comienza el primer vector.
\param[in] incX Posiciones de separación entre los elementos del primer vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in] Y Puntero a la dirección de memoria donde comienza el segundo vector.
\param[in] incY Posiciones de separación entre los elementos del segundo vector.
           Si es un número negativo, el vector se recorre desde el final hasta
           el principio.
\param[in,out] A Puntero a la dirección de memoria donde comienza la matriz A.
               Al término de la ejecución de la función almacena la matriz
               resultado.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si los tamaños del vector y la
      matriz de trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en la matriz y los vectores de entrada. Los argumentos erróneos
      pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que \em incX y/o \em incY sean iguales a 0.
\note Esta función pertenece al nivel 2 de BLAS.
\date 12 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dspr2(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* X,
                 const gblas_int incX,
                 const double* Y,
                 const gblas_int incY,
                 double* A);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$C=\alpha AB+\beta C\f$,
       \f$C=\alpha A^TB+\beta C\f$, \f$C=\alpha A^TB^T+\beta C\f$ o
       \f$C=\alpha AB^T+\beta C\f$, donde \f$\alpha\f$ y \f$\beta\f$ son
       escalares y \f$A/A^T\f$, \f$B/B^T\f$ y \f$C\f$ matrices completas de
       \f$M\times K\f$, \f$K\times N\f$ y \f$M\times N\f$, respectivamente.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] TransA Identificador de matriz traspuesta referido a \em A. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] TransB Identificador de matriz traspuesta referido a \em B. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] M Número de filas de las matrices \em A o \em A^T y \em C.
\param[in] N Número de columnas de las matrices \em B o \em B^T y \em C.
\param[in] K Número de columnas de la matriz \em A o \em A^T y filas de la
           matriz \em B o \em B^T.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz \em A,
           de tamaño \f$M\times K\f$ si trabajamos con \em TransA=#CblasNoTrans
           y \f$K\times M\f$ en otro caso.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           A. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\param[in] B Puntero a la dirección de memoria donde comienza la matriz \em B,
           de tamaño \f$K\times N\f$ si trabajamos con \em TransB=#CblasNoTrans
           y \f$N\times K\f$ en otro caso.
\param[in] ldb Longitud en memoria de las filas o columnas totales de la matriz
           B. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] C Puntero a la dirección de memoria donde comienza la matriz
               \em C, de tamaño \f$M\times N\f$. Al término de la ejecución de
               la función almacena la matriz resultado.
\param[in] ldc Longitud en memoria de las filas o columnas totales de la matriz
           C. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\note Si las variables \em M, \em N y/o \em K son iguales a 0, no se realiza
      ninguna operación.
\note Esta función no comprueba internamente si los tamaños de las matrices de
      trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en las matrices de entrada. Los argumentos erróneos pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em TransA y/o \em TransB no pertenezcan al tipo enumerado
        #CBLAS_TRANSPOSE.
      - Que \em M, \em N o \em K sean menores que 0.
      - Si el almacenamiento es ROW MAJOR ORDER:
        - Que \em lda sea menor que máx(1,M), si la matriz \em A es traspuesta o
          menor que máx(1,K), si no lo es.
        - Que \em ldb sea menor que máx(1,K), si la matriz \em B es traspuesta o
          menor que máx(1,M), si no lo es.
        - Que \em ldc sea menor que máx(1,N).
      - Si el almacenamiento es COLUMN MAJOR ORDER:
        - Que \em lda sea menor que máx(1,K), si la matriz \em A es traspuesta o
          menor que máx(1,M), si no lo es.
        - Que \em ldb sea menor que máx(1,M), si la matriz \em B es traspuesta o
          menor que máx(1,K), si no lo es.
        - Que \em ldc sea menor que máx(1,M).
\note Esta función pertenece al nivel 3 de BLAS.
\date 06 de julio de 2009: Creación de la función.
*/
void cblas_dgemm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,
                 const gblas_int M,
                 const gblas_int N,
                 const gblas_int K,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* B,
                 const gblas_int ldb,
                 const double beta,
                 double* C,
                 const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$C=\alpha AB+\beta C\f$ o
       \f$C=\alpha BA+\beta C\f$, donde \f$\alpha\f$ y \f$\beta\f$ son
       escalares, \f$A\f$ es una matriz simétrica y \f$B\f$ y \f$C\f$ son
       matrices de \f$M\times N\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Side Identificador del lugar donde aparece la matriz \f$A\f$ en el
           producto donde está involucrada: a la derecha o a la izquierda. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_SIDE.
           Dependiendo de su valor, la operación puede quedar como:
           - Si \em Side vale #CblasLeft: \f$C=\alpha AB+\beta C\f$.
           - Si \em Side vale #CblasRight: \f$C=\alpha BA+\beta C\f$.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em A. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] M Número de filas de la matriz \em C.
\param[in] N Número de columnas de la matriz \em C.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz \em A.
           En cuanto a sus dimensiones, varias posibilidades:
           - Si \em Side vale #CblasLeft: Dimensiones \f$M\times M\f$.
           - Si \em Side vale #CblasRight: Dimensiones \f$N\times N\f$.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           A. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\param[in] B Puntero a la dirección de memoria donde comienza la matriz \em B,
           de tamaño \f$M\times N\f$.
\param[in] ldb Longitud en memoria de las filas o columnas totales de la matriz
           B. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] C Puntero a la dirección de memoria donde comienza la matriz
               \em C, de tamaño \f$M\times N\f$. Al término de la ejecución de
               la función almacena la matriz resultado.
\param[in] ldc Longitud en memoria de las filas o columnas totales de la matriz
           C. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\note Si las variables \em M y/o \em N son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de las matrices de
      trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en las matrices de entrada. Los argumentos erróneos pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Side no pertenezca al tipo enumerado #CBLAS_SIDE.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em M o \em N sean menores que 0.
      - Si el almacenamiento es ROW MAJOR ORDER:
        - Si \em Side es #CblasLeft:
            - Que \em lda sea menor que máx(1,N).
        - Si \em Side es #CblasRight:
            - Que \em lda sea menor que máx(1,M).
        - Que \em ldb sea menor que máx(1,N).
        - Que \em ldc sea menor que máx(1,N).
      - Si el almacenamiento es COLUMN MAJOR ORDER:
        - Si \em Side es #CblasLeft:
            - Que \em lda sea menor que máx(1,M).
        - Si \em Side es #CblasRight:
            - Que \em lda sea menor que máx(1,N).
        - Que \em ldb sea menor que máx(1,M).
        - Que \em ldc sea menor que máx(1,M).
\note Esta función pertenece al nivel 3 de BLAS.
\date 13 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dsymm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* B,
                 const gblas_int ldb,
                 const double beta,
                 double* C,
                 const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$C=\alpha AA^T+\beta C\f$ o
       \f$C=\alpha A^TA+\beta C\f$, donde \f$\alpha\f$ y \f$\beta\f$ son
       escalares, \f$C\f$ es una matriz simétrica de \f$N\times N\f$ y \f$A\f$
       es una matriz de \f$N\times K\f$ en el caso de multiplicar \f$AA^T\f$ o
       de \f$K\times N\f$ en el caso de multiplicar \f$A^TA\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em C. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] Trans Identificador de la operación a realizar con \em A. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE. Dos
           posibilidades:
           - Si \em Trans vale #CblasNoTrans: se realiza la operación
             \f$C=\alpha AA^T+\beta C\f$.
           - Si \em Trans vale #CblasTrans o #CblasConjTrans: se realiza la
             operación \f$C=\alpha A^TA+\beta C\f$.
\param[in] N Dimensiones de la matriz \em C (filas==columnas).
\param[in] K Dimensiones de la matriz A. Dos posibilidades:
           - Si \em Trans vale #CblasNoTrans: número de columnas de \em A.
           - Si \em Trans vale #CblasTrans o #CblasConjTrans: número de filas de
             \em A.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz \em A,
           de tamaño \f$N\times K\f$ si trabajamos con \em Trans=#CblasNoTrans y
           \f$K\times N\f$ en otro caso.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           A. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] C Puntero a la dirección de memoria donde comienza la matriz
               \em C, de tamaño \f$N\times N\f$. Al término de la ejecución de
               la función almacena la matriz resultado.
\param[in] ldc Longitud en memoria de las filas o columnas totales de la matriz
           C. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es el
           número de columnas totales de la matriz almacenada en memoria. Si el
           almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total de
           filas. Esto permite el uso de submatrices. Por ejemplo, si en memoria
           se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y sólo
           queremos utilizar las 3 primeras filas (empezando por arriba) y las 3
           primeras columnas (empezando por la izquierda) los parámetros serán:
           \em M=3, \em N=3 y \em lda=5, mientras que si el almacenamiento es
           COLUMN MAJOR ORDER los parámetros correspondientes son: \em M=3,
           \em N=3 y \em lda=4.
\note Si las variables \em N y/o \em K son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de las matrices de
      trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en las matrices de entrada. Los argumentos erróneos pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em Trans no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em N o \em K sean menores que 0.
      - Si el almacenamiento es ROW MAJOR ORDER:
        - Si \em Trans es #CblasNoTrans:
            - Que \em lda sea menor que máx(1,K).
        - Si \em Trans es #CblasTrans o #CblasConjTrans:
            - Que \em lda sea menor que máx(1,N).
      - Si el almacenamiento es COLUMN MAJOR ORDER:
        - Si \em Trans es #CblasNoTrans:
            - Que \em lda sea menor que máx(1,N).
        - Si \em Trans es #CblasTrans o #CblasConjTrans:
            - Que \em lda sea menor que máx(1,K).
      - Que \em ldc sea menor que máx(1,N).
\note Esta función pertenece al nivel 3 de BLAS.
\date 13 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dsyrk(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,
                 const gblas_int N,
                 const gblas_int K,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double beta,
                 double* C,
                 const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$C=\alpha AB^T+\alpha BA^T+\beta C\f$ o
       \f$C=\alpha A^TB+\alpha B^TA+\beta C\f$, donde \f$\alpha\f$ y \f$\beta\f$
       son escalares, \f$C\f$ es una matriz simétrica de \f$N\times N\f$ y
       \f$A\f$ y \f$B\f$ son matrices de \f$N\times K\f$ en el caso de
       multiplicar \f$\alpha AB^T+\alpha BA^T\f$ o de \f$K\times N\f$ en el caso
       de multiplicar \f$\alpha A^TB+\alpha B^TA\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz simétrica
           referenciada, referido a \em C. Ha de ser un elemento perteneciente
           al tipo enumerado #CBLAS_UPLO.
\param[in] Trans Identificador de las operaciones a realizar con \em A y \em B.
           Ha de ser un elemento perteneciente al tipo enumerado
           #CBLAS_TRANSPOSE. Dos posibilidades:
           - Si \em Trans vale #CblasNoTrans: se realiza la operación
             \f$C=\alpha AB^T+\alpha BA^T+\beta C\f$.
           - Si \em Trans vale #CblasTrans o #CblasConjTrans: se realiza la
             operación \f$C=\alpha A^TB+\alpha B^TA+\beta C\f$.
\param[in] N Dimensiones de la matriz \em C (filas==columnas).
\param[in] K Dimensiones de las matrices \em A y \em B. Dos posibilidades:
           - Si \em Trans vale #CblasNoTrans: número de columnas de \em A y
             \em B.
           - Si \em Trans vale #CblasTrans o #CblasConjTrans: número de filas de
             \em A y \em B.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz \em A,
           de tamaño \f$N\times K\f$ si trabajamos con \em Trans=#CblasNoTrans y
           \f$K\times N\f$ en otro caso.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           \em A. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em lda=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em lda=4.
\param[in] B Puntero a la dirección de memoria donde comienza la matriz \em B,
           de tamaño \f$N\times K\f$ si trabajamos con \em Trans=#CblasNoTrans y
           \f$K\times N\f$ en otro caso.
\param[in] ldb Longitud en memoria de las filas o columnas totales de la matriz
           \em B. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em ldb es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em ldb es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em lda=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em lda=4.
\param[in] beta Escalar \f$\beta\f$.
\param[in,out] C Puntero a la dirección de memoria donde comienza la matriz
               \em C, de tamaño \f$N\times N\f$. Al término de la ejecución de
               la función almacena la matriz resultado.
\param[in] ldc Longitud en memoria de las filas o columnas totales de la matriz
           \em C. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em ldc es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em lda=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em lda=4.
\note Si las variables \em N y/o \em K son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de las matrices de
      trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en las matrices de entrada. Los argumentos erróneos pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em Trans no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em N o \em K sean menores que 0.
      - Si el almacenamiento es ROW MAJOR ORDER:
        - Si \em Trans es #CblasNoTrans:
            - Que \em lda sea menor que máx(1,K).
            - Que \em ldb sea menor que máx(1,K).
        - Si \em Trans es #CblasTrans o #CblasConjTrans:
            - Que \em lda sea menor que máx(1,N).
            - Que \em ldb sea menor que máx(1,N).
      - Si el almacenamiento es COLUMN MAJOR ORDER:
        - Si \em Trans es #CblasNoTrans:
            - Que \em lda sea menor que máx(1,N).
            - Que \em ldb sea menor que máx(1,N).
        - Si \em Trans es #CblasTrans o #CblasConjTrans:
            - Que \em lda sea menor que máx(1,K).
            - Que \em ldb sea menor que máx(1,K).
      - Que \em ldc sea menor que máx(1,N).
\note Esta función pertenece al nivel 3 de BLAS.
\date 13 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dsyr2k(const enum CBLAS_ORDER Order,
                  const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,
                  const gblas_int N,
                  const gblas_int K,
                  const double alpha,
                  const double* A,
                  const gblas_int lda,
                  const double* B,
                  const gblas_int ldb,
                  const double beta,
                  double* C,
                  const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza una de las operaciones: \f$B=\alpha AB\f$, \f$B=\alpha A^TB\f$,
       \f$B=\alpha BA\f$ o \f$B=\alpha BA^T\f$, donde \f$\alpha\f$ es un
       escalar, \f$B\f$ es una matriz de \f$M\times N\f$ y \f$A\f$ es una matriz
       triangular de \f$M\times M\f$ en el caso de multiplicar \f$AB\f$ o
       \f$A^TB\f$ o de \f$N\times N\f$ en el caso de multiplicar \f$BA\f$ o
       \f$BA^T\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Side Identificador del lugar donde aparece la matriz \f$A\f$ en el
           producto donde está involucrada: a la derecha o a la izquierda. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_SIDE.
           Depemdiendo de su valor, la operación puede quedar como:
           - Si \em Side vale #CblasLeft: \f$B=\alpha AB\f$ o
             \f$B=\alpha A^TB\f$.
           - Si \em Side vale #CblasRight: \f$B=\alpha BA\f$ o
             \f$B=\alpha BA^T\f$.
\param[in] Uplo Identificador de la parte triangular de la matriz referenciada,
           referido a \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta referido a \em A. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] M Número de filas de la matriz \em B.
\param[in] N Número de columnas de la matriz \em B.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz \em A,
           de tamaño \f$M\times M\f$ si trabajamos con \em Side=#CblasLeft y
           \f$N\times N\f$ en otro caso.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           \em A. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em lda=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em lda=4.
\param[in,out] B Puntero a la dirección de memoria donde comienza la matriz
               \em B, de tamaño \f$M\times N\f$. Al término de la ejecución de
               la función almacena la matriz resultado.
\param[in] ldb Longitud en memoria de las filas o columnas totales de la matriz
           \em B. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em ldb es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em ldb es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em lda=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em lda=4.
\note Si las variables \em M y/o \em N son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de las matrices de
      trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en las matrices de entrada. Los argumentos erróneos pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Side no pertenezca al tipo enumerado #CBLAS_SIDE.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em M o \em N sean menores que 0.
      - Si el almacenamiento es ROW MAJOR ORDER:
        - Si \em Side es #CblasLeft:
            - Que \em lda sea menor que máx(1,N).
        - Si \em Side es #CblasRight:
            - Que \em lda sea menor que máx(1,M).
        - Que \em ldb sea menor que máx(1,N).
      - Si el almacenamiento es COLUMN MAJOR ORDER:
        - Si \em Side es #CblasLeft:
            - Que \em lda sea menor que máx(1,M).
        - Si \em Side es #CblasRight:
            - Que \em lda sea menor que máx(1,N).
        - Que \em ldb sea menor que máx(1,M).
\note Esta función pertenece al nivel 3 de BLAS.
\date 13 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dtrmm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 double* B,
                 const gblas_int ldb);
/******************************************************************************/
/******************************************************************************/
/**
\brief Resuelve una de las ecuaciones matriciales: \f$AX=\alpha B\f$,
       \f$A^TX=\alpha B\f$, \f$XA=\alpha B\f$ o \f$XA^T=\alpha B\f$, donde
       \f$\alpha\f$ es un escalar, \f$X\f$ y \f$B\f$ son matrices de
       \f$M\times N\f$ y \f$A\f$ es una matriz triangular de \f$M\times M\f$ en
       el caso de multiplicar \f$AX\f$ o \f$A^TX\f$ o de \f$N\times N\f$ en el
       caso de multiplicar \f$XA\f$ o \f$XA^T\f$.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Side Identificador del lugar donde aparece la matriz \f$A\f$ en el
           producto donde está involucrada: a la derecha o a la izquierda. Ha de
           ser un elemento perteneciente al tipo enumerado #CBLAS_SIDE.
           Depemdiendo de su valor, la operación puede quedar como:
           - Si \em Side vale #CblasLeft: \f$AX=\alpha B\f$ o
             \f$A^TX=\alpha B\f$.
           - Si \em Side vale #CblasRight: \f$XA=\alpha B\f$ o
             \f$XA^T=\alpha B\f$.
\param[in] Uplo Identificador de la parte triangular de la matriz referenciada,
           referido a \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_UPLO.
\param[in] TransA Identificador de matriz traspuesta referido a \em A. Ha de ser
           un elemento perteneciente al tipo enumerado #CBLAS_TRANSPOSE.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em A.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] M Número de filas de las matrices \em X y \em B.
\param[in] N Número de columnas de la matriz \em X y \em B.
\param[in] alpha Escalar \f$\alpha\f$.
\param[in] A Puntero a la dirección de memoria donde comienza la matriz \em A,
           de tamaño \f$M\times M\f$ si trabajamos con \em Side=#CblasLeft y
           \f$N\times N\f$ en otro caso.
\param[in] lda Longitud en memoria de las filas o columnas totales de la matriz
           \em A. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em lda es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em lda es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em lda=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em lda=4.
\param[in,out] B Puntero a la dirección de memoria donde comienza la matriz
               \em B, de tamaño \f$M\times N\f$. Al término de la ejecución de
               la función almacena la matriz resultado \em X.
\param[in] ldb Longitud en memoria de las filas o columnas totales de la matriz
           \em B. Si el almacenamiento utilizado es ROW MAJOR ORDER, \em ldb es
           el número de columnas totales de la matriz almacenada en memoria. Si
           el almacenamiento es COLUMN MAJOR ORDER, \em ldb es el número total
           de filas. Esto permite el uso de submatrices. Por ejemplo, si en
           memoria se almacena una matriz de \f$4\times5\f$ en ROW MAJOR ORDER y
           sólo queremos utilizar las 3 primeras filas (empezando por arriba) y
           las 3 primeras columnas (empezando por la izquierda) los parámetros
           serán: \em M=3, \em N=3 y \em ldb=5, mientras que si el
           almacenamiento es COLUMN MAJOR ORDER los parámetros correspondientes
           son: \em M=3, \em N=3 y \em ldb=4.
\note Si las variables \em M y/o \em N son iguales a 0, no se realiza ninguna
      operación.
\note Esta función no comprueba internamente si los tamaños de las matrices de
      trabajo son congruentes con las dimensiones indicadas.
\note En el caso de que haya algún argumento de entrada erróneo, esta función
      detiene el programa donde se esté ejecutando. No se libera la memoria
      utilizada en las matrices de entrada. Los argumentos erróneos pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Side no pertenezca al tipo enumerado #CBLAS_SIDE.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em TransA no pertenezca al tipo enumerado #CBLAS_TRANSPOSE.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em M o \em N sean menores que 0.
      - Si el almacenamiento es ROW MAJOR ORDER:
        - Si \em Side es #CblasLeft:
            - Que \em lda sea menor que máx(1,N).
        - Si \em Side es #CblasRight:
            - Que \em lda sea menor que máx(1,M).
        - Que \em ldb sea menor que máx(1,N).
      - Si el almacenamiento es COLUMN MAJOR ORDER:
        - Si \em Side es #CblasLeft:
            - Que \em lda sea menor que máx(1,M).
        - Si \em Side es #CblasRight:
            - Que \em lda sea menor que máx(1,N).
        - Que \em ldb sea menor que máx(1,M).
\note Esta función pertenece al nivel 3 de BLAS.
\date 18 de marzo de 2010: Creación de la función.
\todo Esta función no está probada.
*/
void cblas_dtrsm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 double* B,
                 const gblas_int ldb);
/******************************************************************************/
/******************************************************************************/
//CBLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_daxpy.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_saxpy(const gblas_int N,
                 const float alpha,
                 const float* X,
                 const gblas_int incX,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_idamax.
\note <b>Las posición devuelta sigue la convención del lenguaje C, es decir,
      comienza en 0.</b>
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
CBLAS_INDEX cblas_isamax(const gblas_int N,
                         const float* X,
                         const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dasum.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
float cblas_sasum(const gblas_int N,
                  const float* X,
                  const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dcopy.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_scopy(const gblas_int N,
                 const float* X,
                 const gblas_int incX,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dnrm2.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
float cblas_snrm2(const gblas_int N,
                  const float* X,
                  const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_drot.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_srot(const gblas_int N,
                float* X,
                const gblas_int incX,
                float* Y,
                const gblas_int incY,
                const float c,
                const float s);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_drotg.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_srotg(float* a,
                 float* b,
                 float* c,
                 float* s);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dscal.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sscal(const gblas_int N,
                 const float alpha,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dswap.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sswap(const gblas_int N,
                 float* X,
                 const gblas_int incX,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_ddot.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
float cblas_sdot(const gblas_int N,
                 const float* X,
                 const gblas_int incX,
                 const float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_drotm.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_srotm(const gblas_int N,
                 float* X,
                 const gblas_int incX,
                 float* Y,
                 const gblas_int incY,
                 const float* P);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_drotmg.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_srotmg(float* d1,
                  float* d2,
                  float* b1,
                  const float b2,
                  float* P);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dgemv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,
                 const gblas_int N,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float* X,
                 const gblas_int incX,
                 const float beta,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dgbmv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sgbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,
                 const gblas_int N,
                 const gblas_int KL,
                 const gblas_int KU,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float* X,
                 const gblas_int incX,
                 const float beta,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsymv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssymv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float* X,
                 const gblas_int incX,
                 const float beta,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsbmv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const gblas_int K,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float* X,
                 const gblas_int incX,
                 const float beta,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dspmv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sspmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const float alpha,
                 const float* Ap,
                 const float* X,
                 const gblas_int incX,
                 const float beta,
                 float* Y,
                 const gblas_int incY);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtrmv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_strmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const float* A,
                 const gblas_int lda,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtbmv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_stbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const gblas_int K,
                 const float* A,
                 const gblas_int lda,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtpmv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_stpmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const float* Ap,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtrsv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_strsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const float* A,
                 const gblas_int lda,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtbsv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_stbsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const gblas_int K,
                 const float* A,
                 const gblas_int lda,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtpsv.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_stpsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const float* Ap,
                 float* X,
                 const gblas_int incX);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dger.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sger(const enum CBLAS_ORDER Order,
                const gblas_int M,
                const gblas_int N,
                const float alpha,
                const float* X,
                const gblas_int incX,
                const float* Y,
                const gblas_int incY,
                float* A,
                const gblas_int lda);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsyr.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssyr(const enum CBLAS_ORDER Order,
                const enum CBLAS_UPLO Uplo,
                const gblas_int N,
                const float alpha,
                const float* X,
                const gblas_int incX,
                float* A,
                const gblas_int lda);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dspr.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sspr(const enum CBLAS_ORDER Order,
                const enum CBLAS_UPLO Uplo,
                const gblas_int N,
                const float alpha,
                const float* X,
                const gblas_int incX,
                float* Ap);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsyr2.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssyr2(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const float alpha,
                 const float* X,
                 const gblas_int incX,
                 const float* Y,
                 const gblas_int incY,
                 float* A,
                 const gblas_int lda);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dspr2.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sspr2(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const float alpha,
                 const float* X,
                 const gblas_int incX,
                 const float* Y,
                 const gblas_int incY,
                 float* A);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dgemm.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_sgemm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,
                 const gblas_int M,
                 const gblas_int N,
                 const gblas_int K,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float* B,
                 const gblas_int ldb,
                 const float beta,
                 float* C,
                 const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsymm.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssymm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int M,
                 const gblas_int N,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float* B,
                 const gblas_int ldb,
                 const float beta,
                 float* C,
                 const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsyrk.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssyrk(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,
                 const gblas_int N,
                 const gblas_int K,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 const float beta,
                 float* C,
                 const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dsyr2k.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_ssyr2k(const enum CBLAS_ORDER Order,
                  const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,
                  const gblas_int N,
                  const gblas_int K,
                  const float alpha,
                  const float* A,
                  const gblas_int lda,
                  const float* B,
                  const gblas_int ldb,
                  const float beta,
                  float* C,
                  const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtrmm.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_strmm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int M,
                 const gblas_int N,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 float* B,
                 const gblas_int ldb);
/******************************************************************************/
/******************************************************************************/
/**
\brief Equivalente en precisión simple de la función \ref cblas_dtrsm.
\date 16 de enero de 2014: Creación de la función.
\note Esta función todavía no está probada.
*/
void cblas_strsm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int M,
                 const gblas_int N,
                 const float alpha,
                 const float* A,
                 const gblas_int lda,
                 float* B,
                 const gblas_int ldb);
/******************************************************************************/
/******************************************************************************/
//las funciones siguientes no están programadas en GCBLAS, pero se ponen para
//que este archivo de cabecera pueda ser utilizado con una implementación de
//BLAS que sí las tenga
float cblas_sdsdot(const gblas_int N,const float alpha,const float* X,
                   const gblas_int incX,const float* Y,
                   const gblas_int incY);
double cblas_dsdot(const gblas_int N,const float* X,const gblas_int incX,
                   const float* Y,const gblas_int incY);
void cblas_cdotu_sub(const gblas_int N,const void* X,const gblas_int incX,
                     const void* Y,const gblas_int incY,void* dotu);
void cblas_cdotc_sub(const gblas_int N,const void* X,const gblas_int incX,
                     const void* Y,const gblas_int incY,void* dotc);
void cblas_zdotu_sub(const gblas_int N,const void* X,const gblas_int incX,
                     const void* Y,const gblas_int incY,void* dotu);
void cblas_zdotc_sub(const gblas_int N,const void* X,const gblas_int incX,
                     const void* Y,const gblas_int incY,void* dotc);
float cblas_scnrm2(const gblas_int N,const void* X,const gblas_int incX);
float cblas_scasum(const gblas_int N,const void* X,const gblas_int incX);
double cblas_dznrm2(const gblas_int N,const void* X,const gblas_int incX);
double cblas_dzasum(const gblas_int N,const void* X,const gblas_int incX);
CBLAS_INDEX cblas_icamax(const gblas_int N,const void* X,
                         const gblas_int incX);
CBLAS_INDEX cblas_izamax(const gblas_int N,const void* X,
                         const gblas_int incX);
void cblas_cswap(const gblas_int N,void* X,const gblas_int incX,void* Y,
                 const gblas_int incY);
void cblas_ccopy(const gblas_int N,const void* X,const gblas_int incX,
                 void* Y,const gblas_int incY);
void cblas_caxpy(const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,void* Y,const gblas_int incY);
void cblas_zswap(const gblas_int N,void* X,const gblas_int incX,void* Y,
                 const gblas_int incY);
void cblas_zcopy(const gblas_int N,const void* X,const gblas_int incX,
                 void* Y,const gblas_int incY);
void cblas_zaxpy(const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,void* Y,const gblas_int incY);
void cblas_cscal(const gblas_int N,const void* alpha,void* X,
                 const gblas_int incX);
void cblas_zscal(const gblas_int N,const void* alpha,void* X,
                 const gblas_int incX);
void cblas_csscal(const gblas_int N,const float alpha,void* X,
                  const gblas_int incX);
void cblas_zdscal(const gblas_int N,const double alpha,void* X,
                  const gblas_int incX);
void cblas_cgemv(const enum CBLAS_ORDER Order,const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,const gblas_int N,const void* alpha,
                 const void* A,const gblas_int lda,const void* X,
                 const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_cgbmv(const enum CBLAS_ORDER Order,const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,const gblas_int N,
                 const gblas_int KL,const gblas_int KU,const void* alpha,
                 const void* A,const gblas_int lda,const void* X,
                 const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_ctrmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* A,const gblas_int lda,
                 void* X,const gblas_int incX);
void cblas_ctbmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const gblas_int K,const void* A,
                 const gblas_int lda,void* X,const gblas_int incX);
void cblas_ctpmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* Ap,void* X,
                 const gblas_int incX);
void cblas_ctrsv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* A,const gblas_int lda,
                 void* X,const gblas_int incX);
void cblas_ctbsv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const gblas_int K,const void* A,
                 const gblas_int lda,void* X,const gblas_int incX);
void cblas_ctpsv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* Ap,void* X,
                 const gblas_int incX);
void cblas_zgemv(const enum CBLAS_ORDER Order,const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,const gblas_int N,const void* alpha,
                 const void* A,const gblas_int lda,const void* X,
                 const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_zgbmv(const enum CBLAS_ORDER Order,const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,const gblas_int N,
                 const gblas_int KL,const gblas_int KU,const void* alpha,
                 const void* A,const gblas_int lda,const void* X,
                 const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_ztrmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* A,const gblas_int lda,
                 void* X,const gblas_int incX);
void cblas_ztbmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const gblas_int K,const void* A,
                 const gblas_int lda,void* X,const gblas_int incX);
void cblas_ztpmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* Ap,void* X,
                 const gblas_int incX);
void cblas_ztrsv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* A,const gblas_int lda,
                 void* X,const gblas_int incX);
void cblas_ztbsv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const gblas_int K,const void* A,
                 const gblas_int lda,void* X,const gblas_int incX);
void cblas_ztpsv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,const enum CBLAS_DIAG Diag,
                 const gblas_int N,const void* Ap,void* X,
                 const gblas_int incX);
void cblas_chemv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,const void* X,const gblas_int incX,
                 const void* beta,void* Y,const gblas_int incY);
void cblas_chbmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const gblas_int K,const void* alpha,
                 const void* A,const gblas_int lda,const void* X,
                 const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_chpmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* Ap,
                 const void* X,const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_cgeru(const enum CBLAS_ORDER Order,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* A,const gblas_int lda);
void cblas_cgerc(const enum CBLAS_ORDER Order,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* A,const gblas_int lda);
void cblas_cher(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                const gblas_int N,const float alpha,const void* X,
                const gblas_int incX,void* A,const gblas_int lda);
void cblas_chpr(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                const gblas_int N,const float alpha,const void* X,
                const gblas_int incX,void* A);
void cblas_cher2(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* A,const gblas_int lda);
void cblas_chpr2(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* Ap);
void cblas_zhemv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,const void* X,const gblas_int incX,
                 const void* beta,void* Y,const gblas_int incY);
void cblas_zhbmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const gblas_int K,const void* alpha,
                 const void* A,const gblas_int lda,const void* X,
                 const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_zhpmv(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* Ap,
                 const void* X,const gblas_int incX,const void* beta,void* Y,
                 const gblas_int incY);
void cblas_zgeru(const enum CBLAS_ORDER Order,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* A,const gblas_int lda);
void cblas_zgerc(const enum CBLAS_ORDER Order,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* A,const gblas_int lda);
void cblas_zher(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                const gblas_int N,const double alpha,const void* X,
                const gblas_int incX,void* A,const gblas_int lda);
void cblas_zhpr(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                const gblas_int N,const double alpha,const void* X,
                const gblas_int incX,void* A);
void cblas_zher2(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* A,const gblas_int lda);
void cblas_zhpr2(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const gblas_int N,const void* alpha,const void* X,
                 const gblas_int incX,const void* Y,const gblas_int incY,
                 void* Ap);
void cblas_cgemm(const enum CBLAS_ORDER Order,const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,const gblas_int M,
                 const gblas_int N,const gblas_int K,const void* alpha,
                 const void* A,const gblas_int lda,const void* B,
                 const gblas_int ldb,const void* beta,void* C,
                 const gblas_int ldc);
void cblas_csymm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,const void* B,const gblas_int ldb,
                 const void* beta,void* C,const gblas_int ldc);
void cblas_csyrk(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                 const gblas_int K,const void* alpha,const void* A,
                 const gblas_int lda,const void* beta,void* C,
                 const gblas_int ldc);
void cblas_csyr2k(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                  const gblas_int K,const void* alpha,const void* A,
                  const gblas_int lda,const void* B,const gblas_int ldb,
                  const void* beta,void* C,const gblas_int ldc);
void cblas_ctrmm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,void* B,const gblas_int ldb);
void cblas_ctrsm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,void* B,const gblas_int ldb);
void cblas_zgemm(const enum CBLAS_ORDER Order,const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,const gblas_int M,
                 const gblas_int N,const gblas_int K,const void* alpha,
                 const void* A,const gblas_int lda,const void* B,
                 const gblas_int ldb,const void* beta,void* C,
                 const gblas_int ldc);
void cblas_zsymm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,const void* B,const gblas_int ldb,
                 const void* beta,void* C,const gblas_int ldc);
void cblas_zsyrk(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                 const gblas_int K,const void* alpha,const void* A,
                 const gblas_int lda,const void* beta,void* C,
                 const gblas_int ldc);
void cblas_zsyr2k(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                  const gblas_int K,const void* alpha,const void* A,
                  const gblas_int lda,const void* B,const gblas_int ldb,
                  const void* beta,void* C,const gblas_int ldc);
void cblas_ztrmm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,void* B,const gblas_int ldb);
void cblas_ztrsm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,void* B,const gblas_int ldb);
void cblas_chemm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,const void* B,const gblas_int ldb,
                 const void* beta,void* C,const gblas_int ldc);
void cblas_cherk(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                 const gblas_int K,const float alpha,const void* A,
                 const gblas_int lda,const float beta,void* C,
                 const gblas_int ldc);
void cblas_cher2k(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                  const gblas_int K,const void* alpha,const void* A,
                  const gblas_int lda,const void* B,const gblas_int ldb,
                  const float beta,void* C,const gblas_int ldc);
void cblas_zhemm(const enum CBLAS_ORDER Order,const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,const gblas_int M,
                 const gblas_int N,const void* alpha,const void* A,
                 const gblas_int lda,const void* B,const gblas_int ldb,
                 const void* beta,void* C,const gblas_int ldc);
void cblas_zherk(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                 const gblas_int K,const double alpha,const void* A,
                 const gblas_int lda,const double beta,void* C,
                 const gblas_int ldc);
void cblas_zher2k(const enum CBLAS_ORDER Order,const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,const gblas_int N,
                  const gblas_int K,const void* alpha,const void* A,
                  const gblas_int lda,const void* B,const gblas_int ldb,
                  const double beta,void* C,const gblas_int ldc);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
