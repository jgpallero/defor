/**
\addtogroup transf
\ingroup deformacion
@{
\file partran.h
\brief Declaracion de la estructura y funciones para la extraccion de datos de
       parametros de tranformacion de ficheros de parametros de transformacion
       entre sistemas de referencia.

En este fichero se declaran la estructura y todas las funciones necesarias para
la extraccion de parametros de transformacion de los bloques contenidos en un
fichero de parametros de transformacion entre sistemas de referencia.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 29 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _PARTRAN_H_
#define _PARTRAN_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include"paramtran.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\struct partran
\brief Estructura contenedora de los parametros de transformacion de las
       transformaciones almacenadas en un bloque de un fichero de parametros de
       transformacion entre sistemas de referencia.

En esta estructura se almacenan todos los elementos referentes a los parametros
de transformacion de las transformaciones almacenadas en un bloque de un fichero
de parametros de transformacion entre sistemas de referencia. Se almacenan los
codigos de los sistemas de referencia origen y destino de cada transformacion,
las epocas base de cada sistema implicado y los parametros de traslacion, factor
de escala y rotacion, asi como sus variaciones anuales.
*/
typedef struct
{
    /** \brief Numero de transformaciones que contiene el bloque elegido. */
    int nTransf;
    /** \brief Codigos de los sistemas de referencia origen de cada
               transformacion que hay en el bloque elegido. */
    /**
    Array de cadenas de texto para almacenar los codigos de los sistemas de
    referencia origen de cada transformacion que hay en el bloque elegido del
    fichero de parametros de transformacion entre sistemas de referencia. La
    longitud asignada a cada cadena del array es igual a #LONMAXLINTRAN+1. Este
    array contiene tantas cadenas de texto como elementos tenga
    partran::nTransf. La numeracion de elementos comienza en 0.
    */
    char** sistIni;
    /** \brief Codigos de los sistemas de referencia destino de cada
    transformacion que hay en el bloque elegido. */
    /**
    Array de cadenas de texto para almacenar los codigos de los sistemas de
    referencia destino de cada transformacion que hay en el bloque elegido del
    fichero de parametros de transformacion entre sistemas de referencia. La
    longitud asignada a cada cadena del array es igual a #LONMAXLINTRAN+1. Este
    array contiene tantas cadenas de texto como elementos tenga
    partran::nTransf. La numeracion de elementos comienza en 0.
    */
    char** sistFin;
    /** \brief Epocas base en los sistemas de referencia origen y destino de
               cada transformacion, en formato anyo.decimal_de_anyo */
    /**
    Matriz de dos columnas que almacena la epoca base en los sistemas de
    referencia origen y destino de cada transformacion. Esta matriz contiene
    tantas filas como elementos tenga partran::nTransf. Cada fila corresponde a
    la transformacion almacenada en el mismo indice del array partran::sistIni.
    La numeracion de elementos comienza en 0. Las columnas corresponden a:
    - Col. 0: Epoca base del sistema origen.
    - Col. 1: Epoca base del sistema destino.
    */
    double** epocas;
    /** \brief Parametros de transformacion. */
    /**
    Matriz de siete columnas que almacena los parametros de transformacion que
    hay en el bloque elegido de un fichero de parametros de transformacion entre
    sistemas de referencia. Esta matriz contiene tantas filas como elementos
    tenga partran::nTransf. Cada fila corresponde a la transformacion almacenada
    en el mismo indice del array partran::sistIni. La numeracion de elementos
    comienza en 0. Las columnas corresponden a:
    - Col. 0: Traslacion a lo largo del eje X, en metros.
    - Col. 1: Traslacion a lo largo del eje Y, en metros.
    - Col. 2: Traslacion a lo largo del eje Z, en metros.
    - Col. 3: Factor de escala, en unidades adimensionales.
    - Col. 4: Rotacion en torno al eje X, en radianes.
    - Col. 5: Rotacion en torno al eje Y, en radianes.
    - Col. 6: Rotacion en torno al eje Z, en radianes.
    */
    double** param;
    /** \brief Variacion de los parametros de rotacion. */
    /**
    Matriz de siete columnas que almacena la variacion anual de los parametros
    de transformacion que hay en el bloque elegido de un fichero de parametros
    de transformacion entre sistemas de referencia. Esta matriz contiene tantas
    filas como elementos tenga partran::nTransf. Cada fila corresponde a la
    variacion de los parametros de la transformacion almacenada en el mismo
    indice del array partran::sistIni. La numeracion de elementos comienza en 0.
    Las columnas corresponden a:
    - Col. 0: Variacion de la traslacion a lo largo del eje X, en metros al
              anyo.
    - Col. 1: Variacion de la traslacion a lo largo del eje Y, en metros al
              anyo.
    - Col. 2: Variacion de la traslacion a lo largo del eje Z, en metros al
              anyo.
    - Col. 3: Variacion del factor de escala, en unidades adimensionales al
              anyo.
    - Col. 4: Variacion de la rotacion en torno al eje X, en radianes al anyo.
    - Col. 5: Variacion de la rotacion en torno al eje Y, en radianes al anyo.
    - Col. 6: Variacion de la rotacion en torno al eje Z, en radianes al anyo.
    
    \note Si para un punto determinado no existe matriz de variacion de
          parametros, los elementos correspondientes en esta matriz almacenan el
          valor 0.0.
    \note Las variaciones anuales almacenadas en esta matriz son variaciones
          con respecto a la epoca base del sistema destino.
    */
    double** variac;
}partran;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Inicializa una estructura partran.

Pone el campo partran::nPTransf de la estructura a 0 e inicializa a NULL todos
los elementos de la estructura que sean punteros.
\param[in] parametros Puntero a una estructura partran.
\note Es necesario inicializar una estructura partran antes de su uso porque
      muchas funciones comprueban el estado del campo partran::nTransf para su
      correcto funcionamiento y si la estructura no se ha inicializado podrian
      producirse errores.
*/
void InicializaParTran(partran* parametros);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Asigna memoria para un numero de elementos dado en una estructura
       partran, anyadiendolos a los que ya existan previamente.
\param[in] parametros Puntero a una estructura partran.
\param[in] elementos Numero de elementos para los cuales se asignara memoria.
\return Codigo de error.
\note La estructura partran pasada ha de estar inicializada.
\note Esta funcion actualiza el campo partran::nTransf de la estructura.
*/
int AsignaMemoriaParTran(partran* parametros,
                         int elementos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Libera la memoria ocupada por una estructura partran.
\param[in] parametros Puntero a una estructura partran.
\note Ademas de liberar la memoria ocupada por los elementos que son punteros de
      la estructura, esta funcion pone a cero el campo partran::nTransf.
*/
void LiberaMemoriaParTran(partran* parametros);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee los parametros de transformacion de un bloque de transformaciones
       almacenado en un fichero de parametros de transformacion entre sistemas
       de referencia.
\param[in] idFichero Identificador de fichero abierto para leer.
\param[in] extraeVariacion Indicador de extraccion de la variacion de los
                           parametros de transformacion almacenados en el
                           fichero. Dos posibles valores:
                           - 0: No se extrae la variacion.
                           - 1: Si se extrae la variacion.
\param[in] posFich Direccion de memoria del inicio de la linea de comienzo del
                   bloque de transformaciones elegido.
\param[in] lineaIni Numero de linea del fichero en la que se encuentra la linea
                    de comienzo del bloque de transformaciones elegido.
\param[in] lineaFin Numero de linea del fichero en la que se encuentra la linea
                    de final del bloque de transformaciones elegido.
\param[out] parametros Puntero a una estructura partran. Se almacenaran todos
                       los datos referentes al bloque de parametros solicitado.
\return Codigo de error.
\note Esta funcion no comprueba si los parametros de posicion y lineas de
      fichero son validos, por lo que es necesario que se le pasen valores
      correctos.
\note El puntero de L/E es devuelto a su posicion original en el fichero tras la
      ejecucion de esta funcion.
\note Esta funcion supone que los datos de traslacion y sus velocidades en el
      fuchero pasado vienen dadas en centimetros y centimetros al anyo,
      respectivamente.
\note Esta funcion supone que los datos de factor de escala y su velocidad en el
      fuchero pasado vienen dadas en unidades adimensionales y unidades
      adimensionales al anyo, respectivamente.
\note Esta funcion supone que los datos de rotacion y sus velocidades en el
      fuchero pasado vienen dadas en milisegundos sexagesimales y milisegundos
      sexagesimales al anyo, respectivamente.
\note La estructura partran pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura partran pasada
      no es liberada y el fichero apuntado por idFichero no es cerrado.
*/
int LeeBloqueParTran(FILE* idFichero,
                     int extraeVariacion,
                     long int posFich,
                     int lineaIni,
                     int lineaFin,
                     partran* parametros);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee los parametros de transformacion de un bloque de transformaciones
       almacenado en un fichero de parametros de transformacion entre sistemas
       de referencia.
\param[in] fichero Ruta completa del fichero de trabajo.
\param[in] bloque Nombre del bloque de parametros a extraer.
\param[in] extraeVariacion Indicador de extraccion de la variacion de los
                           parametros de transformacion almacenados en el
                           fichero. Dos posibles valores:
                           - 0: No se extrae la variacion.
                           - 1: Si se extrae la variacion.
\param[out] parametros Puntero a una estructura partran. Se almacenaran todos
                       los datos referentes al bloque de parametros solicitado.
\return Codigo de error.
\note La estructura partran pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura partran pasada
      no es liberada.
*/
int LeeParFicheroTransf(char fichero[],
                        char bloque[],
                        int extraeVariacion,
                        partran* parametros);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la transformacion entre dos sistemas de referencia para un punto.
\param[in] parametros Puntero a una estructura partran.
\param[in] sistRef1 Codigo del sistema de referencia inicio.
\param[in] sistRef2 Codigo del sistema de referencia destino.
\param[in] t1 Epoca del punto de trabajo.
\param[in] x1 Coordenada X del punto de trabajo.
\param[in] y1 Coordenada Y del punto de trabajo.
\param[in] z1 Coordenada Z del punto de trabajo.
\param[in] vx1 Varianza de la coordenada X del punto de trabajo.
\param[in] vx1y1 Covarianza entre las coordenadas X e Y del punto de trabajo.
\param[in] vx1z1 Covarianza entre las coordenadas X y Z del punto de trabajo.
\param[in] vy1 Varianza de la coordenada Y del punto de trabajo.
\param[in] vy1z1 Covarianza entre las coordenadas Y y Z del punto de trabajo.
\param[in] vz1 Varianza de la coordenada Z del punto de trabajo.
\param[out] x2 Coordenada X del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] y2 Coordenada Y del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] z2 Coordenada Z del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] vx2 Varianza de la coordenada X en el sistema destino, en la epoca
                t1.
\param[out] vx2y2 Covarianza entre las coordenadas X e Y en el sistema destino,
                  en la epoca t1.
\param[out] vx2z2 Covarianza entre las coordenadas X y Z en el sistema destino,
                  en la epoca t1.
\param[out] vy2 Varianza de la coordenada Y en el sistema destino, en la epoca
                t1.
\param[out] vy2z2 Covarianza entre las coordenadas Y y Z en el sistema destino,
                  en la epoca t1.
\param[out] vz2 Varianza de la coordenada Z en el sistema destino, en la epoca
                t1.
\return Codigo de error.
\note Esta funcion puede devolver un error #ERRNOTRANSF en el caso en que no se
      pueda realizar la transformacion solicitada. En tal situacion la funcion
      se ejecuta normalmente, pero las coordenadas de salida son las mismas que
      las del punto antes de la transformacion.
*/
int PuntoSistRefTransf(partran* parametros,
                       char sistRef1[],
                       char sistRef2[],
                       double t1,
                       double x1,
                       double y1,
                       double z1,
                       double vx1,
                       double vx1y1,
                       double vx1z1,
                       double vy1,
                       double vy1z1,
                       double vz1,
                       double* x2,
                       double* y2,
                       double* z2,
                       double* vx2,
                       double* vx2y2,
                       double* vx2z2,
                       double* vy2,
                       double* vy2z2,
                       double* vz2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
