/* -*- coding: utf-8 -*- */
/**
\ingroup gcblas
@{
\file vcmoblas.h
\brief Definición de las macros para el cambio de variables de entrada para
       trabajar siempre en COLUMN MAJOR ORDER en \p GCBLAS.
\author José Luis García Pallero, jgpallero@gmail.com
\date 17 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2013-2017, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _VCMOBLAS_H_
#define _VCMOBLAS_H_
/******************************************************************************/
/******************************************************************************/
#include"auxgcblas.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_GEMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dgemv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dgemv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dgemv.
\param[in] Mm Argumento \em M de la función \ref cblas_dgemv.
\param[in] Nn Argumento \em N de la función \ref cblas_dgemv.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\date 17 de enero de 2014: Creación de la macro.
\date 18 de marzo de 2017: Supresión de warning en el compilador PGI.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_GEMV(Order,TransA,Mm,Nn,TA,MM,NN) \
if((Order)==CblasColMajor) { \
    TA = ((TransA)==CblasNoTrans) ? 'N' : 'T'; \
    MM = Mm; \
    NN = Nn; \
} else { \
    TA = ((TransA)==CblasNoTrans) ? 'T' : 'N'; \
    MM = Nn; \
    NN = Mm; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_GBMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dgbmv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dgbmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dgbmv.
\param[in] Mm Argumento \em M de la función \ref cblas_dgbmv.
\param[in] Nn Argumento \em N de la función \ref cblas_dgbmv.
\param[in] KL Argumento \em KL de la función \ref cblas_dgbmv.
\param[in] KU Argumento \em KU de la función \ref cblas_dgbmv.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] KKL Argumento \em KL para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] KKU Argumento \em KU para ejecutar la función en COLUMN MAJOR ORDER.
\date 17 de enero de 2014: Creación de la macro.
\date 18 de marzo de 2017: Supresión de warning en el compilador PGI.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_GBMV(Order,TransA,Mm,Nn,KL,KU,TA,MM,NN,KKL,KKU) \
if((Order)==CblasColMajor) { \
    TA = ((TransA)==CblasNoTrans) ? 'N' : 'T'; \
    MM = Mm; \
    NN = Nn; \
    KKL = KL; \
    KKU = KU; \
} else { \
    TA = ((TransA)==CblasNoTrans) ? 'T' : 'N'; \
    MM = Nn; \
    NN = Mm; \
    KKL = KU; \
    KKU = KL; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SYMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsymv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsymv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsymv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SYMV(Order,Uplo,UPLO) \
if((Order)==CblasColMajor) { \
    UPLO = ((Uplo)==CblasUpper) ? 'U' : 'L'; \
} else { \
    UPLO = ((Uplo)==CblasUpper) ? 'L' : 'U'; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SBMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsbmv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsbmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsbmv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SBMV(Order,Uplo,UPLO) GBLAS_VCMO_SYMV(Order,Uplo,UPLO)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SPMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dspmv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dspmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dspmv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SPMV(Order,Uplo,UPLO) GBLAS_VCMO_SYMV(Order,Uplo,UPLO)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TRMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtrmv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrmv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrmv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG) \
if((Order)==CblasColMajor) { \
    UPLO = ((Uplo)==CblasUpper) ? 'U' : 'L'; \
    TA = ((TransA)==CblasNoTrans) ? 'N' : 'T'; \
} else { \
    UPLO = ((Uplo)==CblasUpper) ? 'L' : 'U'; \
    TA = ((TransA)==CblasNoTrans) ? 'T' : 'N'; \
} \
DIAG = ((Diag)==CblasUnit) ? 'U' : 'N';
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TBMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtbmv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtbmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtbmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtbmv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtbmv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TBMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG) \
    GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TPMV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtpmv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtpmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtpmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtpmv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtpmv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TPMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG) \
    GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TRSV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtrsv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrsv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrsv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrsv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrsv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TRSV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG) \
    GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TBSV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtbsv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtbsv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtbsv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtbsv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtbsv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TBSV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG) \
    GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TPSV
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtpsv siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtpsv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtpsv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtpsv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtpsv.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TPSV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG) \
    GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_GER
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dger siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dger.
\param[in] M Argumento \em M de la función \ref cblas_dger.
\param[in] N Argumento \em N de la función \ref cblas_dger.
\param[in] X Argumento \em X de la función \ref cblas_dger.
\param[in] incX Argumento \em incX de la función \ref cblas_dger.
\param[in] Y Argumento \em Y de la función \ref cblas_dger.
\param[in] incY Argumento \em incY de la función \ref cblas_dger.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] XX Argumento \em X para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] INCXX Argumento \em incX para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] YY Argumento \em Y para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] INCYY Argumento \em incY para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_GER(Order,M,N,X,incX,Y,incY,MM,NN,XX,INCXX,YY,INCYY) \
if((Order)==CblasColMajor) { \
    MM = M; \
    NN = N; \
    XX = X; \
    INCXX = incX; \
    YY = Y; \
    INCYY = incY; \
} else { \
    MM = N; \
    NN = M; \
    XX = Y; \
    INCXX = incY; \
    YY = X; \
    INCYY = incX; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SYR
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsyr siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyr.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyr.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SYR(Order,Uplo,UPLO) GBLAS_VCMO_SYMV(Order,Uplo,UPLO)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SPR
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dspr siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dspr.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dspr.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SPR(Order,Uplo,UPLO) GBLAS_VCMO_SYMV(Order,Uplo,UPLO)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SYR2
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsyr2 siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyr2.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyr2.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SYR2(Order,Uplo,UPLO) GBLAS_VCMO_SYMV(Order,Uplo,UPLO)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SPR2
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dspr2 siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dspr2.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dspr2.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SPR2(Order,Uplo,UPLO) GBLAS_VCMO_SYMV(Order,Uplo,UPLO)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_GEMM
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dgemm siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dgemm.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dgemm.
\param[in] TransB Argumento \em TransB de la función \ref cblas_dgemm.
\param[in] Mm Argumento \em M de la función \ref cblas_dgemm.
\param[in] Nn Argumento \em N de la función \ref cblas_dgemm.
\param[in] A Argumento \em A de la función \ref cblas_dgemm.
\param[in] lda Argumento \em lda de la función \ref cblas_dgemm.
\param[in] B Argumento \em B de la función \ref cblas_dgemm.
\param[in] ldb Argumento \em ldb de la función \ref cblas_dgemm.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TB Argumento \em TransB para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] AA Argumento \em A para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] LDAA Argumento \em lda para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] BB Argumento \em B para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] LDBB Argumento \em ldb para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\date 18 de marzo de 2017: Supresión de warning en el compilador PGI.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_GEMM(Order,TransA,TransB,Mm,Nn,A,lda,B,ldb, \
                        TA,TB,MM,NN,AA,LDAA,BB,LDBB) \
if((Order)==CblasColMajor) { \
    TA = ((TransA)==CblasNoTrans) ? 'N' : 'T'; \
    TB = ((TransB)==CblasNoTrans) ? 'N' : 'T'; \
    MM = Mm; \
    NN = Nn; \
    AA = A; \
    LDAA = lda; \
    BB = B; \
    LDBB = ldb; \
} else { \
    TA = ((TransB)==CblasNoTrans) ? 'N' : 'T'; \
    TB = ((TransA)==CblasNoTrans) ? 'N' : 'T'; \
    MM = Nn; \
    NN = Mm; \
    AA = B; \
    LDAA = ldb; \
    BB = A; \
    LDBB = lda; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SYMM
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsymm siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsymm.
\param[in] Side Argumento \em Side de la función \ref cblas_dsymm.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsymm.
\param[in] M Argumento \em M de la función \ref cblas_dsymm.
\param[in] N Argumento \em N de la función \ref cblas_dsymm.
\param[out] SIDE Argumento \em Side para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SYMM(Order,Side,Uplo,M,N,SIDE,UPLO,MM,NN) \
if((Order)==CblasColMajor) { \
    SIDE = ((Side)==CblasLeft) ? 'L' : 'R'; \
    UPLO = ((Uplo)==CblasUpper) ? 'U' : 'L'; \
    MM = M; \
    NN = N; \
} else { \
    SIDE = ((Side)==CblasLeft) ? 'R' : 'L'; \
    UPLO = ((Uplo)==CblasUpper) ? 'L' : 'U'; \
    MM = N; \
    NN = M; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SYRK
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsyrk siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyrk.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyrk.
\param[in] Trans Argumento \em Trans de la función \ref cblas_dsyrk.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TT Argumento \em Trans para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SYRK(Order,Uplo,Trans,UPLO,TT) \
if((Order)==CblasColMajor) { \
    UPLO = ((Uplo)==CblasUpper) ? 'U' : 'L'; \
    TT = ((Trans)==CblasNoTrans) ? 'N' : 'T'; \
} else { \
    UPLO = ((Uplo)==CblasUpper) ? 'L' : 'U'; \
    TT = ((Trans)==CblasNoTrans) ? 'T' : 'N'; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_SYR2K
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dsyr2k siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyr2k.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyr2k.
\param[in] Trans Argumento \em Trans de la función \ref cblas_dsyr2k.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TT Argumento \em Trans para ejecutar la función en COLUMN MAJOR
            ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_SYR2K(Order,Uplo,Trans,UPLO,TT) \
    GBLAS_VCMO_SYRK(Order,Uplo,Trans,UPLO,TT)
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TRMM
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtrmm siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrmm.
\param[in] Side Argumento \em Side de la función \ref cblas_dtrmm.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrmm.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrmm.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrmm.
\param[in] Mm Argumento \em M de la función \ref cblas_dtrmm.
\param[in] Nn Argumento \em N de la función \ref cblas_dtrmm.
\param[out] SIDE Argumento \em Side para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\date 17 de enero de 2014: Creación de la macro.
\date 18 de marzo de 2017: Supresión de warning en el compilador PGI.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TRMM(Order, \
                        Side,Uplo,TransA,Diag,Mm,Nn,SIDE,UPLO,TA,DIAG,MM,NN) \
if((Order)==CblasColMajor) { \
    SIDE = ((Side)==CblasLeft) ? 'L' : 'R'; \
    UPLO = ((Uplo)==CblasUpper) ? 'U' : 'L'; \
    MM = Mm; \
    NN = Nn; \
} else { \
    SIDE = ((Side)==CblasLeft) ? 'R' : 'L'; \
    UPLO = ((Uplo)==CblasUpper) ? 'L' : 'U'; \
    MM = Nn; \
    NN = Mm; \
} \
TA = ((TransA)==CblasNoTrans) ? 'N' : 'T'; \
DIAG = ((Diag)==CblasUnit) ? 'U' : 'N';
/******************************************************************************/
/******************************************************************************/
/**
\def GBLAS_VCMO_TRSM
\brief Macro para ajustar las variables de entrada para ejecutar la función
       \ref cblas_dtrsm siempre en COLUMN MAJOR ORDER.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrsm.
\param[in] Side Argumento \em Side de la función \ref cblas_dtrsm.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrsm.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrsm.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrsm.
\param[in] M Argumento \em M de la función \ref cblas_dtrsm.
\param[in] N Argumento \em N de la función \ref cblas_dtrsm.
\param[out] SIDE Argumento \em Side para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] UPLO Argumento \em Uplo para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] TA Argumento \em TransA para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] DIAG Argumento \em Diag para ejecutar la función en COLUMN MAJOR
            ORDER.
\param[out] MM Argumento \em M para ejecutar la función en COLUMN MAJOR ORDER.
\param[out] NN Argumento \em N para ejecutar la función en COLUMN MAJOR ORDER.
\date 17 de enero de 2014: Creación de la macro.
\todo Esta macro no está probada.
*/
#define GBLAS_VCMO_TRSM(Order, \
                        Side,Uplo,TransA,Diag,M,N,SIDE,UPLO,TA,DIAG,MM,NN) \
    GBLAS_VCMO_TRMM(Order,Side,Uplo,TransA,Diag,M,N,SIDE,UPLO,TA,DIAG,MM,NN)
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
