/**
\defgroup dibgmt Modulo DIBGMT
\brief En este modulo se reunen los ficheros necesarios para crear dibujos con
       el paquete GMT (Generic Mapping Tools).

Los ficheros descritos a continuacion son el material imprescindible para crear
dibujos con el paquete GMT (Generic Mapping Tools).
@{
\file paramgmt.h
\brief Archivo de declaracion de constantes para la escritura de ficheros de
       ordenes para GMT.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com,jlgpallero@pdi.ucm.es
\author Pablo Jose Gonzalez Mendez, pjgonzal@mat.ucm.es
\date 21 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves de
las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
 */
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _PARAMGMT_H_
#define _PARAMGMT_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def INTERPRETE
    \brief Interprete para la ejecucion de comandos. */
#define INTERPRETE "/bin/bash"
/** \def GMTSET
    \brief Parametros generales de dibujo con GMT. */
#define GMTSET "gmtset \\\n\
                BASEMAP_TYPE fancy \\\n\
                COLOR_NAN 150/150/150 \\\n\
                DOTS_PR_INCH 300 \\\n\
                ANNOT_FONT_PRIMARY Times-Roman \\\n\
                ANNOT_FONT_SIZE_PRIMARY 14 \\\n\
                ANNOT_FONT_SIZE_SECONDARY 14 \\\n\
                HEADER_FONT Times-Roman \\\n\
                LABEL_FONT Times-Roman \\\n\
                LABEL_FONT_SIZE 14 \\\n\
                HEADER_FONT_SIZE 14 \\\n\
                UNIX_TIME_POS 0c/-1.5c \\\n\
                PS_COLOR CMYK \\\n\
                PAPER_MEDIA A4+ \\\n\
                MEASURE_UNIT cm \\\n\
                X_ORIGIN 2c \\\n\
                Y_ORIGIN 2c \\\n\
                BASEMAP_AXES WeSn \\\n\
                PLOT_DEGREE_FORMAT ddd:mm"
/** \def EXTPS
    \brief Extension de un fichero PostScript. */
#define EXTPS      ".ps"
/** \def INDP
    \brief Indicador de fichero de planimetria. */
#define INDP       "P"
/** \def INDH
    \brief Indicador de fichero de altimetria. */
#define INDH       "H"
/** \def NVARFSALP
    \brief Nombre de la variable que almacena el nombre del fichero de salida de
           planimetria. */
#define NVARFSALP  "fSalP"
/** \def NVARFSALH
    \brief Nombre de la variable que almacena el nombre del fichero de salida de
           altimetria. */
#define NVARFSALH  "fSalH"
/** \def NVARDIBCOD
    \brief Nombre de la variable que almacena el indicador de dibujo de los
           codigos de las estaciones. */
#define NVARDIBCOD "dibcod"
/** \def DIBCOD
    \brief Valor del indicador de dibujo de los codigos de las estaciones. */
#define DIBCOD     1
/** \def NVARNCONF
    \brief Nombre de la variable que almacena el nivel de confianza para
           calcular los intervalos de confianza de los errores. */
#define NVARNCONF  "nconf"
/** \def NCONF
    \brief Nivel de confianza del 95 por ciento. */
#define NCONF      0.950
/** \def NVARFACMUL
    \brief Nombre de la variable que almacena el factor de multiplicacion para
           un nivel de confianza concreto. */
#define NVARFACMUL "facmul"
/** \def FACMUL
    \brief Factor de multiplicacion para un nivel de confianza del 95 por
           ciento. */
#define FACMUL     1.64485
/** \def NVARLIMIT
    \brief Nombre de la variable que almacena los limites del dibujo. */
#define NVARLIMIT  "limit"
/** \def LIMIT
    \brief Indicador de limites del dibujo. */
#define LIMIT      "-R"
/** \def NVARBARLAT
    \brief Nombre de la variable que almacena el tipo de barras laterales del
           dibujo. */
#define NVARBARLAT "barrlat"
/** \def BARLAT
    \brief Indicador de barras laterales. */
#define BARLAT     "-B"
/** \def NVARPROYEC
    \brief Nombre de la variable que almacena la proyeccion cartografica
           utilizada. */
#define NVARPROYEC "proyec"
/** \def PROYEC
    \brief Proyeccion cartografica utilizada. */
#define PROYEC     "-JM16"
/** \def NVARORIPAG
    \brief Nombre de la variable que almacena la orientacion del papel. */
#define NVARORIPAG "oripag"
/** \def ORIPAG
    \brief Indicador de orientacion del papel. */
#define ORIPAG     "-P"
/** \def NVARLOGO
    \brief Nombre de la variable que almacena la aparicion del logo de GMT en
           los dibujos. */
#define NVARLOGO   "logo"
/** \def LOGO
    \brief Indicador de apacicion del logo de GMT en los dibujos. */
#define LOGO       "-U"
/** \def NVARRESCOS
    \brief Nombre de la variable que almacena la resolucion de las lineas de
           costa. */
#define NVARRESCOS "rescosta"
/** \def RESCOS
    \brief Indicador de resolucion de las lineas de costa. */
#define RESCOS     "-Df"
/** \def NVAREGRAF
    \brief Nombre de la variable que almacena la escala grafica del dibujo. */
#define NVAREGRAF  "escgraf"
/** \def EGRAF
    \brief Indicador de la escala grafica del dibujo. */
#define EGRAF      "-Lfx2.0/1.0/0/"
/** \def NVAREDEFH
    \brief Nombre de la variable que almacena la escala grafica de referencia
           para las deformaciones horizontales. */
#define NVAREDEFH   "escdefh"
/** \def NVAREDEFV
    \brief Nombre de la variable que almacena la escala grafica de referencia
           para las deformaciones verticales. */
#define NVAREDEFV   "escdefv"
/** \def NVARRIOS
    \brief Nombre de la variable que almacena el indicador de dibujo de rios. */
#define NVARRIOS   "rios"
/** \def RIOS
    \brief Indicador de dibujo de rios. */
#define RIOS       "-Ia/cyan"
/** \def NVARCTIERR
    \brief Nombre de la variable que almacena el color de las partes solidas de
           la Tierra en los dibujos. */
#define NVARCTIERR "colortierra"
/** \def TIERR
    \brief Indicador del color de las partes solidas de la Tierra. */
#define TIERR      "-Gwhite"
/** \def NVARCAGUA
    \brief Nombre de la variable que almacena el color de las partes liquidas de
           la Tierra en los dibujos. */
#define NVARCAGUA  "coloragua"
/** \def AGUA
    \brief Indicador del color de las partes liquidas de la Tierra. */
#define AGUA       "-Scyan"
/** \def NVARPFLECH
    \brief Nombre de la variable que almacena los parametros de dibujo de las
           flechas. */
#define NVARPFLECH  "paramflech"
/** \def PFLECH
    \brief Parametros de dibujo de las flechas. */
#define PFLECH       "-A0.03/0.12/0.05"
/** \def NVARPARELI
    \brief Nombre de la variable que almacena los parametros de dibujo de las
           elipses de error. */
#define NVARPARELI "paramelip"
/** \def PARELI1
    \brief Valor del primer parametro de dibujo de las elipses de error. */
#define PARELI1    "-Se20/"
/** \def PARELI3
    \brief Valor del tercer parametro de dibujo de las elipses de error. */
#define PARELI3    "/5"
/** \def NVARPBERR
    \brief Nombre de la variable que almacena los parametros de dibujo de las
           barras de error. */
#define NVARPBERR  "paramberr"
/** \def PBERR
    \brief Valor de los parametros de dibujo de las barras de error. */
#define PBERR      "-Se20/0.394/5"
/** \def NVARCELIP
    \brief Nombre de la variable que almacena el color de las elipses y barras
           de error. */
#define NVARCELIP  "colorelipse"
/** \def CELIP
    \brief Indicador del color de las elipses y barras de error. */
#define CELIP      "-Gblack"
/** \def NPASOSCOOR
    \brief Numero de indicadores de coordenadas en las barras laterales. */
#define NPASOSCOOR 3
/** \def FRAESCGRAF
    \brief Fraccion del incremeto en coordenadas geodesicas que define el
           tamanyo de escala grafica del dibujo. */
#define FRAESCGRAF 4
/** \def AMPLIAC1P
    \brief Ampliacion de los limites del dibujo (en grados sexagesimales,
           formato decimal) en el caso en que se trabaje con un solo punto. */
#define AMPLIAC1P  0.1
/** \def AMPLIALAT
    \brief Factor de ampliacion de los limites del dibujo en latitud. */
#define AMPLIALAT  0.25
/** \def AMPLIALON
    \brief Factor de ampliacion de los limites del dibujo en longitud. */
#define AMPLIALON  0.25
/** \def POLON
    \brief Latitud geodesica del polo norte. */
#define POLON      90.0
/** \def POLOS
    \brief Latitud geodesica del polo sur. */
#define POLOS      -90.0
/** \def DIFPOLO
    \brief Cantidad a restar de la latitud de los polos para evitar que valga
           exactamente 90.0 grados. */
#define DIFPOLO    0.01
/** \def FORMCODGMT
    \brief Formato de escritura de codigo de estacion en un script de GMT. */
#define FORMCODGMT   "%8s"
/** \def FORMLATGMT
    \brief Formato de escritura de latitudes en un script de GMT. */
#define FORMLATGMT   "%14.9lf"
/** \def FORMLONGMT
    \brief Formato de escritura de longitudes en un script de GMT. */
#define FORMLONGMT   "%14.9lf"
/** \def FORMDEGMT
    \brief Formato de escritura de desplazamientos a lo largo del eje east en un
           script de GMT. */
#define FORMDEGMT    "%7.4lf"
/** \def FORMDNGMT
    \brief Formato de escritura de desplazamientos a lo largo del eje north en
           un script de GMT. */
#define FORMDNGMT    "%7.4lf"
/** \def FORMDUGMT
    \brief Formato de escritura de desplazamientos a lo largo del eje up en un
           script de GMT. */
#define FORMDUGMT    "%7.4lf"
/** \def FORMSEGMT
    \brief Formato de escritura de la desviacion tipica del desplazamiento a lo
           largo del eje east en un script de GMT. */
#define FORMSEGMT    "%12.8lf"
/** \def FORMSNGMT
    \brief Formato de escritura de la desviacion tipica del desplazamiento a lo
           largo del eje north en un script de GMT. */
#define FORMSNGMT    "%12.8lf"
/** \def FORMSUGMT
    \brief Formato de escritura de la desviacion tipica del desplazamiento a lo
           largo del eje up en un script de GMT. */
#define FORMSUGMT    "%12.8lf"
/** \def FORMCORRGMT
    \brief Formato de escritura de coeficientes de correlacion en un script de
           GMT. */
#define FORMCORRGMT  "%12.8lf"
/** \def FORMNCONFGMT
    \brief Formato de escritura de nivel de confianza en un script de GMT. */
#define FORMNCONFGMT "%.3lf"
/** \def FORMFMULGMT
    \brief Formato de escritura de factor de multiplicacion en un script de
           GMT. */
#define FORMFMULGMT  "%.5lf"
/** \def FORMLDIBGMT
    \brief Formato de escritura de limites de dibujo en un script de GMT. */
#define FORMLDIBGMT  "%.2lf"
/** \def FORMPASOCOOR
    \brief Formato de escritura del paso en coordenadas para las barras
           laterales del dibujo en un script de GMT. */
#define FORMPASOCOOR "%.1lf"
/** \def SEPARADGMT
    \brief Separador de campos de datos en un script de GMT. */
#define SEPARADGMT   " "
/** \def LONFORMGMT
    \brief Longitud de la cadena de texto definitoria de formatos de escritura
           un script de GMT. */
#define LONFORMGMT   500
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
