/**
\addtogroup sinex
@{
\file paramsnx.h
\brief Archivo de definicion de constantes para el trabajo con ficheros SINEX.

En este fichero se definen las constantes simbolicas necesarias para identificar
los distintos bloques de datos que componen un fichero SINEX (Solution
INdependent EXchange).
\author Jose Luis Garcia Pallero, jgpallero@gmail.com,jlgpallero@pdi.ucm.es
\date 02 de julio de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves de
las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _PARAMSNX_H_
#define _PARAMSNX_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//parametros generales aplicables a todo el fichero SINEX
/** \def LONMAXLINSNX
    \brief Longitud maxima de las lineas de un fichero SINEX. */
#define LONMAXLINSNX  80
/** \def CADINIBLOQSNX
    \brief Cadena identificadora de comienzo de bloque de un fichero SINEX. */
#define CADINIBLOQSNX "+"
/** \def CADFINBLOQSNX
    \brief Cadena identificadora de final de bloque de un fichero SINEX. */
#define CADFINBLOQSNX "-"
/** \def SEPCADINIBLOQETIQBLOQ
    \brief Cadena de separacion entre la cadena identificadora de inicio de
           bloque y la cadena identificadora del bloque. */
#define SEPCADINIBLOQETIQBLOQ ""
/** \def SEPCADFINBLOQETIQBLOQ
    \brief Cadena de separacion entre la cadena identificadora de fin de bloque
           y la cadena identificadora del bloque. */
#define SEPCADFINBLOQETIQBLOQ ""
/** \def SEPELEMETIQBLOQ
    \brief Cadena de separacion entre la cadena identificadora de bloque y otros
           elementos informativos del bloque como, por ejemplo, el tipo y clase
           de matriz almacenada, etc. */
#define SEPELEMETIQBLOQ " "
/** \def CADCOMENTSNX
    \brief Cadena identificadora de linea de comentario de un fichero SINEX. */
#define CADCOMENTSNX  "*"
/** \def POSINICOMENTSNX
    \brief Posicion en una linea en la que comienza el indicador de comentario
           (las posiciones comienzan en 0). */
#define POSINICOMENTSNX  0
/** \def SIGLO21SNX
    \brief Valor de los dos ultimos digitos del anyo en el formato indicador de
           epoca de observacion para los que corresponde un anyo del siglo
           XXI. */
/**
    El formato indicador de epoca de observacion en un fichero SINEX es
    aa:ddd:sssss, en al cual cada elemento corresponde a:
        - aa: ultimos dos digitos del anyo.
        - ddd: numero del dia del anyo.
        - sssss: numero del segundo del dia.

    Entonces, si aa <= #SIGLO21SNX el anyo pertenece al siglo XXI.

    \note Los campos estan separados por el caracter ":".
*/
#define SIGLO21SNX 50
/** \def DIASANYOSNX
    \brief Dias del anyo. */
#define DIASANYOSNX 365.25
/** \def SEGUNDOSDIASNX
    \brief Segundos del dia. */
#define SEGUNDOSDIASNX 86400.0
/** \def FORMATOEPOCASNX
    \brief Formato de epoca de observacion en un fichero SINEX. */
#define FORMATOEPOCASNX "%d:%d:%d"
/** \def IDNOBLOQUESNX
    \brief Identificador de la posicion de un bloque cuando este no existe en el
           fichero SINEX */
#define IDNOBLOQUESNX -1
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//parametros aplicables al bloque SOLUTION/ESTIMATE del fichero SINEX
/** \def ETIQUETASOLES
    \brief Etiqueta identificadora del bloque SOLUTION/ESTIMATE. */
#define ETIQUETASOLES "SOLUTION/ESTIMATE"
/** \def SEPCAMPOSSOLES
    \brief Cadena de separacion entre los campos de las lineas de datos en un
           bloque SOLUTION/ESTIMATE. */
#define SEPCAMPOSSOLES " "
/** \def COORXSOLES
    \brief Etiqueta identificadora de campo correspondiente a coordenada X en un
           bloque SOLUTION/ESTIMATE. */
#define COORXSOLES "STAX"
/** \def COORYSOLES
    \brief Etiqueta identificadora de campo correspondiente a coordenada Y en un
           bloque SOLUTION/ESTIMATE. */
#define COORYSOLES "STAY"
/** \def COORZSOLES
    \brief Etiqueta identificadora de campo correspondiente a coordenada Z en un
           bloque SOLUTION/ESTIMATE. */
#define COORZSOLES "STAZ"
/** \def VELXSOLES
    \brief Etiqueta identificadora de campo correspondiente a velocidad de
           coordenada X en un bloque SOLUTION/ESTIMATE. */
#define VELXSOLES "VELX"
/** \def VELYSOLES
    \brief Etiqueta identificadora de campo correspondiente a velocidad de
           coordenada Y en un bloque SOLUTION/ESTIMATE. */
#define VELYSOLES "VELY"
/** \def VELZSOLES
    \brief Etiqueta identificadora de campo correspondiente a velocidad de
           coordenada Z en un bloque SOLUTION/ESTIMATE. */
#define VELZSOLES "VELZ"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//parametros aplicables al bloque SOLUTION/MATRIX_ESTIMATE del fichero SINEX
/** \def ETIQUETASOLMATES
    \brief Etiqueta identificadora del bloque SOLUTION/MATRIX_ESTIMATE. */
#define ETIQUETASOLMATES "SOLUTION/MATRIX_ESTIMATE"
/** \def SEPCAMPOSSOLMATES
    \brief Cadena de separacion entre los campos de las lineas de datos en un
           bloque SOLUTION/MATRIX_ESTIMATE. */
#define SEPCAMPOSSOLMATES " "
/** \def TRISUPSOLMATES
    \brief Identificador de matriz triangular superior en un fichero SINEX. */
#define TRISUPSOLMATES "U"
/** \def TRIINFSOLMATES
    \brief Identificador de matriz triangular inferior en un fichero SINEX. */
#define TRIINFSOLMATES "L"
/** \def MATCOVASOLMATES
    \brief Identificador de matriz de varianza-covarianza. */
#define MATCOVASOLMATES "COVA"
/** \def MATCORRSOLMATES
    \brief Identificador de matriz de correlacion. */
#define MATCORRSOLMATES "CORR"
/** \def MATINFOSOLMATES
    \brief Identificador de matriz normal (constreñimientos incluidos). */
#define MATINFOSOLMATES "INFO"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//parametros aplicables al bloque SOLUTION/STATISTICS del fichero SINEX
/** \def ETIQUETASOLSTAT
    \brief Etiqueta identificadora del bloque SOLUTION/STATISTICS. */
#define ETIQUETASOLSTAT "SOLUTION/STATISTICS"
/** \def LONETIQUSOLSTAT
    \brief Longitud de la etiqueta identificadora de parametros del bloque
           SOLUTION/STATISTICS. */
#define LONETIQUSOLSTAT 30
/** \def LONPARAMSOLSTAT
    \brief Longitud del campo que almacena el parametro en el bloque
           SOLUTION/STATISTICS. */
#define LONPARAMSOLSTAT 22
/** \def NUMESPPRINSOLSTAT
    \brief Numero de espacios en blanco al principio de cada linea en el bloque
           SOLUTION/STATISTICS. */
#define NUMESPPRINSOLSTAT   1
/** \def NUMESPETIQUPARAMSOLSTAT
    \brief Numero de espacios en blanco entre la etiqueta identificadora de
           parametro y el parametro en el bloque SOLUTION/STATISTICS. */
#define NUMESPETIQUPARAMSOLSTAT   1
/** \def ETIQUNOBSSOLSTAT
    \brief Etiqueta identificadora del parametro "numero de observaciones" del
           bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUNOBSSOLSTAT     "NUMBER OF OBSERVATIONS        "
/** \def ETIQUNUNKSOLSTAT
    \brief Etiqueta identificadora del parametro "numero de incognitas" del
           bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUNUNKSOLSTAT     "NUMBER OF UNKNOWNS            "
/** \def ETIQUSINTSOLSTAT
    \brief Etiqueta identificadora del parametro "intervalo de muestreo" del
           bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUSINTSOLSTAT     "SAMPLING INTERVAL (SECONDS)   "
/** \def ETIQUSSRSOLSTAT
    \brief Etiqueta identificadora del parametro "sumatorio del cuadrado de los
           residuos" del bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUSSRSOLSTAT      "SQUARE SUM OF RESIDUALS (VTPV)"
/** \def ETIQUPMSSOLSTAT
    \brief Etiqueta identificadora del parametro "sigma de la medida de fase"
           del bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUPMSSOLSTAT      "PHASE MEASUREMENTS SIGMA      "
/** \def ETIQUCMSSOLSTAT
    \brief Etiqueta identificadora del parametro "sigma de la medida de codigo"
           del bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUCMSSOLSTAT      "CODE MEASUREMENTS SIGMA       "
/** \def ETIQUNFREESOLSTAT
    \brief Etiqueta identificadora del parametro "numero de grados de libertad"
           del bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUNFREESOLSTAT    "NUMBER OF DEGREES OF FREEDOM  "
/** \def ETIQUVARFACTSOLSTAT
    \brief Etiqueta identificadora del parametro "estimacion de la varianza del
           observable de peso unidad a posteriori" del bloque
           SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUVARFACTSOLSTAT  "VARIANCE FACTOR               "
/** \def ETIQUWSSSOLSTAT
    \brief Etiqueta identificadora del parametro "WEIGHTED SQUARE SUM OF O-C"
           del bloque SOLUTION/STATISTICS. */
/**
\note Esta etiqueta ha de tener un numero de caracteres igual a
      #LONETIQUSOLSTAT.
*/
#define ETIQUWSSSOLSTAT      "WEIGHTED SQUARE SUM OF O-C    "
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
