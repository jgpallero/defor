/**
\addtogroup sinex
@{
\file statsnx.c
\brief Definicion de la estructura y las funciones para la extraccion de
       parametros estadisticos del ajuste de ficheros SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"general.h"
#include"paramsnx.h"
#include"bloqsnx.h"
#include"statsnx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void InicializaStatSnx(statsnx* estadisticas)
{
    //ponemos a 0 todos los indicadores de existencia
    estadisticas->enobs = 0;
    estadisticas->enunk = 0;
    estadisticas->esint = 0;
    estadisticas->essr = 0;
    estadisticas->epms = 0;
    estadisticas->ecms = 0;
    estadisticas->enfree = 0;
    estadisticas->evarfact = 0;
    estadisticas->ewss = 0;
    //asignamos un valor inicial a la estimacion de la varianza del observable
    //de peso unidad a posteriori
    estadisticas->varfact = 1.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LeeEstadisticasStatSnx(FILE* idFichero,
                            long int posFichSOLSTAT,
                            int lineaIniSOLSTAT,
                            int lineaFinSOLSTAT,
                            statsnx* estadisticas)
{
    //indice para recorrer bucle
    int i=0;
    //longitud de cada linea
    int lonLinea=LONMAXLINSNX;
    //linea leida
    char lineaLeida[lonLinea+2];
    //numero de lineas con datos
    int nLineas=0;
    //etiqueta
    char etiqueta[lonLinea+2];
    //parametro
    char parametro[lonLinea+2];
    //posicion inicial en el fichero
    long int posOriginal=ftell(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura estadisticas
    InicializaStatSnx(estadisticas);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //LECTURA DE PARAMETROS ESTADISTICOS
    //nos colocamos al comienzo de la linea que contiene la cabecera del bloque
    //SOLUTION/STATISTICS
    fseek(idFichero,posFichSOLSTAT,SEEK_SET);
    //leemos la linea que contiene la cabecera del bloque
    fgets(lineaLeida,lonLinea+2,idFichero);
    //numero de lineas con datos en el bloque SOLUTION/STATISTICS
    nLineas = lineaFinSOLSTAT-lineaIniSOLSTAT-1;
    //vamos recorriendo todas las lineas
    for(i=lineaIniSOLSTAT+1;i<lineaIniSOLSTAT+nLineas+1;i++)
    {
        //leemos la siguiente linea del fichero
        fgets(lineaLeida,lonLinea+2,idFichero);
        //trabajamos con ella si no es un comentario
        if(!ExisteSubcadena(lineaLeida,POSINICOMENTSNX,CADCOMENTSNX))
        {
            //leemos la etiqueta
            ExtraeSubcadena(lineaLeida,NUMESPPRINSOLSTAT,
                            LONETIQUSOLSTAT,etiqueta);
            //leemos el parametro
            ExtraeSubcadena(lineaLeida,
                      NUMESPPRINSOLSTAT+LONETIQUSOLSTAT+NUMESPETIQUPARAMSOLSTAT,
                            LONPARAMSOLSTAT,parametro);
            //asignamos los parametros extraidos al miembro correspondiente de
            //la estructura y activamos el indicador de existencia
            if(!strcmp(ETIQUNOBSSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->enobs = 1;
                //asignamos el parametro
                estadisticas->nobs = atoi(parametro);
            }
            else if(!strcmp(ETIQUNUNKSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->enunk = 1;
                //asignamos el parametro
                estadisticas->nunk = atoi(parametro);
            }
            else if(!strcmp(ETIQUSINTSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->esint = 1;
                //asignamos el parametro
                estadisticas->sint = atof(parametro);
            }
            else if(!strcmp(ETIQUSSRSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->essr = 1;
                //asignamos el parametro
                estadisticas->ssr = atof(parametro);
            }
            else if(!strcmp(ETIQUPMSSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->epms = 1;
                //asignamos el parametro
                estadisticas->pms = atof(parametro);
            }
            else if(!strcmp(ETIQUCMSSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->ecms = 1;
                //asignamos el parametro
                estadisticas->cms = atof(parametro);
            }
            else if(!strcmp(ETIQUNFREESOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->enfree = 1;
                //asignamos el parametro
                estadisticas->nfree = atoi(parametro);
            }
            else if(!strcmp(ETIQUVARFACTSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->evarfact = 1;
                //asignamos el parametro
                estadisticas->varfact = atof(parametro);
            }
            else if(!strcmp(ETIQUWSSSOLSTAT,etiqueta))
            {
                //activamos el indicador de existencia
                estadisticas->ewss = 1;
                //asignamos el parametro
                estadisticas->wss = atof(parametro);
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el puntero de L/E en su posicion original en el fichero
    fseek(idFichero,posOriginal,SEEK_SET);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeEstadisticasFicheroSinex(char fichero[],
                                int imprimeVarfact,
                                statsnx* estadisticas)
{
    //codigo de error
    int cError=ERRNOERROR;
    //fichero de trabajo
    FILE* idFichero=NULL;
    //estructura bloqsnx
    bloqsnx bloques;
    //posiciones del bloque de estadisticas
    int posSOLSTAT=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura bloqsnx
    InicializaBloqSnx(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //abrimos el fichero de trabajo para leer
    if((idFichero=fopen(fichero,"rb"))==NULL)
    {
        //lanzamos un mensaje de error
        MensajeErrorABRIRFICHERO((char*)__func__,fichero);
        //salimos de la funcion
        return ERRABRIRFICHERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los bloques del fichero
    cError = LeeBloquesBloqSnx(idFichero,CADINIBLOQSNX,CADFINBLOQSNX,&bloques);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,
                              "LeeBloquesBloqSnx()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqSnx(&bloques);
        //salimos de la funcion
        return cError;
    }
    //buscamos las posiciones de los bloques de trabajo
    posSOLSTAT = PosicionBloqueBloqSnx(&bloques,ETIQUETASOLSTAT);
    if(posSOLSTAT==IDNOBLOQUESNX)
    {
        //si se ha pedido la estimacion de la varianza del observable de peso
        //unidad a posteriori devolvemos el valor 1.0 y salimos del programa
        if(imprimeVarfact)
        {
            fprintf(stdout,"1.0\n");
        }
        //lanzamos un mensaje de error
        MensajeErrorNOBLOQUEFICH((char*)__func__,ETIQUETASOLSTAT,fichero);
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqSnx(&bloques);
        //salimos de la funcion
        return ERRNOBLOQUEFICH;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos las estadisticas
    LeeEstadisticasStatSnx(idFichero,
                           bloques.posicion[posSOLSTAT][0],
                           bloques.lineas[posSOLSTAT][0],
                           bloques.lineas[posSOLSTAT][1],
                           estadisticas);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //cerramos el fichero de trabajo
    fclose(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaBloqSnx(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
