/**
\file paramprg.h
\brief Declaracion de parametros para utilizar en la construccion de programas
       con la biblioteca defor.

En este fichero se declaran las funciones y constantes de longitud, formatos,
etc. para la construccion de programas que utilizan la biblioteca defor.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 13 de julio de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _PARAMPRG_H_
#define _PARAMPRG_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#define FLAGFICHSINEX   "-s"
#define FLAGFICHPUNTOS  "-p"
#define FLAGFICHERO     "-f"
#define FLAGBLOQUE      "-b"
#define FLAGCOD         "-c"
#define FLAGCODTRAN     "-c"
#define FLAGSISTREF     "-sr"
#define FLAGEPOCA       "-t"
#define FLAGEPOCATRAN   "-t"
#define FLAGCOORSNX     "-p"
#define FLAGVELSNX      "-v"
#define FLAGVCCOORSNX   "-ep"
#define FLAGVCVELSNX    "-ev"
#define FLAGCOORPTOS    "-p"
#define FLAGVCCOORPTOS  "-ep"
#define FLAGELIPSE      "-el"
#define FLAGPARTRAN     "-p"
#define FLAGVPARTRAN    "-vp"
#define FLAGVACIO       "VACIO"
#define FLAGNOBS        "-nobs"
#define FLAGNUNK        "-nunk"
#define FLAGSINT        "-sint"
#define FLAGSSR         "-ssr"
#define FLAGPMS         "-pms"
#define FLAGCMS         "-cms"
#define FLAGNFREE       "-nfree"
#define FLAGVARFACT     "-varfact"
#define FLAGWSS         "-wss"
#define FLAGDEFORGMT    "-g"
#define FLAGDEFORCOMP   "-c"
#define FLAGDEFORRES    "-r"
#define FLAGEPOR        "-o"
#define FLAGEPDES       "-d"
#define FLAGSOCTAVE     "-f"
#define FLAGNOCTAVE     "-n"
#define FLAGTSERIEENU   "-e"
#define FLAGTSERIEUTM   "-u"
#define FLAGTSERIEGEOC  "-g"
#define FLAGTSERIEPLANO "-p"
#define FLAGTSERIETODO  "-t"
#define FLAGPIPE        "-p"
#define FLAGEX          "-ex"
#define LONPIPE         500

#define EXTFICHENU  "_enu"
#define EXTFICHUTM  "_utm"
#define EXTFICHGEOC "_geoc"

#define LONMAXNOMBREARG 100
#define LONMAXVALORARG  500
#define LONMAXNOMFICH   500

#define LONFORMATO    500
#define LONCOD        100
#define FORMCOD       "%4s"
#define FORMCODPTOS   "%8s"
#define FORMCODTRAN   "%8s"
#define FORMSISTREF   "%8s"
#define FORMEPOCA     "%13.8lf"
#define FORMEPOCATRAN "%13.8lf"
#define FORMEPOCAPTOS "%13.8lf"
#define FORMCOORSNX   "%22.15E"
#define FORMVELSNX    "%22.15E"
#define FORMVCCOORSNX "%21.14E"
#define FORMVCVELSNX  "%21.14E"
#define FORMNOBS      "%22d"
#define FORMNUNK      "%22d"
#define FORMSINT      "%22.15lf"
#define FORMSSR       "%22.15lf"
#define FORMPMS       "%22.15lf"
#define FORMCMS       "%22.15lf"
#define FORMNFREE     "%22d"
#define FORMVARFACT   "%22.15lf"
#define FORMWSS       "%22.15lf"
#define FORMNPARAM    "%30s"
#define FORMLAT       "%14.9lf"
#define FORMLON       "%14.9lf"
#define FORMH         "%10.4lf"
#define FORMXGEOC     "%13.4lf"
#define FORMYGEOC     "%13.4lf"
#define FORMZGEOC     "%13.4lf"
#define FORMDXGEOC    "%7.4lf"
#define FORMDYGEOC    "%7.4lf"
#define FORMDZGEOC    "%7.4lf"
#define FORMXUTM      "%13.4lf"
#define FORMYUTM      "%13.4lf"
#define FORMHUSOUTM   "%2d"
#define FORMHEMISF    "%1d"
#define FORME         "%7.4lf"
#define FORMN         "%7.4lf"
#define FORMU         "%7.4lf"
#define FORMDE        "%7.4lf"
#define FORMDN        "%7.4lf"
#define FORMDU        "%7.4lf"
#define FORMVC        "%12.5E"
#define FORMSIGMA     "%10.8lf"
#define FORMCOEFCORR  "%11.8lf"
#define FORMSEMIMAYOR "%7.4lf"
#define FORMSEMIMENOR "%7.4lf"
#define FORMACIELIP   "%14.9lf"
#define FORMTX        "%5.2lf"
#define FORMTY        "%5.2lf"
#define FORMTZ        "%5.2lf"
#define FORMD         "%9.2E"
#define FORMRX        "%5.2lf"
#define FORMRY        "%5.2lf"
#define FORMRZ        "%5.2lf"
#define FORMVTX       "%5.2lf"
#define FORMVTY       "%5.2lf"
#define FORMVTZ       "%5.2lf"
#define FORMVD        "%9.2E"
#define FORMVRX       "%5.2lf"
#define FORMVRY       "%5.2lf"
#define FORMVRZ       "%5.2lf"
#define SEPARADOR     " "
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
