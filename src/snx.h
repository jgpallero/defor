/**
\defgroup sinex Modulo SINEX
\brief En este modulo se reunen los ficheros necesarios para extraer informacion
       de ficheros SINEX.

Los ficheros descritos a continuacion son el material imprescindible para
extraer informacion de ficheros SINEX (Solution INdependent EXchange).
@{
\file snx.h
\brief Inclusion de archivos de cabecera para usar la biblioteca "snx".
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _SNX_H_
#define _SNX_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include"errores.h"
#include"general.h"
#include"paramsnx.h"
#include"bloqsnx.h"
#include"possnx.h"
#include"statsnx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
