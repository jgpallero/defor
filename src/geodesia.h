/**
\defgroup geodesia Modulo GEODESIA
\ingroup transf
\ingroup deformacion
\ingroup dibgmt
\brief En este modulo se reunen los ficheros necesarios para realizar calculos
       geodesicos.

Los ficheros descritos a continuacion son el material imprescindible para
realizar calculos geodesicos.
@{
\file geodesia.h
\brief Declaracion de funciones para calculos geodesicos.

En este fichero se declaran varias funciones para el tratamiento de problemas
geodesicos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 24 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _GEODESIA_H_
#define _GEODESIA_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<math.h>
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def PI
    \brief Constante PI. */
#define PI  3.14159265358979323846264338328
/** \def DR
    \brief Paso de grados sexagesimales en formato decimal a radianes. */
#define DR  (PI/180.0)
/** \def RD
    \brief Paso de radianes a grados sexagesimales en formato decimal. */
#define RD  (180.0/PI)
/** \def MSR
    \brief Paso de milisegundos sexagesimales a radianes. */
#define MSR (PI/(1000.0*3600.0*180.0))
/**
    \def CMM
    \brief Paso de centimetros a metros. */
#define CMM 0.01
/** \def DIFLAT
    \brief Diferencial en latitud para parar procesos iterativos, en
           radianes. */
/**
El valor adoptado es una cienmilesima de segundo sexagesimal, que en la
superficie terrestre equivale a una distancia de unos 0.3 milimetros.
*/
#define DIFLAT (5.0e-11)
/** \def DIFUTM
    \brief Diferencial en coordenadas UTM para calcular derivadas, en metros. */
#define DIFUTM (0.0003)
/** \def SEMI_MAYOR
    \brief Semieje mayor del elipsoide GRS80. */
#define SEMI_MAYOR 6378137.0
/** \def APL
    \brief Aplanamiento del elipsoide GRS80. */
#define APL        (1.0/298.257222101)
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula el radio de curvatura del primer vertical para una latitud dada.
\param[in] lat Latitud geodesica del punto de trabajo, en radianes.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\return Radio de curvatura del primer vertical para la latitud de trabajo.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note Esta funcion no comprueba que los datos de entrada esten en el dominio
      correcto.
*/
double RadioPrimVert(double lat,
                     double a,
                     double f);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la longitud de un arco de meridiano desde el ecuador hasta el
       punto de trabajo.
\param[in] lat Latitud geodesica del punto de trabajo, en radianes.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\return Longitud del arco de meridiano.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note Esta funcion no comprueba que los datos de entrada esten en el dominio
      correcto.
\note La longitud del arco de meridiano siempre se expresa como un número
      negativo si el punto esta en el hemisferio sur.
*/
double LonArcMer(double lat,
                 double a,
                 double f);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la latitud correspondiente a la longitud de un arco de meridiano.
\param[in] arcMer Longitud de un arco de meridiano.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\return Latitud correspondiente.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note Esta funcion no comprueba que los datos de entrada esten en el dominio
      correcto.
\note La longitud del arco de meridiano siempre se expresa como un número
      positivo, aunque el punto de trabajo esté en el hemisferio sur.
*/
double ArcMerLat(double arcMer,
                 double a,
                 double f);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de coordenadas geodesicas a coordenadas cartesianas tridimensionales
       geocentricas.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[in] lat Latitud geodesica del punto de trabajo, en radianes.
\param[in] lon Longitud geodesica del punto de trabajo, en radianes.
\param[in] h Altitud elipsoidal del punto de trabajo.
\param[out] x Coordenada X cartesiana geocentrica del punto de trabajo.
\param[out] y Coordenada Y cartesiana geocentrica del punto de trabajo.
\param[out] z Coordenada Z cartesiana geocentrica del punto de trabajo.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba que los datos de entrada esten en el dominio
      correcto.
*/
void GeoTri(double a,
            double f,
            double lat,
            double lon,
            double h,
            double* x,
            double* y,
            double* z);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de coordenadas cartesianas tridimensionales geocentricas a
       coordenadas geodesicas.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[in] x Coordenada X cartesiana geocentrica del punto de trabajo.
\param[in] y Coordenada Y cartesiana geocentrica del punto de trabajo.
\param[in] z Coordenada Z cartesiana geocentrica del punto de trabajo.
\param[out] lat Latitud geodesica del punto de trabajo, en radianes.
\param[out] lon Longitud geodesica del punto de trabajo, en radianes.
\param[out] h Altitud elipsoidal del punto de trabajo.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void TriGeo(double a,
            double f,
            double x,
            double y,
            double z,
            double* lat,
            double* lon,
            double* h);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de coordenadas geodesicas a coordenadas cartesianas tridimensionales
       geocentricas.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[in] lat Latitud geodesica del punto de trabajo, en radianes.
\param[in] lon Longitud geodesica del punto de trabajo, en radianes.
\param[in] h Altitud elipsoidal del punto de trabajo.
\param[in] varlat Varianza de la latitud geodesica.
\param[in] varlatlon Covarianza entre la latitud y la longitud geodesicas.
\param[in] varlath Covarianza entre la latitud geodesica y la altitud.
\param[in] varlon Varianza de la longitud geodesica.
\param[in] varlonh Covarianza entre la longitud geodesica y la altitud.
\param[in] varh Varianza de la altitud elipsoidal.
\param[out] x Coordenada X cartesiana geocentrica del punto de trabajo.
\param[out] y Coordenada Y cartesiana geocentrica del punto de trabajo.
\param[out] z Coordenada Z cartesiana geocentrica del punto de trabajo.
\param[out] varx Varianza de la coordenada X geocentrica.
\param[out] varxy Covarianza entre las coordenadas X e Y geocentricas.
\param[out] varxz Covarianza entre las coordenadas X y Z geocentricas.
\param[out] vary Varianza de la coordenada Y geocentrica.
\param[out] varyz Covarianza entre las coordenadas Y y Z geocentricas.
\param[out] varz Varianza de la coordenada Z geocentrica.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void GeoTriVC(double a,
              double f,
              double lat,
              double lon,
              double h,
              double varlat,
              double varlatlon,
              double varlath,
              double varlon,
              double varlonh,
              double varh,
              double* x,
              double* y,
              double* z,
              double* varx,
              double* varxy,
              double* varxz,
              double* vary,
              double* varyz,
              double* varz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de coordenadas geodesicas a coordenadas cartesianas tridimensionales
       geocentricas.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[in] lat Latitud geodesica del punto de trabajo, en radianes.
\param[in] lon Longitud geodesica del punto de trabajo, en radianes.
\param[in] h Altitud elipsoidal del punto de trabajo.
\param[in] varlat Varianza de la latitud geodesica.
\param[in] varlatlon Covarianza entre la latitud y la longitud geodesicas.
\param[in] varlath Covarianza entre la latitud geodesica y la altitud.
\param[in] varlon Varianza de la longitud geodesica.
\param[in] varlonh Covarianza entre la longitud geodesica y la altitud.
\param[in] varh Varianza de la altitud elipsoidal.
\param[out] x Coordenada X cartesiana geocentrica del punto de trabajo.
\param[out] y Coordenada Y cartesiana geocentrica del punto de trabajo.
\param[out] z Coordenada Z cartesiana geocentrica del punto de trabajo.
\param[out] varx Varianza de la coordenada X geocentrica.
\param[out] varxy Covarianza entre las coordenadas X e Y geocentricas.
\param[out] varxz Covarianza entre las coordenadas X y Z geocentricas.
\param[out] vary Varianza de la coordenada Y geocentrica.
\param[out] varyz Covarianza entre las coordenadas Y y Z geocentricas.
\param[out] varz Varianza de la coordenada Z geocentrica.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void TriGeoVC(double a,
              double f,
              double x,
              double y,
              double z,
              double varx,
              double varxy,
              double varxz,
              double vary,
              double varyz,
              double varz,
              double* lat,
              double* lon,
              double* h,
              double* varlat,
              double* varlatlon,
              double* varlath,
              double* varlon,
              double* varlonh,
              double* varh);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de un punto expresado en coordenadas cartesianas topocentricas
       (east, north, up) a un vector expresado en incrementos de coordenadas
       cartesianas tridimensionales geocentricas con origen en el origen del
       sistema topocentrico.
\param[in] lat Latitud geodesica del origen del sistema topocentrico, en
               radianes.
\param[in] lon Longitud geodesica del origen del sistema topocentrico, en
               radianes.
\param[in] e Coordenada east topocentrica del punto.
\param[in] n Coordenada north topocentrica del punto.
\param[in] u Coordenada up topocentrica del punto.
\param[out] x Incremento en la coordenada X geocentrica del vector solucion.
\param[out] y Incremento en la coordenada Y geocentrica del vector solucion.
\param[out] z Incremento en la coordenada Z geocentrica del vector solucion.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void EnuTri(double lat,
            double lon,
            double e,
            double n,
            double u,
            double* x,
            double* y,
            double* z);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de un vector expresado como incrementos de coordenadas cartesianas
       tridimensionales geocentricas a un punto expresado en coordenadas
       cartesianas topocentricas (east, north, up) con origen en el punto
       inicial del vector.
\param[in] lat Latitud geodesica del punto origen del vector, en radianes.
\param[in] lon Longitud geodesica del punto origen del vector, en radianes.
\param[in] x Incremento en la coordenada X geocentrica del vector.
\param[in] y Incremento en la coordenada Y geocentrica del vector.
\param[in] z Incremento en la coordenada Z geocentrica del vector.
\param[out] e Coordenada east en el sistema topocentrico.
\param[out] n Coordenada north en el sistema topocentrico.
\param[out] u Coordenada up en el sistema topocentrico.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void TriEnu(double lat,
            double lon,
            double x,
            double y,
            double z,
            double* e,
            double* n,
            double* u);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de un punto expresado en coordenadas cartesianas topocentricas
       (east, north, up) a un vector expresado en incrementos de coordenadas
       cartesianas tridimensionales geocentricas con origen en el origen del
       sistema topocentrico y realiza la propagacion de errores correspondiente.
\param[in] lat Latitud geodesica del origen del sistema topocentrico, en
               radianes.
\param[in] lon Longitud geodesica del origen del sistema topocentrico, en
               radianes.
\param[in] e Coordenada east topocentrica del punto.
\param[in] n Coordenada north topocentrica del punto.
\param[in] u Coordenada up topocentrica del punto.
\param[in] vare Varianza de la coordenada east.
\param[in] varen Covarianza entre las coordenadas east y north.
\param[in] vareu Covarianza entre las coordenadas east y up.
\param[in] varn Varianza de la coordenada north.
\param[in] varnu Covarianza entre las coordenadas north y up.
\param[in] varu Varianza de la coordenada up.
\param[out] x Incremento en la coordenada X geocentrica del vector solucion.
\param[out] y Incremento en la coordenada Y geocentrica del vector solucion.
\param[out] z Incremento en la coordenada Z geocentrica del vector solucion.
\param[out] varx Varianza del incremento de coordenada X geocentrica.
\param[out] varxy Covarianza entre los incrementos de coordenadas X e Y
                  geocentricas.
\param[out] varxz Covarianza entre los incrementos de coordenadas X y Z
                  geocentricas.
\param[out] vary Varianza del incremento de coordenada Y geocentrica.
\param[out] varyz Covarianza entre los incrementos de coordenadas Y y Z
                  geocentricas.
\param[out] varz Varianza del incremento de coordenada Z geocentrica.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void EnuTriVC(double lat,
              double lon,
              double e,
              double n,
              double u,
              double vare,
              double varen,
              double vareu,
              double varn,
              double varnu,
              double varu,
              double* x,
              double* y,
              double* z,
              double* varx,
              double* varxy,
              double* varxz,
              double* vary,
              double* varyz,
              double* varz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de un vector expresado como incrementos de coordenadas cartesianas
       tridimensionales geocentricas a un punto expresado en coordenadas
       cartesianas topocentricas (east, north, up) con origen en el punto
       inicial del vector  y realiza la propagacion de errores correspondiente.
\param[in] lat Latitud geodesica del punto origen del vector, en radianes.
\param[in] lon Longitud geodesica del punto origen del vector, en radianes.
\param[in] x Incremento en la coordenada X geocentrica del vector.
\param[in] y Incremento en la coordenada Y geocentrica del vector.
\param[in] z Incremento en la coordenada Z geocentrica del vector.
\param[in] varx Varianza del incremento de coordenada X geocentrica.
\param[in] varxy Covarianza entre los incrementos de coordenadas X e Y
                 geocentricas.
\param[in] varxz Covarianza entre los incrementos de coordenadas X y Z
                 geocentricas.
\param[in] vary Varianza del incremento de coordenada Y geocentrica.
\param[in] varyz Covarianza entre los incrementos de coordenadas Y y Z
                 geocentricas.
\param[in] varz Varianza del incremento de coordenada Z geocentrica.
\param[out] e Coordenada east en el sistema topocentrico.
\param[out] n Coordenada north en el sistema topocentrico.
\param[out] u Coordenada up en el sistema topocentrico.
\param[out] vare Varianza de la coordenada east.
\param[out] varen Covarianza entre las coordenadas east y north.
\param[out] vareu Covarianza entre las coordenadas east y up.
\param[out] varn Varianza de la coordenada north.
\param[out] varnu Covarianza entre las coordenadas north y up.
\param[out] varu Varianza de la coordenada up.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI[.
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void TriEnuVC(double lat,
              double lon,
              double x,
              double y,
              double z,
              double varx,
              double varxy,
              double varxz,
              double vary,
              double varyz,
              double varz,
              double* e,
              double* n,
              double* u,
              double* vare,
              double* varen,
              double* vareu,
              double* varn,
              double* varnu,
              double* varu);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula el huso UTM al que pertenece un punto a partir de su longitud
       geodesica.
\param[in] lon Longitud geodesica del punto de trabajo, en radianes.
\note El dominio de la longitud geodesica es ]-#PI #PI].
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
int HusoUtm(double lon);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la longitud geodesica del meridiano central de un huso UTM dado.
\param[in] huso Numero de huso UTM.
\note El dominio de la longitud geodesica es ]-#PI #PI].
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
double MeridianoCentralUtm(int huso);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Paso de coordenadas geodesicas a coordenadas UTM.
\param[in] lat Latitud geodesica, en radianes.
\param[in] lon Longitud geodesica, en radianes.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[out] x Coordenada X UTM.
\param[out] y Coordenada Y UTM.
\param[out] huso Numero de huso UTM.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI].
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
*/
void GeoUtm(double lat,
            double lon,
            double a,
            double f,
            double* x,
            double* y,
            int* huso);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Paso de coordenadas UTM a geodesicas.
\param[in] x Coordenada X UTM.
\param[in] y Coordenada Y UTM.
\param[in] huso Numero de huso UTM.
\param[in] hemisferio Indicador del hemisferio donde se encuentra el punto de
           trabajo. Dos posibilidades:
           - 0: Hemisferio sur.
           - Distinto de 0: Hemisferio norte.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[out] lat Latitud geodesica, en radianes.
\param[out] lon Longitud geodesica, en radianes
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI].
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
\date 15 de diciembre de 2010: Corregido error que hacia que los puntos en el
      hemisferio sur se calculasen en el norte.
\date 30 de junio de 2011: Corregido error que hacia que se calculase mal la
      longitud.
*/
void UtmGeo(double x,
            double y,
            int huso,
            int hemisferio,
            double a,
            double f,
            double* lat,
            double* lon);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de coordenadas geodesicas a coordenadas UTM y realiza la propagacion
       de errores correspondiente.
\param[in] lat Latitud geodesica, en radianes.
\param[in] lon Longitud geodesica, en radianes.
\param[in] varLat Varianza de la latitud geodesica.
\param[in] varLatLon Covarianza entre la latitud y la longitud geodesicas.
\param[in] varLon Varianza de la longitud geodesica.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[out] x Coordenada X UTM.
\param[out] y Coordenada Y UTM.
\param[out] varx Varianza de la coordenada X UTM.
\param[out] varxy Covarianza entre las coordenadas X e Y UTM.
\param[out] vary Varianza de la coordenada Y UTM.
\param[out] huso Numero de huso UTM.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI].
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
\note Las derivadas necesarias para la propagacion de errores se realizan de
      forma numerica, por lo que puede haber errores en caso de trabajar con
      puntos extremos: limite de husos, ecuador, etc.
*/
void GeoUtmVC(double lat,
              double lon,
              double varLat,
              double varLatLon,
              double varLon,
              double a,
              double f,
              double* x,
              double* y,
              double* varx,
              double* varxy,
              double* vary,
              int* huso);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Pasa de coordenadas UTM a coordenadas geodesicas y realiza la propagacion
       de errores correspondiente.
\param[in] x Coordenada X UTM.
\param[in] y Coordenada Y UTM.
\param[in] varx Varianza de la coordenada X UTM.
\param[in] varxy Covarianza entre las coordenadas X e Y UTM.
\param[in] vary Varianza de la coordenada Y UTM.
\param[in] huso Numero de huso UTM.
\param[in] hemisferio Indicador del hemisferio donde se encuentra el punto de
           trabajo. Dos posibilidades:
           - 0: Hemisferio sur.
           - Distinto de 0: Hemisferio norte.
\param[in] a Semieje mayor del elipsoide de trabajo.
\param[in] f Aplanamiento del elipsoide de trabajo.
\param[in] lat Latitud geodesica, en radianes.
\param[in] lon Longitud geodesica, en radianes.
\param[in] varLat Varianza de la latitud geodesica.
\param[in] varLatLon Covarianza entre la latitud y la longitud geodesicas.
\param[in] varLon Varianza de la longitud geodesica.
\note El dominio de la latitud geodesica es ]-#PI/2 #PI/2[.
\note El dominio de la longitud geodesica es ]-#PI #PI].
\note Esta funcion no comprueba si los datos de entrada estan en el dominio
      correcto.
\note Las derivadas necesarias para la propagacion de errores se realizan de
      forma numerica, por lo que puede haber errores en caso de trabajar con
      puntos extremos: limite de husos, ecuador, etc.
*/
void UtmGeoVC(double x,
              double y,
              double varx,
              double varxy,
              double vary,
              int huso,
              int hemisferio,
              double a,
              double f,
              double* lat,
              double* lon,
              double* varLat,
              double* varLatLon,
              double* varLon);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula el desplazamiento entre dos puntos en dos epocas distintas.
\param[in] x1 Coordenada X del punto en la epoca inicial.
\param[in] y1 Coordenada Y del punto en la epoca inicial.
\param[in] z1 Coordenada Z del punto en la epoca inicial.
\param[in] x2 Coordenada X del punto en la epoca final.
\param[in] y2 Coordenada Y del punto en la epoca final.
\param[in] z2 Coordenada Z del punto en la epoca final.
\param[out] dx Desplazamiento en la coordenada X: X2-X1.
\param[out] dy Desplazamiento en la coordenada Y: Y2-Y1.
\param[out] dz Desplazamiento en la coordenada Z: Z2-Z1.
*/
void RestaEpocas(double x1,
                 double y1,
                 double z1,
                 double x2,
                 double y2,
                 double z2,
                 double* dx,
                 double* dy,
                 double* dz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula el desplazamiento entre dos puntos en dos epocas distintas y
       realiza la propagacion de errores correspondiente.
\param[in] x1 Coordenada X del punto en la epoca inicial.
\param[in] y1 Coordenada Y del punto en la epoca inicial.
\param[in] z1 Coordenada Z del punto en la epoca inicial.
\param[in] varx1 Varianza de la coordenada X del punto en la epoca inicial.
\param[in] varx1y1 Covarianza entre las coordenadas X e Y en la epoca inicial.
\param[in] varx1z1 Covarianza entre las coordenadas X y Z en la epoca inicial.
\param[in] vary1 Varianza de la coordenada Y del punto en la epoca inicial.
\param[in] vary1z1 Covarianza entre las coordenadas Y y Z en la epoca inicial.
\param[in] varz1 Varianza de la coordenada Z del punto en la epoca inicial.
\param[in] x2 Coordenada X del punto en la epoca final.
\param[in] y2 Coordenada Y del punto en la epoca final.
\param[in] z2 Coordenada Z del punto en la epoca final.
\param[in] varx2 Varianza de la coordenada X del punto en la epoca final.
\param[in] varx2y2 Covarianza entre las coordenadas X e Y en la epoca final.
\param[in] varx2z2 Covarianza entre las coordenadas X y Z en la epoca final.
\param[in] vary2 Varianza de la coordenada Y del punto en la epoca final.
\param[in] vary2z2 Covarianza entre las coordenadas Y y Z en la epoca final.
\param[in] varz2 Varianza de la coordenada Z del punto en la epoca final.
\param[out] dx Desplazamiento en la coordenada X: X2-X1.
\param[out] dy Desplazamiento en la coordenada Y: Y2-Y1.
\param[out] dz Desplazamiento en la coordenada Z: Z2-Z1.
\param[out] vardx Varianza del desplazamiento en la coordenada X.
\param[out] vardxdy Covarianza entre los desplazamientos X e Y.
\param[out] vardxdz Covarianza entre los desplazamientos X y Z.
\param[out] vardy Varianza del desplazamiento en la coordenada Y.
\param[out] vardydz Covarianza entre los desplazamientos Y y Z.
\param[out] vardz Varianza del desplazamiento en la coordenada Z.
*/
void RestaEpocasVC(double x1,
                   double y1,
                   double z1,
                   double varx1,
                   double varx1y1,
                   double varx1z1,
                   double vary1,
                   double vary1z1,
                   double varz1,
                   double x2,
                   double y2,
                   double z2,
                   double varx2,
                   double varx2y2,
                   double varx2z2,
                   double vary2,
                   double vary2z2,
                   double varz2,
                   double* dx,
                   double* dy,
                   double* dz,
                   double* vardx,
                   double* vardxdy,
                   double* vardxdz,
                   double* vardy,
                   double* vardydz,
                   double* vardz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula una elipse de error estandar en el plano.
\param[in] varx Varianza de la coordenada X.
\param[in] varxy Covarianza entre las coordenadas X e Y.
\param[in] vary Varianza de la coordenada Y.
\param[out] a Semieje mayor de la elipse.
\param[out] b Semieje menor de la elipse.
\param[out] angulo Angulo en sentido antihorario (en el dominio ]-pi pi], medido
                   a partir de la parte positiva del eje X) del semieje mayor de
                   la elipse, en radianes.
*/
void ElipseErrorPlano(double varx,
                      double varxy,
                      double vary,
                      double *a,
                      double *b,
                      double *angulo);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Reduce un valor del dominio angular matematico, ]-#PI #PI], con sentido
       antihorario y origen en la parte positiva del eje X, al dominio angular
       del acimut, [0 2#PI[.
\param[in] angulo Angulo a transformar.
\return Valor angular convertido al dominio del acimut.
\note Esta funcion no comprueba si el angulo de entrada esta en el dominio
      correcto.
*/
double AnguloMateAcimut(double angulo);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la transformacion entre dos sistemas de referencia segun un
       modelo de Helmert en 3 dimensiones.
\param[in] t0 Epoca base del sistema destino de la transformacion.
\param[in] tx Traslacion a lo largo del eje X.
\param[in] ty Traslacion a lo largo del eje Y.
\param[in] tz Traslacion a lo largo del eje Z.
\param[in] d Factor de escala de la transformacion, adimensional.
\param[in] rx Rotacion alrededor del eje X, en radianes.
\param[in] ry Rotacion alrededor del eje Y, en radianes.
\param[in] rz Rotacion alrededor del eje Z, en radianes.
\param[in] vtx Velocidad de cambio de la traslacion a lo largo del eje X, en
               unidades/anyo.
\param[in] vty Velocidad de cambio de la traslacion a lo largo del eje Y, en
               unidades/anyo.
\param[in] vtz Velocidad de cambio de la traslacion a lo largo del eje Z, en
               unidades/anyo.
\param[in] vd Velocidad de cambio del factor de escala, en adimensional/anyo.
\param[in] vrx Velocidad de cambio del giro alrededor del eje X, en
               radianes/anyo.
\param[in] vry Velocidad de cambio del giro alrededor del eje Y, en
               radianes/anyo.
\param[in] vrz Velocidad de cambio del giro alrededor del eje Z, en
               radianes/anyo.
\param[in] t1 Epoca del punto de trabajo.
\param[in] x1 Coordenada X del punto de trabajo.
\param[in] y1 Coordenada Y del punto de trabajo.
\param[in] z1 Coordenada Z del punto de trabajo.
\param[out] x2 Coordenada X del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] y2 Coordenada Y del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] z2 Coordenada Z del punto de trabajo en el sistema destino, en la
               epoca t1.
\note En esta funcion se entiende por factor de escala la cantidad que se separa
      de 1.0 un factor de escala convencional. Es decir, convencionalmente se
      entiende por factor de escala un escalar D tal que:\n
      b = D*a \n
      donde a y b son valores antes y despues de aplicarles el escalado.\n
      El factor de escala D se puede descomponer como:\n
      D = 1.0+d \n
      de modo que d seria el factor que informa de la variacion que sufre un
      valor al aplicarle un factor de escala. Por ejemplo, si D = 1.0, entonces
      d = 0.0 y al aplicar D a un valor, este no cambia.
*/
void TranSistRef(double t0,
                 double tx,
                 double ty,
                 double tz,
                 double d,
                 double rx,
                 double ry,
                 double rz,
                 double vtx,
                 double vty,
                 double vtz,
                 double vd,
                 double vrx,
                 double vry,
                 double vrz,
                 double t1,
                 double x1,
                 double y1,
                 double z1,
                 double* x2,
                 double* y2,
                 double* z2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la transformacion entre dos sistemas de referencia segun un
       modelo de Helmert en 3 dimensiones y realiza la propagacion de errores
       correspondiente.
\param[in] t0 Epoca base del sistema destino de la transformacion.
\param[in] tx Traslacion a lo largo del eje X.
\param[in] ty Traslacion a lo largo del eje Y.
\param[in] tz Traslacion a lo largo del eje Z.
\param[in] d Factor de escala de la transformacion, adimensional.
\param[in] rx Rotacion alrededor del eje X, en radianes.
\param[in] ry Rotacion alrededor del eje Y, en radianes.
\param[in] rz Rotacion alrededor del eje Z, en radianes.
\param[in] vtx Velocidad de cambio de la traslacion a lo largo del eje X, en
               unidades/anyo.
\param[in] vty Velocidad de cambio de la traslacion a lo largo del eje Y, en
               unidades/anyo.
\param[in] vtz Velocidad de cambio de la traslacion a lo largo del eje Z, en
               unidades/anyo.
\param[in] vd Velocidad de cambio del factor de escala, en adimensional/anyo.
\param[in] vrx Velocidad de cambio del giro alrededor del eje X, en
               radianes/anyo.
\param[in] vry Velocidad de cambio del giro alrededor del eje Y, en
               radianes/anyo.
\param[in] vrz Velocidad de cambio del giro alrededor del eje Z, en
               radianes/anyo.
\param[in] t1 Epoca del punto de trabajo.
\param[in] x1 Coordenada X del punto de trabajo.
\param[in] y1 Coordenada Y del punto de trabajo.
\param[in] z1 Coordenada Z del punto de trabajo.
\param[in] varx1 Varianza de la coordenada X del punto de trabajo.
\param[in] varx1y1 Covarianza entre las coordenadas X e Y del punto de trabajo.
\param[in] varx1z1 Covarianza entre las coordenadas X y Z del punto de trabajo.
\param[in] vary1 Varianza de la coordenada Y del punto de trabajo.
\param[in] vary1z1 Covarianza entre las coordenadas Y y Z del punto de trabajo.
\param[in] varz1 Varianza de la coordenada Z del punto de trabajo.
\param[out] x2 Coordenada X del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] y2 Coordenada Y del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] z2 Coordenada Z del punto de trabajo en el sistema destino, en la
               epoca t1.
\param[out] varx2 Varianza de la coordenada X en el sistema destino, en la epoca
                  t1.
\param[out] varx2y2 Covarianza entre las coordenadas X e Y en el sistema
                    destino, en la epoca t1.
\param[out] varx2z2 Covarianza entre las coordenadas X y Z en el sistema
                    destino, en la epoca t1.
\param[out] vary2 Varianza de la coordenada Y en el sistema destino, en la epoca
                  t1.
\param[out] vary2z2 Covarianza entre las coordenadas Y y Z en el sistema
                    destino, en la epoca t1.
\param[out] varz2 Varianza de la coordenada Z en el sistema destino, en la epoca
                  t1.
\note En esta funcion se entiende por factor de escala la cantidad que se separa
      de 1.0 un factor de escala convencional. Es decir, convencionalmente se
      entiende por factor de escala un escalar D tal que:\n
      b = D*a \n
      donde a y b son valores antes y despues de aplicarles el escalado.\n
      El factor de escala D se puede descomponer como:\n
      D = 1.0+d \n
      de modo que d seria el factor que informa de la variacion que sufre un
      valor al aplicarle un factor de escala. Por ejemplo, si D = 1.0, entonces
      d = 0.0 y al aplicar D a un valor, este no cambia.
*/
void TranSistRefVC(double t0,
                   double tx,
                   double ty,
                   double tz,
                   double d,
                   double rx,
                   double ry,
                   double rz,
                   double vtx,
                   double vty,
                   double vtz,
                   double vd,
                   double vrx,
                   double vry,
                   double vrz,
                   double t1,
                   double x1,
                   double y1,
                   double z1,
                   double varx1,
                   double varx1y1,
                   double varx1z1,
                   double vary1,
                   double vary1z1,
                   double varz1,
                   double* x2,
                   double* y2,
                   double* z2,
                   double* varx2,
                   double* varx2y2,
                   double* varx2z2,
                   double* vary2,
                   double* vary2z2,
                   double* varz2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula las coordenadas de un punto en una epoca distinta a partir de
       unas velocidades de desplazamiento.
\param[in] x1 Coordenada X del punto en la epoca inicial.
\param[in] y1 Coordenada Y del punto en la epoca inicial.
\param[in] z1 Coordenada Z del punto en la epoca inicial.
\param[in] vx Velocidad de desplazamiento de la coordenada X del punto.
\param[in] vy Velocidad de desplazamiento de la coordenada Y del punto.
\param[in] vz Velocidad de desplazamiento de la coordenada Z del punto.
\param[in] fecha1 Epoca inicial.
\param[in] fecha2 Epoca final.
\param[out] x2 Coordenada X del punto en la epoca final.
\param[out] y2 Coordenada Y del punto en la epoca final.
\param[out] z2 Coordenada Z del punto en la epoca final.
*/
void AplicaVelocidad(double x1,
                     double y1,
                     double z1,
                     double vx,
                     double vy,
                     double vz,
                     double fecha1,
                     double fecha2,
                     double* x2,
                     double* y2,
                     double* z2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula las coordenadas de un punto en una epoca distinta a partir de
       unas velocidades de desplazamiento y realiza la propagacion de errores
       correspondiente.
\param[in] x1 Coordenada X del punto en la epoca inicial.
\param[in] y1 Coordenada Y del punto en la epoca inicial.
\param[in] z1 Coordenada Z del punto en la epoca inicial.
\param[in] varx1 Varianza de la coordenada X del punto en la epoca inicial.
\param[in] varx1y1 Covarianza entre las coordenadas X e Y en la epoca inicial.
\param[in] varx1z1 Covarianza entre las coordenadas X y Z en la epoca inicial.
\param[in] vary1 Varianza de la coordenada Y del punto en la epoca inicial.
\param[in] vary1z1 Covarianza entre las coordenadas Y y Z en la epoca inicial.
\param[in] varz1 Varianza de la coordenada Z del punto en la epoca inicial.
\param[in] vx Velocidad de desplazamiento de la coordenada X del punto.
\param[in] vy Velocidad de desplazamiento de la coordenada Y del punto.
\param[in] vz Velocidad de desplazamiento de la coordenada Z del punto.
\param[in] varvx Varianza de la velocidad de la coordenada X del punto.
\param[in] varvxvy Covarianza entre las velocidades de las coordenadas X e Y.
\param[in] varvxvz Covarianza entre las velocidades de las coordenadas X y Z.
\param[in] varvy Varianza de la velocidad de la coordenada Y del punto.
\param[in] varvyvz Covarianza entre las velocidades de la coordenadas Y y Z.
\param[in] varvz Varianza de la velocidad de la coordenada Z del punto.
\param[in] fecha1 Epoca inicial.
\param[in] fecha2 Epoca final.
\param[out] x2 Coordenada X del punto en la epoca final.
\param[out] y2 Coordenada Y del punto en la epoca final.
\param[out] z2 Coordenada Z del punto en la epoca final.
\param[out] varx2 Varianza de la coordenada X del punto en la epoca final.
\param[out] varx2y2 Covarianza entre las coordenadas X e Y en la epoca final.
\param[out] varx2z2 Covarianza entre las coordenadas X y Z en la epoca final.
\param[out] vary2 Varianza de la coordenada Y del punto en la epoca final.
\param[out] vary2z2 Covarianza entre las coordenadas Y y Z en la epoca final.
\param[out] varz2 Varianza de la coordenada Z del punto en la epoca final.
*/
void AplicaVelocidadVC(double x1,
                       double y1,
                       double z1,
                       double varx1,
                       double varx1y1,
                       double varx1z1,
                       double vary1,
                       double vary1z1,
                       double varz1,
                       double vx,
                       double vy,
                       double vz,
                       double varvx,
                       double varvxvy,
                       double varvxvz,
                       double varvy,
                       double varvyvz,
                       double varvz,
                       double fecha1,
                       double fecha2,
                       double* x2,
                       double* y2,
                       double* z2,
                       double* varx2,
                       double* varx2y2,
                       double* varx2z2,
                       double* vary2,
                       double* vary2z2,
                       double* varz2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
