/* -*- coding: utf-8 -*- */
/**
\ingroup coblas gcblas
@{
\file mnfblas.h
\brief Definición de macros para construir el nombre de las funciones de BLAS en
       Fortran.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _MNFBLAS_H_
#define _MNFBLAS_H_
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def ADD_
\brief Identificador para la creación del nombre de las funciones en Fortran.
       Tres posibilidades:
       - \p ADD_: El nombre se pasa a minúscula y se añade un \p _ final.
       - \p UPCASE: El nombre se pasa mayúsculas.
       - \p NOCHANGE: El nombre se queda como está.
\note Dejar descomentada la opción que interese.
\date 16 de enero de 2014: Creación de la constante.
*/
#define ADD_
// #define UPCASE
// #define NOCHANGE
/******************************************************************************/
/******************************************************************************/
//seleccionamos el tipo de nombre a utilizar
#if defined(ADD_)
    //rutina para manejo de errores
    #define GBL_F77_xerbla  xerbla_
    //Nivel 1 para tipo de dato double
    #define GBL_F77_daxpy   daxpy_
    #define GBL_F77_idamax  idamax_
    #define GBL_F77_dasum   dasum_
    #define GBL_F77_dcopy   dcopy_
    #define GBL_F77_dnrm2   dnrm2_
    #define GBL_F77_drot    drot_
    #define GBL_F77_drotg   drotg_
    #define GBL_F77_dscal   dscal_
    #define GBL_F77_dswap   dswap_
    #define GBL_F77_ddot    ddot_
    #define GBL_F77_drotm   drotm_
    #define GBL_F77_drotmg  drotmg_
    //Nivel 1 para tipo de dato float
    #define GBL_F77_saxpy   saxpy_
    #define GBL_F77_isamax  isamax_
    #define GBL_F77_sasum   sasum_
    #define GBL_F77_scopy   scopy_
    #define GBL_F77_snrm2   snrm2_
    #define GBL_F77_srot    srot_
    #define GBL_F77_srotg   srotg_
    #define GBL_F77_sscal   sscal_
    #define GBL_F77_sswap   sswap_
    #define GBL_F77_sdot    sdot_
    #define GBL_F77_srotm   srotm_
    #define GBL_F77_srotmg  srotmg_
    //Nivel 2 para tipo de dato double
    #define GBL_F77_dgemv   dgemv_
    #define GBL_F77_dgbmv   dgbmv_
    #define GBL_F77_dsymv   dsymv_
    #define GBL_F77_dsbmv   dsbmv_
    #define GBL_F77_dspmv   dspmv_
    #define GBL_F77_dtrmv   dtrmv_
    #define GBL_F77_dtbmv   dtbmv_
    #define GBL_F77_dtpmv   dtpmv_
    #define GBL_F77_dtrsv   dtrsv_
    #define GBL_F77_dtbsv   dtbsv_
    #define GBL_F77_dtpsv   dtpsv_
    #define GBL_F77_dger    dger_
    #define GBL_F77_dsyr    dsyr_
    #define GBL_F77_dspr    dspr_
    #define GBL_F77_dsyr2   dsyr2_
    #define GBL_F77_dspr2   dspr2_
    //Nivel 2 para tipo de dato float
    #define GBL_F77_sgemv   sgemv_
    #define GBL_F77_sgbmv   sgbmv_
    #define GBL_F77_ssymv   ssymv_
    #define GBL_F77_ssbmv   ssbmv_
    #define GBL_F77_sspmv   sspmv_
    #define GBL_F77_strmv   strmv_
    #define GBL_F77_stbmv   stbmv_
    #define GBL_F77_stpmv   stpmv_
    #define GBL_F77_strsv   strsv_
    #define GBL_F77_stbsv   stbsv_
    #define GBL_F77_stpsv   stpsv_
    #define GBL_F77_sger    sger_
    #define GBL_F77_ssyr    ssyr_
    #define GBL_F77_sspr    sspr_
    #define GBL_F77_ssyr2   ssyr2_
    #define GBL_F77_sspr2   sspr2_
    //Nivel 3 para tipo de dato double
    #define GBL_F77_dgemm   dgemm_
    #define GBL_F77_dsymm   dsymm_
    #define GBL_F77_dsyrk   dsyrk_
    #define GBL_F77_dsyr2k  dsyr2k_
    #define GBL_F77_dtrmm   dtrmm_
    #define GBL_F77_dtrsm   dtrsm_
    //Nivel 3 para tipo de dato float
    #define GBL_F77_sgemm   sgemm_
    #define GBL_F77_ssymm   ssymm_
    #define GBL_F77_ssyrk   ssyrk_
    #define GBL_F77_ssyr2k  ssyr2k_
    #define GBL_F77_strmm   strmm_
    #define GBL_F77_strsm   strsm_
#elif defined(UPCASE)
    //rutina para manejo de errores
    #define GBL_F77_xerbla  XERBLA
    //Nivel 1 para tipo de dato double
    #define GBL_F77_daxpy   DAXPY
    #define GBL_F77_idamax  IDAMAX
    #define GBL_F77_dasum   DASUM
    #define GBL_F77_dcopy   DCOPY
    #define GBL_F77_dnrm2   DNRM2
    #define GBL_F77_drot    DROT
    #define GBL_F77_drotg   DROTG
    #define GBL_F77_dscal   DSCAL
    #define GBL_F77_dswap   DSWAP
    #define GBL_F77_ddot    DDOT
    #define GBL_F77_drotm   DROTM
    #define GBL_F77_drotmg  DROTMG
    //Nivel 1 para tipo de dato float
    #define GBL_F77_saxpy   SAXPY
    #define GBL_F77_isamax  ISAMAX
    #define GBL_F77_sasum   SASUM
    #define GBL_F77_scopy   SCOPY
    #define GBL_F77_snrm2   SNRM2
    #define GBL_F77_srot    SROT
    #define GBL_F77_srotg   SROTG
    #define GBL_F77_sscal   SSCAL
    #define GBL_F77_sswap   SSWAP
    #define GBL_F77_sdot    SDOT
    #define GBL_F77_srotm   SROTM
    #define GBL_F77_srotmg  SROTMG
    //Nivel 2 para tipo de dato double
    #define GBL_F77_dgemv   DGEMV
    #define GBL_F77_dgbmv   DGBMV
    #define GBL_F77_dsymv   DSYMV
    #define GBL_F77_dsbmv   DSBMV
    #define GBL_F77_dspmv   DSPMV
    #define GBL_F77_dtrmv   DTRMV
    #define GBL_F77_dtbmv   DTBMV
    #define GBL_F77_dtpmv   DTPMV
    #define GBL_F77_dtrsv   DTRSV
    #define GBL_F77_dtbsv   DTBSV
    #define GBL_F77_dtpsv   DTPSV
    #define GBL_F77_dger    DGER
    #define GBL_F77_dsyr    DSYR
    #define GBL_F77_dspr    DSPR
    #define GBL_F77_dsyr2   DSYR2
    #define GBL_F77_dspr2   DSPR2
    //Nivel 2 para tipo de dato float
    #define GBL_F77_sgemv   SGEMV
    #define GBL_F77_sgbmv   SGBMV
    #define GBL_F77_ssymv   SSYMV
    #define GBL_F77_ssbmv   SSBMV
    #define GBL_F77_sspmv   SSPMV
    #define GBL_F77_strmv   STRMV
    #define GBL_F77_stbmv   STBMV
    #define GBL_F77_stpmv   STPMV
    #define GBL_F77_strsv   STRSV
    #define GBL_F77_stbsv   STBSV
    #define GBL_F77_stpsv   STPSV
    #define GBL_F77_sger    SGER
    #define GBL_F77_ssyr    SSYR
    #define GBL_F77_sspr    SSPR
    #define GBL_F77_ssyr2   SSYR2
    #define GBL_F77_sspr2   SSPR2
    //Nivel 3 para tipo de dato double
    #define GBL_F77_dgemm   DGEMM
    #define GBL_F77_dsymm   DSYMM
    #define GBL_F77_dsyrk   DSYRK
    #define GBL_F77_dsyr2k  DSYR2K
    #define GBL_F77_dtrmm   DTRMM
    #define GBL_F77_dtrsm   DTRSM
    //Nivel 3 para tipo de dato float
    #define GBL_F77_sgemm   SGEMM
    #define GBL_F77_ssymm   SSYMM
    #define GBL_F77_ssyrk   SSYRK
    #define GBL_F77_ssyr2k  SSYR2K
    #define GBL_F77_strmm   STRMM
    #define GBL_F77_strsm   STRSM
#elif defined(NOCHANGE)
    //rutina para manejo de errores
    #define GBL_F77_xerbla  xerbla
    //Nivel 1 para tipo de dato double
    #define GBL_F77_daxpy   daxpy
    #define GBL_F77_idamax  idamax
    #define GBL_F77_dasum   dasum
    #define GBL_F77_dcopy   dcopy
    #define GBL_F77_dnrm2   dnrm2
    #define GBL_F77_drot    drot
    #define GBL_F77_drotg   drotg
    #define GBL_F77_dscal   dscal
    #define GBL_F77_dswap   dswap
    #define GBL_F77_ddot    ddot
    #define GBL_F77_drotm   drotm
    #define GBL_F77_drotmg  drotmg
    //Nivel 1 para tipo de dato float
    #define GBL_F77_saxpy   saxpy
    #define GBL_F77_isamax  isamax
    #define GBL_F77_sasum   sasum
    #define GBL_F77_scopy   scopy
    #define GBL_F77_snrm2   snrm2
    #define GBL_F77_srot    srot
    #define GBL_F77_srotg   srotg
    #define GBL_F77_sscal   sscal
    #define GBL_F77_sswap   sswap
    #define GBL_F77_sdot    sdot
    #define GBL_F77_srotm   srotm
    #define GBL_F77_srotmg  srotmg
    //Nivel 2 para tipo de dato double
    #define GBL_F77_dgemv   dgemv
    #define GBL_F77_dgbmv   dgbmv
    #define GBL_F77_dsymv   dsymv
    #define GBL_F77_dsbmv   dsbmv
    #define GBL_F77_dspmv   dspmv
    #define GBL_F77_dtrmv   dtrmv
    #define GBL_F77_dtbmv   dtbmv
    #define GBL_F77_dtpmv   dtpmv
    #define GBL_F77_dtrsv   dtrsv
    #define GBL_F77_dtbsv   dtbsv
    #define GBL_F77_dtpsv   dtpsv
    #define GBL_F77_dger    dger
    #define GBL_F77_dsyr    dsyr
    #define GBL_F77_dspr    dspr
    #define GBL_F77_dsyr2   dsyr2
    #define GBL_F77_dspr2   dspr2
    //Nivel 2 para tipo de dato float
    #define GBL_F77_sgemv   sgemv
    #define GBL_F77_sgbmv   sgbmv
    #define GBL_F77_ssymv   ssymv
    #define GBL_F77_ssbmv   ssbmv
    #define GBL_F77_sspmv   sspmv
    #define GBL_F77_strmv   strmv
    #define GBL_F77_stbmv   stbmv
    #define GBL_F77_stpmv   stpmv
    #define GBL_F77_strsv   strsv
    #define GBL_F77_stbsv   stbsv
    #define GBL_F77_stpsv   stpsv
    #define GBL_F77_sger    sger
    #define GBL_F77_ssyr    ssyr
    #define GBL_F77_sspr    sspr
    #define GBL_F77_ssyr2   ssyr2
    #define GBL_F77_sspr2   sspr2
    //Nivel 3 para tipo de dato double
    #define GBL_F77_dgemm   dgemm
    #define GBL_F77_dsymm   dsymm
    #define GBL_F77_dsyrk   dsyrk
    #define GBL_F77_dsyr2k  dsyr2k
    #define GBL_F77_dtrmm   dtrmm
    #define GBL_F77_dtrsm   dtrsm
    //Nivel 3 para tipo de dato float
    #define GBL_F77_sgemm   sgemm
    #define GBL_F77_ssymm   ssymm
    #define GBL_F77_ssyrk   ssyrk
    #define GBL_F77_ssyr2k  ssyr2k
    #define GBL_F77_strmm   strmm
    #define GBL_F77_strsm   strsm
#endif
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */

