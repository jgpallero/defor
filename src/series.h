/**
\defgroup stemp SERIE-TEMPORAL
@{
\file series.h
\brief Declaracion de las funciones para el calculo de series temporales de las
       posiciones de puntos.

En este fichero se declaran todas las funciones necesarias para el calculo de
series temporales de las posiciones de una serie de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 13 de septiembre de 2009
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _SERIES_H_
#define _SERIES_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"paramprg.h"
#include"posptos.h"
#include"geodesia.h"
#include"general.h"
#include"errores.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def LONMAXLINSTEMP
    \brief Longitud maxima de las lineas del script de dibujo de las series
           temporales. */
#define LONMAXLINSTEMP 80
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def LONMAXTEXTOSCOORDSTEMP
    \brief Longitud maxima de la cadena de texto para almacenar los parametros
           del dibujo dependientes del numero de sistemas de coordenadas. */
#define LONMAXTEXTOSCOORDSTEMP 4000
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def LLAMADAOCTAVESTEMP
    \brief Llamada a GNU Octave. */
#define LLAMADAOCTAVESTEMP "#!/opt/octave/bin/octave -qf"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def FORMSERIEENU
    \brief Formato de escritura de un fichero de serie temporal en el sistema
           ENU. */
#define FORMSERIEENU "%14.9lf %7.4lf %7.4lf %7.4f %8.5lf %8.5lf %8.5lf\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def FORMSERIEUTM
    \brief Formato de escritura de un fichero de serie temporal en el sistema
           UTM. */
#define FORMSERIEUTM "%14.9lf %11.4lf %11.4lf %10.4lf %8.5lf %8.5lf %8.5lf\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def FORMSERIEGEOC
    \brief Formato de escritura de un fichero de serie temporal en coordenadas
           cartesianas tridimensionales geocentricas. */
#define FORMSERIEGEOC "%14.9lf %12.4lf %12.4lf %12.4lf %8.5lf %8.5lf %8.5lf\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def AYUDASCRIPTSTEMP
    \brief Texto de ayuda para el script de dibujo de las series temporales. */
#define AYUDASCRIPTSTEMP "\
%%Este script genera un conjunto de archivos que contienen las series\n\
%%temporales de los puntos de trabajo en coordenadas ENU, cartesianas\n\
%%geocéntricas y/o UTM\n\
%%Este script acepta dos argumentos en la línea de órdenes:\n\
%%- 1: ruta del directorio donde se almacenan los ficheros de datos\n\
%%- 2: ruta del directorio donde se almacenarán los dibujos generados\n\
%%Por defecto, las dos rutas son el directorio de trabajo"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def PARAMSSTEMP1
    \brief Parámetros de los dibujos (primera parte). */
#define PARAMSSTEMP1 "\
%%ELEMENTOS A DIBUJAR\n\
dibSerie = 0;    %%dibujo de la línea de la serie temporal: 0/1\n\
dibErr = 2;      %%dibujo de la línea de la serie de error: 0/1/2 -> no/sí/barra\n\
                 %%elegir barras implica que no se tienen en cuenta los valores\n\
                 %%de 'dibSimErr', 'tamSimPtosErr', 'colErr' ni 'grosErr'\n\
dibVel = 2;      %%dibujo de la línea de la velocidad: 0/1/2 -> no/sí/ponderada\n\
dibSimSerie = 1; %%dibujo de símbolos de la serie: 0/1\n\
dibSimErr = 0;   %%dibujo de símbolos de la serie de errores: 0/1\n\
dibLey = 1;      %%dibujo de la leyenda\n\
dibMalla = 'on'; %%dibujo de la malla coordenada: 'on'/'off'\n\
%%SÍMBOLOS Y COLORES DEL DIBUJO\n\
simPtos = '.';     %%símbolo de los puntos para la serie temporal\n\
simPtosErr = '^';  %%símbolo de los puntos para la serie de error\n\
tamSimPtos = 5;    %%tamaño de los símbolos para la serie temporal\n\
tamSimPtosErr = 1; %%tamaño de los símbolos para la serie de error\n\
colSerie = 'b';    %%color de la serie temporal\n\
colErr = 'g';      %%color de los errores\n\
colVel = 'r';      %%color de la línea de velocidad\n\
grosSerie = 1.15;     %%grosor de la línea y los símbolos de la serie temporal\n\
grosErr = 1;       %%grosor de la línea y los símbolos de la serie de error\n\
grosVel = 1.25;       %%grosor de la línea de velocidad ajustada\n\
posLey = 'southwest';      %%posición de la leyenda\n\
carLey = {'boxon','left'}; %%otras características de la leyenda\n\
%%PARÁMETROS ESTADÍSTICOS\n\
intConf = 0.95;                            %%intervalo de confianza\n\
factExp = abs(norminv((1.0-intConf)/2.0)); %%factor de expansión"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def PARAMSSTEMP2
    \brief Parámetros de los dibujos (segunda parte). */
#define PARAMSSTEMP2 "\
%%PARÁMETROS DE LOS FICHEROS DE SALIDA\n\
extDibujo = 'png';       %%extensión de los dibujos\n\
driver = '-dpng';        %%driver para generar los dibujos\n\
tamFuente = '-F:7';      %%tamaño de fuente para el fichero de salida\n\
oriPapel = '-landscape'; %%orientación del papel para el fichero de salida\n\
recPdfcrop = 1;          %%recorta el dibujo con pdfcrop: 0/1\n\
%%DIRECTORIOS DE TRABAJO\n\
if nargin==0\n\
    dirDatos = pwd;\n\
    dirDibujos = pwd;\n\
else\n\
    argEntrada = argv;\n\
    dirDatos = argEntrada{1};\n\
    dirDibujos = argEntrada{2};\n\
end\n\
%%-------------------------------------------------------------------------------"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP1
    \brief Ordenes para el dibujo de las series temporales (primera parte). */
#define DIBUJOSTEMP1 "\
%%CÁLCULOS\n\
%%indicador de posición en el cell array de la leyenda\n\
pLey = 0;\n\
%%recorremos los tipos de coordenadas de trabajo\n\
for i=1:length(tipoCoor)\n\
    %%comprobamos si no hay que usar el tipo de coordenada\n\
    if usaCoor(i)==0\n\
        %%siguiente vuelta del bucle\n\
        continue;\n\
    end\n\
    %%identificadores de chequeo de límites en X\n\
    hayLimInfX = 0;\n\
    hayLimSupX = 0;\n\
    %%recorremos los puntos de trabajo\n\
    for j=1:length(ptos)\n\
        %%limpiamos el posible dibujo anterior\n\
        clf;\n\
        %%inicializamos la posición en el cell array de la leyenda\n\
        pLey = 1;\n\
        %%creamos el nombre del fichero que guarda las coordenadas del punto\n\
        nombre = sprintf('%%s/%%s%%s',dirDatos,ptos{j},tipoCoor{i});\n\
        %%cargamos las coordenadas\n\
        coor = load(nombre);\n\
        %%calculo el valor mínimo de la coordenada X en el dibujo final\n\
        if limInfX(i)~=0.0\n\
            %%límite inferior impuesto\n\
            minX = limInfX(i);\n\
        else\n\
            %%límite inferior de los datos\n\
            minX = min(coor(:,1));\n\
        end\n\
        %%calculo el valor máximo de la coordenada X en el dibujo final\n\
        if limSupX(i)~=0.0\n\
            %%límite superior impuesto\n\
            maxX = limSupX(i);\n\
        else\n\
            %%límite superior de los datos\n\
            maxX = max(coor(:,1));\n\
        end\n\
        %%calculo la época media como número entero\n\
        mediaX = round(mean([minX maxX]));\n\
        %%reduzco los tiempos a la media\n\
        coor(:,1) = coor(:,1)-mediaX;\n\
        %%comprobamos si se ha impuesto límite inferior para los tiempos\n\
        if limInfX(i)~=0\n\
            %%reducimos el límite a la época media\n\
            limInfXAux = limInfX(i)-mediaX;\n\
            %%indicamos que hay límite inferior\n\
            hayLimInfX = 1;\n\
        end\n\
        %%comprobamos si se ha impuesto límite superior para los tiempos\n\
        if limSupX(i)~=0\n\
            %%reducimos el límite a la época media\n\
            limSupXAux = limSupX(i)-mediaX;\n\
            %%indicamos que hay límite superior\n\
            hayLimSupX = 1;\n\
        end\n\
        %%aplicamos el factor de multiplicación a las coordenadas\n\
        coor(:,2:7) = coor(:,2:7)*factMult(1,i);\n\
        %%aplicamos el factor de expansión a las desviaciones típicas\n\
        coor(:,5:7) = coor(:,5:7)*factExp;\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP2
    \brief Ordenes para el dibujo de las series temporales (segunda parte). */
#define DIBUJOSTEMP2 "\
        %%comprobamos si hay que calcular la velocidad\n\
        if dibVel~=0\n\
            %%número de épocas de trabajo\n\
            nEpocas = size(coor,1);\n\
            %%comprobamos si hay más de una época\n\
            if nEpocas>1\n\
                %%matriz de diseño\n\
                matA = ones(nEpocas,2);\n\
                matA(:,1) = coor(:,1);\n\
                %%vectores de términos independientes\n\
                vecB = coor(:,2:4);\n\
                %%diagonales matrices de pesos (almacenadas en las columnas)\n\
                if (dibVel>1)&&(size(coor,2)==7)\n\
                    %%extraigo la información de error, quitando el factor de\n\
                    %%expansión aplicado previamente\n\
                    matP = coor(:,5:7)./factExp;\n\
                    %%posibles valores iguales a 0 en los errores\n\
                    cerosMatP = matP==0.0;\n\
                    %%número de ceros\n\
                    numCeros = sum(sum(cerosMatP));\n\
                    %%compruebo si hay algún valor igual a 0.0 entre los datos\n\
                    if numCeros==(nEpocas*3)\n\
                        %%si todos los valores son cero, la matriz de pesos es la\n\
                        %%identidad\n\
                        matP = ones(nEpocas,3);\n\
                    elseif sum(sum(cerosMatP))~=0\n\
                        %%sustituyo los ceros por Inf an una matriz auxiliar\n\
                        auxMatP = matP;\n\
                        auxMatP(cerosMatP) = Inf;\n\
                        %%calculo el valor mínimo de los errores\n\
                        valMinError = min(min(auxMatP));\n\
                        %%sustituyo los ceros por un valor diez veces menor que\n\
                        %%el error mínimo\n\
                        matP(cerosMatP) = valMinError/10.0;\n\
                    end\n\
                else\n\
                    %%la matriz de pesos es la identidad si no hay información de\n\
                    %%errores en la entrada\n\
                    matP = ones(nEpocas,3);\n\
                end\n\
                %%convierto las desviaciones típicas en varianzas\n\
                matP = matP.^2;\n\
                %%convierto las varianzas en las diagonales de matrices de pesos\n\
                matP = 1.0./matP;\n\
                %%matriz cofactor\n\
                matQx = inv(matA'*diag(matP(:,1))*matA);\n\
                matQy = inv(matA'*diag(matP(:,2))*matA);\n\
                matQz = inv(matA'*diag(matP(:,3))*matA);\n\
                %%calculamos los parámetros de las rectas\n\
                solTendX = matQx*matA'*diag(matP(:,1))*vecB(:,1);\n\
                solTendY = matQy*matA'*diag(matP(:,2))*vecB(:,2);\n\
                solTendZ = matQz*matA'*diag(matP(:,3))*vecB(:,3);\n\
                %%comprobamos si hay más de dos puntos\n\
                if nEpocas>2\n\
                    %%residuos\n\
                    vX = matA*solTendX-vecB(:,1);\n\
                    vY = matA*solTendY-vecB(:,2);\n\
                    vZ = matA*solTendZ-vecB(:,3);\n\
                    %%grados de libertad del ajuste\n\
                    gradLib = nEpocas-2;\n\
                    %%varianzas de referencia a posteriori\n\
                    s02X = (vX'*diag(matP(:,1))*vX)/gradLib;\n\
                    s02Y = (vY'*diag(matP(:,2))*vY)/gradLib;\n\
                    s02Z = (vZ'*diag(matP(:,3))*vZ)/gradLib;\n\
                    %%matrices de varianzas-covarianzas\n\
                    matExx = s02X*matQx;\n\
                    matEyy = s02Y*matQy;\n\
                    matEzz = s02Z*matQz;\n\
                else\n\
                    %%como sólo hay dos puntos, los errores son 0.0\n\
                    matExx = zeros(2);\n\
                    matEyy = zeros(2);\n\
                    matEzz = zeros(2);\n\
                end\n\
            else\n\
                %%los parámetros son siempre cero\n\
                solTendX = [0.0 0.0]';\n\
                solTendY = [0.0 0.0]';\n\
                solTendZ = [0.0 0.0]';\n\
                matExx = zeros(2);\n\
                matEyy = zeros(2);\n\
                matEzz = zeros(2);\n\
            end\n\
        end\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP3
    \brief Ordenes para el dibujo de las series temporales (tercera parte). */
#define DIBUJOSTEMP3 "\
        %%calculamos los límites por defecto para el eje X\n\
        limitesX = [min(coor(:,1)) max(coor(:,1))];\n\
        %%comprobamos si se fuerza el límite inferior en X\n\
        if hayLimInfX~=0\n\
            %%calculamos el límite\n\
            limitesX(1,1) = limInfXAux;\n\
        end\n\
        %%comprobamos si se fuerza el límite superior en X\n\
        if hayLimSupX~=0\n\
            %%calculamos el límite\n\
            limitesX(1,2) = limSupXAux;\n\
        end\n\
        %%evitamos que los dos límites en X valgan lo mismo\n\
        if limitesX(1)==limitesX(2)\n\
            limitesX(1) = limitesX(1)-0.00005;\n\
            limitesX(2) = limitesX(2)+0.00005;\n\
        end\n\
        %%creamos una nueva figura\n\
        figure(1);\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP4
    \brief Ordenes para el dibujo de las series temporales (cuarta parte). */
#define DIBUJOSTEMP4 "\
        %%COORDENADA ESTE/X/X UTM\n\
        %%nos posicionamos en la subventana correspondiente\n\
        subplot(3,1,1);\n\
        %%comprobamos si hay que dibujar los errores como barras\n\
        if dibErr>1\n\
            %%comprobamos si hay que dibujar la línea de la serie\n\
            if dibSerie~=0\n\
                simSerie = [simPtos,'-',colSerie];\n\
            else\n\
                simSerie = [simPtos,colSerie];\n\
            end\n\
            %%dibujamos los puntos con sus barras de error\n\
            %%en la función 'errorbar' no se pueden indicar las propiedades de\n\
            %%grosor ni tamaño de punto, por lo que hay que hacerlo con 'set'\n\
            %%pero si éste se hace depués de llamar a 'errorbar', para lo cual\n\
            %%hay que almacenar el handle devuelto, luego da errores al poner la\n\
            %%leyenda, por lo que es mejor hacer la llamada conjunta\n\
            set(errorbar(coor(:,1),coor(:,2),coor(:,5),simSerie),...\n\
                'markersize',tamSimPtos,'linewidth',grosSerie);\n\
            %%comprobamos si hay que imprimir leyenda\n\
            if dibLey~=0\n\
                %%indicamos que queremos leyenda para este elemento\n\
                leyenda{pLey} = textoLeySerieError;\n\
                %%aumentamos el contador de posiciones\n\
                pLey = pLey+1;\n\
            end\n\
            %%indicamos que queremos seguir dibujando en la misma ventana\n\
            hold('on');\n\
        else\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP5
    \brief Ordenes para el dibujo de las series temporales (quinta parte). */
#define DIBUJOSTEMP5 "\
            %%dibujamos la línea de la serie, si ha lugar\n\
            if dibSerie~=0\n\
                %%sibujamos la línea\n\
                plot(coor(:,1),coor(:,2),colSerie,'linewidth',grosSerie);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    %%indicamos que queremos leyenda para este elemento\n\
                    leyenda{pLey} = textoLeySerie;\n\
                    %%aumentamos el contador de posiciones\n\
                    pLey = pLey+1;\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos los símbolos de la serie, si ha lugar\n\
            if dibSimSerie~=0\n\
                %%construimos la orden para el color y el símbolo de los puntos\n\
                ctam = sprintf('%%s%%s',colSerie,simPtos);\n\
                %%sibujamos los puntos\n\
                plot(coor(:,1),coor(:,2),ctam,'markersize',tamSimPtos,...\n\
                     'linewidth',grosSerie);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    if dibSerie~=0\n\
                        %%indicamos que no queremos leyenda para este elemento\n\
                        leyenda{pLey} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+1;\n\
                    else\n\
                        %%indicamos que queremos leyenda para este elemento\n\
                        leyenda{pLey} = textoLeySerie;\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+1;\n\
                    end\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos la serie de error, si ha lugar\n\
            if dibErr~=0\n\
                %%sibujamos las líneas\n\
                plot(coor(:,1),coor(:,2)+coor(:,5),colErr,'linewidth',grosErr);\n\
                plot(coor(:,1),coor(:,2)-coor(:,5),colErr,'linewidth',grosErr);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    %%indicamos que queremos leyenda para este elemento\n\
                    leyenda{pLey} = textoLeyError;\n\
                    leyenda{pLey+1} = '';\n\
                    %%aumentamos el contador de posiciones\n\
                    pLey = pLey+2;\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos los símbolos de la serie de error, si ha lugar\n\
            if dibSimErr~=0\n\
                %%construimos la orden para el color y el símbolo de los puntos\n\
                ctam = sprintf('%%s%%s',colErr,simPtosErr);\n\
                %%dibujamos los puntos\n\
                plot(coor(:,1),coor(:,2)+coor(:,5),ctam,...\n\
                     'markersize',tamSimPtosErr,'linewidth',grosErr);\n\
                plot(coor(:,1),coor(:,2)-coor(:,5),ctam,...\n\
                     'markersize',tamSimPtosErr,'linewidth',grosErr);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    if dibErr~=0\n\
                        %%indicamos que no queremos leyenda para este elemento\n\
                        leyenda{pLey} = '';\n\
                        leyenda{pLey+1} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+2;\n\
                    else\n\
                        %%indicamos que queremos leyenda para este elemento\n\
                        leyenda{pLey} = textoLeyError;\n\
                        leyenda{pLey+1} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+2;\n\
                    end\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
        end\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP6
    \brief Ordenes para el dibujo de las series temporales (quinta parte). */
#define DIBUJOSTEMP6 "\
        %%comprobamos si hay que dibujar la velocidad\n\
        if dibVel~=0\n\
            %%calculamos la ordenada correspondiente para los límites en X\n\
            velLimXInf = solTendX(1)*coor(1,1)+solTendX(2);\n\
            velLimXSup = solTendX(1)*coor(end,1)+solTendX(2);\n\
            velLimX = [velLimXInf velLimXSup];\n\
            %%dibujamos los puntos\n\
            plot([coor(1,1) coor(end,1)],velLimX,colVel,'linewidth',grosVel);\n\
            %%comprobamos si hay que imprimir leyenda\n\
            if dibLey~=0\n\
                %%indicamos que queremos leyenda para este elemento\n\
                leyenda{pLey} = textoLeyVel;\n\
                %%aumentamos el contador de posiciones\n\
                pLey = pLey+1;\n\
            end\n\
            %%indicamos que queremos seguir dibujando en la misma ventana\n\
            hold('on');\n\
        end\n\
        %%comprobamos si hay que dibujar la leyenda\n\
        if dibLey~=0\n\
            %%dibujamos la leyenda\n\
            legend(leyenda,'location',posLey);\n\
            %%aplicamos otras características a la leyenda\n\
            for k=1:length(carLey)\n\
                legend(carLey{k});\n\
            end\n\
        end\n\
        %%ponemos los límites en X\n\
        xlim(limitesX);\n\
        %%calculamos los límites por defecto para el eje Y\n\
        intervalo = max(coor(:,2))-min(coor(:,2));\n\
        mediaYAux = (max(coor(:,2))+min(coor(:,2)))/2.0;\n\
        limites = [mediaYAux-intervalo/2.0-max(coor(:,5)),...\n\
                   mediaYAux+intervalo/2.0+max(coor(:,5))];\n\
        %%comprobamos si se fuerza el límite inferior en Y\n\
        if limInfY(1,i)~=0\n\
            %%calculamos el límite sobre la coordenada del primer punto\n\
            limites(1,1) = coor(1,2)-limInfY(1,i)*factMult(1,i);\n\
        end\n\
        %%comprobamos si se fuerza el límite superior en Y\n\
        if limSupY(1,i)~=0\n\
            %%calculamos el límite sobre la coordenada del primer punto\n\
            limites(1,2) = coor(1,2)+limSupY(1,i)*factMult(1,i);\n\
        end\n\
        %%ponemos los límites en Y\n\
        ylim(limites);\n\
        %%aplicamos el formato a las coordenadas de los ejes X e Y\n\
        set(gca,'xticklabel',sprintf([formEjeX{i},' |'],...\n\
            get(gca,'xtick')+mediaX));\n\
        set(gca,'yticklabel',sprintf([formEjeY{i},' |'],get(gca,'ytick')));\n\
        %%el título genérico del dibujo es el nombre del punto\n\
        titulo = sprintf('%%s',ptos{j});\n\
        %%compruebo si hay que añadir información de la velocidad\n\
        if (dibVel~=0)&&(length(etiqTitVel)>0)\n\
            %%construimos el título\n\
            titulo = sprintf(['%%s',etiqTitVel{i}],...\n\
                     titulo,solTendX(1),sqrt(matExx(1,1))*factExp,...\n\
                     solTendY(1),sqrt(matEyy(1,1))*factExp,...\n\
                     solTendZ(1),sqrt(matEzz(1,1))*factExp,intConf*100.0);\n\
        end\n\
        %%le ponemos el título al dibujo\n\
        title(titulo);\n\
        %%ponemos la etiqueta del eje Y\n\
        ylabel(etiqEjeYEste{i});\n\
        %%dibujamos la malla coordenada\n\
        grid(dibMalla);\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP7
    \brief Ordenes para el dibujo de las series temporales (séptima parte). */
#define DIBUJOSTEMP7 "\
        %%COORDENADA NORTE/Y/Y UTM\n\
        %%inicializamos la posición en el cell array de la leyenda\n\
        pLey = 1;\n\
        %%nos posicionamos en la subventana correspondiente\n\
        subplot(3,1,2);\n\
        %%comprobamos si hay que dibujar los errores como barras\n\
        if dibErr>1\n\
            %%comprobamos si hay que dibujar la línea de la serie\n\
            if dibSerie~=0\n\
                simSerie = [simPtos,'-',colSerie];\n\
            else\n\
                simSerie = [simPtos,colSerie];\n\
            end\n\
            %%dibujamos los puntos con sus barras de error\n\
            %%en la función 'errorbar' no se pueden indicar las propiedades de\n\
            %%grosor ni tamaño de punto, por lo que hay que hacerlo con 'set'\n\
            %%pero si éste se hace depués de llamar a 'errorbar', para lo cual\n\
            %%hay que almacenar el handle devuelto, luego da errores al poner la\n\
            %%leyenda, por lo que es mejor hacer la llamada conjunta\n\
            set(errorbar(coor(:,1),coor(:,3),coor(:,6),simSerie),...\n\
                'markersize',tamSimPtos,'linewidth',grosSerie);\n\
            %%comprobamos si hay que imprimir leyenda\n\
            if dibLey~=0\n\
                %%indicamos que queremos leyenda para este elemento\n\
                leyenda{pLey} = textoLeySerieError;\n\
                %%aumentamos el contador de posiciones\n\
                pLey = pLey+1;\n\
            end\n\
            %%indicamos que queremos seguir dibujando en la misma ventana\n\
            hold('on');\n\
        else\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP8
    \brief Ordenes para el dibujo de las series temporales (octava parte). */
#define DIBUJOSTEMP8 "\
            %%dibujamos la línea de la serie, si ha lugar\n\
            if dibSerie~=0\n\
                %%dibujamos la línea\n\
                plot(coor(:,1),coor(:,3),colSerie,'linewidth',grosSerie);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    %%indicamos que queremos leyenda para este elemento\n\
                    leyenda{pLey} = textoLeySerie;\n\
                    %%aumentamos el contador de posiciones\n\
                    pLey = pLey+1;\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos los símbolos de la serie, si ha lugar\n\
            if dibSimSerie~=0\n\
                %%construimos la orden para el color y el símbolo de los puntos\n\
                ctam = sprintf('%%s%%s',colSerie,simPtos);\n\
                %%sibujamos los puntos\n\
                plot(coor(:,1),coor(:,3),ctam,'markersize',tamSimPtos,...\n\
                     'linewidth',grosSerie);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    if dibSerie~=0\n\
                        %%indicamos que no queremos leyenda para este elemento\n\
                        leyenda{pLey} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+1;\n\
                    else\n\
                        %%indicamos que queremos leyenda para este elemento\n\
                        leyenda{pLey} = textoLeySerie;\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+1;\n\
                    end\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos la serie de error, si ha lugar\n\
            if dibErr~=0\n\
                %%sibujamos las líneas\n\
                plot(coor(:,1),coor(:,3)+coor(:,6),colErr,'linewidth',grosErr);\n\
                plot(coor(:,1),coor(:,3)-coor(:,6),colErr,'linewidth',grosErr);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    %%indicamos que queremos leyenda para este elemento\n\
                    leyenda{pLey} = textoLeyError;\n\
                    leyenda{pLey+1} = '';\n\
                    %%aumentamos el contador de posiciones\n\
                    pLey = pLey+2;\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos los símbolos de la serie de error, si ha lugar\n\
            if dibSimErr~=0\n\
                %%construimos la orden para el color y el símbolo de los puntos\n\
                ctam = sprintf('%%s%%s',colErr,simPtosErr);\n\
                %%sibujamos los puntos\n\
                plot(coor(:,1),coor(:,3)+coor(:,6),ctam,...\n\
                     'markersize',tamSimPtosErr,'linewidth',grosErr);\n\
                plot(coor(:,1),coor(:,3)-coor(:,6),ctam,...\n\
                     'markersize',tamSimPtosErr,'linewidth',grosErr);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    if dibErr~=0\n\
                        %%indicamos que no queremos leyenda para este elemento\n\
                        leyenda{pLey} = '';\n\
                        leyenda{pLey+1} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+2;\n\
                    else\n\
                        %%indicamos que queremos leyenda para este elemento\n\
                        leyenda{pLey} = textoLeyError;\n\
                        leyenda{pLey+1} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+2;\n\
                    end\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
        end\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP9
    \brief Ordenes para el dibujo de las series temporales (novena parte). */
#define DIBUJOSTEMP9 "\
        %%comprobamos si hay que dibujar la velocidad\n\
        if dibVel~=0\n\
            %%calculamos la ordenada correspondiente para los límites en X\n\
            velLimYInf = solTendY(1)*coor(1,1)+solTendY(2);\n\
            velLimYSup = solTendY(1)*coor(end,1)+solTendY(2);\n\
            velLimY = [velLimYInf velLimYSup];\n\
            %%dibujamos los puntos\n\
            plot([coor(1,1) coor(end,1)],velLimY,colVel,'linewidth',grosVel);\n\
            %%comprobamos si hay que imprimir leyenda\n\
            if dibLey~=0\n\
                %%indicamos que queremos leyenda para este elemento\n\
                leyenda{pLey} = textoLeyVel;\n\
                %%aumentamos el contador de posiciones\n\
                pLey = pLey+1;\n\
            end\n\
            %%indicamos que queremos seguir dibujando en la misma ventana\n\
            hold('on');\n\
        end\n\
        %%comprobamos si hay que dibujar la leyenda\n\
        if dibLey~=0\n\
            %%dibujamos la leyenda\n\
            legend(leyenda,'location',posLey);\n\
            %%aplicamos otras características a la leyenda\n\
            for k=1:length(carLey)\n\
                legend(carLey{k});\n\
            end\n\
        end\n\
        %%ponemos los límites en X\n\
        xlim(limitesX);\n\
        %%calculamos los límites por defecto para el eje Y\n\
        intervalo = max(coor(:,3))-min(coor(:,3));\n\
        mediaYAux = (max(coor(:,3))+min(coor(:,3)))/2.0;\n\
        limites = [mediaYAux-intervalo/2.0-max(coor(:,6)),...\n\
                   mediaYAux+intervalo/2.0+max(coor(:,6))];\n\
        %%comprobamos si se fuerza el límite inferior en Y\n\
        if limInfY(1,i)~=0\n\
            %%calculamos el límite sobre la coordenada del primer punto\n\
            limites(1,1) = coor(1,2)-limInfY(1,i)*factMult(1,i);\n\
        end\n\
        %%comprobamos si se fuerza el límite superior en Y\n\
        if limSupY(1,i)~=0\n\
            %%calculamos el límite sobre la coordenada del primer punto\n\
            limites(1,2) = coor(1,2)+limSupY(1,i)*factMult(1,i);\n\
        end\n\
        %%ponemos los límites en Y\n\
        ylim(limites);\n\
        %%aplicamos el formato a las coordenadas de los ejes X e Y\n\
        set(gca,'xticklabel',sprintf([formEjeX{i},' |'],...\n\
            get(gca,'xtick')+mediaX));\n\
        set(gca,'yticklabel',sprintf([formEjeY{i},' |'],get(gca,'ytick')));\n\
        %%ponemos la etiqueta del eje Y\n\
        ylabel(etiqEjeYNorte{i});\n\
        %%dibujamos la malla coordenada\n\
        grid(dibMalla);\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP10
    \brief Ordenes para el dibujo de las series temporales (décima parte). */
#define DIBUJOSTEMP10 "\
        %%COORDENADA ARRIBA/Z/H elipsoidal\n\
        %%inicializamos la posición en el cell array de la leyenda\n\
        pLey = 1;\n\
        %%nos posicionamos en la subventana correspondiente\n\
        subplot(3,1,3);\n\
        %%comprobamos si hay que dibujar los errores como barras\n\
        if dibErr>1\n\
            %%comprobamos si hay que dibujar la línea de la serie\n\
            if dibSerie~=0\n\
                simSerie = [simPtos,'-',colSerie];\n\
            else\n\
                simSerie = [simPtos,colSerie];\n\
            end\n\
            %%dibujamos los puntos con sus barras de error\n\
            %%en la función 'errorbar' no se pueden indicar las propiedades de\n\
            %%grosor ni tamaño de punto, por lo que hay que hacerlo con 'set'\n\
            %%pero si éste se hace depués de llamar a 'errorbar', para lo cual\n\
            %%hay que almacenar el handle devuelto, luego da errores al poner la\n\
            %%leyenda, por lo que es mejor hacer la llamada conjunta\n\
            set(errorbar(coor(:,1),coor(:,4),coor(:,7),simSerie),...\n\
                'markersize',tamSimPtos,'linewidth',grosSerie);\n\
            %%comprobamos si hay que imprimir leyenda\n\
            if dibLey~=0\n\
                %%indicamos que queremos leyenda para este elemento\n\
                leyenda{pLey} = textoLeySerieError;\n\
                %%aumentamos el contador de posiciones\n\
                pLey = pLey+1;\n\
            end\n\
            %%indicamos que queremos seguir dibujando en la misma ventana\n\
            hold('on');\n\
        else\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP11
    \brief Ordenes para el dibujo de las series temporales (undécima parte). */
#define DIBUJOSTEMP11 "\
            %%dibujamos la línea de la serie, si ha lugar\n\
            if dibSerie~=0\n\
                %%sibujamos la línea\n\
                plot(coor(:,1),coor(:,4),colSerie,'linewidth',grosSerie);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    %%indicamos que queremos leyenda para este elemento\n\
                    leyenda{pLey} = textoLeySerie;\n\
                    %%aumentamos el contador de posiciones\n\
                    pLey = pLey+1;\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos los símbolos de la serie, si ha lugar\n\
            if dibSimSerie~=0\n\
                %%construimos la orden para el color y el símbolo de los puntos\n\
                ctam = sprintf('%%s%%s',colSerie,simPtos);\n\
                %%dibujamos los puntos\n\
                plot(coor(:,1),coor(:,4),ctam,'markersize',tamSimPtos,...\n\
                     'linewidth',grosSerie);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    if dibSerie~=0\n\
                        %%indicamos que no queremos leyenda para este elemento\n\
                        leyenda{pLey} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+1;\n\
                    else\n\
                        %%indicamos que queremos leyenda para este elemento\n\
                        leyenda{pLey} = textoLeySerie;\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+1;\n\
                    end\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos la serie de error, si ha lugar\n\
            if dibErr~=0\n\
                %%dibujamos las líneas\n\
                plot(coor(:,1),coor(:,4)+coor(:,7),colErr,'linewidth',grosErr);\n\
                plot(coor(:,1),coor(:,4)-coor(:,7),colErr,'linewidth',grosErr);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    %%indicamos que queremos leyenda para este elemento\n\
                    leyenda{pLey} = textoLeyError;\n\
                    leyenda{pLey+1} = '';\n\
                    %%aumentamos el contador de posiciones\n\
                    pLey = pLey+2;\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
            %%dibujamos los símbolos de la serie de error, si ha lugar\n\
            if dibSimErr~=0\n\
                %%construimos la orden para el color y el símbolo de los puntos\n\
                ctam = sprintf('%%s%%s',colErr,simPtosErr);\n\
                %%dibujamos los puntos\n\
                plot(coor(:,1),coor(:,4)+coor(:,7),ctam,...\n\
                     'markersize',tamSimPtosErr,'linewidth',grosErr);\n\
                plot(coor(:,1),coor(:,4)-coor(:,7),ctam,...\n\
                     'markersize',tamSimPtosErr,'linewidth',grosErr);\n\
                %%comprobamos si hay que imprimir leyenda\n\
                if dibLey~=0\n\
                    if dibErr~=0\n\
                        %%indicamos que no queremos leyenda para este elemento\n\
                        leyenda{pLey} = '';\n\
                        leyenda{pLey+1} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+2;\n\
                    else\n\
                        %%indicamos que queremos leyenda para este elemento\n\
                        leyenda{pLey} = textoLeyError;\n\
                        leyenda{pLey+1} = '';\n\
                        %%aumentamos el contador de posiciones\n\
                        pLey = pLey+2;\n\
                    end\n\
                end\n\
                %%indicamos que queremos seguir dibujando en la misma ventana\n\
                hold('on');\n\
            end\n\
        end\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def DIBUJOSTEMP12
    \brief Ordenes para el dibujo de las series temporales (duodécima parte). */
#define DIBUJOSTEMP12 "\
        %%comprobamos si hay que dibujar la velocidad\n\
        if dibVel~=0\n\
            %%calculamos la ordenada correspondiente para los límites en Z\n\
            velLimZInf = solTendZ(1)*coor(1,1)+solTendZ(2);\n\
            velLimZSup = solTendZ(1)*coor(end,1)+solTendZ(2);\n\
            velLimZ = [velLimZInf velLimZSup];\n\
            %%dibujamos los puntos\n\
            plot([coor(1,1) coor(end,1)],velLimZ,colVel,'linewidth',grosVel);\n\
            %%comprobamos si hay que imprimir leyenda\n\
            if dibLey~=0\n\
                %%indicamos que queremos leyenda para este elemento\n\
                leyenda{pLey} = textoLeyVel;\n\
                %%aumentamos el contador de posiciones\n\
                pLey = pLey+1;\n\
            end\n\
            %%indicamos que queremos seguir dibujando en la misma ventana\n\
            hold('on');\n\
        end\n\
        %%comprobamos si hay que dibujar la leyenda\n\
        if dibLey~=0\n\
            %%dibujamos la leyenda\n\
            legend(leyenda,'location',posLey);\n\
            %%aplicamos otras características a la leyenda\n\
            for k=1:length(carLey)\n\
                legend(carLey{k});\n\
            end\n\
        end\n\
        %%ponemos los límites en X\n\
        xlim(limitesX);\n\
        %%calculamos los límites por defecto para el eje Y\n\
        intervalo = max(coor(:,4))-min(coor(:,4));\n\
        mediaYAux = (max(coor(:,4))+min(coor(:,4)))/2.0;\n\
        limites = [mediaYAux-intervalo/2.0-max(coor(:,7)),...\n\
                   mediaYAux+intervalo/2.0+max(coor(:,7))];\n\
        %%comprobamos si se fuerza el límite inferior en Y\n\
        if limInfY(1,i)~=0\n\
            %%calculamos el límite sobre la coordenada del primer punto\n\
            limites(1,1) = coor(1,2)-limInfY(1,i)*factMult(1,i);\n\
        end\n\
        %%comprobamos si se fuerza el límite superior en Y\n\
        if limSupY(1,i)~=0\n\
            %%calculamos el límite sobre la coordenada del primer punto\n\
            limites(1,2) = coor(1,2)+limSupY(1,i)*factMult(1,i);\n\
        end\n\
        %%ponemos los límites en Y\n\
        ylim(limites);\n\
        %%aplicamos el formato a las coordenadas de los ejes X e Y\n\
        set(gca,'xticklabel',sprintf([formEjeX{i},' |'],...\n\
            get(gca,'xtick')+mediaX));\n\
        set(gca,'yticklabel',sprintf([formEjeY{i},' |'],get(gca,'ytick')));\n\
        %%ponemos la etiqueta del eje Y\n\
        ylabel(etiqEjeYArriba{i});\n\
        %%dibujamos la malla coordenada\n\
        grid(dibMalla);\n\
        %%ponemos la etiqueta del eje X\n\
        xlabel(etiqEjeX);\n\
        %%creamos el nombre del dibujo de salida\n\
        nombre = sprintf('%%s/%%s%%s.%%s',dirDibujos,ptos{j},tipoCoor{i},extDibujo);\n\
        %%exportamos el dibujo\n\
        print(nombre,driver,tamFuente);\n\
        %%comprobamos si hay que recortar el dibujo con 'pdfcrop'\n\
        if (recPdfcrop~=0)&&strcmp(driver,'-dpdf')\n\
            system(sprintf('pdfcrop %%s %%s',nombre,nombre));\n\
        end\n\
        %%en versiones modernas de Octave, 'oriPapel' da muchos problemas\n\
        %%print(nombre,driver,oriPapel,tamFuente);\n\
    end\n\
end\n"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula las coordenadas de un punto en todas las epocas almacenadas en
       una estructura posptos.
\param[in] posicion Puntero a una estructura posptos, que contiene las
           coordenadas de todos los puntos de trabajo.
\param[in] codPto Codigo del punto de trabajo.
\param[out] nEpocas Numero de epocas para las que hay coordenadas.
\return Dos posibilidades:
        - Matriz de \em nEpocas filas por 10 columnas que almacena las
          coordenadas del punto de trabajo en todas las epocas en las que
          aparece. Cada columna es:
          - Col. 0: Epoca de las coordenadas.
          - Col. 1: Coordenada X geocentrica.
          - Col. 2: Coordenada Y geocentrica.
          - Col. 3: Coordenada Z geocentrica.
          - Col. 4: Varianza de la coordenada X geocentrica.
          - Col. 5: Covarianza entre las coordenadas X e Y.
          - Col. 6: Covarianza entre las coordenadas X y Z.
          - Col. 7: Varianza de la coordenada Y geocentrica.
          - Col. 8: Covarianza entre las coordenadas Y y Z.
          - Col. 9: Varianza de la coordenada Z geocentrica.
          Las filas estan ordenadas de epoca mas antigua a mas reciente.
        - Si hay un error de asignacion de memoria, se devuelve el valor NULL.
\note En el caso de producirse un error (la funcion devuelve NULL) la posible
      memoria ocupada por la estructura posptos pasada no es liberada.
*/
double** CoordPtosSerie(const posptos* posicion,
                        const char codPto[],
                        int* nEpocas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Convierte una serie de coordenadas cartesianas tridimensionales
       geocentricas en coordenadas UTM.
\param[in] coor Matriz que almacena una serie de coordenadas cartesianas
           tridimensionales geocentricas. Esta matriz tiene el mismo formato que
           la matriz de salida de la funcion CoordPtosSerie().
\param[in] filas Numero de filas de la matriz \em coor.
\return Dos posibilidades:
        - Matriz de \em filas filas por 10 columnas que almacena las coordenadas
          UTM correspondientes a la matriz \em coor. Cada columna es:
          - Col. 0: Epoca de las coordenadas.
          - Col. 1: Coordenada X UTM.
          - Col. 2: Coordenada Y UTM.
          - Col. 3: Coordenada H elipsoidal.
          - Col. 4: Varianza de la coordenada X UTM.
          - Col. 5: Covarianza entre las coordenadas X e Y.
          - Col. 6: Covarianza entre las coordenadas X y H (siempre vale 0.0).
          - Col. 7: Varianza de la coordenada Y UTM.
          - Col. 8: Covarianza entre las coordenadas Y y H (siempre vale 0.0).
          - Col. 9: Varianza de la coordenada H elipsoidal.
          Las filas estan ordenadas de epoca mas antigua a mas reciente.
        - Si hay un error de asignacion de memoria, se devuelve el valor NULL.
\note En el caso de producirse un error (la funcion devuelve NULL) la posible
      memoria ocupada por la matriz \em coor pasada no es liberada.
*/
double** ConvierteUtm(double** coor,
                      const int filas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Convierte una serie de coordenadas cartesianas tridimensionales
       geocentricas en incrementos en el sistema ENU.
\param[in] coor Matriz que almacena una serie de coordenadas cartesianas
           tridimensionales geocentricas. Esta matriz tiene el mismo formato que
           la matriz de salida de la funcion CoordPtosSerie().
\param[in] filas Numero de filas de la matriz \em coor.
\return Dos posibilidades:
        - Matriz de \em filas filas por 10 columnas que almacena los incrementos
          de coordenadas con respecto al primer punto en el sistema ENU,
          correspondientes a la matriz \em coor. Cada columna es:
          - Col. 0: Epoca de las coordenadas.
          - Col. 1: Coordenada east en el sistema topocentrico.
          - Col. 2: Coordenada north en el sistema topocentrico.
          - Col. 3: Coordenada up en el sistema topocentrico.
          - Col. 4: Varianza de la coordenada east.
          - Col. 5: Covarianza entre las coordenadas east y north.
          - Col. 6: Covarianza entre las coordenadas east y up.
          - Col. 7: Varianza de la coordenada north.
          - Col. 8: Covarianza entre las coordenadas north y up.
          - Col. 9: Varianza de la coordenada up.
          Las filas estan ordenadas de epoca mas antigua a mas reciente.
        - Si hay un error de asignacion de memoria, se devuelve el valor NULL.
\note Los parámetros de error ya son los correspondientes a los desplazamientos
      en el sistema ENU.
\note En el caso de producirse un error (la funcion devuelve NULL) la posible
      memoria ocupada por la matriz \em coor pasada no es liberada.
*/
double** ConvierteEnu(double** coor,
                      const int filas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Crea una cadena de texto que almacena las ordenes correspondientes para
       el manejo de los parametros de dibujo de los puntos de trabajo y los
       sistemas de coordenadas en el script de GNU Octave.
\param[in] ptosDist Vector de cadenas de texto que contiene los codigos de los
           puntos de trabajo distintos.
\param[in] nPtosDist Numero de puntos distintos.
\param[in] tipoSeries Identificador del tipo de series a calcular. Varias
           posibilidades:
           - #FLAGTSERIEENU: Calcula series en el sistema ENU.
           - #FLAGTSERIEUTM: Calcula series en coordenadas UTM.
           - #FLAGTSERIEGEOC: Calcula series en coordenadas cartesianas
                              tridimensionales geocentricas.
           - #FLAGTSERIEPLANO: Calcula series en el sistema ENU y en coordenadas
                               UTM.
           - #FLAGTSERIETODO: Calcula series en el sistema ENU, en coordenadas
                              UTM y en ordenadas cartesianas tridimensionales
                              geocentricas.
\param[out] texto Cadena de texto que almacena las ordenes correspondientes para
            el manejo de los parametros de dibujo de los puntos de trabajo y los
            sistemas de coordenadas en el script de GNU Octave.
\note Esta funcion no comprueba si los argumentos de entrada contienen valores
      validos.
\note Esta funcion no comprueba si el argumento \em texto tiene asignada memoria
      suficiente.
*/
void TextoScriptTipoCoor(char** ptosDist,
                         const int nPtosDist,
                         const char tipoSeries[],
                         char texto[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula las series temporales de las posiciones de un conjunto de puntos
       y las almacena en un ficheros para ler leidos por la funcion load() de
       GNU Octave.
\param[in] posicion Puntero a una estructura posptos, que contiene las
           coordenadas de todos los puntos de trabajo.
\param[in] ptosDist Vector de cadenas de texto que contiene los codigos de los
           puntos de trabajo distintos.
\param[in] nPtosDist Numero de puntos distintos.
\param[in] tipoSeries Identificador del tipo de series a calcular. Varias
           posibilidades:
           - #FLAGTSERIEENU: Calcula series en el sistema ENU.
           - #FLAGTSERIEUTM: Calcula series en coordenadas UTM.
           - #FLAGTSERIEGEOC: Calcula series en coordenadas cartesianas
                              tridimensionales geocentricas.
           - #FLAGTSERIEPLANO: Calcula series en el sistema ENU y en coordenadas
                               UTM.
           - #FLAGTSERIETODO: Calcula series en el sistema ENU, en coordenadas
                              UTM y en ordenadas cartesianas tridimensionales
                              geocentricas.
\param[in] directorio Nombre del directorio donde se guardaran los ficheros de
           coordenadas creados.
\param[out] texto Cadena de texto que almacena las ordenes correspondientes para
            el manejo de los parametros de dibujo de los puntos de trabajo y los
            sistemas de coordenadas en el script de GNU Octave.
\return Codigo de error.
\return Un fichero por cada punto de trabajo en el directorio indicado con las
        series temporales en el o los sistemas elegidos. Los nombres de los
        ficheros se componen de dos partes:
        - Codigo del punto de trabajo.
        - Indicador del sistema de coordenadas al que esta referida la solucion.
          Varias posibilidades:
          - #EXTFICHENU: Si el sistema es ENU.
          - #EXTFICHUTM: Si el sistema es UTM.
          - #EXTFICHGEOC: Si el sistema es cartesiano tridimensional
                          geocentrico.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura posptos pasada
      no es liberada.
\note Esta funcion no comprueba si los argumentos de entrada contienen valores
      validos.
\note Esta funcion no comprueba si el argumento \em texto tiene asignada memoria
      suficiente.
*/
int FicherosSeriesTempOctave(const posptos* posicion,
                             char** ptosDist,
                             const int nPtosDist,
                             const char tipoSeries[],
                             const char directorio[],
                             char texto[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula las series temporales de las posiciones de un conjunto de puntos
       y genera un script de GNU Octave para realizar los dibujos.
\param[in] posicion Puntero a una estructura posptos, que contiene las
           coordenadas de todos los puntos de trabajo.
\param[in] tipoSeries Identificador del tipo de series a calcular. Varias
           posibilidades:
           - #FLAGTSERIEENU: Calcula series en el sistema ENU.
           - #FLAGTSERIEUTM: Calcula series en coordenadas UTM.
           - #FLAGTSERIEGEOC: Calcula series en coordenadas cartesianas
                              tridimensionales geocentricas.
           - #FLAGTSERIEPLANO: Calcula series en el sistema ENU y en coordenadas
                               UTM.
           - #FLAGTSERIETODO: Calcula series en el sistema ENU, en coordenadas
                              UTM y en ordenadas cartesianas tridimensionales
                              geocentricas.
\param[in] directorio Nombre del directorio donde se guardaran los ficheros de
           coordenadas creados.
\param[in,out] idFichero Identificador de fichero abierto para escribir. Si se
               pasa \p NULL, no se escribe el script.
\param[in] fichero Nombre del fichero apuntado por \em idFichero.
\return Codigo de error.
\note El puntero de L/E es devuelto tras la ultima posicion escrita en el
      fichero tras la ejecucion de esta funcion.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura posptos pasada
      no es liberada y el fichero apuntado por idFichero no es cerrado.
\note Esta funcion no comprueba si los argumentos de entrada contienen valores
      validos.
*/
int SerieTempPtosOctave(const posptos* posicion,
                        const char tipoSeries[],
                        const char directorio[],
                        FILE* idFichero,
                        char fichero[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
