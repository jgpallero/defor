/* -*- coding: utf-8 -*- */
/**
\ingroup gcblas
@{
\file gcblasd.c
\brief Definición de las funciones de \p GCBLAS para tipo de dato \p double.

Para que la macro \ref GCBLAS_INFO_STDERR sea capaz de escribir su mensaje en la
salida de error \em stderr hay que pasar en la compilación la variable del
preprocesador \em ESCRIBE_INFO_BLAS_STDERR. En \p gcc, las variables para el
preprocesador se pasan como \em -DXXX, donde \em XXX es la variable a
introducir.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2014-2015, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"auxgcblas.h"
#include"gcblas.h"
#include"errgcblas.h"
#include"vcmoblas.h"
#include"gcoblasf.h"
/******************************************************************************/
/******************************************************************************/
void cblas_daxpy(const gblas_int N,
                 const double alpha,
                 const double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_daxpy(&N,&alpha,X,&incX,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
CBLAS_INDEX cblas_idamax(const gblas_int N,
                         const double* X,
                         const gblas_int incX)
{
    //variable de salida
    CBLAS_INDEX pos=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    pos = (CBLAS_INDEX)GBL_F77_idamax(&N,X,&incX);
    //hacemos que los índices empiecen en 0
    pos = (pos>0) ? pos-1 : pos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return pos;
}
/******************************************************************************/
/******************************************************************************/
double cblas_dasum(const gblas_int N,
                   const double* X,
                   const gblas_int incX)
{
    //variable de salida
    double asum=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    asum = GBL_F77_dasum(&N,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return asum;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dcopy(const gblas_int N,
                 const double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_dcopy(&N,X,&incX,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
double cblas_dnrm2(const gblas_int N,
                   const double* X,
                   const gblas_int incX)
{
    //variable de salida
    double nrm2=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    nrm2 = GBL_F77_dnrm2(&N,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return nrm2;
}
/******************************************************************************/
/******************************************************************************/
void cblas_drot(const gblas_int N,
                double* X,
                const gblas_int incX,
                double* Y,
                const gblas_int incY,
                const double c,
                const double s)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_drot(&N,X,&incX,Y,&incY,&c,&s);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_drotg(double* a,
                 double* b,
                 double* c,
                 double* s)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_drotg(a,b,c,s);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dscal(const gblas_int N,
                 const double alpha,
                 double* X,
                 const gblas_int incX)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_dscal(&N,&alpha,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dswap(const gblas_int N,
                 double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_dswap(&N,X,&incX,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
double cblas_ddot(const gblas_int N,
                  const double* X,
                  const gblas_int incX,
                  const double* Y,
                  const gblas_int incY)
{
    //variable de salida
    double dot=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    dot = GBL_F77_ddot(&N,X,&incX,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return dot;
}
/******************************************************************************/
/******************************************************************************/
void cblas_drotm(const gblas_int N,
                 double* X,
                 const gblas_int incX,
                 double* Y,
                 const gblas_int incY,
                 const double* P)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_drotm  (&N,X,&incX,Y,&incY,P);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_drotmg(double* d1,
                  double* d2,
                  double* b1,
                  const double b2,
                  double* P)
{
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //llamamos a la función de Fortran
    GBL_F77_drotmg (d1,d2,b1,&b2,P);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dgemv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char TA=0;
    gblas_int MM=0,NN=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_GEMV(pos,Order,TransA,M,N,lda,incX,incY);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_GEMV(Order,TransA,M,N,TA,MM,NN);
    //llamamos a la función de Fortran
    GBL_F77_dgemv(&TA,&MM,&NN,&alpha,A,&lda,X,&incX,&beta,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dgbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const gblas_int M,
                 const gblas_int N,
                 const gblas_int KL,
                 const gblas_int KU,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char TA=0;
    gblas_int MM=0,NN=0,KKL=0,KKU=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_GBMV(pos,Order,TransA,M,N,KL,KU,lda,incX,incY);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_GBMV(Order,TransA,M,N,KL,KU,TA,MM,NN,KKL,KKU);
    //llamamos a la función de Fortran
    GBL_F77_dgbmv(&TA,&MM,&NN,&KKL,&KKU,&alpha,A,&lda,X,&incX,&beta,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsymv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SYMV(pos,Order,Uplo,N,lda,incX,incY);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SYMV(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dsymv(&UPLO,&N,&alpha,A,&lda,X,&incX,&beta,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const gblas_int K,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SBMV(pos,Order,Uplo,N,K,lda,incX,incY);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SBMV(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dsbmv(&UPLO,&N,&K,&alpha,A,&lda,X,&incX,&beta,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dspmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* Ap,
                 const double* X,
                 const gblas_int incX,
                 const double beta,
                 double* Y,
                 const gblas_int incY)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SPMV(pos,Order,Uplo,N,incX,incY);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SPMV(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dspmv(&UPLO,&N,&alpha,Ap,X,&incX,&beta,Y,&incY);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtrmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TA=0,DIAG=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TRMV(pos,Order,Uplo,TransA,Diag,N,lda,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TRMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG);
    //llamamos a la función de Fortran
    GBL_F77_dtrmv(&UPLO,&TA,&DIAG,&N,A,&lda,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtbmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const gblas_int K,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TA=0,DIAG=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TBMV(pos,Order,Uplo,TransA,Diag,N,K,lda,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TBMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG);
    //llamamos a la función de Fortran
    GBL_F77_dtbmv(&UPLO,&TA,&DIAG,&N,&K,A,&lda,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtpmv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* Ap,
                 double* X,
                 const gblas_int incX)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TA=0,DIAG=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TPMV(pos,Order,Uplo,TransA,Diag,N,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TPMV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG);
    //llamamos a la función de Fortran
    GBL_F77_dtpmv(&UPLO,&TA,&DIAG,&N,Ap,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtrsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TA=0,DIAG=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TRSV(pos,Order,Uplo,TransA,Diag,N,lda,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TRSV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG);
    //llamamos a la función de Fortran
    GBL_F77_dtrsv(&UPLO,&TA,&DIAG,&N,A,&lda,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtbsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const gblas_int K,
                 const double* A,
                 const gblas_int lda,
                 double* X,
                 const gblas_int incX)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TA=0,DIAG=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TBSV(pos,Order,Uplo,TransA,Diag,N,K,lda,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TBSV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG);
    //llamamos a la función de Fortran
    GBL_F77_dtbsv(&UPLO,&TA,&DIAG,&N,&K,A,&lda,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtpsv(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int N,
                 const double* Ap,
                 double* X,
                 const gblas_int incX)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TA=0,DIAG=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TPSV(pos,Order,Uplo,TransA,Diag,N,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TBSV(Order,Uplo,TransA,Diag,UPLO,TA,DIAG);
    //llamamos a la función de Fortran
    GBL_F77_dtpsv(&UPLO,&TA,&DIAG,&N,Ap,X,&incX);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dger(const enum CBLAS_ORDER Order,
                const gblas_int M,
                const gblas_int N,
                const double alpha,
                const double* X,
                const gblas_int incX,
                const double* Y,
                const gblas_int incY,
                double* A,
                const gblas_int lda)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    int MM=0,NN=0,INCXX=0,INCYY=0;
    const double* XX=NULL;
    const double* YY=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_GER(pos,Order,M,N,incX,incY,lda);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_GER(Order,M,N,X,incX,Y,incY,MM,NN,XX,INCXX,YY,INCYY);
    //llamamos a la función de Fortran
    GBL_F77_dger(&MM,&NN,&alpha,XX,&INCXX,YY,&INCYY,A,&lda);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsyr(const enum CBLAS_ORDER Order,
                const enum CBLAS_UPLO Uplo,
                const gblas_int N,
                const double alpha,
                const double* X,
                const gblas_int incX,
                double* A,
                const gblas_int lda)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SYR(pos,Order,Uplo,N,incX,lda);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SYR(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dsyr(&UPLO,&N,&alpha,X,&incX,A,&lda);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dspr(const enum CBLAS_ORDER Order,
                const enum CBLAS_UPLO Uplo,
                const gblas_int N,
                const double alpha,
                const double* X,
                const gblas_int incX,
                double* Ap)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SPR(pos,Order,Uplo,N,incX);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SPR(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dspr(&UPLO,&N,&alpha,X,&incX,Ap);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsyr2(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* X,
                 const gblas_int incX,
                 const double* Y,
                 const gblas_int incY,
                 double* A,
                 const gblas_int lda)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SYR2(pos,Order,Uplo,N,incX,incY,lda);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SYR2(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dsyr2(&UPLO,&N,&alpha,X,&incX,Y,&incY,A,&lda);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dspr2(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int N,
                 const double alpha,
                 const double* X,
                 const gblas_int incX,
                 const double* Y,
                 const gblas_int incY,
                 double* A)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SPR2(pos,Order,Uplo,N,incX,incY);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SPR2(Order,Uplo,UPLO);
    //llamamos a la función de Fortran
    GBL_F77_dspr2(&UPLO,&N,&alpha,X,&incX,Y,&incY,A);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dgemm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_TRANSPOSE TransB,
                 const gblas_int M,
                 const gblas_int N,
                 const gblas_int K,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* B,
                 const gblas_int ldb,
                 const double beta,
                 double* C,
                 const gblas_int ldc)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char TA=0,TB=0;
    gblas_int MM=0,NN=0,LDAA=0,LDBB=0;
    const double* AA=NULL;
    const double* BB=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_GEMM(pos,Order,TransA,TransB,M,N,K,lda,ldb,ldc);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_GEMM(Order,
                    TransA,TransB,M,N,A,lda,B,ldb,TA,TB,MM,NN,AA,LDAA,BB,LDBB);
    //llamamos a la función de Fortran
    GBL_F77_dgemm(&TA,&TB,&MM,&NN,&K,&alpha,AA,&LDAA,BB,&LDBB,&beta,C,&ldc);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsymm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double* B,
                 const gblas_int ldb,
                 const double beta,
                 double* C,
                 const gblas_int ldc)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char SIDE=0,UPLO=0;
    gblas_int MM=0,NN=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SYMM(pos,Order,Side,Uplo,M,N,lda,ldb,ldc);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SYMM(Order,Side,Uplo,M,N,SIDE,UPLO,MM,NN);
    //llamamos a la función de Fortran
    GBL_F77_dsymm(&SIDE,&UPLO,&MM,&NN,&alpha,A,&lda,B,&ldb,&beta,C,&ldc);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsyrk(const enum CBLAS_ORDER Order,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE Trans,
                 const gblas_int N,
                 const gblas_int K,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 const double beta,
                 double* C,
                 const gblas_int ldc)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TT=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SYRK(pos,Order,Uplo,Trans,N,K,lda,ldc);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SYRK(Order,Uplo,Trans,UPLO,TT);
    //llamamos a la función de Fortran
    GBL_F77_dsyrk(&UPLO,&TT,&N,&K,&alpha,A,&lda,&beta,C,&ldc);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dsyr2k(const enum CBLAS_ORDER Order,
                  const enum CBLAS_UPLO Uplo,
                  const enum CBLAS_TRANSPOSE Trans,
                  const gblas_int N,
                  const gblas_int K,
                  const double alpha,
                  const double* A,
                  const gblas_int lda,
                  const double* B,
                  const gblas_int ldb,
                  const double beta,
                  double* C,
                  const gblas_int ldc)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char UPLO=0,TT=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_SYR2K(pos,Order,Uplo,Trans,N,K,lda,ldb,ldc);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_SYR2K(Order,Uplo,Trans,UPLO,TT);
    //llamamos a la función de Fortran
    GBL_F77_dsyr2k(&UPLO,&TT,&N,&K,&alpha,A,&lda,B,&ldb,&beta,C,&ldc);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtrmm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 double* B,
                 const gblas_int ldb)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char SIDE=0,UPLO=0,TA=0,DIAG=0;
    gblas_int MM=0,NN=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TRMM(pos,Order,Side,Uplo,TransA,Diag,M,N,lda,ldb);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TRMM(Order,Side,Uplo,TransA,Diag,M,N,SIDE,UPLO,TA,DIAG,MM,NN);
    //llamamos a la función de Fortran
    GBL_F77_dtrmm(&SIDE,&UPLO,&TA,&DIAG,&MM,&NN,&alpha,A,&lda,B,&ldb);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
void cblas_dtrsm(const enum CBLAS_ORDER Order,
                 const enum CBLAS_SIDE Side,
                 const enum CBLAS_UPLO Uplo,
                 const enum CBLAS_TRANSPOSE TransA,
                 const enum CBLAS_DIAG Diag,
                 const gblas_int M,
                 const gblas_int N,
                 const double alpha,
                 const double* A,
                 const gblas_int lda,
                 double* B,
                 const gblas_int ldb)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //variables de entrada
    char SIDE=0,UPLO=0,TA=0,DIAG=0;
    gblas_int MM=0,NN=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posible mensaje informativo en stderr
    GCBLAS_INFO_STDERR("\n*****LLAMADA A BLAS: GCBLAS, ",__func__);
    //comprobamos los posibles errores en los argumentos de entrada
    GCBLAS_ERROR_TRSM(pos,Order,Side,Uplo,TransA,Diag,M,N,lda,ldb);
    //lanzamos el mensaje de error correspondiente, si ha lugar
    pos!=0 ? cblas_xerbla(pos,__func__,"") : (void)NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ajustamos las variables para column major order
    GBLAS_VCMO_TRSM(Order,Side,Uplo,TransA,Diag,M,N,SIDE,UPLO,TA,DIAG,MM,NN);
    //llamamos a la función de Fortran
    GBL_F77_dtrsm(&SIDE,&UPLO,&TA,&DIAG,&MM,&NN,&alpha,A,&lda,B,&ldb);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
