/* -*- coding: utf-8 -*- */
/**
\ingroup gcblas
@{
\file gcoblasf.h
\brief Declaración de las funciones para tipo de dato \p double y \p float de la
       implementación de BLAS en formato Fortran \p COBLAS
       (https://github.com/rljames/coblas), con identificador \p const en los
       argumentos de entrada que lo necesiten.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _GCOBLASF_H_
#define _GCOBLASF_H_
/******************************************************************************/
/******************************************************************************/
#include"mnfblas.h"
#include"tdblas.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
//NIVEL 1 DE BLAS PARA TIPO DE DATO DOUBLE
/******************************************************************************/
int       GBL_F77_daxpy  (const gblas_int *N, const double *A, const double *x,
                          const gblas_int *INCX, double *y,
                          const gblas_int *INCY);
gblas_int GBL_F77_idamax (const gblas_int *N, const double *x,
                          const gblas_int *INCX);
double    GBL_F77_dasum  (const gblas_int *N, const double *x,
                          const gblas_int *INCX);
int       GBL_F77_dcopy  (const gblas_int *N, const double *x,
                          const gblas_int *INCX, double *y,
                          const gblas_int *INCY);
double    GBL_F77_dnrm2  (const gblas_int *N, const double *x,
                          const gblas_int *INCX);
int       GBL_F77_drot   (const gblas_int *N, double *x, const gblas_int *INCX,
                          double *y, const gblas_int *INCY, const double *C,
                          const double *S);
int       GBL_F77_drotg  (double *A, double *B, double *C, double *S);
int       GBL_F77_dscal  (const gblas_int *N, const double *A, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dswap  (const gblas_int *N, double *x, const gblas_int *INCX,
                          double *y, const gblas_int *INCY);
double    GBL_F77_ddot   (const gblas_int *N, const double *x,
                          const gblas_int *INCX, const double *y,
                          const gblas_int *INCY);
int       GBL_F77_drotm  (const gblas_int *N, double *x, const gblas_int *INCX,
                          double *y, const gblas_int *INCY,
                          const double *param);
int       GBL_F77_drotmg (double *d1, double *d2, double *x1, const double *y1,
                          double *param);
/******************************************************************************/
//NIVEL 2 DE BLAS PARA TIPO DE DATO DOUBLE
/******************************************************************************/
int       GBL_F77_dgemv  (const char *Trans, const gblas_int *M,
                          const gblas_int *N, const double *Alpha,
                          const double *A, const gblas_int *LDA,
                          const double *x, const gblas_int *INCX,
                          const double *Beta, double *y, const gblas_int *INCY);
int       GBL_F77_dgbmv  (const char *Trans, const gblas_int *M,
                          const gblas_int *N, const gblas_int *KL,
                          const gblas_int *KU, const double *Alpha,
                          const double *A, const gblas_int *LDA,
                          const double *x, const gblas_int *INCX,
                          const double *Beta, double *y, const gblas_int *INCY);
int       GBL_F77_dsymv  (const char *UPLO, const gblas_int *N,
                          const double *Alpha, const double *A,
                          const gblas_int *LDA, const double *x,
                          const gblas_int *INCX, const double *Beta, double *y,
                          const gblas_int *INCY);
int       GBL_F77_dsbmv  (const char *UPLO, const gblas_int *N,
                          const gblas_int *K, const double *Alpha,
                          const double *A, const gblas_int *LDA,
                          const double *x, const gblas_int *INCX,
                          const double *Beta, double *y, const gblas_int *INCY);
int       GBL_F77_dspmv  (const char *UPLO, const gblas_int *N,
                          const double *Alpha, const double *A,
                          const double *x, const gblas_int *INCX,
                          const double *Beta, double *y,const gblas_int *INCY);
int       GBL_F77_dtrmv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const double *A,
                          const gblas_int *LDA, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dtbmv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const gblas_int *K,
                          const double *A, const gblas_int *LDA, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dtpmv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const double *A, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dtrsv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const double *A,
                          const gblas_int *LDA, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dtbsv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const gblas_int *K,
                          const double *A, const gblas_int *LDA, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dtpsv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const double *A, double *x,
                          const gblas_int *INCX);
int       GBL_F77_dger   (const gblas_int *M, const gblas_int *N,
                          const double *Alpha, const double *x,
                          const gblas_int *INCX, const double *y,
                          const gblas_int *INCY, double *A,
                          const gblas_int *LDA);
int       GBL_F77_dsyr   (const char *UPLO, const gblas_int *N,
                          const double *Alpha, const double *x,
                          const gblas_int *INCX, double *A,
                          const gblas_int *LDA);
int       GBL_F77_dspr   (const char *UPLO, const gblas_int *N,
                          const double *Alpha, const double *x,
                          const gblas_int *INCX, double *A);
int       GBL_F77_dsyr2  (const char *UPLO, const gblas_int *N,
                          const double *Alpha, const double *x,
                          const gblas_int *INCX, const double *y,
                          const gblas_int *INCY, double *A,
                          const gblas_int *LDA);
int       GBL_F77_dspr2  (const char *UPLO, const gblas_int *N,
                          const double *Alpha, const double *x,
                          const gblas_int *INCX, const double *y,
                          const gblas_int *INCY, double *A);
/******************************************************************************/
//NIVEL 3 DE BLAS PARA TIPO DE DATO DOUBLE
/******************************************************************************/
int       GBL_F77_dgemm  (const char *TRANSA, const char *TRANSB,
                          const gblas_int *M, const gblas_int *N,
                          const gblas_int *K, const double *Alpha,
                          const double *A, const gblas_int *LDA,
                          const double *B, const gblas_int *LDB,
                          const double *Beta, double *C, const gblas_int *LDC);
int       GBL_F77_dsymm  (const char *SIDE, const char *UPLO,
                          const gblas_int *M, const gblas_int *N,
                          const double *Alpha, const double *A,
                          const gblas_int *LDA, const double *B,
                          const gblas_int *LDB, const double *Beta, double *C,
                          const gblas_int *LDC);
int       GBL_F77_dsyrk  (const char *UPLO, const char *TRANS,
                          const gblas_int *N, const gblas_int *K,
                          const double *Alpha, const double *A,
                          const gblas_int *LDA, const double *Beta, double *C,
                          const gblas_int *LDC);
int       GBL_F77_dsyr2k (const char *UPLO, const char *TRANS,
                          const gblas_int *N, const gblas_int *K,
                          const double *Alpha, const double *A,
                          const gblas_int *LDA, const double *B,
                          const gblas_int *LDB, const double *Beta, double *C,
                          const gblas_int *LDC);
int       GBL_F77_dtrmm  (const char *SIDE, const char *UPLO, const char *TRANS,
                          const char *DIAG, const gblas_int *M,
                          const gblas_int *N, const double *Alpha,
                          const double *A, const gblas_int *LDA, double *B,
                          const gblas_int *LDB);
int       GBL_F77_dtrsm  (const char *SIDE, const char *UPLO, const char *TRANS,
                          const char *DIAG, const gblas_int *M,
                          const gblas_int *N, const double *Alpha,
                          const double *A, const gblas_int *LDA, double *B,
                          const gblas_int *LDB);
/******************************************************************************/
/******************************************************************************/
//NIVEL 1 DE BLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
int       GBL_F77_saxpy  (const gblas_int *N, const float *A, const float *x,
                          const gblas_int *INCX, float *y,
                          const gblas_int *INCY);
gblas_int GBL_F77_isamax (const gblas_int *N, const float *x,
                          const gblas_int *INCX);
float     GBL_F77_sasum  (const gblas_int *N, const float *x,
                          const gblas_int *INCX);
int       GBL_F77_scopy  (const gblas_int *N, const float *x,
                          const gblas_int *INCX, float *y,
                          const gblas_int *INCY);
float     GBL_F77_snrm2  (const gblas_int *N, const float *x,
                          const gblas_int *INCX);
int       GBL_F77_srot   (const gblas_int *N, float *x, const gblas_int *INCX,
                          float *y, const gblas_int *INCY, const float *C,
                          const float *S);
int       GBL_F77_srotg  (float *A, float *B, float *C, float *S);
int       GBL_F77_sscal  (const gblas_int *N, const float *A, float *x,
                          const gblas_int *INCX);
int       GBL_F77_sswap  (const gblas_int *N, float *x, const gblas_int *INCX,
                          float *y, const gblas_int *INCY);
float     GBL_F77_sdot   (const gblas_int *N, const float *x,
                          const gblas_int *INCX, const float *y,
                          const gblas_int *INCY);
int       GBL_F77_srotm  (const gblas_int *N, float *x, const gblas_int *INCX,
                          float *y, const gblas_int *INCY, const float *param);
int       GBL_F77_srotmg (float *d1, float *d2, float *x1, const float *y1,
                          float *param);
/******************************************************************************/
//NIVEL 2 DE BLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
int       GBL_F77_sgemv  (const char *Trans, const gblas_int *M,
                          const gblas_int *N, const float *Alpha,
                          const float *A, const gblas_int *LDA,
                          const float *x, const gblas_int *INCX,
                          const float *Beta, float *y, const gblas_int *INCY);
int       GBL_F77_sgbmv  (const char *Trans, const gblas_int *M,
                          const gblas_int *N, const gblas_int *KL,
                          const gblas_int *KU, const float *Alpha,
                          const float *A, const gblas_int *LDA,
                          const float *x, const gblas_int *INCX,
                          const float *Beta, float *y, const gblas_int *INCY);
int       GBL_F77_ssymv  (const char *UPLO, const gblas_int *N,
                          const float *Alpha, const float *A,
                          const gblas_int *LDA, const float *x,
                          const gblas_int *INCX, const float *Beta, float *y,
                          const gblas_int *INCY);
int       GBL_F77_ssbmv  (const char *UPLO, const gblas_int *N,
                          const gblas_int *K, const float *Alpha,
                          const float *A, const gblas_int *LDA,
                          const float *x, const gblas_int *INCX,
                          const float *Beta, float *y, const gblas_int *INCY);
int       GBL_F77_sspmv  (const char *UPLO, const gblas_int *N,
                          const float *Alpha, const float *A,
                          const float *x, const gblas_int *INCX,
                          const float *Beta, float *y,const gblas_int *INCY);
int       GBL_F77_strmv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const float *A,
                          const gblas_int *LDA, float *x,
                          const gblas_int *INCX);
int       GBL_F77_stbmv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const gblas_int *K,
                          const float *A, const gblas_int *LDA, float *x,
                          const gblas_int *INCX);
int       GBL_F77_stpmv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const float *A, float *x,
                          const gblas_int *INCX);
int       GBL_F77_strsv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const float *A,
                          const gblas_int *LDA, float *x,
                          const gblas_int *INCX);
int       GBL_F77_stbsv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const gblas_int *K,
                          const float *A, const gblas_int *LDA, float *x,
                          const gblas_int *INCX);
int       GBL_F77_stpsv  (const char *UPLO, const char *TRANS, const char *DIAG,
                          const gblas_int *N, const float *A, float *x,
                          const gblas_int *INCX);
int       GBL_F77_sger   (const gblas_int *M, const gblas_int *N,
                          const float *Alpha, const float *x,
                          const gblas_int *INCX, const float *y,
                          const gblas_int *INCY, float *A,
                          const gblas_int *LDA);
int       GBL_F77_ssyr   (const char *UPLO, const gblas_int *N,
                          const float *Alpha, const float *x,
                          const gblas_int *INCX, float *A,
                          const gblas_int *LDA);
int       GBL_F77_sspr   (const char *UPLO, const gblas_int *N,
                          const float *Alpha, const float *x,
                          const gblas_int *INCX, float *A);
int       GBL_F77_ssyr2  (const char *UPLO, const gblas_int *N,
                          const float *Alpha, const float *x,
                          const gblas_int *INCX, const float *y,
                          const gblas_int *INCY, float *A,
                          const gblas_int *LDA);
int       GBL_F77_sspr2  (const char *UPLO, const gblas_int *N,
                          const float *Alpha, const float *x,
                          const gblas_int *INCX, const float *y,
                          const gblas_int *INCY, float *A);
/******************************************************************************/
//NIVEL 3 DE BLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
int       GBL_F77_sgemm  (const char *TRANSA, const char *TRANSB,
                          const gblas_int *M, const gblas_int *N,
                          const gblas_int *K, const float *Alpha,
                          const float *A, const gblas_int *LDA,
                          const float *B, const gblas_int *LDB,
                          const float *Beta, float *C, const gblas_int *LDC);
int       GBL_F77_ssymm  (const char *SIDE, const char *UPLO,
                          const gblas_int *M, const gblas_int *N,
                          const float *Alpha, const float *A,
                          const gblas_int *LDA, const float *B,
                          const gblas_int *LDB, const float *Beta, float *C,
                          const gblas_int *LDC);
int       GBL_F77_ssyrk  (const char *UPLO, const char *TRANS,
                          const gblas_int *N, const gblas_int *K,
                          const float *Alpha, const float *A,
                          const gblas_int *LDA, const float *Beta, float *C,
                          const gblas_int *LDC);
int       GBL_F77_ssyr2k (const char *UPLO, const char *TRANS,
                          const gblas_int *N, const gblas_int *K,
                          const float *Alpha, const float *A,
                          const gblas_int *LDA, const float *B,
                          const gblas_int *LDB, const float *Beta, float *C,
                          const gblas_int *LDC);
int       GBL_F77_strmm  (const char *SIDE, const char *UPLO, const char *TRANS,
                          const char *DIAG, const gblas_int *M,
                          const gblas_int *N, const float *Alpha,
                          const float *A, const gblas_int *LDA, float *B,
                          const gblas_int *LDB);
int       GBL_F77_strsm  (const char *SIDE, const char *UPLO, const char *TRANS,
                          const char *DIAG, const gblas_int *M,
                          const gblas_int *N, const float *Alpha,
                          const float *A, const gblas_int *LDA, float *B,
                          const gblas_int *LDB);
/******************************************************************************/
/******************************************************************************/
//RUTINA DE AVISO DE ERROR
int       GBL_F77_xerbla (const char *srname, int *info);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */

