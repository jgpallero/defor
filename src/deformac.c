/**
\addtogroup deformacion
@{
\file deformac.c
\brief Definicion de la estructura y funciones para el calculo de deformaciones
       entre puntos.

En este fichero se definen la estructura y todas las funciones necesarias para
el calculo de deformaciones en puntos singulares a partir de sus coordenadas
cartesianas tridimensionales geocentricas.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 24 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
 */
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"errores.h"
#include"general.h"
#include"ptos.h"
#include"tran.h"
#include"geodesia.h"
#include"deformac.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void InicializaDeformac(deformac* defor)
{
    //inicializamos el contador de puntos a 0
    defor->nPuntos = 0;
    //inicializamos los miembros que son punteros a NULL
    defor->puntos = NULL;
    defor->sistAntes = NULL;
    defor->sistDespues = NULL;
    defor->indices = NULL;
    defor->epocas = NULL;
    defor->pos = NULL;
    defor->def = NULL;
    defor->vcdef = NULL;
    defor->elipse = NULL;
    defor->tsr = NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int AsignaMemoriaDeformac(deformac* defor,
                          int elementos)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de elementos es correcto
    if(elementos<=0)
    {
        //escribimos el mensaje de error
        MensajeErrorNELEMMENORCERO((char*)__func__);
        //salimos de la funcion
        return ERRNELEMMENORCERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aumentamos el contador de puntos
    defor->nPuntos += elementos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las filas a las matrices de datos
    defor->puntos=(char**)realloc(defor->puntos,
                                          (size_t)defor->nPuntos*sizeof(char*));
    defor->sistAntes=(char**)realloc(defor->sistAntes,
                                          (size_t)defor->nPuntos*sizeof(char*));
    defor->sistDespues=(char**)realloc(defor->sistDespues,
                                          (size_t)defor->nPuntos*sizeof(char*));
    defor->indices=(int**)realloc(defor->indices,
                                           (size_t)defor->nPuntos*sizeof(int*));
    defor->epocas=(double**)realloc(defor->epocas,
                                        (size_t)defor->nPuntos*sizeof(double*));
    defor->pos=(double**)realloc(defor->pos,
                                        (size_t)defor->nPuntos*sizeof(double*));
    defor->def=(double**)realloc(defor->def,
                                        (size_t)defor->nPuntos*sizeof(double*));
    defor->vcdef=(double**)realloc(defor->vcdef,
                                        (size_t)defor->nPuntos*sizeof(double*));
    defor->elipse=(double**)realloc(defor->elipse,
                                        (size_t)defor->nPuntos*sizeof(double*));
    defor->tsr=(int*)realloc(defor->tsr,(size_t)defor->nPuntos*sizeof(int));
    //controlamos los posibles errores en la asignacion de memoria
    if((defor->puntos==NULL)||(defor->sistAntes==NULL)||
       (defor->sistDespues==NULL)||(defor->indices==NULL)||
       (defor->epocas==NULL)||(defor->pos==NULL)||(defor->def==NULL)||
       (defor->vcdef==NULL)||(defor->elipse==NULL)||(defor->tsr==NULL))
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las columnas a las matrices de datos
    for(i=0;i<elementos;i++)
    {
        defor->puntos[defor->nPuntos-i-1]=(char*)malloc(LONMAXLINPTOS+1);
        defor->sistAntes[defor->nPuntos-i-1]=(char*)malloc(LONMAXLINTRAN+1);
        defor->sistDespues[defor->nPuntos-i-1]=(char*)malloc(LONMAXLINTRAN+1);
        defor->indices[defor->nPuntos-i-1]=(int*)malloc(2*sizeof(int));
        defor->epocas[defor->nPuntos-i-1]=(double*)malloc(2*sizeof(double));
        defor->pos[defor->nPuntos-i-1]=(double*)malloc(3*sizeof(double));
        defor->def[defor->nPuntos-i-1]=(double*)malloc(3*sizeof(double));
        defor->vcdef[defor->nPuntos-i-1]=(double*)malloc(6*sizeof(double));
        defor->elipse[defor->nPuntos-i-1]=(double*)malloc(3*sizeof(double));
        //controlamos los posibles errores en la asignacion de memoria
        if((defor->puntos[defor->nPuntos-i-1]==NULL)||
           (defor->sistAntes[defor->nPuntos-i-1]==NULL)||
           (defor->sistDespues[defor->nPuntos-i-1]==NULL)||
           (defor->indices[defor->nPuntos-i-1]==NULL)||
           (defor->epocas[defor->nPuntos-i-1]==NULL)||
           (defor->pos[defor->nPuntos-i-1]==NULL)||
           (defor->def[defor->nPuntos-i-1]==NULL)||
           (defor->vcdef[defor->nPuntos-i-1]==NULL)||
           (defor->elipse[defor->nPuntos-i-1]==NULL))
        {
            //escribimos el mensaje de error
            MensajeErrorNOMEMORIA((char*)__func__);
            //salimos de la funcion
            return ERRNOMEMORIA;
        }
        //vamos inicializando a 0 el vector de indices
        defor->indices[defor->nPuntos-i-1][0] = 0;
        defor->indices[defor->nPuntos-i-1][1] = 0;
        //vamos inicializando a 0 el vector de epocas
        defor->epocas[defor->nPuntos-i-1][0] = 0.0;
        defor->epocas[defor->nPuntos-i-1][1] = 0.0;
        //vamos inicializando a 0 la matriz de coordenadas
        defor->pos[defor->nPuntos-i-1][0] = 0.0;
        defor->pos[defor->nPuntos-i-1][1] = 0.0;
        defor->pos[defor->nPuntos-i-1][2] = 0.0;
        //vamos inicializando a 0 la matriz de deformacion
        defor->def[defor->nPuntos-i-1][0] = 0.0;
        defor->def[defor->nPuntos-i-1][1] = 0.0;
        defor->def[defor->nPuntos-i-1][2] = 0.0;
        //vamos inicializando a 0 la matriz de varianza-covarianza
        defor->vcdef[defor->nPuntos-i-1][0] = 0.0;
        defor->vcdef[defor->nPuntos-i-1][1] = 0.0;
        defor->vcdef[defor->nPuntos-i-1][2] = 0.0;
        defor->vcdef[defor->nPuntos-i-1][3] = 0.0;
        defor->vcdef[defor->nPuntos-i-1][4] = 0.0;
        defor->vcdef[defor->nPuntos-i-1][5] = 0.0;
        //vamos inicializando a 0 la matriz de elipses de error
        defor->elipse[defor->nPuntos-i-1][0] = 0.0;
        defor->elipse[defor->nPuntos-i-1][1] = 0.0;
        defor->elipse[defor->nPuntos-i-1][2] = 0.0;
        //vamos inicializando a 0 el vector de indicadores de transformacion
        //entre sistemas de referencia
        defor->tsr[defor->nPuntos-i-1] = 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LiberaMemoriaDeformac(deformac* defor)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //verificamos si hemos pasado una estructura vacia
    if(!defor->nPuntos)
    {
        //salimos de la funcion
        return;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada a las matrices y a los array de cadenas de
    //caracteres
    for(i=0;i<defor->nPuntos;i++)
    {
        free(defor->puntos[i]);
        free(defor->sistAntes[i]);
        free(defor->sistDespues[i]);
        free(defor->indices[i]);
        free(defor->epocas[i]);
        free(defor->pos[i]);
        free(defor->def[i]);
        free(defor->vcdef[i]);
        free(defor->elipse[i]);
    }
    free(defor->puntos);
    free(defor->sistAntes);
    free(defor->sistDespues);
    free(defor->indices);
    free(defor->epocas);
    free(defor->pos);
    free(defor->def);
    free(defor->vcdef);
    free(defor->elipse);
    free(defor->tsr);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el contador de puntos a 0
    defor->nPuntos = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void CambiaSistRef(posptos* antes,
                   posptos* despues,
                   partran* transf,
                   int posA,
                   int posD,
                   int* hecho,
                   double* x,
                   double* y,
                   double* z,
                   double* varx,
                   double* varxy,
                   double* varxz,
                   double* vary,
                   double* varyz,
                   double* varz)
{
    //indicador de error
    int cError=ERRNOERROR;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion
    cError = PuntoSistRefTransf(transf,
                                despues->sistRef[posD],antes->sistRef[posA],
                                despues->epocas[posD],
                                despues->pos[posD][0],despues->pos[posD][1],
                                despues->pos[posD][2],
                                despues->vcpos[posD][0],despues->vcpos[posD][1],
                                despues->vcpos[posD][2],despues->vcpos[posD][3],
                                despues->vcpos[posD][4],despues->vcpos[posD][5],
                                x,y,z,
                                varx,varxy,varxz,vary,varyz,varz);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si se hacho transformacion o no
    if(cError==ERRNOERROR)
    {
        *hecho = SITSR;
    }
    else
    {
        *hecho = NOTSR;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int CalcDeforDeformac(posptos* antes,
                      posptos* despues,
                      deformac* defor)
{
    //codigo de error
    int cError=ERRNOERROR;
    //indices para recorrer bucles
    int i=0,j=0;
    //codigo de los puntos de trabajo
    char codAntes[LONMAXLINPTOS+2],codDespues[LONMAXLINPTOS+2];
    //variables para almacenar la posicion de los puntos en coordenadas
    //geodesicas
    double lat=0.0,lon=0.0,h=0.0;
    //variables para almacenar la deformacion y su error en el sistema
    //cartesiano tridimensional geocentrico
    double dx=0.0,dy=0.0,dz=0.0;
    double vcdx=0.0,vcdxdy=0.0,vcdxdz=0.0,vcdy=0.0,vcdydz=0.0,vcdz=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos todos los puntos de la estructura despues
    for(i=0;i<despues->nPuntos;i++)
    {
        //extraemos el codigo del punto de trabajo
        strcpy(codDespues,despues->puntos[i]);
        //le quitamos los espacios en blanco que pueda tener
        QuitaEspacios(codDespues);
        //recorremos todos los puntos de la estructura antes
        for(j=0;j<antes->nPuntos;j++)
        {
            //extraemos el codigo del punto de trabajo
            strcpy(codAntes,antes->puntos[j]);
            //le quitamos los espacios en blanco que pueda tener
            QuitaEspacios(codAntes);
            //calculamos deformacion si el punto es el mismo
            if(!strcmp(codDespues,codAntes))
            {
                //asignamos memoria para un nuevo punto
                cError = AsignaMemoriaDeformac(defor,1);
                //comprobamos el codigo de error devuelto
                if(cError!=ERRNOERROR)
                {
                    //lanzamos el mensaje de error
                    MensajeErrorPropagado((char*)__func__,
                                          "AsignaMemoriaDeformac()");
                    //salimos de la funcion
                    return cError;
                }
                //asignamos el codigo del punto a la estructura defor
                strcpy(defor->puntos[defor->nPuntos-1],antes->puntos[j]);
                //asignamos el sistema de referencia antes de la deformacion
                strcpy(defor->sistAntes[defor->nPuntos-1],antes->sistRef[j]);
                //asignamos el sistema de referencia despues de la deformacion
                strcpy(defor->sistDespues[defor->nPuntos-1],
                       despues->sistRef[i]);
                //asignamos el indice de la estructura antes
                defor->indices[defor->nPuntos-1][0] = j;
                //asignamos el indice de la estructura despues
                defor->indices[defor->nPuntos-1][1] = i;
                //asignamos la epoca de la estructura antes
                defor->epocas[defor->nPuntos-1][0] = antes->epocas[j];
                //asignamos la epoca de la estructura despues
                defor->epocas[defor->nPuntos-1][1] = despues->epocas[i];
                //asignamos las coordenadas del punto antes de la deformacion
                defor->pos[defor->nPuntos-1][0] = antes->pos[j][0];
                defor->pos[defor->nPuntos-1][1] = antes->pos[j][1];
                defor->pos[defor->nPuntos-1][2] = antes->pos[j][2];
                //calculamos la deformacion en el sistema geocentrico
                RestaEpocasVC(antes->pos[j][0],antes->pos[j][1],
                              antes->pos[j][2],
                              antes->vcpos[j][0],antes->vcpos[j][1],
                              antes->vcpos[j][2],antes->vcpos[j][3],
                              antes->vcpos[j][4],antes->vcpos[j][5],
                              despues->pos[i][0],despues->pos[i][1],
                              despues->pos[i][2],
                              despues->vcpos[i][0],despues->vcpos[i][1],
                              despues->vcpos[i][2],despues->vcpos[i][3],
                              despues->vcpos[i][4],despues->vcpos[i][5],
                              &dx,&dy,&dz,
                              &vcdx,&vcdxdy,&vcdxdz,&vcdy,&vcdydz,&vcdz);
                //calculamos la posicion en geodesicas del punto antes de la
                //deformacion
                TriGeo(SEMI_MAYOR,APL,
                       defor->pos[defor->nPuntos-1][0],
                       defor->pos[defor->nPuntos-1][1],
                       defor->pos[defor->nPuntos-1][2],
                       &lat,&lon,&h);
                //transformamos al plano horizonte local
                TriEnuVC(lat,lon,
                         dx,dy,dz,
                         vcdx,vcdxdy,vcdxdz,vcdy,vcdydz,vcdz,
                         &defor->def[defor->nPuntos-1][0],
                         &defor->def[defor->nPuntos-1][1],
                         &defor->def[defor->nPuntos-1][2],
                         &defor->vcdef[defor->nPuntos-1][0],
                         &defor->vcdef[defor->nPuntos-1][1],
                         &defor->vcdef[defor->nPuntos-1][2],
                         &defor->vcdef[defor->nPuntos-1][3],
                         &defor->vcdef[defor->nPuntos-1][4],
                         &defor->vcdef[defor->nPuntos-1][5]);
                //calculamos las elipses de error
                ElipseErrorPlano(defor->vcdef[defor->nPuntos-1][0],
                                 defor->vcdef[defor->nPuntos-1][1],
                                 defor->vcdef[defor->nPuntos-1][3],
                                 &defor->elipse[defor->nPuntos-1][0],
                                 &defor->elipse[defor->nPuntos-1][1],
                                 &defor->elipse[defor->nPuntos-1][2]);
                //metemos el angulo de orientacion de la elipse en el dominio
                //del acimut
                defor->elipse[defor->nPuntos-1][2] =
                           AnguloMateAcimut(defor->elipse[defor->nPuntos-1][2]);
                //activamos el indicador de transformacion entre sistemas de
                //referencia
                defor->tsr[defor->nPuntos-1] = SITSR;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int CalcDeforTransfDeformac(posptos* antes,
                            posptos* despues,
                            partran* transf,
                            deformac* defor)
{
    //codigo de error
    int cError=ERRNOERROR;
    //indices para recorrer bucles
    int i=0,j=0;
    //codigo de los puntos de trabajo
    char codAntes[LONMAXLINPTOS+2],codDespues[LONMAXLINPTOS+2];
    //sistemas de referencia de los puntos de trabajo
    char sistAntes[LONMAXLINPTOS+2],sistDespues[LONMAXLINPTOS+2];
    //indicador de transformacion realizada
    int transfHecha=0;
    //variables para almacenar las coordenadas transformadas y su error en el
    //sistema cartesiano tridimensional geocentrico
    double x=0.0,y=0.0,z=0.0;
    double varx=0.0,varxy=0.0,varxz=0.0,vary=0.0,varyz=0.0,varz=0.0;
    //variables para almacenar la posicion de los puntos en coordenadas
    //geodesicas
    double lat=0.0,lon=0.0,h=0.0;
    //variables para almacenar la deformacion y su error en el sistema
    //cartesiano tridimensional geocentrico
    double dx=0.0,dy=0.0,dz=0.0;
    double vcdx=0.0,vcdxdy=0.0,vcdxdz=0.0,vcdy=0.0,vcdydz=0.0,vcdz=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos todos los puntos de la estructura despues
    for(i=0;i<despues->nPuntos;i++)
    {
        //extraemos el codigo del punto de trabajo
        strcpy(codDespues,despues->puntos[i]);
        //le quitamos los espacios en blanco que pueda tener
        QuitaEspacios(codDespues);
        //extraemos el codigo del sistema de referencia
        strcpy(sistDespues,despues->sistRef[i]);
        //le quitamos los espacios en blanco que pueda tener
        QuitaEspacios(sistDespues);
        //recorremos todos los puntos de la estructura antes
        for(j=0;j<antes->nPuntos;j++)
        {
            //extraemos el codigo del punto de trabajo
            strcpy(codAntes,antes->puntos[j]);
            //le quitamos los espacios en blanco que pueda tener
            QuitaEspacios(codAntes);
            //extraemos el codigo del sistema de referencia
            strcpy(sistAntes,antes->sistRef[j]);
            //le quitamos los espacios en blanco que pueda tener
            QuitaEspacios(sistAntes);
            //calculamos deformacion si el punto es el mismo
            if(!strcmp(codDespues,codAntes))
            {
                //realizamos cambio de sistema de referencia, si ha lugar
                if(strcmp(sistDespues,sistAntes))
                {
                    //realizamos el cambio de sistema de referencia
                    CambiaSistRef(antes,despues,transf,j,i,
                                  &transfHecha,
                                  &x,&y,&z,
                                  &varx,&varxy,&varxz,&vary,&varyz,&varz);
                }
                else
                {
                    //las coordenadas no cambian
                    x = despues->pos[i][0];
                    y = despues->pos[i][1];
                    z = despues->pos[i][2];
                    varx = despues->vcpos[i][0];
                    varxy = despues->vcpos[i][1];
                    varxz = despues->vcpos[i][2];
                    vary = despues->vcpos[i][3];
                    varyz = despues->vcpos[i][4];
                    varz = despues->vcpos[i][5];
                    //se marca la transformacion como hecha porque no era
                    //obligatorio hacerla
                    transfHecha = SITSR;
                }
                //asignamos memoria para un nuevo punto
                cError = AsignaMemoriaDeformac(defor,1);
                //comprobamos el codigo de error devuelto
                if(cError!=ERRNOERROR)
                {
                    //lanzamos el mensaje de error
                    MensajeErrorPropagado((char*)__func__,
                                          "AsignaMemoriaDeformac()");
                    //salimos de la funcion
                    return cError;
                }
                //asignamos el codigo del punto a la estructura defor
                strcpy(defor->puntos[defor->nPuntos-1],antes->puntos[j]);
                //asignamos el sistema de referencia antes de la deformacion
                strcpy(defor->sistAntes[defor->nPuntos-1],antes->sistRef[j]);
                //asignamos el sistema de referencia despues de la deformacion
                strcpy(defor->sistDespues[defor->nPuntos-1],
                       despues->sistRef[i]);
                //asignamos el indice de la estructura antes
                defor->indices[defor->nPuntos-1][0] = j;
                //asignamos el indice de la estructura despues
                defor->indices[defor->nPuntos-1][1] = i;
                //asignamos la epoca de la estructura antes
                defor->epocas[defor->nPuntos-1][0] = antes->epocas[j];
                //asignamos la epoca de la estructura despues
                defor->epocas[defor->nPuntos-1][1] = despues->epocas[i];
                //asignamos las coordenadas del punto antes de la deformacion
                defor->pos[defor->nPuntos-1][0] = antes->pos[j][0];
                defor->pos[defor->nPuntos-1][1] = antes->pos[j][1];
                defor->pos[defor->nPuntos-1][2] = antes->pos[j][2];
                //calculamos la deformacion en el sistema geocentrico
                RestaEpocasVC(antes->pos[j][0],antes->pos[j][1],
                              antes->pos[j][2],
                              antes->vcpos[j][0],antes->vcpos[j][1],
                              antes->vcpos[j][2],antes->vcpos[j][3],
                              antes->vcpos[j][4],antes->vcpos[j][5],
                              x,y,z,
                              varx,varxy,varxz,vary,varyz,varz,
                              &dx,&dy,&dz,
                              &vcdx,&vcdxdy,&vcdxdz,&vcdy,&vcdydz,&vcdz);
                //calculamos la posicion en geodesicas del punto antes de la
                //deformacion
                TriGeo(SEMI_MAYOR,APL,
                       defor->pos[defor->nPuntos-1][0],
                       defor->pos[defor->nPuntos-1][1],
                       defor->pos[defor->nPuntos-1][2],
                       &lat,&lon,&h);
                //transformamos al plano horizonte local
                TriEnuVC(lat,lon,
                         dx,dy,dz,
                         vcdx,vcdxdy,vcdxdz,vcdy,vcdydz,vcdz,
                         &defor->def[defor->nPuntos-1][0],
                         &defor->def[defor->nPuntos-1][1],
                         &defor->def[defor->nPuntos-1][2],
                         &defor->vcdef[defor->nPuntos-1][0],
                         &defor->vcdef[defor->nPuntos-1][1],
                         &defor->vcdef[defor->nPuntos-1][2],
                         &defor->vcdef[defor->nPuntos-1][3],
                         &defor->vcdef[defor->nPuntos-1][4],
                         &defor->vcdef[defor->nPuntos-1][5]);
                //calculamos las elipses de error
                ElipseErrorPlano(defor->vcdef[defor->nPuntos-1][0],
                                 defor->vcdef[defor->nPuntos-1][1],
                                 defor->vcdef[defor->nPuntos-1][3],
                                 &defor->elipse[defor->nPuntos-1][0],
                                 &defor->elipse[defor->nPuntos-1][1],
                                 &defor->elipse[defor->nPuntos-1][2]);
                //metemos el angulo de orientacion de la elipse en el dominio
                //del acimut
                defor->elipse[defor->nPuntos-1][2] =
                           AnguloMateAcimut(defor->elipse[defor->nPuntos-1][2]);
                //activamos o desactivamos el indicador de transformacion entre
                //sistemas de referencia
                if(transfHecha==SITSR)
                {
                    defor->tsr[defor->nPuntos-1] = SITSR;
                }
                else
                {
                    defor->tsr[defor->nPuntos-1] = NOTSR;
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
