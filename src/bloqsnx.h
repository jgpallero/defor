/**
\addtogroup sinex
@{
\file bloqsnx.h
\brief Declaracion de la estructura bloqsnx y sus funciones asociadas.

En este fichero se declaran la estructura bloqsnx y todas las funciones
necesarias para la extraccion del nombre de los bloques de datos existentes en
un fichero SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _BLOQSNX_H_
#define _BLOQSNX_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include"paramsnx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\struct bloqsnx
\brief Estructura contenedora de los bloques existentes en un fichero SINEX.

Es esta estructura se almacena un listado de los bloques existentes en un
fichero SINEX, asi como informacion acerca de las zonas del fichero donde
comienzan y terminan dichos bloques.
*/
typedef struct
{
    /** \brief Indicador del numero de version del fichero SINEX procesado. */
    /**
    La longitud asignada a esta cadena de texto es igual a #LONMAXLINSNX+1. */
    char version[LONMAXLINSNX+1];
    /** \brief Numero de bloques que contiene el fichero. */
    int nBloques;
    /** \brief Nombres de los bloques que hay en el fichero. */
    /**
    Array de cadenas de texto para almacenar los nombres de los bloques
    encontrados en el fichero SINEX. La longitud asignada a cada cadena del
    array es igual a #LONMAXLINSNX+1. Este array contiene tantas cadenas de
    texto como elementos tenga bloqsnx::nBloques. La numeracion de elementos
    comienza en 0.
    */
    char** bloques;
    /** \brief Posiciones de inicio y final de cada bloque en el fichero. */
    /**
    Matriz de dos columnas que almacena las direcciones de memoria de
    comienzo de las lineas que contienen las etiquetas de principio y final
    de los bloques del fichero. Esta matriz tiene tantas filas como
    elementos tenga bloqsnx::nBloques. La numeracion de elementos comienza
    en 0. Cada fila contiene la informacion asociada al bloque almacenado
    con el mismo indice en bloqsnx::bloques. Las columnas corresponden a:
        - Col. 0: Direccion de comienzo de la linea que contiene la etiqueta de
                  principio de bloque.
        - Col. 1: Direccion de comienzo de la etiqueta que senyala el final de
                  bloque.
    */
    long int** posicion;
    /** \brief Numeros de linea de inicio y final de cada bloque. */
    /**
    Matriz de dos columnas que almacena los numeros de linea de las lineas
    que contienen las etiquetas de principio y final de los bloques del
    fichero. Esta matriz tiene tantas filas como elementos tenga
    bloqsnx::nBloques. La numeracion de elementos comienza en 0. Cada fila
    contiene la informacion asociada al bloque almacenado con el mismo
    indice en bloqsnx::bloques. Las columnas corresponden a:
        - Col. 0: Numero de la linea que contiene la etiqueta de principio de
                  bloque.
        - Col. 1: Numero de la linea que contiene la etiqueta de final de
                  bloque.
    */
    int** lineas;
}bloqsnx;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Inicializa una estructura bloqsnx.

Pone el campo bloqsnx::nBloques de la estructura a 0 e inicializa a NULL todos
los elementos de la estructura que sean punteros.
\param[in] bloques Puntero a una estructura bloqsnx.
\note Es necesario inicializar una estructura bloqsnx antes de su uso porque
      muchas funciones comprueban el estado del campo bloqsnx::nBloques para su
      correcto funcionamiento y si la estructura no se ha inicializado podrian
      producirse errores.
*/
void InicializaBloqSnx(bloqsnx* bloques);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Asigna memoria para un numero de elementos dado en una estructura
       bloqsnx, anyadienolos a los que existan previamente.
\param[in] bloques Puntero a una estructura bloqsnx.
\param[in] elementos Numero de elementos para los cuales se asignara memoria.
\return Codigo de error.
\note La estructura bloqsnx pasada ha de estar inicializada.
\note Esta funcion actualiza el campo bloqsnx::nBloques de la estructura.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura bloqsnx pasada
      no es liberada.
*/
int AsignaMemoriaBloqSnx(bloqsnx* bloques,
                         int elementos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Libera la memoria ocupada por una estructura bloqsnx.
\param[in] bloques Puntero a una estructura bloqsnx.
\note Ademas de liberar la memoria ocupada por los elementos que son punteros de
      la estructura, esta funcion pone a cero el campo bloqsnx::nBloques. El
      campo bloqsnx::version mantiene su valor original previo a la liberacion
      de memoria.
*/
void LiberaMemoriaBloqSnx(bloqsnx* bloques);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee los bloques existentes en un fichero SINEX.
\param[in] idFichero Identificador de fichero abierto para leer.
\param[in] inicioBloque Cadena de caracteres identificativa del comienzo de un
                        bloque.
\param[in] finBloque Cadena de caracteres identificativa del final de un bloque.
\param[in] bloques Puntero a una estructura bloqsnx en la que sus campos
                   contienen toda la informacion relevante que identifica el
                   tipo y la posicion de todos los bloques existentes.
\return Codigo de error.
\note Esta funcion espera que el puntero de L/E este al comienzo del fichero
      SINEX abierto.
\note El puntero de L/E es devuelto a su posicion original en el fichero tras la
      ejecucion de esta funcion.
\note La estructura bloqsnx pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura bloqsnx pasada
      no es liberada y el fichero apuntado por idFichero no es cerrado.
*/
int LeeBloquesBloqSnx(FILE* idFichero,
                      char inicioBloque[],
                      char finBloque[],
                      bloqsnx* bloques);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la posicion en la matriz bloques de una estructura bloqsnx que
       ocupa un identificador de bloque pasado.
\param[in] bloques Puntero a una estructura bloqsnx.
\param[in] idBloque Identificador del bloque a buscar.
\return Indentificador de la posicion del bloque pasado:
        - #IDNOBLOQUESNX: Si el identificador de bloque pasado no se encuentra
                          en la estructura.
        - Si el identificador pasado se encuentra en la estructura se devuelve
          la posicion en que se encuentra en la matriz correspondiente.
*/
int PosicionBloqueBloqSnx(bloqsnx* bloques,
                          char idBloque[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
