/**
\addtogroup puntos
\ingroup deformacion
@{
\file paramptos.h
\brief Archivo de definicion de constantes para el trabajo con ficheros de base
       de datos de puntos.

En este fichero se definen las constantes simbolicas necesarias para identificar
los bloques de datos que componen un fichero de base de datos de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com,jlgpallero@pdi.ucm.es
\date 13 de julio de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves de
las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _PARAMPTOS_H_
#define _PARAMPTOS_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def LONMAXLINPTOS
    \brief Longitud maxima de las lineas de un fichero de base de datos de
           puntos. */
#define LONMAXLINPTOS  80
/** \def CADINIBLOQPTOS
    \brief Cadena identificadora de comienzo de bloque de un fichero de base de
           datos de puntos. */
#define CADINIBLOQPTOS "+"
/** \def CADFINBLOQPTOS
    \brief Cadena identificadora de final de bloque de un fichero de base de
           datos de puntos. */
#define CADFINBLOQPTOS "-"
/** \def SEPCADINIBLOQETIQBLOQPTOS
    \brief Cadena de separacion entre la cadena identificadora de inicio de
           bloque y la cadena identificadora del bloque. */
#define SEPCADINIBLOQETIQBLOQPTOS ""
/** \def SEPCADFINBLOQETIQBLOQPTOS
    \brief Cadena de separacion entre la cadena identificadora de fin de bloque
           y la cadena identificadora del bloque. */
#define SEPCADFINBLOQETIQBLOQPTOS ""
/** \def SEPELEMETIQBLOQPTOS
    \brief Cadena de separacion entre la cadena identificadora de bloque y otros
           elementos informativos del bloque, si existen */
#define SEPELEMETIQBLOQPTOS " "
/** \def CADIDPUNTOPTOS
    \brief Cadena identificadora de inicio de punto en un bloque de un fichero
           de base de datos de puntos. */
#define CADIDPUNTOPTOS ">"
/** \def POSINIIDPUNTOPTOS
    \brief Posicion en una linea en la que comienza el indicador de inicio de
           punto en un bloque de un fichero de base de datos de puntos (las
           posiciones comienzan en 0). */
#define POSINIIDPUNTOPTOS  0
/** \def SEPCADIDPUNTOINIPTOPTOS
    \brief Cadena de separacion entre la cadena identificadora de inicio de
           punto y el comienzo de la definicion del punto en un bloque de un
           fichero de base de datos de puntos. */
#define SEPCADIDPUNTOINIPTOPTOS ""
/** \def SEPCAMPOSPUNTOPTOS
    \brief Cadena de separacion entre los campos de las lineas de definicion de
           un punto en un bloque de un fichero de base de datos de puntos. */
#define SEPCAMPOSPUNTOPTOS " "
/** \def CADCOMENTPTOS
    \brief Cadena identificadora de linea de comentario de un fichero de base de
           datos de puntos. */
#define CADCOMENTPTOS  "*"
/** \def POSINICOMENTPTOS
    \brief Posicion en una linea en la que comienza el indicador de comentario
           (las posiciones comienzan en 0). */
#define POSINICOMENTPTOS  0
/** \def IDNOBLOQUEPTOS
    \brief Identificador de la posicion de un bloque cuando este no existe en el
           fichero de base de datos de puntos. */
#define IDNOBLOQUEPTOS -1
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
