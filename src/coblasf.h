/* -*- coding: utf-8 -*- */
/**
\defgroup coblas Módulo COBLAS
\brief En este módulo se reúnen las funciones de la implementación de BLAS en
       formato Fortran \p COBLAS (https://github.com/rljames/coblas), para tipos
       de dato \p double y \p float.
@{
\file coblasf.h
\brief Declaración de las funciones para tipo de dato \p double y \p float de la
       implementación de BLAS en formato Fortran \p COBLAS
       (https://github.com/rljames/coblas).
\note Este fichero es sólo para uso interno y, aunque sí se copiará en el
      directorio de ficheros de cabecera de GEOC, no se incluirá en ningún otro
      fichero de cabecera para que no genere conflicto con la declaración de las
      mismas funciones con argumentos \p const.
\note Se ha sustituido el tipo de dato \p int por \p gblas_int en los argumentos
      de entrada.
\note Se ha sustituido el nombre original de las funciones por una macro.
\author José Luis García Pallero, jgpallero@gmail.com
\date 15 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2014, Rodney Lynn James (de la versión original, también BSD)
Copyright (c) 2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _COBLASF_H_
#define _COBLASF_H_
/******************************************************************************/
/******************************************************************************/
#include<ctype.h>
#include<math.h>
#include<stdio.h>
#include"mnfblas.h"
#include"tdblas.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
//NIVEL 1 DE BLAS PARA TIPO DE DATO DOUBLE
/******************************************************************************/
int       GBL_F77_daxpy  (gblas_int *N, double *A, double *x, gblas_int *INCX,
                          double *y, gblas_int *INCY);
gblas_int GBL_F77_idamax (gblas_int *N, double *x, gblas_int *INCX);
double    GBL_F77_dasum  (gblas_int *N, double *x, gblas_int *INCX);
int       GBL_F77_dcopy  (gblas_int *N, double *x, gblas_int *INCX, double *y,
                          gblas_int *INCY);
double    GBL_F77_dnrm2  (gblas_int *N, double *x, gblas_int *INCX);
int       GBL_F77_drot   (gblas_int *N, double *x, gblas_int *INCX, double *y,
                          gblas_int *INCY, double *C, double *S);
int       GBL_F77_drotg  (double *A, double *B, double *C, double *S);
int       GBL_F77_dscal  (gblas_int *N, double *A, double *x, gblas_int *INCX);
int       GBL_F77_dswap  (gblas_int *N, double *x, gblas_int *INCX, double *y,
                          gblas_int *INCY);
double    GBL_F77_ddot   (gblas_int *N, double *x, gblas_int *INCX, double *y,
                          gblas_int *INCY);
int       GBL_F77_drotm  (gblas_int *N, double *x, gblas_int *INCX, double *y,
                          gblas_int *INCY, double *param);
int       GBL_F77_drotmg (double *d1, double *d2, double *x1, double *y1,
                          double *param);
/******************************************************************************/
//NIVEL 2 DE BLAS PARA TIPO DE DATO DOUBLE
/******************************************************************************/
int       GBL_F77_dgemv  (char *Trans, gblas_int *M, gblas_int *N,
                          double *Alpha, double *A, gblas_int *LDA, double *x,
                          gblas_int *INCX, double *Beta, double *y,
                          gblas_int *INCY);
int       GBL_F77_dgbmv  (char *Trans, gblas_int *M, gblas_int *N,
                          gblas_int *KL, gblas_int *KU, double *Alpha,
                          double *A, gblas_int *LDA, double *x, gblas_int *INCX,
                          double *Beta, double *y, gblas_int *INCY);
int       GBL_F77_dsymv  (char *UPLO, gblas_int *N, double *Alpha, double *A,
                          gblas_int *LDA, double *x, gblas_int *INCX,
                          double *Beta, double *y, gblas_int *INCY);
int       GBL_F77_dsbmv  (char *UPLO, gblas_int *N, gblas_int *K, double *Alpha,
                          double *A, gblas_int *LDA, double *x, gblas_int *INCX,
                          double *Beta, double *y, gblas_int *INCY);
int       GBL_F77_dspmv  (char *UPLO, gblas_int *N, double *Alpha, double *A,
                          double *x, gblas_int *INCX, double *Beta, double *y,
                          gblas_int *INCY);
int       GBL_F77_dtrmv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          double *A, gblas_int *LDA, double *x,
                          gblas_int *INCX);
int       GBL_F77_dtbmv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          gblas_int *K, double *A, gblas_int *LDA, double *x,
                          gblas_int *INCX);
int       GBL_F77_dtpmv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          double *A, double *x, gblas_int *INCX);
int       GBL_F77_dtrsv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          double *A, gblas_int *LDA, double *x,
                          gblas_int *INCX);
int       GBL_F77_dtbsv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          gblas_int *K, double *A, gblas_int *LDA, double *x,
                          gblas_int *INCX);
int       GBL_F77_dtpsv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          double *A, double *x, gblas_int *INCX);
int       GBL_F77_dger   (gblas_int *M, gblas_int *N, double *Alpha, double *x,
                          gblas_int *INCX, double *y, gblas_int *INCY,
                          double *A, gblas_int *LDA);
int       GBL_F77_dsyr   (char *UPLO, gblas_int *N, double *Alpha, double *x,
                          gblas_int *INCX, double *A, gblas_int *LDA);
int       GBL_F77_dspr   (char *UPLO, gblas_int *N, double *Alpha, double *x,
                          gblas_int *INCX, double *A);
int       GBL_F77_dsyr2  (char *UPLO, gblas_int *N, double *Alpha, double *x,
                          gblas_int *INCX, double *y, gblas_int *INCY,
                          double *A, gblas_int *LDA);
int       GBL_F77_dspr2  (char *UPLO, gblas_int *N, double *Alpha, double *x,
                          gblas_int *INCX, double *y, gblas_int *INCY,
                          double *A);
/******************************************************************************/
//NIVEL 3 DE BLAS PARA TIPO DE DATO DOUBLE
/******************************************************************************/
int       GBL_F77_dgemm  (char *TRANSA, char *TRANSB, gblas_int *M,
                          gblas_int *N, gblas_int *K, double *Alpha, double *A,
                          gblas_int *LDA, double *B, gblas_int *LDB,
                          double *Beta, double *C, gblas_int *LDC);
int       GBL_F77_dsymm  (char *SIDE, char *UPLO, gblas_int *M, gblas_int *N,
                          double *Alpha, double *A, gblas_int *LDA, double *B,
                          gblas_int *LDB, double *Beta, double *C,
                          gblas_int *LDC);
int       GBL_F77_dsyrk  (char *UPLO, char *TRANS, gblas_int *N, gblas_int *K,
                          double *Alpha, double *A, gblas_int *LDA,
                          double *Beta, double *C, gblas_int *LDC);
int       GBL_F77_dsyr2k (char *UPLO, char *TRANS, gblas_int *N, gblas_int *K,
                          double *Alpha, double *A, gblas_int *LDA, double *B,
                          gblas_int *LDB, double *Beta, double *C,
                          gblas_int *LDC);
int       GBL_F77_dtrmm  (char *SIDE, char *UPLO, char *TRANS, char *DIAG,
                          gblas_int *M, gblas_int *N, double *Alpha, double *A,
                          gblas_int *LDA, double *B, gblas_int *LDB);
int       GBL_F77_dtrsm  (char *SIDE, char *UPLO, char *TRANS, char *DIAG,
                          gblas_int *M, gblas_int *N, double *Alpha, double *A,
                          gblas_int *LDA, double *B, gblas_int *LDB);
/******************************************************************************/
/******************************************************************************/
//NIVEL 1 DE BLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
int       GBL_F77_saxpy  (gblas_int *N, float *A, float *x, gblas_int *INCX,
                          float *y, gblas_int *INCY);
gblas_int GBL_F77_isamax (gblas_int *N, float *x, gblas_int *INCX);
float     GBL_F77_sasum  (gblas_int *N, float *x, gblas_int *INCX);
int       GBL_F77_scopy  (gblas_int *N, float *x, gblas_int *INCX, float *y,
                          gblas_int *INCY);
float     GBL_F77_snrm2  (gblas_int *N, float *x, gblas_int *INCX);
int       GBL_F77_srot   (gblas_int *N, float *x, gblas_int *INCX, float *y,
                          gblas_int *INCY, float *C, float *S);
int       GBL_F77_srotg  (float *A, float *B, float *C, float *S);
int       GBL_F77_sscal  (gblas_int *N, float *A, float *x, gblas_int *INCX);
int       GBL_F77_sswap  (gblas_int *N, float *x, gblas_int *INCX, float *y,
                          gblas_int *INCY);
float     GBL_F77_sdot   (gblas_int *N, float *x, gblas_int *INCX, float *y,
                          gblas_int *INCY);
int       GBL_F77_srotm  (gblas_int *N, float *x, gblas_int *INCX, float *y,
                          gblas_int *INCY, float *param);
int       GBL_F77_srotmg (float *d1, float *d2, float *x1, float *y1,
                          float *param);
/******************************************************************************/
//NIVEL 2 DE BLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
int       GBL_F77_sgemv  (char *Trans, gblas_int *M, gblas_int *N, float *Alpha,
                          float *A, gblas_int *LDA, float *x, gblas_int *INCX,
                          float *Beta, float *y, gblas_int *INCY);
int       GBL_F77_sgbmv  (char *Trans, gblas_int *M, gblas_int *N,
                          gblas_int *KL, gblas_int *KU, float *Alpha, float *A,
                          gblas_int *LDA, float *x, gblas_int *INCX,
                          float *Beta, float *y, gblas_int *INCY);
int       GBL_F77_ssymv  (char *UPLO, gblas_int *N, float *Alpha, float *A,
                          gblas_int *LDA, float *x, gblas_int *INCX,
                          float *Beta, float *y, gblas_int *INCY);
int       GBL_F77_ssbmv  (char *UPLO, gblas_int *N, gblas_int *K, float *Alpha,
                          float *A, gblas_int *LDA, float *x, gblas_int *INCX,
                          float *Beta, float *y, gblas_int *INCY);
int       GBL_F77_sspmv  (char *UPLO, gblas_int *N, float *Alpha, float *A,
                          float *x, gblas_int *INCX, float *Beta, float *y,
                          gblas_int *INCY);
int       GBL_F77_strmv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          float *A, gblas_int *LDA, float *x, gblas_int *INCX);
int       GBL_F77_stbmv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          gblas_int *K, float *A, gblas_int *LDA, float *x,
                          gblas_int *INCX);
int       GBL_F77_stpmv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          float *A, float *x, gblas_int *INCX);
int       GBL_F77_strsv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          float *A, gblas_int *LDA, float *x, gblas_int *INCX);
int       GBL_F77_stbsv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          gblas_int *K, float *A, gblas_int *LDA, float *x,
                          gblas_int *INCX);
int       GBL_F77_stpsv  (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                          float *A, float *x, gblas_int *INCX);
int       GBL_F77_sger   (gblas_int *M, gblas_int *N, float *Alpha, float *x,
                          gblas_int *INCX, float *y, gblas_int *INCY, float *A,
                          gblas_int *LDA);
int       GBL_F77_ssyr   (char *UPLO, gblas_int *N, float *Alpha, float *x,
                          gblas_int *INCX, float *A, gblas_int *LDA);
int       GBL_F77_sspr   (char *UPLO, gblas_int *N, float *Alpha, float *x,
                          gblas_int *INCX, float *A);
int       GBL_F77_ssyr2  (char *UPLO, gblas_int *N, float *Alpha, float *x,
                          gblas_int *INCX, float *y, gblas_int *INCY, float *A,
                          gblas_int *LDA);
int       GBL_F77_sspr2  (char *UPLO, gblas_int *N, float *Alpha, float *x,
                          gblas_int *INCX, float *y, gblas_int *INCY, float *A);
/******************************************************************************/
//NIVEL 3 DE BLAS PARA TIPO DE DATO FLOAT
/******************************************************************************/
int       GBL_F77_sgemm  (char *TRANSA, char *TRANSB, gblas_int *M,
                          gblas_int *N, gblas_int *K, float *Alpha, float *A,
                          gblas_int *LDA, float *B, gblas_int *LDB, float *Beta,
                          float *C, gblas_int *LDC);
int       GBL_F77_ssymm  (char *SIDE, char *UPLO, gblas_int *M, gblas_int *N,
                          float *Alpha, float *A, gblas_int *LDA, float *B,
                          gblas_int *LDB, float *Beta, float *C,
                          gblas_int *LDC);
int       GBL_F77_ssyrk  (char *UPLO, char *TRANS, gblas_int *N, gblas_int *K,
                          float *Alpha, float *A, gblas_int *LDA, float *Beta,
                          float *C, gblas_int *LDC);
int       GBL_F77_ssyr2k (char *UPLO, char *TRANS, gblas_int *N, gblas_int *K,
                          float *Alpha, float *A, gblas_int *LDA, float *B,
                          gblas_int *LDB, float *Beta, float *C,
                          gblas_int *LDC);
int       GBL_F77_strmm  (char *SIDE, char *UPLO, char *TRANS, char *DIAG,
                          gblas_int *M, gblas_int *N, float *Alpha, float *A,
                          gblas_int *LDA, float *B, gblas_int *LDB);
int       GBL_F77_strsm  (char *SIDE, char *UPLO, char *TRANS, char *DIAG,
                          gblas_int *M, gblas_int *N, float *Alpha, float *A,
                          gblas_int *LDA, float *B, gblas_int *LDB);
/******************************************************************************/
/******************************************************************************/
//RUTINA DE AVISO DE ERROR
int       GBL_F77_xerbla (const char *srname, int *info);
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */

