/* -*- coding: utf-8 -*- */
/**
\ingroup coblas
@{
\file coblasfd.c
\brief Definición de las funciones de \p COBLAS para tipo de dato \p double.
\author José Luis García Pallero, jgpallero@gmail.com
\date 16 de enero de 2014
\version 1.0
\copyright
Copyright (c) 2014, Rodney Lynn James (de la versión original, también BSD)
Copyright (c) 2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#include"coblasf.h"
/******************************************************************************/
/******************************************************************************/
///Macro interna para calcular el máximo entre dos números
#define max(a,b) ((a) >= (b) ? (a) : (b))
///Macro interna para calcular el mínimo entre dos números
#define min(a,b) ((a) <  (b) ? (a) : (b))
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_daxpy   (gblas_int *N, double *A, double *x, gblas_int *INCX,
                           double *y, gblas_int *INCY)
{
    const double zero=0.0;
    const double a=(*A);
    const gblas_int n=(*N);
    const gblas_int incx=(*INCX);
    const gblas_int incy=(*INCY);
    gblas_int i,ix,iy;

    if((n>0)&&(a!=zero))
    {
        if((incx==1)&&(incy==1))
        {
            for(i=0;i<n;i++)
                y[i]+=a*x[i];
        }
        else
        {
            ix=(incx>0)?0:(1-n)*incx;
            iy=(incy>0)?0:(1-n)*incy;
            for(i=0;i<n;i++)
            {
                y[iy]+=a*x[ix];
                ix+=incx;
                iy+=incy;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
gblas_int GBL_F77_idamax  (gblas_int *N, double *x, gblas_int *INCX)
{
    const gblas_int incx=*INCX;
    const gblas_int n=*N;
    gblas_int i,ix,imax;
    double maxval;
    if((n<1) || (incx<=0))
        imax=0;
    else if(n==1)
        imax=1;
    else if(incx==1)
    {
        maxval=fabs(x[0]);
        imax=1;
        for(i=1;i<n;i++)
        {
            if(fabs(x[i])>maxval)
            {
                imax=i+1;
                maxval=fabs(x[i]);
            }
        }
    }
    else
    {
        maxval=fabs(x[0]);
        imax=1;
        ix=incx;
        for(i=1;i<n;i++)
        {
            if(fabs(x[ix])>maxval)
            {
                imax=i+1;
                maxval=fabs(x[ix]);
            }
            ix+=incx;
        }
    }
    return imax;
}
/******************************************************************************/
/******************************************************************************/
double    GBL_F77_dasum   (gblas_int *N, double *x, gblas_int *INCX)
{
    gblas_int n=*N;
    gblas_int incx=*INCX;
    gblas_int i;
    double sum=0.0;
    if(n>0)
    {
        if(incx==1)
        {
            for(i=0;i<n;i++)
                sum+=fabs(x[i]);
        }
        else if(incx>1)
        {
            for(i=0;i<n*incx;i+=incx)
                sum+=fabs(x[i]);
        }
    }
    return sum;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dcopy   (gblas_int *N, double *x, gblas_int *INCX, double *y,
                           gblas_int *INCY)
{
    gblas_int n=*N;
    gblas_int incx=*INCX;
    gblas_int incy=*INCY;
    gblas_int i,ix,iy;
    if(n>0)
    {
        if((incx==1)&&(incy==1))
        {
            for(i=0;i<n;i++)
                y[i]=x[i];
        }
        else
        {
            ix = incx>0 ? 0 : (1-n)*incx;
            iy = incy>0 ? 0 : (1-n)*incy;
            for(i=0;i<n;i++)
            {
                y[iy]=x[ix];
                ix+=incx;
                iy+=incy;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
double    GBL_F77_dnrm2   (gblas_int *N, double *x, gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const double one=1.0;
    const double zero=0.0;
    double norm=0.0;
    double scale,ssq,a,b;
    gblas_int i;
    if((n>0)&&(incx>0))
    {
        if(n==1)
        {
            norm=fabs(x[0]);
        }
        else
        {
            scale=zero;
            ssq=one;
            for(i=0;i<n*incx;i+=incx)
            {
                if(x[i]!=zero)
                {
                    a=fabs(x[i]);
                    if(scale<a)
                    {
                        b=scale/a;
                        ssq=one+ssq*b*b;
                        scale=a;
                    }
                    else
                    {
                        b=a/scale;
                        ssq+=b*b;
                    }
                }
            }
            norm=scale*sqrt(ssq);
        }
    }
    return norm;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_drot    (gblas_int *N, double *x, gblas_int *INCX, double *y,
                           gblas_int *INCY, double *C, double *S)
{
    const double c=*C;
    const double s=*S;
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    double temp;
    gblas_int i,ix,iy;
    if(n>0)
    {
        if((incx==1)&&(incy==1))
        {
            for(i=0;i<n;i++)
            {
                temp=c*x[i]+s*y[i];
                y[i]=c*y[i]-s*x[i];
                x[i]=temp;
            }
        }
        else
        {
            ix = incx>0 ? 0 : (1-n)*incx;
            iy = incy>0 ? 0 : (1-n)*incy;
            for(i=0;i<n;i++)
            {
                temp=c*x[ix]+s*y[iy];
                y[iy]=c*y[iy]-s*x[ix];
                x[ix]=temp;
                ix+=incx;
                iy+=incy;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_drotg   (double *A, double *B, double *C, double *S)
{
    double roe,r,scale,z,ta,tb;
    double a=*A;
    double b=*B;
    double c=*C;
    double s=*S;

    if(fabs(a)>fabs(b))
        roe=a;
    else
        roe=b;
    scale=fabs(a)+fabs(b);

    if(scale==0.0)
    {
        c=1.0;
        s=0.0;
        r=0.0;
        z=0.0;
    }
    else
    {
        ta=(a/scale);
        tb=(b/scale);
        if(roe>0.0)
            r=scale*sqrt(ta*ta+tb*tb);
        else
            r=-scale*sqrt(ta*ta+tb*tb);
        c=a/r;
        s=b/r;
        if(fabs(a)>fabs(b))
            z=s;
        else if(c!=0.0)
            z=1.0/c;
        else
            z=1.0;
    }
    *A=r;
    *B=z;
    *C=c;
    *S=s;
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dscal   (gblas_int *N, double *A, double *x, gblas_int *INCX)
{
    const double alpha=*A;
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    gblas_int i,ix;
    if((n>0)&&(incx>0))
    {
        if(incx==1)
        {
            for(i=0;i<n;i++)
                x[i]*=alpha;
        }
        else
        {
            ix=0;
            for(i=0;i<n;i++)
            {
                x[ix]*=alpha;
                ix+=incx;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dswap   (gblas_int *N, double *x, gblas_int *INCX, double *y,
                           gblas_int *INCY)
{
    const gblas_int n=(*N);
    const gblas_int incx=(*INCX);
    const gblas_int incy=(*INCY);
    double temp;
    gblas_int i,ix,iy;
    if(n>0)
    {
        if((incx==1)&&(incy==1))
        {
            for(i=0;i<n;i++)
            {
                temp=x[i];
                x[i]=y[i];
                y[i]=temp;
            }
        }
        else
        {
            ix = incx>0 ? 0 : (1-n)*incx;
            iy = incy>0 ? 0 : (1-n)*incy;
            for(i=0;i<n;i++)
            {
                temp=x[ix];
                x[ix]=y[iy];
                y[iy]=temp;
                ix+=incx;
                iy+=incy;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
double    GBL_F77_ddot    (gblas_int *N, double *x, gblas_int *INCX, double *y,
                           gblas_int *INCY)
{
    double sum=0.0;
    gblas_int n=*N;
    gblas_int incx=*INCX;
    gblas_int incy=*INCY;
    gblas_int i,ix,iy;
    if(n>0)
    {
        if((incx==1)&&(incy==1))
        {
            for(i=0;i<n;i++)
                sum+=x[i]*y[i];
        }
        else
        {
            ix = incx>0 ? 0 : (1-n)*incx;
            iy = incy>0 ? 0 : (1-n)*incy;
            for(i=0;i<n;i++)
            {
                sum+=x[ix]*y[iy];
                ix+=incx;
                iy+=incy;
            }
        }
    }
    return sum;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_drotm   (gblas_int *N, double *x, gblas_int *INCX, double *y,
                           gblas_int *INCY, double *param)
{
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const gblas_int flag = (int)(param[0]);
    const double h11  = param[1];
    const double h21  = param[2];
    const double h12  = param[3];
    const double h22  = param[4];
    double w, z;
    gblas_int i,ix,iy;

    if(n>0)
    {
        if((incx==incy)&&(incx==1))
        {
            switch(flag)
            {
                case -1:
                    for(i=0;i<n;i++)
                    {
                        w=x[i];
                        z=y[i];
                        x[i]=w*h11+z*h12;
                        y[i]=w*h21+z*h22;
                    }
                    break;

                case 0:
                    for(i=0;i<n;i++)
                    {
                        w=x[i];
                        z=y[i];
                        x[i]=w+z*h12;
                        y[i]=w*h21+z;
                    }
                    break;

                case 1:
                    for(i=0;i<n;i++)
                    {
                        w=x[i];
                        z=y[i];
                        x[i]=w*h11+z;
                        y[i]=-w+z*h22;
                    }
                    break;

                default:
                    break;
            }
        }
        else
        {
            ix=(incx>0)?0:(1-n)*incx;
            iy=(incy>0)?0:(1-n)*incy;
            switch(flag)
            {
                case -1:
                    for(i=0;i<n;i++)
                    {
                        w=x[ix];
                        z=y[iy];
                        x[ix]=w*h11+z*h12;
                        y[iy]=w*h21+z*h22;
                        ix+=incx;
                        iy+=incy;
                    }
                    break;

                case 0:
                    for(i=0;i<n;i++)
                    {
                        w=x[ix];
                        z=y[iy];
                        x[ix]=w+z*h12;
                        y[iy]=w*h21+z;
                        ix+=incx;
                        iy+=incy;
                    }
                    break;

                case 1:
                    for(i=0;i<n;i++)
                    {
                        w=x[ix];
                        z=y[iy];
                        x[ix]=w*h11+z;
                        y[iy]=-w+z*h22;
                        ix+=incx;
                        iy+=incy;
                    }
                    break;

                default:
                    break;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_drotmg  (double *d1, double *d2, double *x1, double *y1,
                           double *param)
{
    const double zero = 0.0;
    const double one = 1.0;
    const double two = 2.0;
    const double gam = 4096.0;
    const double gamsq = 16777216.0;
    const double rgamsq = 5.9604645e-8;
    double r=zero;
    double su=zero;
    double sp1=zero;
    double sp2=zero;
    double sq1=zero;
    double sq2=zero;
    double sh11=zero;
    double sh12=zero;
    double sh21=zero;
    double sh22=zero;
    double flag=zero;
    double stemp=zero;

    if (*d1 < zero)
    {
        flag = -one;
        sh11 = zero;
        sh12 = zero;
        sh21 = zero;
        sh22 = zero;
        *d1 = zero;
        *d2 = zero;
        *x1 = zero;
    }
    else
    {
        sp2 = *d2 * *y1;
        if (sp2 == zero)
        {
            flag = -two;
            param[0] = flag;
            return 0;
        }
        sp1 = *d1 * *x1;
        sq2 = sp2 * *y1;
        sq1 = sp1 * *x1;
        if(fabs(sq1)>fabs(sq2))
        {
            sh21 = -(*y1) / *x1;
            sh12 = sp2 / sp1;
            su = one - sh12 * sh21;
            if(su > zero)
            {
                flag = zero;
                *d1 /= su;
                *d2 /= su;
                *x1 *= su;
            }
        }
        else
        {
            if(sq2 < zero)
            {
                flag = -one;
                sh11 = zero;
                sh12 = zero;
                sh21 = zero;
                sh22 = zero;
                *d1 = zero;
                *d2 = zero;
                *x1 = zero;
            }
            else
            {
                flag = one;
                sh11 = sp1 / sp2;
                sh22 = *x1 / *y1;
                su = one + sh11 * sh22;
                stemp = *d2 / su;
                *d2 = *d1 / su;
                *d1 = stemp;
                *x1 = *y1 * su;
            }
        }
        if(*d1 != zero)
        {
            while(*d1 <= rgamsq || *d1 >= gamsq)
            {
                if (flag == zero)
                {
                    sh11 = one;
                    sh22 = one;
                    flag = -one;
                }
                else
                {
                    sh21 = -one;
                    sh12 = one;
                    flag = -one;
                }
                if(*d1 <= rgamsq)
                {
                    r = gam;
                    *d1 *= r*r;
                    *x1 /= gam;
                    sh11 /= gam;
                    sh12 /= gam;
                }
                else
                {
                    r = gam;
                    *d1 /= r * r;
                    *x1 *= gam;
                    sh11 *= gam;
                    sh12 *= gam;
                }
            }
        }
        if(*d2 != zero)
        {
            while(fabs(*d2) <= rgamsq || fabs(*d2) >= gamsq)
            {
                if(flag == zero)
                {
                    sh11 = one;
                    sh22 = one;
                    flag = -one;
                }
                else
                {
                    sh21 = -one;
                    sh12 = one;
                    flag = -one;
                }
                if(fabs(*d2) <= rgamsq)
                {
                    r = gam;
                    *d2 *= r * r;
                    sh21 /= gam;
                    sh22 /= gam;
                }
                else
                {
                    r = gam;
                    *d2 /= r * r;
                    sh21 *= gam;
                    sh22 *= gam;
                }
            }
        }
    }
    if(flag < zero)
    {
        param[1] = sh11;
        param[2] = sh21;
        param[3] = sh12;
        param[4] = sh22;
    }
    else if(flag == zero)
    {
        param[2] = sh21;
        param[3] = sh12;
    }
    else
    {
        param[1] = sh11;
        param[4] = sh22;
    }
    param[0] = flag;
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dgemv   (char *Trans, gblas_int *M, gblas_int *N,
                           double *Alpha, double *A, gblas_int *LDA, double *x,
                           gblas_int *INCX, double *Beta, double *y,
                           gblas_int *INCY)
{
    const double one=1.0;
    const double zero=0.0;
    const gblas_int m=*M;
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const double alpha=*Alpha;
    const double beta=*Beta;
    char trans;
    gblas_int kx,ky,lenx,leny,i,ix,iy,j,jx,jy;
    int info=0;
    double temp;

    trans=(char)toupper(*Trans);

    if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=1;
    else if(m<0)
        info=2;
    else if(n<0)
        info=3;
    else if(lda<m)
        info=6;
    else if(incx==0)
        info=8;
    else if(incy==0)
        info=11;

    if(info>0)
    {
        GBL_F77_xerbla("DGEMV ",&info);
        return 0;
    }

    if((m==0)||(n==0))
        return 0;

    lenx=(trans=='N')?n:m;
    leny=(trans=='N')?m:n;
    kx=(incx>0)?0:(1-lenx)*incx;
    ky=(incy>0)?0:(1-leny)*incy;

    if(beta==zero)
    {
        if(incy==1)
        {
            for(i=0;i<leny;i++)
                y[i]=zero;
        }
        else
        {
            iy=ky;
            for(i=0;i<leny;i++)
            {
                y[iy]=zero;
                iy+=incy;
            }

        }
    }
    else if(beta!=one)
    {
        if(incy==1)
        {
            for(i=0;i<leny;i++)
                y[i]*=beta;
        }
        else
        {
            iy=ky;
            for(i=0;i<leny;i++)
            {
                y[iy]*=beta;
                iy+=incy;
            }
        }
    }

    if(alpha!=zero)
    {
        if(trans=='N')
        {
            jx=kx;
            if(incy==1)
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    for(i=0;i<m;i++)
                        y[i]+=temp*A[i];
                    A+=lda;
                    jx+=incx;
                }
            else
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    iy=ky;
                    for(i=0;i<m;i++)
                    {
                        y[iy]+=temp*A[i];
                        iy+=incy;
                    }
                    A+=lda;
                    jx+=incx;
                }
        }
        else
        {
            jy=ky;
            if(incx==1)
                for(j=0;j<n;j++)
                {
                    temp=zero;
                    for(i=0;i<m;i++)
                        temp+=A[i]*x[i];
                    y[jy]+=alpha*temp;
                    jy+=incy;
                    A+=lda;
                }
            else
                for(j=0;j<n;j++)
                {
                    temp=zero;
                    ix=kx;
                    for(i=0;i<m;i++)
                    {
                        temp+=A[i]*x[ix];
                        ix+=incx;
                    }
                    y[jy]+=alpha*temp;
                    jy+=incy;
                    A+=lda;
                }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dgbmv  (char *Trans, gblas_int *M, gblas_int *N,
                          gblas_int *KL, gblas_int *KU, double *Alpha,
                          double *A, gblas_int *LDA, double *x, gblas_int *INCX,
                          double *Beta, double *y, gblas_int *INCY)
{
    const double one=1.0;
    const double zero=0.0;
    const gblas_int m=*M;
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const gblas_int kl=*KL;
    const gblas_int ku=*KU;
    const double alpha=*Alpha;
    const double beta=*Beta;
    char trans;
    gblas_int kx,ky,lenx,leny,i,ix,iy,j,jx,jy,k,i0,im;
    int info=0;
    double temp;

    trans=(char)toupper(*Trans);

    if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=1;
    else if(m<0)
        info=2;
    else if(n<0)
        info=3;
    else if(kl<0)
        info=4;
    else if(ku<0)
        info=5;
    else if(lda<kl+ku+1)
        info=8;
    else if(incx==0)
        info=10;
    else if(incy==0)
        info=13;

    if(info>0)
    {
        GBL_F77_xerbla("DGBMV ",&info);
        return 0;
    }

    if((m==0)||(n==0))
        return 0;

    lenx=(trans=='N')?n:m;
    leny=(trans=='N')?m:n;
    kx=(incx>0)?0:(1-lenx)*incx;
    ky=(incy>0)?0:(1-leny)*incy;

    if(beta==zero)
    {
        if(incy==1)
        {
            for(i=0;i<leny;i++)
                y[i]=zero;
        }
        else
        {
            iy=ky;
            for(i=0;i<leny;i++)
            {
                y[iy]=zero;
                iy+=incy;
            }

        }
    }
    else if(beta!=one)
    {
        if(incy==1)
        {
            for(i=0;i<leny;i++)
                y[i]*=beta;
        }
        else
        {
            iy=ky;
            for(i=0;i<leny;i++)
            {
                y[iy]*=beta;
                iy+=incy;
            }
        }
    }

    if(alpha!=zero)
    {
        if(trans=='N')
        {
            jx=kx;
            if(incy==1)
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    k=ku-j;
                    i0=max(0,j-ku);
                    im=min(m-1,j+kl);
                    for(i=i0;i<=im;i++)
                        y[i]+=temp*A[k+i];
                    A+=lda;
                    jx+=incx;
                }
            else
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    iy=ky;
                    k=ku-j;
                    i0=max(0,j-ku);
                    im=min(m-1,j+kl);
                    for(i=i0;i<=im;i++)
                    {
                        y[iy]+=temp*A[k+i];
                        iy+=incy;
                    }
                    A+=lda;
                    jx+=incx;
                    if(j>=ku)
                        ky+=incy;
                }
        }
        else
        {
            jy=ky;
            if(incx==1)
                for(j=0;j<n;j++)
                {
                    temp=zero;
                    k=ku-j;
                    i0=max(0,j-ku);
                    im=min(m-1,j+kl);
                    for(i=i0;i<=im;i++)
                        temp+=A[k+i]*x[i];
                    y[jy]+=alpha*temp;
                    jy+=incy;
                    A+=lda;
                }
            else
                for(j=0;j<n;j++)
                {
                    temp=zero;
                    k=ku-j;
                    i0=max(0,j-ku);
                    im=min(m-1,j+kl);
                    ix=kx;
                    for(i=i0;i<=im;i++)
                    {
                        temp+=A[k+i]*x[ix];
                        ix+=incx;
                    }
                    y[jy]+=alpha*temp;
                    jy+=incy;
                    A+=lda;
                    if(j>=ku)
                        kx+=incx;
                }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsymv   (char *UPLO, gblas_int *N, double *Alpha, double *A,
                           gblas_int *LDA, double *x, gblas_int *INCX,
                           double *Beta, double *y, gblas_int *INCY)
{
    const double one=1.0;
    const double zero=0.0;
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const double alpha=*Alpha;
    const double beta=*Beta;
    char uplo;
    gblas_int kx,ky,i,ix,iy,j,jx,jy;
    int info=0;
    double temp,sum;

    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if(n<0)
        info=2;
    else if(lda<n)
        info=5;
    else if(incx==0)
        info=7;
    else if(incy==0)
        info=10;

    if(info>0)
    {
        GBL_F77_xerbla("DSYMV ",&info);
        return 0;
    }

    if(n==0)
        return 0;

    kx=(incx>0)?0:(1-n)*incx;
    ky=(incy>0)?0:(1-n)*incy;

    if(beta==zero)
    {
        if(incy==1)
        {
            for(i=0;i<n;i++)
                y[i]=zero;
        }
        else
        {
            iy=ky;
            for(i=0;i<n;i++)
            {
                y[iy]=zero;
                iy+=incy;
            }

        }
    }
    else if(beta!=one)
    {
        if(incy==1)
        {
            for(i=0;i<n;i++)
                y[i]*=beta;
        }
        else
        {
            iy=ky;
            for(i=0;i<n;i++)
            {
                y[iy]*=beta;
                iy+=incy;
            }
        }
    }

    if(alpha!=zero)
    {
        if(uplo=='U')
        {
            if((incx==1)&&(incy==1))
            {
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[j];
                    sum=zero;
                    for(i=0;i<j;i++)
                    {
                        y[i]+=temp*A[i];
                        sum+=A[i]*x[i];
                    }
                    y[j]+=temp*A[j]+alpha*sum;
                    A+=lda;
                }
            }
            else
            {
                jx=kx;
                jy=ky;
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    sum=zero;
                    ix=kx;
                    iy=ky;
                    for(i=0;i<j;i++)
                    {
                        y[iy]+=temp*A[i];
                        sum+=A[i]*x[ix];
                        ix+=incx;
                        iy+=incy;
                    }
                    y[jy]+=temp*A[j]+alpha*sum;
                    A+=lda;
                    jx+=incx;
                    jy+=incy;
                }
            }
        }
        else
        {
            if((incx==1)&&(incy==1))
            {
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[j];
                    sum=zero;
                    y[j]+=temp*A[j];
                    for(i=j+1;i<n;i++)
                    {
                        y[i]+=temp*A[i];
                        sum+=A[i]*x[i];
                    }
                    y[j]+=alpha*sum;
                    A+=lda;
                }
            }
            else
            {
                jx=kx;
                jy=ky;
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    sum=zero;
                    y[jy]+=temp*A[j];
                    ix=jx;
                    iy=jy;
                    for(i=j+1;i<n;i++)
                    {
                        ix+=incx;
                        iy+=incy;
                        y[iy]+=temp*A[i];
                        sum+=A[i]*x[ix];
                    }
                    y[jy]+=alpha*sum;
                    jx+=incx;
                    jy+=incy;
                    A+=lda;
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsbmv  (char *UPLO, gblas_int *N, gblas_int *K, double *Alpha,
                          double *A, gblas_int *LDA, double *x, gblas_int *INCX,
                          double *Beta, double *y, gblas_int *INCY)
{
   const double one=1.0;
   const double zero=0.0;
   const gblas_int n=*N;
   const gblas_int k=*K;
   const gblas_int lda=*LDA;
   const gblas_int incx=*INCX;
   const gblas_int incy=*INCY;
   const double alpha=*Alpha;
   const double beta=*Beta;
   char uplo;
   gblas_int kx,ky,i,ix,iy,j,jx,jy,i0,in;
   int info=0;
   double temp,sum;

   uplo=(char)toupper(*UPLO);

   if((uplo!='U')&&(uplo!='L'))
      info=1;
   else if(n<0)
      info=2;
   else if(k<0)
      info=3;
   else if(lda<k+1)
      info=6;
   else if(incx==0)
      info=8;
   else if(incy==0)
      info=11;

   if(info>0)
   {
      GBL_F77_xerbla("DSBMV ",&info);
      return 0;
   }

   if(n==0)
      return 0;

   kx=(incx>0)?0:(1-n)*incx;
   ky=(incy>0)?0:(1-n)*incy;

   if(beta==zero)
   {
      if(incy==1)
      {
         for(i=0;i<n;i++)
            y[i]=zero;
      }
      else
      {
         iy=ky;
         for(i=0;i<n;i++)
         {
            y[iy]=zero;
            iy+=incy;
         }

      }
   }
   else if(beta!=one)
   {
      if(incy==1)
      {
         for(i=0;i<n;i++)
            y[i]*=beta;
      }
      else
      {
         iy=ky;
         for(i=0;i<n;i++)
         {
            y[iy]*=beta;
            iy+=incy;
         }
      }
   }

   if(alpha!=zero)
   {
      if(uplo=='U')
      {
         if((incx==1)&&(incy==1))
         {
            for(j=0;j<n;j++)
            {
               temp=alpha*x[j];
               sum=zero;
               i0=max(0,j-k);
               for(i=i0;i<j;i++)
               {
                  y[i]+=temp*A[k-j+i];
                  sum+=A[k-j+i]*x[i];
               }
               y[j]+=temp*A[k]+alpha*sum;
               A+=lda;
            }
         }
         else
         {
            jx=kx;
            jy=ky;
            for(j=0;j<n;j++)
            {
               temp=alpha*x[jx];
               sum=zero;
               ix=kx;
               iy=ky;
               i0=max(0,j-k);
               for(i=i0;i<j;i++)
               {
                  y[iy]+=temp*A[k-j+i];
                  sum+=A[k-j+i]*x[ix];
                  ix+=incx;
                  iy+=incy;
               }
               y[jy]+=temp*A[k]+alpha*sum;
               A+=lda;
               jx+=incx;
               jy+=incy;
               if(j>=k)
               {
                  kx+=incx;
                  ky+=incy;
               }
            }
         }
      }
      else
      {
         if((incx==1)&&(incy==1))
         {
            for(j=0;j<n;j++)
            {
               temp=alpha*x[j];
               sum=zero;
               y[j]+=temp*A[0];
               in=min(n-1,j+k);
               for(i=j+1;i<=in;i++)
               {
                  y[i]+=temp*A[i-j];
                  sum+=A[i-j]*x[i];
               }
               y[j]+=alpha*sum;
               A+=lda;
            }
         }
         else
         {
            jx=kx;
            jy=ky;
            for(j=0;j<n;j++)
            {
               temp=alpha*x[jx];
               sum=zero;
               ix=jx;
               iy=jy;
               in=min(n-1,j+k);
               y[jy]+=temp*A[0];
               for(i=j+1;i<=in;i++)
               {
                  ix+=incx;
                  iy+=incy;
                  y[iy]+=temp*A[i-j];
                  sum+=A[i-j]*x[ix];
               }
               y[jy]+=alpha*sum;
               jx+=incx;
               jy+=incy;
               A+=lda;
            }
         }
      }
   }
   return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dspmv   (char *UPLO, gblas_int *N, double *Alpha, double *A,
                           double *x, gblas_int *INCX, double *Beta, double *y,
                           gblas_int *INCY)
{
    const double one=1.0;
    const double zero=0.0;
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const double alpha=*Alpha;
    const double beta=*Beta;
    char uplo;
    gblas_int kx,ky,i,ix,iy,j,jx,jy;
    int info=0;
    double temp,sum;

    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if(n<0)
        info=2;
    else if(incx==0)
        info=6;
    else if(incy==0)
        info=9;

    if(info>0)
    {
        GBL_F77_xerbla("DSPMV ",&info);
        return 0;
    }

    if(n==0)
        return 0;

    kx=(incx>0)?0:(1-n)*incx;
    ky=(incy>0)?0:(1-n)*incy;

    if(beta==zero)
    {
        if(incy==1)
        {
            for(i=0;i<n;i++)
                y[i]=zero;
        }
        else
        {
            iy=ky;
            for(i=0;i<n;i++)
            {
                y[iy]=zero;
                iy+=incy;
            }

        }
    }
    else if(beta!=one)
    {
        if(incy==1)
        {
            for(i=0;i<n;i++)
                y[i]*=beta;
        }
        else
        {
            iy=ky;
            for(i=0;i<n;i++)
            {
                y[iy]*=beta;
                iy+=incy;
            }
        }
    }

    if(alpha!=zero)
    {
        if(uplo=='U')
        {
            if((incx==1)&&(incy==1))
            {
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[j];
                    sum=zero;
                    for(i=0;i<j;i++)
                    {
                        y[i]+=temp*A[i];
                        sum+=A[i]*x[i];
                    }
                    y[j]+=temp*A[j]+alpha*sum;
                    A+=j+1;
                }
            }
            else
            {
                jx=kx;
                jy=ky;
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    sum=zero;
                    ix=kx;
                    iy=ky;
                    for(i=0;i<j;i++)
                    {
                        y[iy]+=temp*A[i];
                        sum+=A[i]*x[ix];
                        ix+=incx;
                        iy+=incy;
                    }
                    y[jy]+=temp*A[j]+alpha*sum;
                    A+=j+1;
                    jx+=incx;
                    jy+=incy;
                }
            }
        }
        else
        {
            if((incx==1)&&(incy==1))
            {
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[j];
                    sum=zero;
                    y[j]+=temp*A[0];
                    for(i=j+1;i<n;i++)
                    {
                        y[i]+=temp*A[i-j];
                        sum+=A[i-j]*x[i];
                    }
                    y[j]+=alpha*sum;
                    A+=n-j;
                }
            }
            else
            {
                jx=kx;
                jy=ky;
                for(j=0;j<n;j++)
                {
                    temp=alpha*x[jx];
                    sum=zero;
                    ix=jx;
                    iy=jy;
                    y[jy]+=temp*A[0];
                    for(i=j+1;i<n;i++)
                    {
                        ix+=incx;
                        iy+=incy;
                        y[iy]+=temp*A[i-j];
                        sum+=A[i-j]*x[ix];
                    }
                    y[jy]+=alpha*sum;
                    jx+=incx;
                    jy+=incy;
                    A+=n-j;
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtrmv   (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                           double *A, gblas_int *LDA, double *x,
                           gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    char uplo,trans,diag;
    gblas_int i,j,kx,jx,ix;
    int info=0;
    gblas_int nounit;

    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if((diag!='U')&&(diag!='N'))
        info=3;
    else if(n<0)
        info=4;
    else if(lda<n)
        info=6;
    else if(incx==0)
        info=8;

    if(info>0)
    {
        GBL_F77_xerbla("DTRMV ",&info);
        return info;
    }

    if(n==0)
        return 0;

    nounit=(diag=='N');

    if(incx==1)
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<j;i++)
                        x[i]+=x[j]*A[i];
                    if(nounit)
                        x[j]*=A[j];
                    A+=lda;
                }
            }
            else
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    for(i=n-1;i>j;i--)
                        x[i]+=x[j]*A[i];
                    if(nounit)
                        x[j]*=A[j];
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    if(nounit)
                        x[j]*=A[j];
                    for(i=j-1;i>=0;i--)
                        x[j]+=A[i]*x[i];
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[j]*=A[j];
                    for(i=j+1;i<n;i++)
                        x[j]+=A[i]*x[i];
                    A+=lda;
                }
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(trans=='N')
        {
            if(uplo=='U')
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=kx;
                    for(i=0;i<j;i++)
                    {
                        x[ix]+=x[jx]*A[i];
                        ix+=incx;
                    }
                    if(nounit)
                        x[jx]*=A[j];
                    A+=lda;
                    jx+=incx;
                }
            }
            else
            {
                A+=n*lda;
                kx+=n*incx;
                jx=kx;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    jx-=incx;
                    ix=kx;
                    for(i=n-1;i>j;i--)
                    {
                        ix-=incx;
                        x[ix]+=x[jx]*A[i];
                    }
                    if(nounit)
                        x[jx]*=A[j];
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                A+=n*lda;
                jx=kx+n*incx;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    jx-=incx;
                    ix=jx;
                    if(nounit)
                        x[jx]*=A[j];
                    for(i=j-1;i>=0;i--)
                    {
                        ix-=incx;
                        x[jx]+=A[i]*x[ix];
                    }
                }
            }
            else
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=jx;
                    if(nounit)
                        x[jx]*=A[j];
                    for(i=j+1;i<n;i++)
                    {
                        ix+=incx;
                        x[jx]+=A[i]*x[ix];
                    }
                    A+=lda;
                    jx+=incx;
                }
            }
        }
    }

    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtbmv   (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                           gblas_int *K, double *A, gblas_int *LDA, double *x,
                           gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int k=*K;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    char uplo,trans,diag;
    gblas_int i,j,kx,jx,ix,i0,in;
    int info=0;
    gblas_int nounit;

    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if((diag!='U')&&(diag!='N'))
        info=3;
    else if(n<0)
        info=4;
    else if(k<0)
        info=5;
    else if(lda<k+1)
        info=7;
    else if(incx==0)
        info=9;

    if(info>0)
    {
        GBL_F77_xerbla("DTBMV ",&info);
        return info;
    }

    if(n==0)
        return 0;

    nounit=(diag=='N');

    if(incx==1)
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                for(j=0;j<n;j++)
                {
                    i0=max(0,j-k);
                    for(i=i0;i<j;i++)
                        x[i]+=x[j]*A[k-j+i];
                    if(nounit)
                        x[j]*=A[k];
                    A+=lda;
                }
            }
            else
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    in=min(n-1,j+k);
                    for(i=in;i>j;i--)
                        x[i]+=x[j]*A[i-j];
                    if(nounit)
                        x[j]*=A[0];
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    if(nounit)
                        x[j]*=A[k];
                    i0=max(0,j-k);
                    for(i=j-1;i>=i0;i--)
                        x[j]+=A[k+i-j]*x[i];
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[j]*=A[0];
                    in=min(n-1,j+k);
                    for(i=j+1;i<=in;i++)
                        x[j]+=A[i-j]*x[i];
                    A+=lda;
                }
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(trans=='N')
        {
            if(uplo=='U')
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    i0=max(0,j-k);
                    ix=kx+i0*incx;
                    for(i=i0;i<j;i++)
                    {
                        x[ix]+=x[jx]*A[k+i-j];
                        ix+=incx;
                    }
                    if(nounit)
                        x[jx]*=A[k];
                    A+=lda;
                    jx+=incx;
                }
            }
            else
            {
                A+=n*lda;
                kx+=n*incx;
                jx=kx;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    jx-=incx;
                    in=min(n-1,j+k);
                    ix=kx-(n-in)*incx;
                    for(i=in;i>j;i--)
                    {
                        x[ix]+=x[jx]*A[i-j];
                        ix-=incx;
                    }
                    if(nounit)
                        x[jx]*=A[0];
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                A+=n*lda;
                jx=kx+n*incx;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    jx-=incx;
                    ix=jx;
                    if(nounit)
                        x[jx]*=A[k];
                    i0=max(0,j-k);
                    for(i=j-1;i>=i0;i--)
                    {
                        ix-=incx;
                        x[jx]+=A[k+i-j]*x[ix];
                    }
                }
            }
            else
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=jx;
                    if(nounit)
                        x[jx]*=A[0];
                    in=min(n-1,j+k);
                    for(i=j+1;i<=in;i++)
                    {
                        ix+=incx;
                        x[jx]+=A[i-j]*x[ix];
                    }
                    A+=lda;
                    jx+=incx;
                }
            }
        }
    }

    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtpmv   (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                           double *A, double *x, gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    char uplo,trans,diag;
    int info=0;
    gblas_int nounit;
    gblas_int i,j,kx,jx,ix;

    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if((diag!='U')&&(diag!='N'))
        info=3;
    else if(n<0)
        info=4;
    else if(incx==0)
        info=7;

    if(info>0)
    {
        GBL_F77_xerbla("DTPMV ",&info);
        return info;
    }

    if(n==0)
        return 0;

    nounit=(diag=='N');

    if(incx==1)
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<j;i++)
                        x[i]+=x[j]*A[i];
                    if(nounit)
                        x[j]*=A[j];
                    A+=j+1;
                }
            }
            else
            {
                A+=n*(n+1)/2;
                for(j=n-1;j>=0;j--)
                {
                    A-=n-j;
                    for(i=n-1;i>j;i--)
                        x[i]+=x[j]*A[i-j];
                    if(nounit)
                        x[j]*=A[0];
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                A+=n*(n+1)/2;
                for(j=n-1;j>=0;j--)
                {
                    A-=j+1;
                    if(nounit)
                        x[j]*=A[j];
                    for(i=j-1;i>=0;i--)
                        x[j]+=A[i]*x[i];
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[j]*=A[0];
                    for(i=j+1;i<n;i++)
                        x[j]+=A[i-j]*x[i];
                    A+=n-j;
                }
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(trans=='N')
        {
            if(uplo=='U')
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=kx;
                    for(i=0;i<j;i++)
                    {
                        x[ix]+=x[jx]*A[i];
                        ix+=incx;
                    }
                    if(nounit)
                        x[jx]*=A[j];
                    A+=j+1;
                    jx+=incx;
                }
            }
            else
            {
                A+=n*(n+1)/2;
                kx+=n*incx;
                jx=kx;
                for(j=n-1;j>=0;j--)
                {
                    A-=n-j;
                    jx-=incx;
                    ix=kx;
                    for(i=n-1;i>j;i--)
                    {
                        ix-=incx;
                        x[ix]+=x[jx]*A[i-j];
                    }
                    if(nounit)
                        x[jx]*=A[0];
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                A+=n*(n+1)/2;
                jx=kx+n*incx;
                for(j=n-1;j>=0;j--)
                {
                    A-=j+1;
                    jx-=incx;
                    ix=jx;
                    if(nounit)
                        x[jx]*=A[j];
                    for(i=j-1;i>=0;i--)
                    {
                        ix-=incx;
                        x[jx]+=A[i]*x[ix];
                    }
                }
            }
            else
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=jx;
                    if(nounit)
                        x[jx]*=A[0];
                    for(i=j+1;i<n;i++)
                    {
                        ix+=incx;
                        x[jx]+=A[i-j]*x[ix];
                    }
                    A+=n-j;
                    jx+=incx;
                }
            }
        }
    }

    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtrsv   (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                           double *A, gblas_int *LDA, double *x,
                           gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    char uplo,trans,diag;
    gblas_int i,j,ix,jx,kx;
    gblas_int nounit;
    int info=0;

    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if((diag!='U')&&(diag!='N'))
        info=3;
    else if(n<0)
        info=4;
    else if(lda<n)
        info=6;
    else if(incx==0)
        info=8;

    if(info>0)
    {
        GBL_F77_xerbla("DTRSV ",&info);
        return info;
    }

    if(n==0)
        return 0;

    nounit=(diag=='N');


    if(incx==1)
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    if(nounit)
                        x[j]=x[j]/A[j];
                    for(i=j-1;i>=0;i--)
                        x[i]-=x[j]*A[i];
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[j]=x[j]/A[j];
                    for(i=j+1;i<n;i++)
                        x[i]-=x[j]*A[i];
                    A+=lda;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<j;i++)
                        x[j]-=x[i]*A[i];
                    if(nounit)
                        x[j]=x[j]/A[j];
                    A+=lda;
                }
            }
            else
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    for(i=n-1;i>j;i--)
                        x[j]-=x[i]*A[i];
                    if(nounit)
                        x[j]=x[j]/A[j];
                }
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(trans=='N')
        {
            if(uplo=='U')
            {
                A+=n*lda;
                jx=kx+n*incx;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    jx-=incx;
                    ix=jx;
                    if(nounit)
                        x[jx]=x[jx]/A[j];
                    for(i=j-1;i>=0;i--)
                    {
                        ix-=incx;
                        x[ix]-=x[jx]*A[i];
                    }
                }
            }
            else
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[jx]=x[jx]/A[j];
                    ix=jx;
                    for(i=j+1;i<n;i++)
                    {
                        ix+=incx;
                        x[ix]-=x[jx]*A[i];
                    }
                    jx+=incx;
                    A+=lda;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=kx;
                    for(i=0;i<j;i++)
                    {
                        x[jx]-=x[ix]*A[i];
                        ix+=incx;
                    }
                    if(nounit)
                        x[jx]=x[jx]/A[j];
                    jx+=incx;
                    A+=lda;
                }
            }
            else
            {
                A+=n*lda;
                kx+=n*incx;
                jx=kx;
                for(j=n-1;j>=0;j--)
                {
                    jx-=incx;
                    ix=kx;
                    A-=lda;
                    for(i=n-1;i>j;i--)
                    {
                        ix-=incx;
                        x[jx]-=x[ix]*A[i];
                    }
                    if(nounit)
                        x[jx]=x[jx]/A[j];
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtbsv   (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                           gblas_int *K, double *A, gblas_int *LDA, double *x,
                           gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int k=*K;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    char uplo,trans,diag;
    gblas_int i,j,ix,jx,kx,i0,in;
    gblas_int nounit;
    int info=0;

    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if((diag!='U')&&(diag!='N'))
        info=3;
    else if(n<0)
        info=4;
    else if(k<0)
        info=5;
    else if(lda<k+1)
        info=7;
    else if(incx==0)
        info=9;

    if(info>0)
    {
        GBL_F77_xerbla("DTBSV ",&info);
        return info;
    }

    if(n==0)
        return 0;

    nounit=(diag=='N');

    if(incx==1)
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    if(nounit)
                        x[j]=x[j]/A[k];
                    i0=max(0,j-k);
                    for(i=j-1;i>=i0;i--)
                        x[i]-=x[j]*A[k-j+i];
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[j]=x[j]/A[0];
                    in=min(n-1,j+k);
                    for(i=j+1;i<=in;i++)
                        x[i]-=x[j]*A[i-j];
                    A+=lda;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                for(j=0;j<n;j++)
                {
                    i0=max(0,j-k);
                    for(i=i0;i<j;i++)
                        x[j]-=x[i]*A[k-j+i];
                    if(nounit)
                        x[j]=x[j]/A[k];
                    A+=lda;
                }
            }
            else
            {
                A+=n*lda;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    in=min(n-1,j+k);
                    for(i=in;i>j;i--)
                        x[j]-=x[i]*A[i-j];
                    if(nounit)
                        x[j]=x[j]/A[0];
                }
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(trans=='N')
        {
            if(uplo=='U')
            {
                A+=n*lda;
                jx=kx+n*incx;
                for(j=n-1;j>=0;j--)
                {
                    A-=lda;
                    jx-=incx;
                    ix=jx;
                    if(nounit)
                        x[jx]=x[jx]/A[k];
                    i0=max(0,j-k);
                    for(i=j-1;i>=i0;i--)
                    {
                        ix-=incx;
                        x[ix]-=x[jx]*A[k-j+i];
                    }
                }
            }
            else
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[jx]=x[jx]/A[0];
                    ix=jx;
                    in=min(n-1,j+k);
                    for(i=j+1;i<=in;i++)
                    {
                        ix+=incx;
                        x[ix]-=x[jx]*A[i-j];
                    }
                    jx+=incx;
                    A+=lda;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    i0=max(0,j-k);
                    ix=kx+i0*incx;
                    for(i=i0;i<j;i++)
                    {
                        x[jx]-=x[ix]*A[k-j+i];
                        ix+=incx;
                    }
                    if(nounit)
                        x[jx]=x[jx]/A[k];
                    jx+=incx;
                    A+=lda;
                }
            }
            else
            {
                A+=n*lda;
                kx+=n*incx;
                jx=kx;
                for(j=n-1;j>=0;j--)
                {
                    jx-=incx;
                    A-=lda;
                    in=min(n-1,j+k);
                    ix=kx-(n-in)*incx;
                    for(i=in;i>j;i--)
                    {
                        x[jx]-=x[ix]*A[i-j];
                        ix-=incx;
                    }
                    if(nounit)
                        x[jx]=x[jx]/A[0];
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtpsv   (char *UPLO, char *TRANS, char *DIAG, gblas_int *N,
                           double *A, double *x, gblas_int *INCX)
{
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    char uplo,trans,diag;
    gblas_int i,j,ix,jx,kx;
    gblas_int nounit;
    int info=0;

    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if((diag!='U')&&(diag!='N'))
        info=3;
    else if(n<0)
        info=4;
    else if(incx==0)
        info=7;

    if(info>0)
    {
        GBL_F77_xerbla("DTPSV ",&info);
        return info;
    }

    if(n==0)
        return 0;

    nounit=(diag=='N');


    if(incx==1)
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                A+=n*(n+1)/2;
                for(j=n-1;j>=0;j--)
                {
                    A-=j+1;
                    if(nounit)
                        x[j]=x[j]/A[j];
                    for(i=j-1;i>=0;i--)
                        x[i]-=x[j]*A[i];
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[j]=x[j]/A[0];
                    for(i=j+1;i<n;i++)
                        x[i]-=x[j]*A[i-j];
                    A+=n-j;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<j;i++)
                        x[j]-=x[i]*A[i];
                    if(nounit)
                        x[j]=x[j]/A[j];
                    A+=j+1;
                }
            }
            else
            {
                A+=n*(n+1)/2;
                for(j=n-1;j>=0;j--)
                {
                    A-=n-j;
                    for(i=n-1;i>j;i--)
                        x[j]-=x[i]*A[i-j];
                    if(nounit)
                        x[j]=x[j]/A[0];
                }
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(trans=='N')
        {
            if(uplo=='U')
            {
                A+=n*(n+1)/2;
                jx=kx+n*incx;
                for(j=n-1;j>=0;j--)
                {
                    A-=j+1;
                    jx-=incx;
                    ix=jx;
                    if(nounit)
                        x[jx]=x[jx]/A[j];
                    for(i=j-1;i>=0;i--)
                    {
                        ix-=incx;
                        x[ix]-=x[jx]*A[i];
                    }
                }
            }
            else
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        x[jx]=x[jx]/A[0];
                    ix=jx;
                    for(i=j+1;i<n;i++)
                    {
                        ix+=incx;
                        x[ix]-=x[jx]*A[i-j];
                    }
                    jx+=incx;
                    A+=n-j;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                jx=kx;
                for(j=0;j<n;j++)
                {
                    ix=kx;
                    for(i=0;i<j;i++)
                    {
                        x[jx]-=x[ix]*A[i];
                        ix+=incx;
                    }
                    if(nounit)
                        x[jx]=x[jx]/A[j];
                    jx+=incx;
                    A+=j+1;
                }
            }
            else
            {
                A+=n*(n+1)/2;
                kx+=n*incx;
                jx=kx;
                for(j=n-1;j>=0;j--)
                {
                    jx-=incx;
                    ix=kx;
                    A-=n-j;
                    for(i=n-1;i>j;i--)
                    {
                        ix-=incx;
                        x[jx]-=x[ix]*A[i-j];
                    }
                    if(nounit)
                        x[jx]=x[jx]/A[0];
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dger    (gblas_int *M, gblas_int *N, double *Alpha, double *x,
                           gblas_int *INCX, double *y, gblas_int *INCY,
                           double *A, gblas_int *LDA)
{
    const gblas_int m=*M;
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const gblas_int lda=*LDA;
    const double alpha=*Alpha;
    const double zero=0.0;
    gblas_int i,j,kx,jy,ix;
    int info=0;

    if(m<0)
        info=1;
    else if(n<0)
        info=2;
    else if(incx==0)
        info=5;
    else if(incy==0)
        info=7;
    else if(lda<m)
        info=9;

    if(info!=0)
    {
        GBL_F77_xerbla("DGER  ",&info);
        return info;
    }

    if((m==0)||(n==0)||(alpha==zero))
        return 0;

    jy=(incy>0)?0:(1-n)*incy;
    kx=(incx>0)?0:(1-m)*incx;

    if(incx==1)
    {
        for(j=0;j<n;j++)
        {
            for(i=0;i<m;i++)
                A[i]+=x[i]*alpha*y[jy];
            jy+=incy;
            A+=lda;
        }
    }
    else
    {
        for(j=0;j<n;j++)
        {
            ix=kx;
            for(i=0;i<m;i++)
            {
                A[i]+=x[ix]*alpha*y[jy];
                ix+=incx;
            }
            A+=lda;
            jy+=incy;
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsyr    (char *UPLO, gblas_int *N, double *Alpha, double *x,
                           gblas_int *INCX, double *A, gblas_int *LDA)
{
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    const double alpha=*Alpha;
    const double zero=0.0;
    double t;
    char uplo;
    gblas_int i,j,kx,jx,ix;
    int info=0;

    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if(n<0)
        info=2;
    else if(incx==0)
        info=5;
    else if(lda<n)
        info=7;

    if(info>0)
    {
        GBL_F77_xerbla("DSYR  ",&info);
        return info;
    }

    if((n==0)||(alpha==zero))
        return 0;

    if(incx==1)
    {
        if(uplo=='U')
        {
            for(j=0;j<n;j++)
            {
                t=alpha*x[j];
                for(i=0;i<=j;i++)
                    A[i]+=x[i]*t;
                A+=lda;
            }
        }
        else
        {
            for(j=0;j<n;j++)
            {
                t=alpha*x[j];
                for(i=j;i<n;i++)
                    A[i]+=x[i]*t;
                A+=lda;
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(uplo=='U')
        {
            jx=kx;
            for(j=0;j<n;j++)
            {
                t=alpha*x[jx];
                ix=kx;
                for(i=0;i<=j;i++)
                {
                    A[i]+=x[ix]*t;
                    ix+=incx;
                }
                A+=lda;
                jx+=incx;
            }
        }
        else
        {
            jx=kx;
            for(j=0;j<n;j++)
            {
                t=alpha*x[jx];
                ix=jx;
                for(i=j;i<n;i++)
                {
                    A[i]+=x[ix]*t;
                    ix+=incx;
                }
                jx+=incx;
                A+=lda;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dspr    (char *UPLO, gblas_int *N, double *Alpha, double *x,
                           gblas_int *INCX, double *A)
{
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const double alpha=*Alpha;
    const double zero=0.0;
    double t;
    char uplo;
    gblas_int i,j,kx,jx,ix;
    int info=0;

    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if(n<0)
        info=2;
    else if(incx==0)
        info=5;

    if(info>0)
    {
        GBL_F77_xerbla("DSPR  ",&info);
        return info;
    }

    if((n==0)||(alpha==zero))
        return 0;

    if(incx==1)
    {
        if(uplo=='U')
        {
            for(j=0;j<n;j++)
            {
                t=alpha*x[j];
                for(i=0;i<=j;i++)
                    A[i]+=x[i]*t;
                A+=j+1;
            }
        }
        else
        {
            for(j=0;j<n;j++)
            {
                t=alpha*x[j];
                for(i=j;i<n;i++)
                    A[i-j]+=x[i]*t;
                A+=n-j;
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        if(uplo=='U')
        {
            jx=kx;
            for(j=0;j<n;j++)
            {
                t=alpha*x[jx];
                ix=kx;
                for(i=0;i<=j;i++)
                {
                    A[i]+=x[ix]*t;
                    ix+=incx;
                }
                A+=j+1;
                jx+=incx;
            }
        }
        else
        {
            jx=kx;
            for(j=0;j<n;j++)
            {
                t=alpha*x[jx];
                ix=jx;
                for(i=j;i<n;i++)
                {
                    A[i-j]+=x[ix]*t;
                    ix+=incx;
                }
                jx+=incx;
                A+=n-j;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsyr2   (char *UPLO, gblas_int *N, double *Alpha, double *x,
                           gblas_int *INCX, double *y, gblas_int *INCY,
                           double *A, gblas_int *LDA)
{
    const gblas_int n=*N;
    const gblas_int lda=*LDA;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const double alpha=*Alpha;
    const double zero=0.0;
    double tx,ty;
    char uplo;
    gblas_int i,j,kx,ky,jx,ix,jy,iy;
    int info=0;

    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if(n<0)
        info=2;
    else if(incx==0)
        info=5;
    else if(incy==0)
        info=7;
    else if(lda<n)
        info=9;

    if(info>0)
    {
        GBL_F77_xerbla("DSYR2 ",&info);
        return info;
    }

    if((n==0)||(alpha==zero))
        return 0;

    if((incx==1)&&(incy==1))
    {
        if(uplo=='U')
        {
            for(j=0;j<n;j++)
            {
                tx=alpha*x[j];
                ty=alpha*y[j];
                for(i=0;i<=j;i++)
                    A[i]+=x[i]*ty+y[i]*tx;
                A+=lda;
            }
        }
        else
        {
            for(j=0;j<n;j++)
            {
                tx=alpha*x[j];
                ty=alpha*y[j];
                for(i=j;i<n;i++)
                    A[i]+=x[i]*ty+y[i]*tx;
                A+=lda;
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        ky=(incy>0)?0:(1-n)*incy;
        if(uplo=='U')
        {
            jx=kx;
            jy=ky;
            for(j=0;j<n;j++)
            {
                tx=alpha*x[jx];
                ty=alpha*y[jy];
                ix=kx;
                iy=ky;
                for(i=0;i<=j;i++)
                {
                    A[i]+=x[ix]*ty+y[iy]*tx;
                    ix+=incx;
                    iy+=incy;
                }
                A+=lda;
                jx+=incx;
                jy+=incy;
            }
        }
        else
        {
            jx=kx;
            jy=ky;
            for(j=0;j<n;j++)
            {
                tx=alpha*x[jx];
                ty=alpha*y[jy];
                ix=jx;
                iy=jy;
                for(i=j;i<n;i++)
                {
                    A[i]+=x[ix]*ty+y[iy]*tx;
                    ix+=incx;
                    iy+=incy;
                }
                jx+=incx;
                jy+=incy;
                A+=lda;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dspr2   (char *UPLO, gblas_int *N, double *Alpha, double *x,
                           gblas_int *INCX, double *y, gblas_int *INCY,
                           double *A)
{
    const gblas_int n=*N;
    const gblas_int incx=*INCX;
    const gblas_int incy=*INCY;
    const double alpha=*Alpha;
    const double zero=0.0;
    double tx,ty;
    char uplo;
    gblas_int i,j,kx,ky,jx,ix,jy,iy;
    int info=0;

    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if(n<0)
        info=2;
    else if(incx==0)
        info=5;
    else if(incy==0)
        info=7;

    if(info>0)
    {
        GBL_F77_xerbla("DSPR2 ",&info);
        return info;
    }

    if((n==0)||(alpha==zero))
        return 0;

    if((incx==1)&&(incy==1))
    {
        if(uplo=='U')
        {
            for(j=0;j<n;j++)
            {
                tx=alpha*x[j];
                ty=alpha*y[j];
                for(i=0;i<=j;i++)
                    A[i]+=x[i]*ty+y[i]*tx;
                A+=j+1;
            }
        }
        else
        {
            for(j=0;j<n;j++)
            {
                tx=alpha*x[j];
                ty=alpha*y[j];
                for(i=j;i<n;i++)
                    A[i-j]+=x[i]*ty+y[i]*tx;
                A+=n-j;
            }
        }
    }
    else
    {
        kx=(incx>0)?0:(1-n)*incx;
        ky=(incy>0)?0:(1-n)*incy;
        if(uplo=='U')
        {
            jx=kx;
            jy=ky;
            for(j=0;j<n;j++)
            {
                tx=alpha*x[jx];
                ty=alpha*y[jy];
                ix=kx;
                iy=ky;
                for(i=0;i<=j;i++)
                {
                    A[i]+=x[ix]*ty+y[iy]*tx;
                    ix+=incx;
                    iy+=incy;
                }
                A+=j+1;
                jx+=incx;
                jy+=incy;
            }
        }
        else
        {
            jx=kx;
            jy=ky;
            for(j=0;j<n;j++)
            {
                tx=alpha*x[jx];
                ty=alpha*y[jy];
                ix=jx;
                iy=jy;
                for(i=j;i<n;i++)
                {
                    A[i-j]+=x[ix]*ty+y[iy]*tx;
                    ix+=incx;
                    iy+=incy;
                }
                jx+=incx;
                jy+=incy;
                A+=n-j;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dgemm   (char *TRANSA, char *TRANSB, gblas_int *M,
                           gblas_int *N, gblas_int *K, double *Alpha, double *A,
                           gblas_int *LDA, double *B, gblas_int *LDB,
                           double *Beta, double *C, gblas_int *LDC)
{
    const gblas_int m=*M;
    const gblas_int n=*N;
    const gblas_int k=*K;
    const double alpha=*Alpha;
    const double beta=*Beta;
    const gblas_int lda=*LDA;
    const gblas_int ldb=*LDB;
    const gblas_int ldc=*LDC;
    const double zero=0.0;
    const double one=1.0;
    char transA,transB;
    int info=0;
    gblas_int i,j,l;
    double *a,*b,*c;
    double s,t;

    transA=(char)toupper(*TRANSA);
    transB=(char)toupper(*TRANSB);

    if((transA!='N')&&(transA!='T')&&(transA!='C'))
        info=1;
    else if((transB!='N')&&(transB!='T')&&(transB!='C'))
        info=2;
    else if(m<0)
        info=3;
    else if(n<0)
        info=4;
    else if(k<0)
        info=5;
    else if(lda<max(1,(transA!='N')?k:m))
        info=8;
    else if(ldb<max(1,(transB!='N')?n:k))
        info=10;
    else if(ldc<max(1,m))
        info=13;

    if(info>0)
    {
        GBL_F77_xerbla("DGEMM ",&info);
        return info;
    }

    if((m==0)||(n==0)||(((alpha==zero)||(k==0))&&(beta==one)))
        return 0;


    if(alpha==zero)
    {
        c=C;
        if(beta==zero)
        {
            for(j=0;j<n;j++)
            {
                for(i=0;i<m;i++)
                    c[i]=zero;
                c+=ldc;
            }
        }
        else
        {
            for(j=0;j<n;j++)
            {
                for(i=0;i<m;i++)
                    c[i]*=beta;
                c+=ldc;
            }
        }
    }
    else if((transA=='N')&&(transB=='N'))
    {
        c=C;
        b=B;
        for(j=0;j<n;j++)
        {
            for(i=0;i<m;i++)
                c[i]*=beta;
            a=A;
            for(l=0;l<k;l++)
            {
                for(i=0;i<m;i++)
                {
                    c[i]+=alpha*b[l]*a[i];
                }
                a+=lda;
            }
            b+=ldb;
            c+=ldc;
        }
    }
    else if((transA!='N')&&(transB=='N'))
    {
        b=B;
        c=C;
        for(j=0;j<n;j++)
        {
            a=A;
            for(i=0;i<m;i++)
            {
                s=zero;
                for(l=0;l<k;l++)
                {
                    s+=a[l]*b[l];
                }
                c[i]=alpha*s+beta*c[i];
                a+=lda;
            }
            b+=ldb;
            c+=ldc;
        }
    }
    else if((transA=='N')&&(transB!='N'))
    {
        c=C;
        for(j=0;j<n;j++)
        {
            for(i=0;i<m;i++)
                c[i]*=beta;
            a=A;
            b=B;
            for(l=0;l<k;l++)
            {
                t=alpha*b[j];
                for(i=0;i<m;i++)
                {
                    c[i]+=t*a[i];
                }
                a+=lda;
                b+=ldb;
            }
            c+=ldc;
        }
    }
    else
    {
        c=C;
        for(j=0;j<n;j++)
        {
            a=A;
            for(i=0;i<m;i++)
            {
                s=zero;
                b=B;
                for(l=0;l<k;l++)
                {
                    s+=a[l]*b[j];
                    b+=ldb;
                }
                c[i]=alpha*s+beta*c[i];
                a+=lda;
            }
            c+=ldc;
        }

    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsymm   (char *SIDE, char *UPLO, gblas_int *M, gblas_int *N,
                           double *Alpha, double *A, gblas_int *LDA, double *B,
                           gblas_int *LDB, double *Beta, double *C,
                           gblas_int *LDC)
{
    const gblas_int m=*M;
    const gblas_int n=*N;
    const double alpha=*Alpha;
    const double beta=*Beta;
    const gblas_int lda=*LDA;
    const gblas_int ldb=*LDB;
    const gblas_int ldc=*LDC;
    const double zero=0.0;
    const double one=1.0;
    char side,uplo;
    int info=0;
    gblas_int i,j,k;
    double *a,*b,*c,*at,*bt;
    double s,t;

    side=(char)toupper(*SIDE);
    uplo=(char)toupper(*UPLO);

    if((side!='L')&&(side!='R'))
        info=1;
    else if((uplo!='U')&&(uplo!='L'))
        info=2;
    else if(m<0)
        info=3;
    else if(n<0)
        info=4;
    else if(lda<max(1,(side=='L')?m:n))
        info=7;
    else if(ldb<max(1,m))
        info=9;
    else if(ldc<max(1,m))
        info=12;

    if(info>0)
    {
        GBL_F77_xerbla("DSYMM ",&info);
        return info;
    }

    if((m==0)||(n==0)||((alpha==zero)&&(beta==one)))
        return 0;

    if(alpha==zero)
    {
        c=C;
        if(beta==zero)
        {
            for(j=0;j<n;j++)
            {
                for(i=0;i<m;i++)
                    c[i]=zero;
                c+=ldc;
            }
        }
        else
        {
            for(j=0;j<n;j++)
            {
                for(i=0;i<m;i++)
                    c[i]*=beta;
                c+=ldc;
            }
        }
    }
    else if(side=='L')
    {
        if(uplo=='U')
        {
            b=B;
            c=C;
            for(j=0;j<n;j++)
            {
                a=A;
                for(i=0;i<m;i++)
                {
                    t=alpha*b[i];
                    s=zero;
                    for(k=0;k<i;k++)
                    {
                        c[k]+=t*a[k];
                        s+=b[k]*a[k];
                    }
                    c[i]=beta*c[i]+t*a[i]+alpha*s;
                    a+=lda;
                }
                b+=ldb;
                c+=ldc;
            }
        }
        else
        {
            b=B;
            c=C;
            for(j=0;j<n;j++)
            {
                a=A+m*lda;
                for(i=m-1;i>=0;i--)
                {
                    a-=lda;
                    t=alpha*b[i];
                    s=zero;
                    for(k=i+1;k<m;k++)
                    {
                        c[k]+=t*a[k];
                        s+=b[k]*a[k];
                    }
                    c[i]=beta*c[i]+t*a[i]+alpha*s;
                }
                b+=ldb;
                c+=ldc;
            }
        }
    }
    else
    {
        if(uplo=='U')
        {
            a=A;
            c=C;
            b=B;
            for(j=0;j<n;j++)
            {
                t=alpha*a[j];
                for(i=0;i<m;i++)
                    c[i]=c[i]*beta+t*b[i];
                bt=B;
                for(k=0;k<j;k++)
                {
                    t=alpha*a[k];
                    for(i=0;i<m;i++)
                        c[i]+=bt[i]*t;
                    bt+=ldb;
                }
                at=A+(j+1)*lda;
                bt=B+(j+1)*ldb;
                for(k=j+1;k<n;k++)
                {
                    t=alpha*at[j];
                    for(i=0;i<m;i++)
                        c[i]+=t*bt[i];
                    at+=lda;
                    bt+=ldb;
                }
                a+=lda;
                b+=ldb;
                c+=ldc;
            }
        }
        else
        {
            a=A;
            c=C;
            b=B;
            for(j=0;j<n;j++)
            {
                t=alpha*a[j];
                for(i=0;i<m;i++)
                    c[i]=c[i]*beta+t*b[i];
                bt=B;
                at=A;
                for(k=0;k<j;k++)
                {
                    t=alpha*at[j];
                    for(i=0;i<m;i++)
                        c[i]+=bt[i]*t;
                    at+=lda;
                    bt+=ldb;
                }
                bt=B+(j+1)*ldb;
                for(k=j+1;k<n;k++)
                {
                    t=alpha*at[k];
                    for(i=0;i<m;i++)
                        c[i]+=t*bt[i];
                    bt+=ldb;
                }
                a+=lda;
                b+=ldb;
                c+=ldc;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsyrk   (char *UPLO, char *TRANS, gblas_int *N, gblas_int *K,
                           double *Alpha, double *A, gblas_int *LDA,
                           double *Beta, double *C, gblas_int *LDC)
{

    const gblas_int n=*N;
    const gblas_int k=*K;
    const double alpha=*Alpha;
    const double beta=*Beta;
    const gblas_int lda=*LDA;
    const gblas_int ldc=*LDC;
    const double zero=0.0;
    const double one=1.0;
    char trans,uplo;
    int info=0;
    gblas_int i,j,l;
    double *a,*c,*at;
    double t;

    trans=(char)toupper(*TRANS);
    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if(n<0)
        info=3;
    else if(k<0)
        info=4;
    else if(lda<max(1,(trans=='N')?n:k))
        info=7;
    else if(ldc<max(1,n))
        info=10;

    if(info>0)
    {
        GBL_F77_xerbla("DSYRK ",&info);
        return info;
    }

    if((n==0)||(((alpha==zero)||(k==0))&&(beta==one)))
        return 0;

    if(alpha==zero)
    {
        if(uplo=='U')
        {
            c=C;
            if(beta==zero)
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<=j;i++)
                        c[i]=zero;
                    c+=ldc;
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<=j;i++)
                        c[i]*=beta;
                    c+=ldc;
                }
            }
        }
        else
        {
            c=C;
            if(beta==zero)
            {
                for(j=0;j<n;j++)
                {
                    for(i=j;i<n;i++)
                        c[i]=zero;
                    c+=ldc;
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    for(i=j;i<n;i++)
                        c[i]*=beta;
                    c+=ldc;
                }
            }
        }
    }
    else if(trans=='N')
    {
        if(uplo=='U')
        {
            c=C;
            for(j=0;j<n;j++)
            {
                for(i=0;i<=j;i++)
                    c[i]*=beta;
                a=A;
                for(l=0;l<k;l++)
                {
                    t=alpha*a[j];
                    for(i=0;i<=j;i++)
                        c[i]+=t*a[i];
                    a+=lda;
                }
                c+=ldc;
            }
        }
        else
        {
            c=C;
            for(j=0;j<n;j++)
            {
                for(i=j;i<n;i++)
                    c[i]*=beta;
                a=A;
                for(l=0;l<k;l++)
                {
                    t=alpha*a[j];
                    for(i=j;i<n;i++)
                        c[i]+=t*a[i];
                    a+=lda;
                }
                c+=ldc;
            }
        }
    }
    else
    {
        if(uplo=='U')
        {
            c=C;
            at=A;
            for(j=0;j<n;j++)
            {
                a=A;
                for(i=0;i<=j;i++)
                {
                    t=zero;
                    for(l=0;l<k;l++)
                        t+=a[l]*at[l];
                    c[i]=alpha*t+beta*c[i];
                    a+=lda;
                }
                at+=lda;
                c+=ldc;
            }
        }
        else
        {
            at=A;
            c=C;
            for(j=0;j<n;j++)
            {
                a=A+j*lda;
                for(i=j;i<n;i++)
                {
                    t=zero;
                    for(l=0;l<k;l++)
                        t+=a[l]*at[l];
                    c[i]=alpha*t+beta*c[i];
                    a+=lda;
                }
                at+=lda;
                c+=ldc;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dsyr2k  (char *UPLO, char *TRANS, gblas_int *N, gblas_int *K,
                           double *Alpha, double *A, gblas_int *LDA, double *B,
                           gblas_int *LDB, double *Beta, double *C,
                           gblas_int *LDC)
{
    const gblas_int n=*N;
    const gblas_int k=*K;
    const double alpha=*Alpha;
    const double beta=*Beta;
    const gblas_int lda=*LDA;
    const gblas_int ldb=*LDB;
    const gblas_int ldc=*LDC;
    const double zero=0.0;
    const double one=1.0;
    char trans,uplo;
    int info=0;
    gblas_int i,j,l;
    double *a,*b,*c,*at,*bt;
    double s,t;

    trans=(char)toupper(*TRANS);
    uplo=(char)toupper(*UPLO);

    if((uplo!='U')&&(uplo!='L'))
        info=1;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=2;
    else if(n<0)
        info=3;
    else if(k<0)
        info=4;
    else if(lda<max(1,(trans=='N')?n:k))
        info=7;
    else if(ldb<max(1,(trans=='N')?n:k))
        info=9;
    else if(ldc<max(1,n))
        info=12;

    if(info>0)
    {
        GBL_F77_xerbla("DSYR2K ",&info);
        return info;
    }

    if((n==0)||(((alpha==zero)||(k==0))&&(beta==one)))
        return 0;

    if(alpha==zero)
    {
        if(uplo=='U')
        {
            c=C;
            if(beta==zero)
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<=j;i++)
                        c[i]=zero;
                    c+=ldc;
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    for(i=0;i<=j;i++)
                        c[i]*=beta;
                    c+=ldc;
                }
            }
        }
        else
        {
            c=C;
            if(beta==zero)
            {
                for(j=0;j<n;j++)
                {
                    for(i=j;i<n;i++)
                        c[i]=zero;
                    c+=ldc;
                }
            }
            else
            {
                for(j=0;j<n;j++)
                {
                    for(i=j;i<n;i++)
                        c[i]*=beta;
                    c+=ldc;
                }
            }
        }
    }
    else if(trans=='N')
    {
        if(uplo=='U')
        {
            c=C;
            for(j=0;j<n;j++)
            {
                for(i=0;i<=j;i++)
                    c[i]*=beta;
                a=A;
                b=B;
                for(l=0;l<k;l++)
                {
                    s=alpha*b[j];
                    t=alpha*a[j];
                    for(i=0;i<=j;i++)
                        c[i]+=s*a[i]+t*b[i];
                    a+=lda;
                    b+=ldb;
                }
                c+=ldc;
            }
        }
        else
        {
            c=C;
            for(j=0;j<n;j++)
            {
                for(i=j;i<n;i++)
                    c[i]*=beta;
                a=A;
                b=B;
                for(l=0;l<k;l++)
                {
                    s=alpha*b[j];
                    t=alpha*a[j];
                    for(i=j;i<n;i++)
                        c[i]+=s*a[i]+t*b[i];
                    a+=lda;
                    b+=ldb;
                }
                c+=ldc;
            }
        }
    }
    else
    {
        if(uplo=='U')
        {
            c=C;
            at=A;
            bt=B;
            for(j=0;j<n;j++)
            {
                a=A;
                b=B;
                for(i=0;i<=j;i++)
                {
                    s=zero;
                    t=zero;
                    for(l=0;l<k;l++)
                    {
                        s+=b[l]*at[l];
                        t+=a[l]*bt[l];
                    }
                    c[i]=alpha*t+alpha*s+beta*c[i];
                    a+=lda;
                    b+=ldb;
                }
                at+=lda;
                bt+=ldb;
                c+=ldc;
            }
        }
        else
        {
            c=C;
            at=A;
            bt=B;
            for(j=0;j<n;j++)
            {
                a=A+j*lda;
                b=B+j*ldb;
                for(i=j;i<n;i++)
                {
                    s=zero;
                    t=zero;
                    for(l=0;l<k;l++)
                    {
                        s+=b[l]*at[l];
                        t+=a[l]*bt[l];
                    }
                    c[i]=alpha*t+alpha*s+beta*c[i];
                    a+=lda;
                    b+=ldb;
                }
                at+=lda;
                bt+=ldb;
                c+=ldc;
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtrmm   (char *SIDE, char *UPLO, char *TRANS, char *DIAG,
                           gblas_int *M, gblas_int *N, double *Alpha, double *A,
                           gblas_int *LDA, double *B, gblas_int *LDB)
{
    const gblas_int m=*M;
    const gblas_int n=*N;
    const double alpha=*Alpha;
    const gblas_int lda=*LDA;
    const gblas_int ldb=*LDB;
    const double zero=0.0;
    char side,uplo,trans,diag;
    gblas_int i,j,k;
    double *a,*b,*bt;
    double t;
    gblas_int nounit;
    int info=0;

    side=(char)toupper(*SIDE);
    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    nounit=(diag=='N')?1:0;

    if((side!='L')&&(side!='R'))
        info=1;
    else if((uplo!='U')&&(uplo!='L'))
        info=2;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=3;
    else if((diag!='U')&&(diag!='N'))
        info=4;
    else if(m<0)
        info=5;
    else if(n<0)
        info=6;
    else if(lda<max(1,(side=='L')?m:n))
        info=9;
    else if(ldb<max(1,m))
        info=11;

    if(info!=0)
    {
        GBL_F77_xerbla("DTRMM ",&info);
        return info;
    }

    if((m==0)||(n==0))
        return 0;

    if(alpha==zero)
    {
        b=B;
        for(j=0;j<n;j++)
        {
            for(i=0;i<m;i++)
                b[i]=zero;
            b+=ldb;
        }
    }
    else if(side=='L')
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                b=B;
                for(j=0;j<n;j++)
                {
                    a=A;
                    for(k=0;k<m;k++)
                    {
                        t=alpha*b[k];
                        for(i=0;i<k;i++)
                            b[i]+=t*a[i];
                        if(nounit)
                            t*=a[k];
                        b[k]=t;
                        a+=lda;
                    }
                    b+=ldb;
                }
            }
            else
            {
                b=B;
                for(j=0;j<n;j++)
                {
                    a=A+m*lda;
                    for(k=m-1;k>=0;k--)
                    {
                        a-=lda;
                        t=alpha*b[k];
                        b[k]=t;
                        if(nounit)
                            b[k]*=a[k];
                        for(i=k+1;i<m;i++)
                            b[i]+=t*a[i];
                    }
                    b+=ldb;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                b=B;
                for(j=0;j<n;j++)
                {
                    a=A+m*lda;
                    for(i=m-1;i>=0;i--)
                    {
                        a-=lda;
                        t=b[i];
                        if(nounit)
                            t*=a[i];
                        for(k=0;k<i;k++)
                            t+=a[k]*b[k];
                        b[i]=alpha*t;
                    }
                    b+=ldb;
                }
            }
            else
            {
                b=B;
                for(j=0;j<n;j++)
                {
                    a=A;
                    for(i=0;i<m;i++)
                    {
                        t=b[i];
                        if(nounit)
                            t*=a[i];
                        for(k=i+1;k<m;k++)
                            t+=a[k]*b[k];
                        b[i]=alpha*t;
                        a+=lda;
                    }
                    b+=ldb;
                }
            }
        }
    }
    else
    {
        if(trans=='N')
        {
            if(uplo=='U')
            {
                a=A+n*lda;
                b=B+n*ldb;
                for(j=n-1;j>=0;j--)
                {
                    a-=lda;
                    b-=ldb;
                    if(nounit)
                        t=alpha*a[j];
                    else
                        t=alpha;
                    for(i=0;i<m;i++)
                        b[i]*=t;
                    bt=B;
                    for(k=0;k<j;k++)
                    {
                        t=alpha*a[k];
                        for(i=0;i<m;i++)
                            b[i]+=t*bt[i];
                        bt+=ldb;
                    }
                }
            }
            else
            {
                a=A;
                b=B;
                for(j=0;j<n;j++)
                {
                    if(nounit)
                        t=alpha*a[j];
                    else
                        t=alpha;
                    for(i=0;i<m;i++)
                        b[i]*=t;
                    bt=b+ldb;
                    for(k=j+1;k<n;k++)
                    {
                        t=alpha*a[k];
                        for(i=0;i<m;i++)
                            b[i]+=t*bt[i];
                        bt+=ldb;
                    }
                    a+=lda;
                    b+=ldb;
                }
            }
        }
        else
        {
            if(uplo=='U')
            {
                a=A;
                bt=B;
                for(k=0;k<n;k++)
                {
                    b=B;
                    for(j=0;j<k;j++)
                    {
                        t=alpha*a[j];
                        for(i=0;i<m;i++)
                            b[i]+=t*bt[i];
                        b+=ldb;
                    }
                    if(nounit)
                        t=alpha*a[k];
                    else
                        t=alpha;
                    for(i=0;i<m;i++)
                        b[i]*=t;
                    a+=lda;
                    bt+=ldb;
                }
            }
            else
            {
                a=A+n*lda;
                bt=B+n*ldb;
                for(k=n-1;k>=0;k--)
                {
                    a-=lda;
                    bt-=ldb;
                    b=B+(k+1)*ldb;
                    for(j=k+1;j<n;j++)
                    {
                        t=alpha*a[j];
                        for(i=0;i<m;i++)
                            b[i]+=t*bt[i];
                        b+=ldb;
                    }
                    if(nounit)
                        t=alpha*a[k];
                    else
                        t=alpha;
                    for(i=0;i<m;i++)
                        bt[i]*=t;
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
int       GBL_F77_dtrsm   (char *SIDE, char *UPLO, char *TRANS, char *DIAG,
                           gblas_int *M, gblas_int *N, double *Alpha, double *A,
                           gblas_int *LDA, double *B, gblas_int *LDB)
{
    const gblas_int m=*M;
    const gblas_int n=*N;
    const double alpha=*Alpha;
    const gblas_int lda=*LDA;
    const gblas_int ldb=*LDB;
    const double zero=0.0;
    const double one=1.0;
    char side,uplo,trans,diag;
    gblas_int i,j,k;
    double *a,*b,*bt;
    double t;
    gblas_int nounit;
    int info=0;

    side=(char)toupper(*SIDE);
    uplo=(char)toupper(*UPLO);
    trans=(char)toupper(*TRANS);
    diag=(char)toupper(*DIAG);

    nounit=(diag=='N')?1:0;

    if((side!='L')&&(side!='R'))
        info=1;
    else if((uplo!='U')&&(uplo!='L'))
        info=2;
    else if((trans!='N')&&(trans!='T')&&(trans!='C'))
        info=3;
    else if((diag!='U')&&(diag!='N'))
        info=4;
    else if(m<0)
        info=5;
    else if(n<0)
        info=6;
    else if(lda<max(1,(side=='L')?m:n))
        info=9;
    else if(ldb<max(1,m))
        info=11;

    if(info!=0)
    {
        GBL_F77_xerbla("DTRSM ",&info);
        return info;
    }

    if((m==0)||(n==0))
        return 0;

    if(alpha==zero)
    {
        b=B;
        for(j=0;j<n;j++)
        {
            for(i=0;i<m;i++)
                b[i]=zero;
            b+=ldb;
        }
    }
    else
    {
        if(side=='L')
        {
            if(trans=='N')
            {
                if(uplo=='U')
                {
                    b=B;
                    for(j=0;j<n;j++)
                    {
                        for(i=0;i<m;i++)
                            b[i]*=alpha;
                        a=A+m*lda;
                        for(k=m-1;k>=0;k--)
                        {
                            a-=lda;
                            if(nounit)
                                b[k]=b[k]/a[k];
                            for(i=0;i<k;i++)
                                b[i]-=b[k]*a[i];
                        }
                        b+=ldb;
                    }
                }
                else
                {
                    b=B;
                    for(j=0;j<n;j++)
                    {
                        for(i=0;i<m;i++)
                            b[i]*=alpha;
                        a=A;
                        for(k=0;k<m;k++)
                        {
                            if(nounit)
                                b[k]=b[k]/a[k];
                            for(i=k+1;i<m;i++)
                                b[i]-=b[k]*a[i];
                            a+=lda;
                        }
                        b+=ldb;
                    }
                }
            }
            else
            {
                if(uplo=='U')
                {
                    b=B;
                    for(j=0;j<n;j++)
                    {
                        a=A;
                        for(i=0;i<m;i++)
                        {
                            t=alpha*b[i];
                            for(k=0;k<i;k++)
                                t-=a[k]*b[k];
                            if(nounit)
                                t=t/a[i];
                            b[i]=t;
                            a+=lda;
                        }
                        b+=ldb;
                    }
                }
                else
                {
                    b=B;
                    for(j=0;j<n;j++)
                    {
                        a=A+m*lda;
                        for(i=m-1;i>=0;i--)
                        {
                            a-=lda;
                            t=alpha*b[i];
                            for(k=i+1;k<m;k++)
                                t-=a[k]*b[k];
                            if(nounit)
                                t=t/a[i];
                            b[i]=t;
                        }
                        b+=ldb;
                    }
                }
            }
        }
        else
        {
            if(trans=='N')
            {
                if(uplo=='U')
                {
                    b=B;
                    a=A;
                    for(j=0;j<n;j++)
                    {
                        for(i=0;i<m;i++)
                            b[i]*=alpha;
                        bt=B;
                        for(k=0;k<j;k++)
                        {
                            for(i=0;i<m;i++)
                                b[i]-=a[k]*bt[i];
                            bt+=ldb;
                        }
                        if(nounit)
                        {
                            t=one/a[j];
                            for(i=0;i<m;i++)
                                b[i]*=t;
                        }
                        a+=lda;
                        b+=ldb;
                    }
                }
                else
                {
                    b=B+n*ldb;
                    a=A+n*lda;
                    for(j=n-1;j>=0;j--)
                    {
                        a-=lda;
                        b-=ldb;
                        for(i=0;i<m;i++)
                            b[i]*=alpha;
                        bt=B+(j+1)*ldb;
                        for(k=j+1;k<n;k++)
                        {
                            for(i=0;i<m;i++)
                                b[i]-=a[k]*bt[i];
                            bt+=ldb;
                        }
                        if(nounit)
                        {
                            t=one/a[j];
                            for(i=0;i<m;i++)
                                b[i]*=t;
                        }
                    }
                }
            }
            else
            {
                if(uplo=='U')
                {
                    b=B+n*ldb;
                    a=A+n*lda;
                    for(k=n-1;k>=0;k--)
                    {
                        a-=lda;
                        b-=ldb;
                        if(nounit)
                        {
                            t=one/a[k];
                            for(i=0;i<m;i++)
                                b[i]*=t;
                        }
                        bt=B;
                        for(j=0;j<k;j++)
                        {
                            t=a[j];
                            for(i=0;i<m;i++)
                                bt[i]-=t*b[i];
                            bt+=ldb;
                        }
                        for(i=0;i<m;i++)
                            b[i]*=alpha;
                    }
                }
                else
                {
                    a=A;
                    b=B;
                    for(k=0;k<n;k++)
                    {
                        if(nounit)
                        {
                            t=one/a[k];
                            for(i=0;i<m;i++)
                                b[i]*=t;
                        }
                        bt=B+(k+1)*ldb;
                        for(j=k+1;j<n;j++)
                        {
                            t=a[j];
                            for(i=0;i<m;i++)
                                bt[i]-=t*b[i];
                            bt+=ldb;
                        }
                        for(i=0;i<m;i++)
                            b[i]*=alpha;
                        a+=lda;
                        b+=ldb;
                    }
                }
            }
        }
    }
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
