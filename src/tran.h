/**
\defgroup transf Modulo TRANSF
\ingroup deformacion
\brief En este modulo se reunen los ficheros necesarios para trabajar con
       ficheros de parametros de transformacion entre sistemas de referencia.

Los ficheros descritos a continuacion son el material imprescindible para
trabajar con ficheros de parametros de transformacion entre sistemas de
referencia.
@{
\file tran.h
\brief Inclusion de archivos de cabecera para usar la biblioteca "tran".
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 24 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _TRAN_H_
#define _TRAN_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include"errores.h"
#include"general.h"
#include"geodesia.h"
#include"paramtran.h"
#include"bloqtran.h"
#include"partran.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
