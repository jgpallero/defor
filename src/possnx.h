/**
\addtogroup sinex
@{
\file possnx.h
\brief Declaracion de la estructura y funciones para la extraccion de datos de
       posicion de ficheros SINEX.

En este fichero se declaran la estructura y todas las funciones necesarias para
la extraccion de datos de posicion y sus errores asociados de los puntos
contenidos en un fichero SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _POSSNX_H_
#define _POSSNX_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include"paramsnx.h"
#include"statsnx.h"
#include"matriz.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\struct possnx
\brief Estructura contenedora de los datos de posicion de los puntos almacenados
       en un fichero SINEX.

En esta estructura se almacenan todos los elementos referentes a la posicion de
los puntos almacenados en un fichero SINEX. Se almacenan las coordenadas de los
puntos asi como las correspondientes matrices de error y, si el fichero SINEX
las contiene, las velocidades de cada punto y sus correspondientes matrices de
error.
*/
typedef struct
{
    /** \brief Numero de puntos que contiene el fichero. */
    int nPuntos;
    /** \brief Codigos de los puntos que hay en el fichero. */
    /**
    Array de cadenas de texto para almacenar los codigos de los puntos que hay
    en el fichero SINEX. La longitud asignada a cada cadena del array es igual a
    #LONMAXLINSNX+1. Este array contiene tantas cadenas de texto como elementos
    tenga possnx::nPuntos. La numeracion de elementos comienza en 0.
    */
    char** puntos;
    /** \brief Epoca de calculo de los puntos en formato aa:ddd:sssss */
    /**
    Array de cadenas de texto para almacenar la epoca de calculo de los puntos
    que hay en el fichero SINEX. La longitud asignada a cada cadena del array es
    igual a #LONMAXLINSNX+1. Este array contiene tantas cadenas de texto como
    elementos tenga possnx::nPuntos. La numeracion de elementos comienza en 0.
    Cada epoca corresponde al punto almacenado en el mismo indice del array
    possnx::puntos. El formato de cada fecha representativa de la epoca es el
    siguiente:
        - aa: ultimos dos digitos del anyo.
        - ddd: numero del dia del anyo.
        - sssss: numero del segundo del dia.

    \note Los campos estan separados por el caracter ":".
    */
    char** epocas;
    /** \brief Identificador del monumeto fisico correspondiente a cada punto */
    /**
    Array de cadenas de texto para almacenar el identificador del monumeto
    fisico correspondiente a cada punto del fichero SINEX. La longitud asignada
    a cada cadena del array es igual a #LONMAXLINSNX+1. Este array contiene
    tantas cadenas de texto como elementos tenga possnx::nPuntos. La numeracion
    de elementos comienza en 0. Cada identificador de solucion corresponde al
    punto almacenado en el mismo indice del array possnx::puntos.
    */
    char** pt;
    /** \brief Identificador de la solucion a la que pertenece cada punto */
    /**
    Array de cadenas de texto para almacenar el identificador de la solucion a
    la que pertenece cada punto del fichero SINEX. La longitud asignada a cada
    cadena del array es igual a #LONMAXLINSNX+1. Este array contiene tantas
    cadenas de texto como elementos tenga possnx::nPuntos. La numeracion de
    elementos comienza en 0. Cada identificador de solucion corresponde al punto
    almacenado en el mismo indice del array possnx::puntos.
     */
    char** idsol;
    /** \brief Indicador de existencia de las coordenadas de los puntos. */
    /**
    Matriz de tres columnas que almacena un indicador de existencia de cada
    coordenada de los puntos almacenados en el fichero SINEX. Esta matriz
    contiene tantas filas de texto como elementos tenga possnx::nPuntos. Cada
    fila corresponde al punto almacenado en el mismo indice del array
    possnx::puntos. La numeracion de elementos comienza en 0. Las columnas
    corresponden a:
        - Col. 0: Existencia de la coordenada X del punto.
        - Col. 1: Existencia de la coordenada Y del punto.
        - Col. 2: Existencia de la coordenada Z del punto.

    \note Los valores identificadores de existencia son:
        - 0: No existe.
        - 1: Si existe.
    */
    int** epos;
    /** \brief Indicador de existencia de las velocidades de las coordenadas de
               los puntos. */
    /**
    Matriz de tres columnas que almacena un indicador de existencia de cada
    velocidad de las coordenadas de los puntos almacenados en el fichero SINEX.
    Esta matriz contiene tantas filas como elementos tenga possnx::nPuntos. Cada
    fila corresponde al punto almacenado en el mismo indice del array
    possnx::puntos. La numeracion de elementos comienza en 0. Las columnas
    corresponden a:
        - Col. 0: Existencia de la velocidad de la coordenada X del punto.
        - Col. 1: Existencia de la velocidad de la coordenada Y del punto.
        - Col. 2: Existencia de la velocidad de la coordenada Z del punto.

    \note Los valores identificadores de existencia son:
        - 0: No existe.
        - 1: Si existe.
    */
    int** evel;
    /** \brief Coordenadas de los puntos. */
    /**
    Matriz de tres columnas que almacena las coordenadas de los puntos que hay
    en el fichero SINEX. Esta matriz contiene tantas filas como elementos tenga
    possnx::nPuntos. Cada fila corresponde al punto almacenado en el mismo
    indice del array possnx::puntos. La numeracion de elementos comienza en 0.
    Las columnas corresponden a:
        - Col. 0: Coordenada X del punto.
        - Col. 1: Coordenada Y del punto.
        - Col. 2: Coordenada Z del punto.

    \note En el caso de que alguna coordenada no exista en el fichero, su
          posicion en la matriz almacena el valor 0.
    */
    double** pos;
    /** \brief Velocidad de los puntos. */
    /**
    Matriz de tres columnas que almacena las velocidades de los puntos que hay
    en el fichero SINEX. Esta matriz contiene tantas filas como elementos tenga
    possnx::nPuntos. Cada fila corresponde al punto almacenado en el mismo
    indice del array possnx::puntos. La numeracion de elementos comienza en 0.
    Las columnas corresponden a:
        - Col. 0: Velocidad de la coordenada X del punto.
        - Col. 1: Velocidad de la coordenada Y del punto.
        - Col. 2: Velocidad de la coordenada Z del punto.

    \note En el caso de que alguna velocidad no exista en el fichero, su
          posicion en la matriz almacena el valor 0.
    */
    double** vel;
    /** \brief Matriz de error asociada a las coordenadas de los puntos. */
    /**
    Matriz de seis columnas que almacena las varianzas y covarianzas (o
    varianzas y coeficientes de correlacion) asociadas a las coordenadas del
    punto de trabajo. Esta matriz contiene tantas filas como elementos tenga
    possnx::nPuntos. Cada fila corresponde al punto almacenado en el mismo
    indice del array possnx::puntos. La numeracion de elementos comienza en 0.
    Las columnas corresponden a:
        - Col. 0: Varianza de la coordenada X del punto.
        - Col. 1: Covarianza (o coeficiente de correlacion) entre las
                  coordenadas XY.
        - Col. 2: Covarianza (o coeficiente de correlacion) entre las
                  coordenadas XZ.
        - Col. 3: Varianza de la coordenada Y del punto.
        - Col. 4: Covarianza (o coeficiente de correlacion) entre las
                  coordenadas YZ.
        - Col. 5: Varianza de la coordenada Z del punto.

    \note Las posiciones en la matriz correspondientes a elementos que no
          existan almacenan el valor 0.
    */
    double** vcpos;
    /** \brief Matriz de error asociada a las velocidades de las coordenadas de
               los puntos. */
    /**
    Matriz de seis columnas que almacena las varianzas y covarianzas (o
    varianzas y coeficientes de correlacion) asociadas a las velocidades de las
    coordenadas del punto de trabajo. Esta matriz contiene tantas filas como
    elementos tenga possnx::nPuntos. Cada fila corresponde al punto almacenado
    en el mismo indice del array possnx::puntos. La numeracion de elementos
    comienza en 0. Las columnas corresponden a:
        - Col. 0: Varianza de la velocidad de la coordenada X del punto.
        - Col. 1: Covarianza (o coeficiente de correlacion) entre las
                  velocidades de las coordenadas XY.
        - Col. 2: Covarianza (o coeficiente de correlacion) entre las
                  velocidades de las coordenadas XZ.
        - Col. 3: Varianza de la velocidad de la coordenada Y del punto.
        - Col. 4: Covarianza (o coeficiente de correlacion) entre las
                  velocidades de las coordenadas YZ.
        - Col. 5: Varianza de lavelocidad de la coordenada Z del punto.

    \note Las posiciones en la matriz correspondientes a elementos que no
          existan almacenan el valor 0.
    */
    double** vcvel;
    /** \brief Numeros de incognita correspondientes a las coordenadas de los
               puntos. */
    /**
    Matriz de tres columnas que almacena los numeros de incognita del ajuste
    almacenado en el fichero SINEX correspondientes a las coordenadas de los
    puntos de trabajo. Esta matriz contiene tantas filas como elementos tenga
    possnx::nPuntos. Cada fila corresponde al punto almacenado en el mismo
    indice del array possnx::puntos. La numeracion de elementos comienza en 0.
    Las columnas corresponden a:
        - Col. 0: Numero de incognita de la coordenada X del punto.
        - Col. 1: Numero de incognita de la coordenada Y del punto.
        - Col. 2: Numero de incognita de la coordenada Z del punto.

    \note En el caso de que alguna coordenada no exista en el fichero, su numero
          de incognita almacena el valor 0.
     */
    int** nincpos;
    /** \brief Numeros de incognita correspondientes a las velocidades de los
               puntos. */
    /**
    Matriz de tres columnas que almacena los numeros de incognita del ajuste
    almacenado en el fichero SINEX correspondientes a las velocidades de las
    coordenadas de los puntos de trabajo. Esta matriz contiene tantas filas como
    elementos tenga possnx::nPuntos. Cada fila corresponde al punto almacenado
    en el mismo indice del array possnx::puntos. La numeracion de elementos
    comienza en 0. Las columnas corresponden a:
        - Col. 0: Numero de incognita de la velocidad de la coordenada X del
                  punto.
        - Col. 1: Numero de incognita de la velocidad de la coordenada Y del
                  punto.
        - Col. 2: Numero de incognita de la velocidad de la coordenada Z del
                  punto.

    \note En el caso de que alguna velocidad no exista en el fichero, su numero
          de incognita almacena el valor 0.
    */
    int** nincvel;
    /** \brief Codigo del tipo de matriz de error almacenada en el fichero
               SINEX. */
    /**
    Cadena de texto de longitud #LONMAXLINSNX+1.
    */
    char tipoMat[LONMAXLINSNX+1];
}possnx;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Inicializa una estructura possnx.

Pone el campo possnx::nPuntos de la estructura a 0 e inicializa a NULL todos
los elementos de la estructura que sean punteros.
\param[in] posicion Puntero a una estructura possnx.
\note Es necesario inicializar una estructura possnx antes de su uso porque
      muchas funciones comprueban el estado del campo possnx::nPuntos para su
      correcto funcionamiento y si la estructura no se ha inicializado podrian
      producirse errores.
*/
void InicializaPosSnx(possnx* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Asigna memoria para un numero de elementos dado en una estructura
       possnx, anyadiendolos a los que ya existan previamente.
\param[in] posicion Puntero a una estructura possnx.
\param[in] elementos Numero de elementos para los cuales se asignara memoria.
\return Codigo de error.
\note La estructura possnx pasada ha de estar inicializada.
\note Esta funcion actualiza el campo possnx::nPuntos de la estructura.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura possnx pasada no
      es liberada.
\note Todos los elementos numéricos se inicializan a 0.0, matrices incluidas.
*/
int AsignaMemoriaPosSnx(possnx* posicion,
                        int elementos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Libera la memoria ocupada por una estructura possnx.
\param[in] posicion Puntero a una estructura possnx.
\note Ademas de liberar la memoria ocupada por los elementos que son punteros de
      la estructura, esta funcion pone a cero el campo possnx::nPuntos.
*/
void LiberaMemoriaPosSnx(possnx* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee los parametros de error de la solucion estimada almacenada en un
       fichero SINEX.
\param[in] idFichero Identificador de fichero abierto para leer.
\param[in] extraePosicion Indicador de extraccion de los parametros de error de
                          las coordenadas de los puntos almacenados en el
                          fichero. Dos posibles valores:
                          - 0: No se extraen parametros.
                          - 1: Si se extraen parametros.
\param[in] extraeVelocidad Indicador de extraccion de los parametros de error de
                           las velocidades de las coordenadas de los puntos
                           almacenados en el fichero. Dos posibles valores:
                           - 0: No se extraen parametros.
                           - 1: Si se extraen parametros.
\param[in] numeroIncognitas Numero total de incognitas almacenadas en el fichero
                            SINEX.
\param[in] posFichSOLMATES Direccion de memoria del inicio de la linea de
                           comienzo del bloque SOLUTION/MATRIX_ESTIMATE, del
                           cual se extraeran los parametros de error de la
                           solucion estimada.
\param[in] lineaIniSOLMATES Numero de linea del fichero en la que se encuentra
                            la linea de comienzo del bloque
                            SOLUTION/MATRIX_ESTIMATE.
\param[in] lineaFinSOLMATES Numero de linea del fichero en la que se encuentra
                            la linea de final del bloque
                            SOLUTION/MATRIX_ESTIMATE.
\param[out] infoS02 Identificador de que en el cálculo explícito de la matriz de
                    varianzas-covarianzas a posteriori se ha considerado que la
                    estimación de la varianza de referencia del observable de
                    peso unidad a posteriori se ha tomado como 1 porque no se
                    almacena la información en el bloque SOLUTION/STATISTICS.
                    Dos posibles valores:
                    - 0: Todo ha ido bien
                    - Distinto de 0: Se ha considerado el valor s02=1.0.
\param[out] posicion Puntero a una estructura possnx. Se almacenaran todos los
                     datos referentes a los parametros de error de la solucion
                     contenida en el fichero SINEX.
\return Codigo de error.
\note Esta funcion no comprueba si los parametros de posicion y lineas de
      fichero son validos, por lo que es necesario que se le pasen valores
      correctos.
\note El puntero de L/E es devuelto a su posicion original en el fichero tras la
      ejecucion de esta funcion.
\note La estructura possnx pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura possnx pasada no
      es liberada y el fichero apuntado por idFichero no es cerrado.
*/
int LeeParametrosErrorPosSnx(FILE* idFichero,
                             int extraePosicion,
                             int extraeVelocidad,
                             int numeroIncognitas,
                             long int posFichSOLMATES,
                             int lineaIniSOLMATES,
                             int lineaFinSOLMATES,
                             int* infoS02,
                             possnx* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee la solucion estimada almacenada en un fichero SINEX.
\param[in] idFichero Identificador de fichero abierto para leer.
\param[in] extraePosicion Indicador de extraccion de las coordenadas de los
                          puntos almacenados en el fichero. Dos posibles
                          valores:
                          - 0: No se extraen coordenadas.
                          - 1: Si se extraen coordenadas.
\param[in] extraeVelocidad Indicador de extraccion de las velocidades de las
                           coordenadas de los puntos almacenados en el fichero.
                           Dos posibles valores:
                           - 0: No se extraen velocidades.
                           - 1: Si se extraen velocidades.
\param[in] posFichSOLES Direccion de memoria del inicio de la linea de comienzo
                        del bloque SOLUTION/ESTIMATE, del cual se extraeran las
                        coordenadas y velocidades de los puntos.
\param[in] lineaIniSOLES Numero de linea del fichero en la que se encuentra la
                         linea de comienzo del bloque SOLUTION/ESTIMATE.
\param[in] lineaFinSOLES Numero de linea del fichero en la que se encuentra la
                         linea de final del bloque SOLUTION/ESTIMATE.
\param[in] posFichSOLMATES Direccion de memoria del inicio de la linea de
                           comienzo del bloque SOLUTION/MATRIX_ESTIMATE, del
                           cual se extraeran los parametros de error de la
                           solucion estimada. Si se pasa el valor -1, se
                           considera que no hay bloque SOLUTION/MATRIX_ESTIMATE
                           y los parámetros de error se extraen de
                           SOLUTION/ESTIMATE.
\param[in] lineaIniSOLMATES Numero de linea del fichero en la que se encuentra
                            la linea de comienzo del bloque
                            SOLUTION/MATRIX_ESTIMATE. Si se pasa el valor -1, se
                            considera que no hay bloque SOLUTION/MATRIX_ESTIMATE
                            y los parámetros de error se extraen de
                            SOLUTION/ESTIMATE.
\param[in] lineaFinSOLMATES Numero de linea del fichero en la que se encuentra
                            la linea de final del bloque
                            SOLUTION/MATRIX_ESTIMATE. Si se pasa el valor -1, se
                            considera que no hay bloque SOLUTION/MATRIX_ESTIMATE
                            y los parámetros de error se extraen de
                            SOLUTION/ESTIMATE.
\param[out] infoS02 Identificador de que en el cálculo explícito de la matriz de
                    varianzas-covarianzas a posteriori se ha considerado que la
                    estimación de la varianza de referencia del observable de
                    peso unidad a posteriori se ha tomado como 1 porque no se
                    almacena la información en el bloque SOLUTION/STATISTICS.
                    Dos posibles valores:
                    - 0: Todo ha ido bien
                    - Distinto de 0: Se ha considerado el valor s02=1.0.
\param[out] posicion Puntero a una estructura possnx. Se almacenaran todos los
                     datos solicitados referentes a la solucion contenida en el
                     fichero SINEX.
\return Codigo de error.
\note Esta funcion no comprueba si los parametros de posicion y lineas de
      fichero son validos, por lo que es necesario que se le pasen valores
      correctos.
\note El puntero de L/E es devuelto a su posicion original en el fichero tras la
      ejecucion de esta funcion.
\note La estructura possnx pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura possnx pasada no
      es liberada y el fichero apuntado por idFichero no es cerrado.
\note Si el fichero no contiene matriz de varianza-covarianza o de correlación,
      las varianzas de cada coordenada o velocidad se extraen del bloque
      SOLUTION/ESTIMATE. De este modo, la matriz será sólo diagonal, no
      completa.
*/
int LeeSolucionEstimadaPosSnx(FILE* idFichero,
                              int extraePosicion,
                              int extraeVelocidad,
                              long int posFichSOLES,
                              int lineaIniSOLES,
                              int lineaFinSOLES,
                              long int posFichSOLMATES,
                              int lineaIniSOLMATES,
                              int lineaFinSOLMATES,
                              int* infoS02,
                              possnx* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Comprueba si un anyo es bisiesto.
\param[in] anyo Anyo a comprobar.
\return Indicador, dos posibilidades:
        - 0: Si el anyo no es bisiesto.
        - 1: Si el anyo es bisiesto.
\date 02 de marzo de 2009: Creacion de la funcion.
*/
int EsBisiesto(const int anyo);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Convierte una epoca de observacion de formato SINEX a formato anyo
       decimal.
\param[in] epoca Epoca en formato SINEX.
\return Epoca convertida a formato anyo.decimal de anyo.
\note El formato indicador de epoca de observacion en un fichero SINEX es
      aa:ddd:sssss, en el cual cada elemento corresponde a:
      - aa: ultimos dos digitos del anyo.
      - ddd: numero del dia del anyo, teniendo en cuenta que el 1 de enero a las
        0 horas se almacena con el número de día 001 en lugar de 000, por lo que
        para transformar la fecha a año decimal siempre se resta una unidad al
        día almacenado en el formato SINEX.
      - sssss: numero del segundo del dia.

\note Los campos estan separados por el caracter ":".
\note Esta funcion no comprueba que la epoca pasada este en el formato correcto.
\note El decimal del año se refiere al año de trabajo. Es decir, si el año es
      normal, la parte decimal se calcula sobre 365, mientras que si es bisiesto
      sobre 366.
\date 27 de julio de 2011: Corrección de bug que hacía que se utilizase un día
      de más el el cálculo del año decimal. Gracias a Nicolas d'Oreye por
      descubrir el error.
*/
double EpocaSnxFechaAnyo(char epoca[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee la solucion estimada almacenada en un fichero SINEX.
\param[in] fichero Ruta completa del fichero de trabajo.
\param[in] extraePosicion Indicador de extraccion de las coordenadas de los
                          puntos almacenados en el fichero. Dos posibles
                          valores:
                          - 0: No se extraen coordenadas.
                          - 1: Si se extraen coordenadas.
\param[in] extraeVelocidad Indicador de extraccion de las velocidades de las
                           coordenadas de los puntos almacenados en el fichero.
                           Dos posibles valores:
                           - 0: No se extraen velocidades.
                           - 1: Si se extraen velocidades.
\param[in] extraeError Indicador de extracción de parámetros de error. Dos
                       posibles valores:
                       - 0: No se extraen parámetros de error.
                       - 1: Sí se extraen parámetros de error.
\param[in] convierteVC Indicador de conversion de las matrices de error de
                       matrices de correlaciones a matrices varianza-covarianza,
                       si ha lugar. Dos posibles valores:
                       - 0: No se hace la conversion.
                       - 1: Si se hace la conversion.
\param[out] infoS02 Identificador de que en el cálculo explícito de la matriz de
                    varianzas-covarianzas a posteriori se ha considerado que la
                    estimación de la varianza de referencia del observable de
                    peso unidad a posteriori se ha tomado como 1 porque no se
                    almacena la información en el bloque SOLUTION/STATISTICS.
                    Dos posibles valores:
                    - 0: Todo ha ido bien
                    - Distinto de 0: Se ha considerado el valor s02=1.0.
\param[out] posicion Puntero a una estructura possnx. Se almacenaran todos los
                     datos solicitados referentes a la solucion contenida en el
                     fichero SINEX.
\return Codigo de error.
\note En el caso de que se haga la conversion de las matrices de error, el campo
      possnx::tipoMat se actualizara al indicador correspondiente a matriz de
      varianza-covarianza (#MATCOVASOLMATES), aunque en el fichero original las
      matrices de error no sean de este tipo.
\note La estructura possnx pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura possnx pasada no
      es liberada.
*/
int LeePosVelFicheroSinex(char fichero[],
                          int extraePosicion,
                          int extraeVelocidad,
                          int extraeError,
                          int convierteVC,
                          int* infoS02,
                          possnx* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
