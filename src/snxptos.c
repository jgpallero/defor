/**
\addtogroup deformacion
@{
\file snxptos.c
\brief Definicion de las funciones necesarias para la copia de los datos de
       posicion de una estructura snx en una estructura ptos.

\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<string.h>
#include<math.h>
#include"errores.h"
#include"general.h"
#include"paramsnx.h"
#include"possnx.h"
#include"posptos.h"
#include"snxptos.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int CopiaPosSnxEnPosPtos(possnx* sinex,
                         char sistRef[],
                         posptos* puntos)
{
    //codigo de error
    int cError=ERRNOERROR;
    //variable para recorrer bucles
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para un numero de puntos igual al existente en la
    //estructura possnx
    cError = AsignaMemoriaPosPtos(puntos,sinex->nPuntos);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,
                              "AsignaMemoriaPosPtos()");
        //salimos de la funcion
        return cError;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //copiamos los elementos necesarios
    for(i=0;i<puntos->nPuntos;i++)
    {
        //copiamos los codigos de los puntos
        strcpy(puntos->puntos[i],sinex->puntos[i]);
        //copiamos el identificador de sistema de referencia
        strcpy(puntos->sistRef[i],sistRef);
        //copiamos la epoca
        puntos->epocas[i] = EpocaSnxFechaAnyo(sinex->epocas[i]);
        //copiamos las coordenadas de los puntos
        puntos->pos[i][0] = sinex->pos[i][0];
        puntos->pos[i][1] = sinex->pos[i][1];
        puntos->pos[i][2] = sinex->pos[i][2];
        //copiamos las matrices de varianza-covarianza de los puntos
        puntos->vcpos[i][0] = sinex->vcpos[i][0];
        puntos->vcpos[i][1] = sinex->vcpos[i][1];
        puntos->vcpos[i][2] = sinex->vcpos[i][2];
        puntos->vcpos[i][3] = sinex->vcpos[i][3];
        puntos->vcpos[i][4] = sinex->vcpos[i][4];
        puntos->vcpos[i][5] = sinex->vcpos[i][5];
        //si la estructura possnx contiene matrices de correlacion las
        //convertimos en matrices varianza-covarianza
        if(!strcmp(sinex->tipoMat,MATCORRSOLMATES))
        {
            //calculamos las covarianzas
            MatrizCorrMatrizVarCov(sinex->vcpos[i][0],
                                   sinex->vcpos[i][3],
                                   sinex->vcpos[i][5],
                                   sinex->vcpos[i][1],
                                   sinex->vcpos[i][2],
                                   sinex->vcpos[i][4],
                                   &puntos->vcpos[i][1],
                                   &puntos->vcpos[i][2],
                                   &puntos->vcpos[i][4]);
            //convertimos las desviaciones tipicas en varianzas
            puntos->vcpos[i][0] = pow(sinex->vcpos[i][0],2.0);
            puntos->vcpos[i][3] = pow(sinex->vcpos[i][3],2.0);
            puntos->vcpos[i][5] = pow(sinex->vcpos[i][5],2.0);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
