/**
\defgroup general Modulo GENERAL
\ingroup sinex
\ingroup puntos
\ingroup transf
\ingroup deformacion
\ingroup dibgmt
\brief En este modulo se reunen los ficheros necesarios para realizar
       operaciones genericas matematicas y con cadenas de texto.

Los ficheros descritos a continuacion son el material imprescindible para
realizar operaciones genericas matematicas y con cadenas de texto.
@{
\file general.h
\brief Declaracion de funciones de utilidad general para el trabajo con ficheros
       SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _GENERAL_H_
#define _GENERAL_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdlib.h>
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def TRISUP
    \brief Identificador de matriz triangular superior. */
#define TRISUP "U"
/** \def TRIINF
    \brief Identificador de matriz triangular inferior. */
#define TRIINF "L"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Elimina todos los espacios en blanco de una cadena de texto.
\param[in] cadena Cadena de texto de la que se eliminaran los espacios.
\note La cadena con los espacios eliminados es devuelta en el mismo argumento de
      entrada.
*/
void QuitaEspacios(char cadena[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Extrae una subcadena de una cadena de texto.
\param[in] cadena Cadena de texto de la que se extraera la subcadena.
\param[in] comienzo Posicion de comienzo en la cadena para empezar a extraer,
                    (los indices comienzan en 0).
\param[in] longitud Longitud de la subcadena a extraer.
\param[out] subcadena Subcadena extraida.
\note Esta funcion no comprueba si la longitud pasada es mayor que la longitud
      de la cadena pasada para extraer ni mayor que la longitud de la variable
      pasada para guardar la subcadena extraida.
*/
void ExtraeSubcadena(char cadena[],
                     int comienzo,
                     int longitud,
                     char subcadena[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Extrae una subcadena de una cadena de texto hasta una determinada marca
       de corte.
\param[in] cadena Cadena de texto de la que se extraera la subcadena.
\param[in] comienzo Posicion de comienzo en la cadena para empezar a extraer
                    (los indices comienzan en 0).
\param[in] delimitador Cadena de texto que indica el punto hasta el que se
                       extraera la subcadena. El delimitador no entra en la
                       subcadena extraida.
\param[out] subcadena Subcadena extraida.
\note Esta funcion no comprueba si la variable pasada para almacenar la
      subcadena extraida es lo suficientemente larga. En el caso de que el
      delimitador no se encuentre en la cadena original la subcadena extraida
      tendra como punto de parada el caracter "\n" de la cadena original o, si
      no existe, el caracter "\0".
*/
void ExtraeSubcadenaDelim(char cadena[],
                          int comienzo,
                          char delimitador[],
                          char subcadena[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Comprueba si una subcadena existe en una cadena de texto.
\param[in] cadena Cadena de texto.
\param[in] comienzo Posicion de comienzo en la cadena para empezar a buscar (los
                    indices comienzan en 0).
\param[in] subcadena Subcadena a buscar.
\return Dos posibles valores:
        - 0: La subcadena no existe de la posicion de comienzo en adelante en la
             cadena pasada.
        - 1: La subcadena si existe de la posicion de comienzo en adelante en la
             cadena pasada.
*/
int ExisteSubcadena(char cadena[],
                    int comienzo,
                    char subcadena[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Comprueba si tres cadenas de texto existen al mismo tiempo y en la misma
       posicion en tres listas de cadenas de texto, cada cadena en su respectiva
       lista.
\param[in] lista1 Primera lista de cadenas de texto.
\param[in] lista2 Segunda lista de cadenas de texto.
\param[in] lista3 Tercera lista de cadenas de texto.
\param[in] longitudListas Numero de elementos de las listas pasadas.
\param[in] cadena1 Primera cadena de texto a buscar.
\param[in] cadena2 Segunda cadena de texto a buscar.
\param[in] cadena3 Tercera cadena de texto a buscar.
\return Dos posibles valores:
        - -1: Las cadenas pasadas no existen.
        - Distinto de -1: Posicion en las listas de entrada en que se encuentran
                          las cadenas pasadas. Las posiciones comienzan en 0.

\note Todas las listas pasadas han de tener la misma longitud.
\note Si el argumento longitudListas es mayor que el numero de elementos de las
      listas pasadas se produciran errores al intentar acceder a elementos que
      no existen.
\note Si el argumento longitudListas es menor que el numero de elementos de las
      listas pasadas solo se comprobara la existencia de las cadenas en los
      longitudListas primeros elementos de las listas de entrada.
\note Si el argumento longitudListas vale 0 se devuelve el valor -1.
*/
int ExisteCadena(char** lista1,
                 char** lista2,
                 char** lista3,
                 int longitudListas,
                 char cadena1[],
                 char cadena2[],
                 char cadena3[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Comprueba si dos cadenas de texto existen al mismo tiempo y en la misma
       posicion en dos listas de cadenas de texto.
\param[in] lista1 Primera lista de cadenas de texto.
\param[in] lista2 Segunda lista de cadenas de texto.
\param[in] longitudLista Numero de elementos de las listas pasadas.
\param[in] cadena1 Primera cadena de texto a buscar.
\param[in] cadena2 Segunda cadena de texto a buscar.
\param[out] posicion Posicion en las listas de entrada en que se encuentran las
                     cadenas pasadas. Dos posibles valores:
                     - -1: Las cadenas pasadas no existen.
                     - Distinto de -1: Posicion en las listas de entrada en que
                                       se encuentran las cadenas pasadas. Las
                                       posiciones comienzan en 0.
\param[out] orden Indicador del orden de las cadenas en las listas. Dos posibles
                  valores:
                  1: cadena1 esta en lista1 y cadena2 esta en lista2.
                  -1: cadena1 esta en lista2 y cadena2 esta en lista1.

\note Todas las listas pasadas han de tener la misma longitud.
\note Si el argumento longitudListas es mayor que el numero de elementos de las
      listas pasadas se produciran errores al intentar acceder a elementos que
      no existen.
\note Si el argumento longitudListas es menor que el numero de elementos de las
      listas pasadas solo se comprobara la existencia de las cadenas en los
      longitudListas primeros elementos de las listas de entrada.
\note Si el argumento longitudListas vale 0 se devuelve el valor de posicion es
      -1 y el de orden 0.
*/
void ExistenCadenasEnDosListas(char** lista1,
                               char** lista2,
                               int longitudLista,
                               char cadena1[],
                               char cadena2[],
                               int* posicion,
                               int* orden);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la ruta a seguir para llegar de una cadena a otra en dos pasos.
\param[in] lista1 Primera lista de cadenas de texto.
\param[in] lista2 Segunda lista de cadenas de texto.
\param[in] longitudLista Numero de elementos de las listas pasadas.
\param[in] cadena1 Primera cadena de texto a buscar.
\param[in] cadena2 Segunda cadena de texto a buscar.
\param[out] posicion1 Posicion en las listas de entrada en que se encuentra
                      cadena1 y la cadena comun entre cadena1 y cadena2 para
                      realizar el primer paso de la ruta. Dos posibles valores:
                     - -1: cadena1 no existe en ninguna lista.
                     - Distinto de -1: Posicion en las listas de entrada en que
                                       se encuentra cadena1. Las posiciones
                                       comienzan en 0.
\param[out] orden1 Indicador del orden de cadena1 cadenaComun. Dos posibles
                   valores:
                   1: cadena1 esta en lista1 y cadenaComun esta en lista2.
                   -1: cadena1 esta en lista2 y cadenaComun esta en lista1.
\param[out] posicion2 Posicion en las listas de entrada en que se encuentra
                      cadena2 y la cadena comun entre cadena1 y cadena2 para
                      realizar el segundo paso de la ruta. Dos posibles valores:
                     - -1: cadena2 no existe en ninguna lista.
                     - Distinto de -1: Posicion en las listas de entrada en que
                                       se encuentra cadena2. Las posiciones
                                       comienzan en 0.
\param[out] orden2 Indicador del orden de cadena2 cadenaComun. Dos posibles
                   valores:
                   1: cadena2 esta en lista1 y cadenaComun esta en lista2.
                   -1: cadena2 esta en lista2 y cadenaComun esta en lista1.

\note Todas las listas pasadas han de tener la misma longitud.
\note Si el argumento longitudListas es mayor que el numero de elementos de las
      listas pasadas se produciran errores al intentar acceder a elementos que
      no existen.
\note Si el argumento longitudListas es menor que el numero de elementos de las
      listas pasadas solo se comprobara la existencia de las cadenas en los
      longitudListas primeros elementos de las listas de entrada.
\note Si el argumento longitudListas vale 0 se devuelve los valores de posicion
      son -1 y los de orden 0.
*/
void Cadena1ACadena2(char** lista1,
                     char** lista2,
                     int longitudLista,
                     char cadena1[],
                     char cadena2[],
                     int* posicion1,
                     int* orden1,
                     int* posicion2,
                     int* orden2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la posicion de un elemento de una matriz triangular en un vector
       que almacena una matriz triangular inferior por columnas.
\param[in] uplo Identificador de la clase de matriz a la que se refieren las
                coordenadas del punto de trabajo. Dos posibilidades:
                - Cadena "U": Las coordenadas fila/columna pasadas se refieren
                              a una matriz triangular superior.
                - Cadena "L": Las coordenadas fila/columna pasadas se refieren
                              a una matriz triangular inferior.
\param[in] dimensiones Dimensiones de la matriz a la que pertece el punto de
                       trabajo (con una dimension es suficiente porque la matriz
                       es cuadrada).
\param[in] fila Fila del punto de trabajo.
\param[in] columna Columna del punto de trabajo.
\return Posicion en el vector del elemento de trabajo.
\note Las coordenadas matriciales del punto de trabajo vendran referidas a
      indices que comienzan en 1.
\note La posicion calculada en el vector viene referida a un indice que comienza
      en 1.
\note Si las coordenadas del punto de trabajo corresponden a una matriz
      triangular superior, se transforman a las correspondientes a una matriz
      triangular inferior.
\note Esta funcion no comprueba si el parametro "uplo" contiene las cadenas de
      texto correctas.
\note Si alguno de los parametros (o los dos) "fila" o "columna" vale 0, la
      funcion devolvera el valor 0.
*/
int PosicionVectorTriangular(char uplo[],
                             int dimensiones,
                             int fila,
                             int columna);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula las posiciones en un vector que almacena una matriz triangular
       inferior de todos los elementos de una matriz de 3x3 simetrica que es una
       submatriz extraida de la matriz original almacenada en el vector.
\param[in] dimensiones Dimensiones de la matriz almacenada en el vector (con una
                       dimension es suficiente porque la matriz es cuadrada).
\param[in] filcolx Fila y columna del elemento (1,1) de la matriz de 3x3.
\param[in] filcoly Fila y columna del elemento (2,2) de la matriz de 3x3.
\param[in] filcolz Fila y columna del elemento (3,3) de la matriz de 3x3.
\param[out] posx Posicion en el vector del elemento (1,1) de la matriz de 3x3.
\param[out] posy Posicion en el vector del elemento (2,2) de la matriz de 3x3.
\param[out] posz Posicion en el vector del elemento (3,3) de la matriz de 3x3.
\param[out] posxy Posicion en el vector del elemento (2,1) de la matriz de 3x3.
\param[out] posxz Posicion en el vector del elemento (3,1) de la matriz de 3x3.
\param[out] posyz Posicion en el vector del elemento (3,2) de la matriz de 3x3.
\note Las posiciones calculadas consideran que los indices fila y columna
      pasados comienzan en 1.
\note Las posiciones calculadas consideran que el indice del vector comienza en
      1.
*/
void PosVectorMatrizTriInf(int dimensiones,
                           int filcolx,
                           int filcoly,
                           int filcolz,
                           int* posx,
                           int* posy,
                           int* posz,
                           int* posxy,
                           int* posxz,
                           int* posyz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula el coeficiente de correlacion entre dos variables.
\param[in] dtx Desviacion tipica de la variable X.
\param[in] dty Desviacion tipica de la variable Y.
\param[in] covxy Covarianza entre las variables X e Y.
\param[out] r Coeficiente de correlacion calculado.
 */
void CoefCorr(double dtx,
              double dty,
              double covxy,
              double *r);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Convierte una matriz de coeficientes de correlacion en una matriz de
       varianza-covarianza.
\param[in] stdX Desviacion tipica de la coordenada X.
\param[in] stdY Desviacion tipica de la coordenada Y.
\param[in] stdZ Desviacion tipica de la coordenada Z.
\param[in] corrXY Coeficiente de correlacion entre las coordenadas X e Y.
\param[in] corrXZ Coeficiente de correlacion entre las coordenadas X y Z.
\param[in] corrYZ Coeficiente de correlacion entre las coordenadas Y y Z.
\param[out] covXY Covarianza entre las coordenadas X e Y.
\param[out] covXZ Covarianza entre las coordenadas X y Z.
\param[out] covYZ Covarianza entre las coordenadas Y y Z.
*/
void MatrizCorrMatrizVarCov(double stdX,
                            double stdY,
                            double stdZ,
                            double corrXY,
                            double corrXZ,
                            double corrYZ,
                            double* covXY,
                            double* covXZ,
                            double* covYZ);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Convierte una matriz de varianza-covarianza en una matriz de coeficientes
       de correlacion.
\param[in] varX Varianza de la coordenada X.
\param[in] varY Varianza de la coordenada Y.
\param[in] varZ Varianza de la coordenada Z.
\param[in] covXY Covarianza entre las coordenadas X e Y.
\param[in] covXZ Covarianza entre las coordenadas X y Z.
\param[in] covYZ Covarianza entre las coordenadas Y y Z.
\param[out] corrXY Coeficiente de correlacion entre las coordenadas X e Y.
\param[out] corrXZ Coeficiente de correlacion entre las coordenadas X y Z.
\param[out] corrYZ Coeficiente de correlacion entre las coordenadas Y y Z.
\note Si la varianza de alguna coordenada vale 0.0 la correlacion que implique
      a alguna de esas coordenadas tambien valdra 0.0.
*/
void MatrizVarCovMatrizCorr(double varX,
                            double varY,
                            double varZ,
                            double covXY,
                            double covXZ,
                            double covYZ,
                            double* corrXY,
                            double* corrXZ,
                            double* corrYZ);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Extrae las posiciones de los valores maximo y minimo de una lista.
\param[in] lista Lista de valores.
\param[in] nElementos Numero de elementos almacenados en la lista.
\param[out] posMax Posicion en la lista del elemento de valor maximo.
\param[out] posMin Posicion en la lista del elemento de valor minimo.
\note Si el argumento nElementos es mayor que el numero de elementos de la lista
      pasada se produciran errores al intentar acceder a elementos que no
      existen.
\note Si el argumento nElementos es menor que el numero de elementos de la lista
      pasada solo se trabajara sobre los nElementos primeros elementos de la
      lista de entrada.
\note Si el argumento nElementos vale 0 se devuelve el valor 0 en los dos
      argumentos de salida.
*/
void MaxMin(double* lista,
            int nElementos,
            int* posMax,
            int* posMin);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la mediana de una serie de datos.
\param[in] vector Vector de datos (no hace falta que esté ordenado).
\param[in] nElementos Numero de elementos almacenados en el vector.
\return Mediana.
\note Esta función no comprueba si el argumento \em vector contiene el número de
      datos indicado en \em nElementos.
\note Si el argumento \em nElementos vale 0 se devuelve el valor 0.
\note Al término de la función, el vector pasado está ordenado de menor a mayor.
*/
double Mediana(double* vector,
               const size_t nElementos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Función de comparación de dos valores de tipo double para su utilización
       en la función qsort().
\param[in] a Primer valor.
\param[in] b Segundo valor.
\return Resultado de la comparación. Tres posibles valores:
        - -1: El primer valor es menor que el segundo.
        - 0: El primer valor es igual que el segundo.
        - 1: El primer valor es mayor que el segundo.
\note Esta funcion convierte internamente a double los valores pasados.
*/
int ComparaDoubleQsort(const void* a,
                       const void* b);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Extrae los codigos de una serie de codigos pasada de tal forma que solo
       aparezcan una vez los puntos repetidos.
\param[in] codigos Vector de cadenas de caracteres con los codigos de los
           puntos.
\param[in] numEnt Numero de codigos en el vector \em codigos.
\param[out] numSal Numero de codigos de puntos individuales.
\return Dos posibles valores:
        - Si todo va bien, la funcion devuelve un vector de cadenas de
          caracteres de \em numSal elementos con los codigos de los puntos
          individuales.
        - Si ocurre un error de asignacion de memoria, la funcion devuelve NULL.
*/
char** CodigosIndividuales(char** codigos,
                           const size_t numEnt,
                           size_t* numSal);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Asigna memoria para una matriz bidimensional en estilo C.
\param[in] fil Número de filas de la matriz.
\param[in] col Número de columnas de la matriz.
\return Puntero a la matriz creada. Si ocurre algún error de asignación de
        memoria, se devuelve NULL.
\note La memoria asignada no se inicializa a ningún valor.
\date 14 de enero de 2010: Creación de la función.
*/
double** AsigMemMatrizC(const size_t fil,
                        const size_t col);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Libera memoria de una matriz bidimensional en estilo C.
\param[in] matriz Puntero al espacio de memoria a liberar.
\param[in] fil Número de filas de la matriz.
\date 14 de enero de 2010: Creación de la función.
*/
void LibMemMatrizC(double** matriz,
                   const size_t fil);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
