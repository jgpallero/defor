/**
\addtogroup general
\ingroup sinex
\ingroup puntos
\ingroup transf
\ingroup deformacion
\ingroup dibgmt
@{
\file general.c
\brief Definicion de funciones de utilidad general para el trabajo con ficheros
       SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<math.h>
#include"general.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void QuitaEspacios(char cadena[])
{
    //indice para recorrer un bucle
    int i=0;
    //indicador de posicion
    int pos=0;
    //cadena auxiliar
    char aux[strlen(cadena)+1];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //copiamos la cadena pasada en la variable auxiliar
    strcpy(aux,cadena);
    //reinicializamos la cadena pasada a cadena vacia
    strcpy(cadena,"");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //eliminamos los espacios
    for(i=0;i<(int)strlen(aux);i++)
    {
        //copiamos la posicion actual si no es un espacio
        if(aux[i]!=' ')
        {
            cadena[pos] = aux[i];
            //aumentamos el contador de posicion
            pos++;
        }
        //anadimos el caracter terminador de cadena de texto
        cadena[pos] = '\0';
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ExtraeSubcadena(char cadena[],
                     int comienzo,
                     int longitud,
                     char subcadena[])
{
    //indice para recorrer un bucle
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos la etiqueta
    for(i=0;i<longitud;i++)
    {
        subcadena[i] = cadena[i+comienzo];
    }
    //anadimos el caracter terminador de cadena de texto
    subcadena[i] = '\0';
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ExtraeSubcadenaDelim(char cadena[],
                          int comienzo,
                          char delimitador[],
                          char subcadena[])
{
    //indice para recorrer un bucle
    int i=0;
    //longitud de la cadena pasada
    int lonCadena=(int)strlen(cadena);
    //cadena pasada sin los elementos anteriores al comienzo
    char nuevaCadena[lonCadena-comienzo+1];
    //posicion del caracter "\n"
    int posCRLF=(int)strcspn(cadena,"\n");
    //contador de posicion para la copia de una subcadena
    int pos=0;
    //variable que guarda la subcadena del delimitador en adelante
    char* delimAdelante=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos de la cadena original la subcadena delimitada por los caracteres
    //anteriores al comienzo y el caracter "\n" (si existe)
    for(i=comienzo;i<posCRLF;i++)
    {
        nuevaCadena[pos] = cadena[i];
        pos++;
    }
    nuevaCadena[pos] = '\0';
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //guardamos la subcadena que hay del delimitador en adelante en nuevaCadena
    delimAdelante = strstr(nuevaCadena,delimitador);
    //extraemos la subcadena buscada
    if(delimAdelante==NULL)
    {
        //copiamos nuevaCadena en la cadena de salida
        strcpy(subcadena,nuevaCadena);
    }
    else
    {
        //extraemos la subcadena de interes
        for(i=0;i<(int)(strlen(nuevaCadena)-strlen(delimAdelante));i++)
        {
            subcadena[i] = nuevaCadena[i];
        }
        subcadena[i] = '\0';
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int ExisteSubcadena(char cadena[],
                    int comienzo,
                    char subcadena[])
{
    //variable para recorrer un bucle
    int i=0;
    //variable de salida
    int existe=0;
    //longitud de la cadena original
    int lonCadena=(int)strlen(cadena);
    //longitud de la subcadena a buscar
    int lonSubcadena=(int)strlen(subcadena);
    //variable para almacenar el candidato a subcadena
    char candidato[lonSubcadena+1];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si la subcadena a buscar es mas larga que lo que queda de cadena original
    //no existe
    if(lonSubcadena>lonCadena-comienzo)
    {
        return existe;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos la subcadena candidata de la cadena original
    for(i=comienzo;i<comienzo+lonSubcadena;i++)
    {
        candidato[i] = cadena[i];
    }
    candidato[i] = '\0';
    //comparamos la cadena extraida con la subcadena pasada
    existe = !strcmp(subcadena,candidato);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return existe;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int ExisteCadena(char** lista1,
                 char** lista2,
                 char** lista3,
                 int longitudListas,
                 char cadena1[],
                 char cadena2[],
                 char cadena3[])
{
    //indice para recorrer un bucle
    int i=0;
    //indicador de posicion en la lista
    int existe=-1;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos la existencia de la cadena pasada
    for(i=0;i<longitudListas;i++)
    {
        if((!strcmp(lista1[i],cadena1))&&
           (!strcmp(lista2[i],cadena2))&&
           (!strcmp(lista3[i],cadena3)))
        {
            //marcamos el indicador de existencia
            existe = i;
            //salimos del bucle
            break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return existe;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ExistenCadenasEnDosListas(char** lista1,
                               char** lista2,
                               int longitudLista,
                               char cadena1[],
                               char cadena2[],
                               int* posicion,
                               int* orden)
{
    //indice para recorrer un bucle
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos las variables de salida
    *posicion = -1;
    *orden = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos las listas
    for(i=0;i<longitudLista;i++)
    {
        //comprobamos si hay correspondencia directa
        if((!strcmp(lista1[i],cadena1))&&(!strcmp(lista2[i],cadena2)))
        {
            //guardamos la posicion
            *posicion = i;
            //guardamos el indicador de correspondencia directa
            *orden = 1;
            //salimos de la funcion
            return;
        }
        //comprobamos si hay correspondencia inversa
        if((!strcmp(lista1[i],cadena2))&&(!strcmp(lista2[i],cadena1)))
        {
            //guardamos la posicion
            *posicion = i;
            //guardamos el indicador de correspondencia directa
            *orden = -1;
            //salimos de la funcion
            return;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void Cadena1ACadena2(char** lista1,
                     char** lista2,
                     int longitudLista,
                     char cadena1[],
                     char cadena2[],
                     int* posicion1,
                     int* orden1,
                     int* posicion2,
                     int* orden2)
{
    //indice para recorrer un bucle
    int i=0,j=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos las variables de salida
    *posicion1 = -1;
    *orden1 = 0;
    *posicion2 = -1;
    *orden2 = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos las listas
    for(i=0;i<longitudLista;i++)
    {
        //buscamos la cadena origen en la lista origen
        if(!strcmp(cadena1,lista1[i]))
        {
            //posicion
            *posicion1 = i;
            //indicador de correspondencia directa
            *orden1 = 1;
            //recorremos las listas
            for(j=0;j<longitudLista;j++)
            {
                //buscamos la cadena comun en la lista origen y la cadena
                //destino en la lista destino
                if((!strcmp(lista2[i],lista1[j]))&&(!strcmp(cadena2,lista2[j])))
                {
                    //posicion
                    *posicion2 = j;
                    //indicador de correspondencia directa
                    *orden2 = 1;
                    //salimos del bucle
                    break;
                }
                //buscamos la cadena comun en la lista destino y la cadena
                //destino en la lista origen
                if((!strcmp(lista2[i],lista2[j]))&&(!strcmp(cadena2,lista1[j])))
                {
                    //posicion
                    *posicion2 = j;
                    //indicador de correspondencia directa
                    *orden2 = -1;
                    //salimos del bucle
                    break;
                }
            }
            //salimos del bucle
            break;
        }
        //buscamos la cadena origen en la lista destino
        if(!strcmp(cadena1,lista2[i]))
        {
            //posicion
            *posicion1 = i;
            //indicador de correspondencia directa
            *orden1 = -1;
            //recorremos las listas
            for(j=0;j<longitudLista;j++)
            {
                //buscamos la cadena comun en la lista origen y la cadena
                //destino en la lista destino
                if((!strcmp(lista1[i],lista1[j]))&&(!strcmp(cadena2,lista2[j])))
                {
                    //posicion
                    *posicion2 = j;
                    //indicador de correspondencia directa
                    *orden2 = 1;
                    //salimos del bucle
                    break;
                }
                //buscamos la cadena comun en la lista destino y la cadena
                //destino en la lista origen
                if((!strcmp(lista1[i],lista2[j]))&&(!strcmp(cadena2,lista1[j])))
                {
                    //posicion
                    *posicion2 = j;
                    //indicador de correspondencia directa
                    *orden2 = -1;
                    //salimos del bucle
                    break;
                }
            }
            //salimos del bucle
            break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int PosicionVectorTriangular(char uplo[],
                             int dimensiones,
                             int fila,
                             int columna)
{
    //posicion en el vector
    int posicion=0;
    //variable auxiliar
    int aux=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si algun elemento vale 0 devolvemos la posicion 0
    if((!fila)||(!columna))
    {
        return 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si las coordenadas de entrada pertenecen a una matriz triangular superior
    //las transformamos a las correspondientes en una triangular inferior
    if((!strcmp(uplo,"U"))||(!strcmp(uplo,"u")))
    {
        aux = fila;
        fila = columna;
        columna = aux;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //elementos del vector triangular inferior
    posicion = dimensiones*(dimensiones+1)/2;
    //le restamos todos los elementos desde la columna siguiente al elemento a
    //calcular en adelante
    posicion -= (dimensiones-columna)*(dimensiones-columna+1)/2;
    //quitamos el numero de elementos desde la fila del punto de trabajo hasta
    //el final
    posicion -= (dimensiones-fila);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return posicion;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void PosVectorMatrizTriInf(int dimensiones,
                           int filcolx,
                           int filcoly,
                           int filcolz,
                           int* posx,
                           int* posy,
                           int* posz,
                           int* posxy,
                           int* posxz,
                           int* posyz)
{
    //coordenadas de los elementos fuera de la diagonal principal
    int filxy=0,colxy=0,filxz=0,colxz=0,filyz=0,colyz=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posiciones de los elementos de la diagonal principal
    *posx = PosicionVectorTriangular(TRIINF,dimensiones,filcolx,filcolx);
    *posy = PosicionVectorTriangular(TRIINF,dimensiones,filcoly,filcoly);
    *posz = PosicionVectorTriangular(TRIINF,dimensiones,filcolz,filcolz);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos las coordenadas de los elementos fuera de la diagonal principal
    filxy = filcoly;
    colxy = filcolx;
    filxz = filcolz;
    colxz = filcolx;
    filyz = filcolz;
    colyz = filcoly;
    //posiciones de los elementos fuera de la diagonal principal
    *posxy = PosicionVectorTriangular(TRIINF,dimensiones,filxy,colxy);
    *posxz = PosicionVectorTriangular(TRIINF,dimensiones,filxz,colxz);
    *posyz = PosicionVectorTriangular(TRIINF,dimensiones,filyz,colyz);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void CoefCorr(double dtx,
              double dty,
              double covxy,
              double *r)
{
    //calculamos el coeficiente de correlacion
    if((dtx==0.0)||(dty==0.0))
    {
        *r = 0.0;
    }
    else
    {
        *r = covxy/(dtx*dty);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MatrizCorrMatrizVarCov(double stdX,
                            double stdY,
                            double stdZ,
                            double corrXY,
                            double corrXZ,
                            double corrYZ,
                            double* covXY,
                            double* covXZ,
                            double* covYZ)
{
    //calculamos las covarianzas
    *covXY = corrXY*stdX*stdY;
    *covXZ = corrXZ*stdX*stdZ;
    *covYZ = corrYZ*stdY*stdZ;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MatrizVarCovMatrizCorr(double varX,
                            double varY,
                            double varZ,
                            double covXY,
                            double covXZ,
                            double covYZ,
                            double* corrXY,
                            double* corrXZ,
                            double* corrYZ)
{
    //calculamos los coeficientes de correlacion
    CoefCorr(sqrt(varX),sqrt(varY),covXY,corrXY);
    CoefCorr(sqrt(varX),sqrt(varZ),covXZ,corrXZ);
    CoefCorr(sqrt(varY),sqrt(varZ),covYZ,corrYZ);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void MaxMin(double* lista,
            int nElementos,
            int* posMax,
            int* posMin)
{
    //indice para recorrer bucles
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos los argumentos de salida a 0
    *posMax = 0;
    *posMin = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos la lista desde el segundo elemento en adelante porque ya hemos
    //inicializado posMax y posMin con las posiciones del primer elemento
    for(i=1;i<nElementos;i++)
    {
        //comprobamos si el elemento es mayor que el guardado
        if(lista[i]>lista[*posMax])
        {
            *posMax = i;
        }
        //comprobamos si el elemento es menor que el guardado
        if(lista[i]<lista[*posMin])
        {
            *posMin = i;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double Mediana(double* vector,
               const size_t nElementos)
{
    //valor calculado
    double mediana=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ordenamos el vector de trabajo de menor a mayor
    qsort(vector,nElementos,sizeof(double),ComparaDoubleQsort);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //solo trabajamos si el numero de elementos es mayor que 0
    if(nElementos)
    {
        //distinguimos entre un número par e impar de elementos de trabajo
        if(nElementos%2)
        {
            //NÚMERO DE ELEMENTOS IMPAR
            //la mediana es el elemento central del vector
            mediana = vector[(size_t)floor((double)nElementos/2.0)];
        }
        else
        {
            //NÚMERO DE ELEMENTOS PAR
            //la mediana es la media de los dos elementos centrales del vector
            mediana = (vector[nElementos/2]+vector[nElementos/2-1])/2.0;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return mediana;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int ComparaDoubleQsort(const void* a,
                       const void* b)
{
    //valor calculado
    int valor=0;
    //valores de entrada
    double A=*((double*)a);
    double B=*((double*)b);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si a es menor que b
    valor = A>=B ? 1 : -1;
    //si a no es menor que b
    if(valor)
    {
        //comprobamos si a es mayor que b o si a es igual a b
        valor = A>B ? 1 : 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return valor;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
char** CodigosIndividuales(char** codigos,
                           const size_t numEnt,
                           size_t* numSal)
{
    //indices para recorrer bucles
    size_t i=0,j=0;
    //longitud del codigo mas largo
    size_t lonMax=0;
    //matrices para almacenar los codigos de los puntos
    char** codInd=NULL;
    char** codIndAux=NULL;
    //indicador de existencia de punto repetido
    int existe=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salida rapida si no hay ningun punto de trabajo
    if(numEnt==0)
    {
        *numSal = 0;
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la longitud del codigo mas largo
    for(i=0;i<numEnt;i++)
    {
        if(strlen(codigos[i])>lonMax)
        {
            lonMax = strlen(codigos[i]);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para la matriz auxiliar
    codIndAux = (char**)malloc(numEnt*sizeof(char*));
    //comprobamos si ha habido errores
    if(codIndAux==NULL)
    {
        //salimos de la funcion
        return NULL;
    }
    for(i=0;i<numEnt;i++)
    {
        //asignamos memoria para cada codigo
        codIndAux[i] = (char*)malloc((lonMax+1)*sizeof(char));
        //comprobamos si ha habido errores
        if(codIndAux[i]==NULL)
        {
            //liberamos la memoria asignada
            for(j=0;j<i;j++)
            {
                free(codIndAux[j]);
            }
            free(codIndAux);
            //salimos de la funcion
            return NULL;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //copiamos el primer codigo a la lista auxiliar
    strcpy(codIndAux[0],codigos[0]);
    //inicializamos el contador de puntos individuales
    *numSal = 1;
    //contamos el numero de puntos individuales
    for(i=1;i<numEnt;i++)
    {
        //desactivamos el indicador de existencia de punto igual
        existe = 0;
        //recorremos los puntos ya asignados
        for(j=0;j<(*numSal);j++)
        {
            //comprobamos si el punto ya ha sido asignado
            if(!strcmp(codigos[i],codIndAux[j]))
            {
                //activamos el indicador de existencia
                existe = 1;
            }
        }
        //si no existe coincidencia, asignamos el codigo al vector auxiliar y
        //aumentamos el contador de puntos
        if(!existe)
        {
            strcpy(codIndAux[*numSal],codigos[i]);
            (*numSal)++;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para la matriz de salida
    codInd = (char**)malloc((*numSal)*sizeof(char*));
    //comprobamos si ha habido errores
    if(codInd==NULL)
    {
        //liberamos la memoria asignada
        for(i=0;i<numEnt;i++)
        {
            free(codIndAux[i]);
        }
        free(codIndAux);
        //salimos de la funcion
        return NULL;
    }
    for(i=0;i<(*numSal);i++)
    {
        //asignamos memoria para cada codigo
        codInd[i] = (char*)malloc((lonMax+1)*sizeof(char));
        //comprobamos si ha habido errores
        if(codInd[i]==NULL)
        {
            //liberamos la memoria asignada
            for(j=0;j<i;j++)
            {
                free(codInd[j]);
            }
            free(codInd);
            for(j=0;j<numEnt;j++)
            {
                free(codIndAux[j]);
            }
            free(codIndAux);
            //salimos de la funcion
            return NULL;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //copiamos los datos a la matriz de salida
    for(i=0;i<(*numSal);i++)
    {
        strcpy(codInd[i],codIndAux[i]);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada
    for(i=0;i<numEnt;i++)
    {
        free(codIndAux[i]);
    }
    free(codIndAux);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return codInd;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double** AsigMemMatrizC(const size_t fil,
                        const size_t col)
{
    //índices para recorrer bucles
    size_t i=0,j=0;
    //matriz de salida
    double** matriz=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para el array principal
    matriz = (double**)malloc(fil*sizeof(double*));
    //comprobamos los errores
    if(matriz==NULL)
    {
        //salimos de la función
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos las filas
    for(i=0;i<fil;i++)
    {
        //vamos asignando memoria para cada fila
        matriz[i] = (double*)malloc(col*sizeof(double));
        //comprobamos los errores
        if(matriz[i]==NULL)
        {
            //liberamos la memoria previamente asignada
            for(j=0;j<i;j++)
            {
                free(matriz[j]);
            }
            //liberamos la memoria del array principal
            free(matriz);
            //salimos de la función
            return NULL;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return matriz;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LibMemMatrizC(double** matriz,
                   const size_t fil)
{
    //índice para recorrer un bucle
    size_t i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    if(matriz!=NULL)
    {
        //recorremos las filas
        for(i=0;i<fil;i++)
        {
            //vamos liberando memoria
            free(matriz[i]);
        }
        //liberamos la memoria del array principal
        free(matriz);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
