/**
\addtogroup general
@{
\file matriz.c
\brief Definición de funciones para la inversión de una matriz simétrica
       definida positiva por el método de Cholesky según Lapack.

En este fichero se definen las funciones necesarias para invertir una matriz
simétrica definida positiva almacenada en fomato empaquetado. Este código es
copiapega del que tengo hecho en LIBGEOC
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlg.pallero@upm.es
\date 7 de mayo de 2021
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include"matriz.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void gclapack_xerbla(gblas_int p,
                     const char *rout,
                     const char *form,
                     ...)
{
    //puntero a la lista de argumentos variable
    //inicializarlo a NULL da error en el iBook con Debian y gcc 4.3.3
    va_list argVar;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el puntero argVar con el último elemento conocido
    va_start(argVar,form);
    //imprimimos la información referente al parámetro que ha fallado
    if(p)
    {
        //información general
        fprintf(stderr,"\n"
"**********\n"
"**********ERROR EN CLAPACK: EL PARÁMETRO %d DE LA FUNCIÓN '%s' ES INCORRECTO\n"
"**********EL PROGRAMA FINALIZARÁ MEDIANTE LA LLAMADA A exit(EXIT_FAILURE)\n"
"**********\n\n",(int)p,rout);
    }
    //imprimimos los valores opcionales
    vfprintf(stderr,form,argVar);
    //retorno normal de la función
    va_end(argVar);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //detenemos la ejecución del programa
    exit(EXIT_FAILURE);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la fnción
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int gclapack_dpptrf(const enum CBLAS_ORDER Order,
                    const enum CBLAS_UPLO Uplo,
                    const gblas_int N,
                    double* Ap)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //índices para recorrer bucles
    gblas_int i=0,j=0;
    //índices auxiliares
    gblas_int ii=0,ic=0,jj=0;
    //variable auxiliar
    double aux=0.0;
    //variables relacionadas con matrices
    enum CBLAS_ORDER ColMajor=CblasColMajor;
    enum CBLAS_UPLO Lower=CblasLower;
    enum CBLAS_UPLO Upper=CblasUpper;
    enum CBLAS_DIAG NonUnit=CblasNonUnit;
    enum CBLAS_TRANSPOSE Trans=CblasTrans;
    //variable de estado
    int estado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos los posibles errores en los argumentos de entrada
    GEOCLPK_ERROR_PPTRF(pos,Order,Uplo,N);
    //salimos de la función, si ha lugar
    if(pos!=0)
    {
        //asignamos la variable de salida
        estado = -pos;
        //lanzamos el mensaje de error
        gclapack_xerbla(pos,__func__,"");
        //salimos de la función
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si N es 0, no se realizan cálculos
    if(N==0)
    {
        //salimos de la función
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distinguimos entre factorización U'*U y L*L'
    if(((Order==CblasColMajor)&&(Uplo==CblasLower))||
       ((Order==CblasRowMajor)&&(Uplo==CblasUpper)))
    {
        //inicializamos un índice auxiliar
        jj = 0;
        //recorremos las columnas de la matriz Ap
        for(j=0;j<N;j++)
        {
            //calculamos el elemento L(j,j) y chequeamos si la matriz es
            //definida positiva
            aux = Ap[jj];
            if(aux<=0.0)
            {
                //asignamos el elemento a la matriz de salida
                Ap[jj] = aux;
                //asignamos el estado de error
                estado = j;
                //salimos de la función
                return estado;
            }
            //asignamos el elemento a la matriz de salida
            aux = sqrt(aux);
            Ap[jj] = aux;
            //calculamos los elementos j+1 a N de la columna j y actualizamos la
            //submatriz
            if(j<(N-1))
            {
                //llamamos a cblas_dscal
                cblas_dscal(N-j-1,1.0/aux,&Ap[jj+1],1);
                //llamamos a cblas_dspr
                cblas_dspr(ColMajor,Lower,N-j-1,-1.0,&Ap[jj+1],1,&Ap[jj+N-j]);
                //actualizamos la posición auxiliar
                jj += N-j;
            }
        }
    }
    else if(((Order==CblasColMajor)&&(Uplo==CblasUpper))||
            ((Order==CblasRowMajor)&&(Uplo==CblasLower)))
    {
        //inicializamos un índice auxiliar
        ii = 0;
        //recorremos las filas de la matriz Ap
        for(i=1;i<=N;i++)
        {
            //actualizamos contadores
            ic = ii+1;
            ii = ii+i;
            //calculamos los elementos 0 a i-1 de la columna i
            if(i>1)
            {
                //llamamos a cblas_dtpsv
                cblas_dtpsv(ColMajor,Upper,Trans,NonUnit,i-1,&Ap[0],&Ap[ic-1],
                            1);
            }
            //calculamos el elemento U(j,j) y chequeamos si la matriz es
            //definida positiva
            aux = Ap[ii-1]-cblas_ddot(i-1,&Ap[ic-1],1,&Ap[ic-1],1);
            if(aux<=0.0)
            {
                //asignamos el elemento a la matriz de salida
                Ap[ii-1] = aux;
                //asignamos el estado de error
                estado = i-1;
                //salimos de la función
                return estado;
            }
            //asignamos el elemento a la matriz de salida
            Ap[ii-1] = sqrt(aux);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return estado;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int gclapack_dpptri(const enum CBLAS_ORDER Order,
                    const enum CBLAS_UPLO Uplo,
                    const gblas_int N,
                    double* Ap)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //índices para recorrer bucles
    gblas_int i=0,j=0,ii=0,jj=0,iAux=0,jAux=0;
    //variable auxiliar
    double aux=0.0;
    //variables relacionadas con matrices
    enum CBLAS_ORDER ColMajor=CblasColMajor;
    enum CBLAS_UPLO Lower=CblasLower;
    enum CBLAS_UPLO Upper=CblasUpper;
    enum CBLAS_DIAG NonUnit=CblasNonUnit;
    enum CBLAS_TRANSPOSE Trans=CblasTrans;
    //variable de estado
    int estado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos los posibles errores en los argumentos de entrada
    GEOCLPK_ERROR_PPTRI(pos,Order,Uplo,N);
    //salimos de la función, si ha lugar
    if(pos!=0)
    {
        //asignamos la variable de salida
        estado = -pos;
        //lanzamos el mensaje de error
        gclapack_xerbla(pos,__func__,"");
        //salimos de la función
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si N es 0, no se realizan cálculos
    if(N==0)
    {
        //salimos de la función
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //invertimos el factor de Cholesky
    estado = gclapack_dtptri(Order,Uplo,NonUnit,N,Ap);
    //salimos, si ha habido algún error
    if(estado)
    {
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //multiplicamos inv(U)*inv(U)' o inv(L)'*inv(L)
    if(((Order==CblasColMajor)&&(Uplo==CblasLower))||
       ((Order==CblasRowMajor)&&(Uplo==CblasUpper)))
    {
        //inicializamos un índice
        jj = 1;
        //recorremos las columnas
        for(j=1;j<=N;j++)
        {
            jAux = jj+N-j+1;
            Ap[jj-1] = cblas_ddot(N-j+1,&Ap[jj-1],1,&Ap[jj-1],1);
            if(j<N)
            {
                cblas_dtpmv(ColMajor,Lower,Trans,NonUnit,N-j,&Ap[jAux-1],
                            &Ap[jj],1);
            }
            jj = jAux;
        }
    }
    else if(((Order==CblasColMajor)&&(Uplo==CblasUpper))||
            ((Order==CblasRowMajor)&&(Uplo==CblasLower)))
    {
        //inicializamos un índice
        ii = 0;
        //recorremos las columnas
        for(i=1;i<=N;i++)
        {
            iAux = ii+1;
            ii += i;
            if(i>1)
            {
                cblas_dspr(ColMajor,Upper,i-1,1.0,&Ap[iAux-1],1,&Ap[0]);
            }
            aux = Ap[ii-1];
            cblas_dscal(i,aux,&Ap[iAux-1],1);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return estado;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int gclapack_dtptri(const enum CBLAS_ORDER Order,
                    const enum CBLAS_UPLO Uplo,
                    const enum CBLAS_DIAG Diag,
                    const gblas_int N,
                    double* Ap)
{
    //posición del posible error en los argumentos de entrada
    gblas_int pos=0;
    //índices para recorrer bucles
    gblas_int i=0,j=0,ii=0,jj=0,jAux=0;
    //variable auxiliar
    double aux=0.0;
    //variables relacionadas con matrices
    enum CBLAS_ORDER ColMajor=CblasColMajor;
    enum CBLAS_UPLO Lower=CblasLower;
    enum CBLAS_UPLO Upper=CblasUpper;
    enum CBLAS_TRANSPOSE NoTrans=CblasNoTrans;
    //variable de estado
    int estado=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos los posibles errores en los argumentos de entrada
    GEOCLPK_ERROR_TPTRI(pos,Order,Uplo,Diag,N);
    //salimos de la función, si ha lugar
    if(pos!=0)
    {
        //asignamos la variable de salida
        estado = -pos;
        //lanzamos el mensaje de error
        gclapack_xerbla(pos,__func__,"");
        //salimos de la función
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si N es 0, no se realizan cálculos
    if(N==0)
    {
        //salimos de la función
        return estado;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distinguimos los tipos de ordenación y la parte triangular de trabajo
    if(((Order==CblasColMajor)&&(Uplo==CblasLower))||
       ((Order==CblasRowMajor)&&(Uplo==CblasUpper)))
    {
        //comprobamos si la matriz es singular
        if(Diag==CblasNonUnit)
        {
            //inicializamos un índice auxiliar
            jj = 1;
            //recorremos las columnas de la matriz
            for(j=1;j<=N;j++)
            {
                //comprobamos el elemento de la diagonal
                if(Ap[jj-1]==0.0)
                {
                    //asignamos el estado de error
                    estado = j-1;
                    //salimos de la función
                    return estado;
                }
                //calculamos la posición del elemento de trabajo
                jj += N-j+1;
            }
        }
        //inicializamos un índice auxiliar
        jj = N*(N+1)/2;
        //recorremos las columnas de la matriz desde el final
        for(j=N;j>0;j--)
        {
            if(Diag==CblasNonUnit)
            {
                Ap[jj-1] = 1.0/Ap[jj-1];
                aux = -Ap[jj-1];
            }
            else
            {
                aux = -1.0;
            }
            //calculamos los elementos j+1 a N de la columna j
            if(j<N)
            {
                //llamamos a cblas_dtpmv
                cblas_dtpmv(ColMajor,Lower,NoTrans,Diag,N-j,&Ap[jAux-1],&Ap[jj],
                            1);
                //llamamos a cblas_dscal
                cblas_dscal(N-j,aux,&Ap[jj],1);
            }
            //actualizamos los índices
            jAux = jj;
            jj += j-2-N;
        }
    }
    else if(((Order==CblasColMajor)&&(Uplo==CblasUpper))||
            ((Order==CblasRowMajor)&&(Uplo==CblasLower)))
    {
        //comprobamos si la matriz es singular
        if(Diag==CblasNonUnit)
        {
            //inicializamos un índice auxiliar
            ii = 0;
            //recorremos las filas de la matriz
            for(i=1;i<=N;i++)
            {
                //calculamos la posición del elemento de trabajo
                ii += i;
                //comprobamos el elemento de la diagonal
                if(Ap[ii-1]==0.0)
                {
                    //asignamos el estado de error
                    estado = i-1;
                    //salimos de la función
                    return estado;
                }
            }
        }
        //inicializamos un índice auxiliar
        ii = 1;
        //recorremos las filas de la matriz
        for(i=1;i<=N;i++)
        {
            if(Diag==CblasNonUnit)
            {
                Ap[ii+i-2] = 1.0/Ap[ii+i-2];
                aux = -Ap[ii+i-2];
            }
            else
            {
                aux = -1.0;
            }
            //llamamos a cblas_dtpmv
            cblas_dtpmv(ColMajor,Upper,NoTrans,Diag,i-1,&Ap[0],&Ap[ii-1],1);
            //llamamos a cblas_dscal
            cblas_dscal(i-1,aux,&Ap[ii-1],1);
            //actualizamos el índice
            ii += i;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la función
    return estado;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
