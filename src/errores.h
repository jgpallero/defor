/**
\defgroup errores Modulo ERRORES
\ingroup sinex
\ingroup puntos
\ingroup transf
\ingroup deformacion
\brief En este modulo se reunen los ficheros necesarios para realizar el
       tratamiento de errores.

Los ficheros descritos a continuacion son el material imprescindible para
realizar un correcto tratamiento de los errores que pueden aparacer en la
programacion con la biblioteca DEFOR.
@{
\file errores.h
\brief Declaracion de funciones para el tratamiento de errores.

En este fichero se declaran varias funciones para el tratamiento de errores.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 29 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _ERRORES_H_
#define _ERRORES_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def ERRNOERROR
    \brief Indicador de que no ha ocurrido ningun error. */
#define ERRNOERROR         0
/** \def ERRNELEMMENORCERO
    \brief Indicador de que se ha pasado un argumento 'numero de elementos'
           menor o igual que 0. */
#define ERRNELEMMENORCERO  1
/** \def ERRNOMEMORIA
    \brief Indicador de que ha ocurrido en la asignacion dinamica de memoria. */
#define ERRNOMEMORIA       2
/** \def ERRNELEMLEIDOSFICH
    \brief Indicador de que ha ocurrido un error al leer un numero dado de
           elementos de un fichero. */
#define ERRNELEMLEIDOSFICH 3
/** \def ERRCLASMATNOVALIDA
    \brief Indicador de clase de matriz no valida . */
#define ERRCLASMATNOVALIDA 4
/** \def ERRTIPOMATNOVALIDO
    \brief Indicador de tipo de matriz no valido . */
#define ERRTIPOMATNOVALIDO 5
/** \def ERRABRIRFICHERO
    \brief Indicador de que ha ocurrido un error al abrir un fichero. */
#define ERRABRIRFICHERO    6
/** \def ERRNOBLOQUEFICH
    \brief Indicador de que no existe un bloque buscado en un fichero. */
#define ERRNOBLOQUEFICH    7
/** \def ERRNOTRANSF
    \brief Indicador de que no existe transformacion entre dos sistemas de
           referencia. */
#define ERRNOTRANSF 8
/** \def ERRNOPTOSSTEMP
    \brief Indicador de que no existen puntos individuales para calcular series
           temporales. */
#define ERRNOPTOSSTEMP 9
/** \def ERRESCFICH
    \brief Indicador de que ha ocurrido un error al escribir en un fichero. */
#define ERRESCFICH 10
/** \def ERRCHOL
    \brief Indicador de que ha ocurrido un error en la descomposición de
           Cholesky. */
#define ERRCHOL 11
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRNELEMMENORCERO.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
*/
void MensajeErrorNELEMMENORCERO(char funcion[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRNOMEMORIA.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
*/
void MensajeErrorNOMEMORIA(char funcion[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRNELEMLEIDOSFICH.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] numeroLinea Numero de linea del fichero en la que ha ocurrido el
                       error.
*/
void MensajeErrorNELEMLEIDOSFICH(char funcion[],
                                 int numeroLinea);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRCLASMATNOVALIDA.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] clMat Cadena identificadora de la clase de matriz que ha producido el
                 error.
*/
void MensajeErrorCLASMATNOVALIDA(char funcion[],
                                 char clMat[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRTIPOMATNOVALIDO.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] tipoMat Cadena identificadora del tipo de matriz que ha producido el
                   error.
*/
void MensajeErrorTIPOMATNOVALIDO(char funcion[],
                                 char tipoMat[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRABRIRFICHERO.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] fichero Nombre del fichero que ha producido el error.
*/
void MensajeErrorABRIRFICHERO(char funcion[],
                              char fichero[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRNOBLOQUEFICH.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] bloque Nombre del bloque buscado que ha producido el error.
\param[in] fichero Nombre del fichero que ha producido el error.
*/
void MensajeErrorNOBLOQUEFICH(char funcion[],
                              char bloque[],
                              char fichero[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRNOTRANSF.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] sr1 Nombre del sistema de referencia de inicio que ha producido el
               error.
\param[in] sr2 Nombre del sistema de referencia de destino que ha producido el
               error.
\param[in] bloque Nombre del bloque buscado que ha producido el error.
\param[in] fichero Nombre del fichero que ha producido el error.
*/
void MensajeErrorNOTRANSF(char funcion[],
                          char sr1[],
                          char sr2[],
                          char bloque[],
                          char fichero[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRNOPTOSSTEMP.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
*/
void MensajeErrorERRNOPTOSSTEMP(char funcion[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRESCFICH.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] fichero Nombre del fichero que ha producido el error.
*/
void MensajeErrorERRESCFICH(char funcion[],
                            char fichero[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, un mensaje informando de la
       ocurrencia de un error de codigo #ERRCHOL.
\param[in] funcion Nombre de la funcion en la que ha ocurrido el error.
\param[in] numeroLinea Numero de linea del fichero en la que ha ocurrido el
                       error.
*/
void MensajeErrorERRCHOL(char funcion[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, informacion acerca de que funcion
       ha llamado a la funcion en la que se ha producido un error.
\param[in] funcAct Nombre de la funcion desde la que se llama a la funcion en la
                   que se ha producido el error.
\param[in] funcOrig Nombre de la funcion en la que se ha producido el error.
*/
void MensajeErrorPropagado(char funcAct[],
                           char funcOrig[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en la salida de error, stderr, informacion acerca del tipo de
       erro que ha ocurrido en un programa.
\param[in] cError Codigo del error.
\param[in] programa Nombre del programa en el que ha ocurrido el error.
*/
void MensajeErrorPrograma(int cError,
                          char programa[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
