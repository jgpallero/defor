/**
\addtogroup dibgmt
@{
\file dibgmt.h
\brief Declaracion de funciones para la escritura de ficheros de ordenes para
       GMT.

En este fichero se declaran las funciones necesarias para la escritura de
ficheros de ordenes para la representacion grafica de datos con GMT.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\author Pablo Jose Gonzalez Mendez, pjgonzal@mat.ucm.es
\date 21 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _DIBGMT_H_
#define _DIBGMT_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include"errores.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en un fichero de ordenes de GMT las variables necesarias para
       representar datos de deformacion.
\param[in] idF Identificador de fichero abierto para escribir.
\param[in] fichSalP Nombre del fichero de salida de planimetria.
\param[in] fichSalH Nombre del fichero de salida de altimetria.
\param[in] latNW Latitud geodesica ampliada del limite NW del dibujo, en grados
                 sexagesimales, formato decimal.
\param[in] lonNW Longitud geodesica ampliada del limite NW del dibujo, en grados
                 sexagesimales, formato decimal.
\param[in] latSE Latitud geodesica ampliada del limite SE del dibujo, en grados
                 sexagesimales, formato decimal.
\param[in] lonSE Longitud geodesica ampliada del limite SE del dibujo, en grados
                 sexagesimales, formato decimal.
\param[in] latNWOrig Latitud geodesica original del limite NW del dibujo, en
                     grados sexagesimales, formato decimal.
\param[in] lonNWOrig Longitud geodesica original del limite NW del dibujo, en
                     grados sexagesimales, formato decimal.
\param[in] latSEOrig Latitud geodesica original del limite SE del dibujo, en
                     grados sexagesimales, formato decimal.
\param[in] lonSEOrig Longitud geodesica original del limite SE del dibujo, en
                     grados sexagesimales, formato decimal.
\param[in] pasoCoor Paso en coordenadas geodesicas para las escalas laterales
                    del dibujo, en grados sexagesimales, formato decimal.
\param[in] tamEscala Tamanyo de la escala grafica del dibujo, en kilometros.
\param[in] escalaRefH Tamanyo de la escala de referencia para las deformaciones
           horizontales, en centimetros.
\param[in] escalaRefV Tamanyo de la escala de referencia para las deformaciones
           verticales, en centimetros.
\note Esta funcion empieza a escribir en el lugar donde se encuentre el puntero
      de L/E del fichero pasado.
\note Esta funcion acaba de escribir en el fichero indicado con una nueva linea
      y un retorno de carro.
*/
void CabeceraGMT(FILE* idF,
                 char fichSalP[],
                 char fichSalH[],
                 double latNW,
                 double lonNW,
                 double latSE,
                 double lonSE,
                 double latNWOrig,
                 double lonNWOrig,
                 double latSEOrig,
                 double lonSEOrig,
                 double pasoCoor,
                 double tamEscala,
                 double escalaRefH,
                 double escalaRefV);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en un fichero de ordenes de GMT los comandos necesarios para
       dibujar un mapa de deformacion en el plano de una serie de puntos.
\param[in] idF Identificador de fichero abierto para escribir.
\param[in] nPuntos Numero de puntos a dibujar.
\param[in] cod Vector que almacena los codigos de los puntos a dibujar.
\param[in] lat Vector que almacena la latitud (en grados sexagesimales, formato
               decimal) de los puntos a dibujar.
\param[in] lon Vector que almacena la longitud (en grados sexagesimales, formato
               decimal) de los puntos a dibujar.
\param[in] de Vector que almacena el desplazamiento a lo largo del eje east de
              los puntos a dibujar.
\param[in] dn Vector que almacena el desplazamiento a lo largo del eje north de
              los puntos a dibujar.
\param[in] sde Vector que almacena la desviacion tipica del desplazamiento a lo
               largo del eje east de los puntos a dibujar.
\param[in] sdn Vector que almacena la desviacion tipica del desplazamiento a lo
               largo del eje north de los puntos a dibujar.
\param[in] cdedn Vector que almacena el coeficiente de correlacion entre las
                 desviaciones tipicas de los desplazamientos a lo largo de los
                 ejes east y north de los puntos a dibujar.
\note Todos los vectores pasados han de tener la misma longitud.
\note Si el argumento nPuntos es mayor que el numero de elementos de los
      vectores pasados se produciran errores al intentar acceder a elementos que
      no existen.
\note Si el argumento nPuntos es menor que el numero de elementos de los
      vectores pasados solo se escribiran en el fichero los nPuntos primeros
      elementos de los vectores.
\note Esta funcion empieza a escribir en el lugar donde se encuentre el puntero
      de L/E del fichero pasado.
\note Esta funcion acaba de escribir en el fichero indicado con una nueva linea
      y un retorno de carro.
*/
void DeformacionPlanoGMT(FILE* idF,
                         int nPuntos,
                         char** cod,
                         double* lat,
                         double* lon,
                         double* de,
                         double* dn,
                         double* sde,
                         double* sdn,
                         double* cdedn);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en un fichero de ordenes de GMT los comandos necesarios para
       dibujar un mapa de deformacion altimetrica de una serie de puntos.
\param[in] idF Identificador de fichero abierto para escribir.
\param[in] nPuntos Numero de puntos a dibujar.
\param[in] cod Vector que almacena los codigos de los puntos a dibujar.
\param[in] lat Vector que almacena la latitud (en grados sexagesimales, formato
               decimal) de los puntos a dibujar.
\param[in] lon Vector que almacena la longitud (en grados sexagesimales, formato
               decimal) de los puntos a dibujar.
\param[in] du Vector que almacena el desplazamiento a lo largo del eje up de los
              puntos a dibujar.
\param[in] sdu Vector que almacena la desviacion tipica del desplazamiento a lo
               largo del eje up de los puntos a dibujar.
\note Todos los vectores pasados han de tener la misma longitud.
\note Si el argumento nPuntos es mayor que el numero de elementos de los
      vectores pasados se produciran errores al intentar acceder a elementos que
      no existen.
\note Si el argumento nPuntos es menor que el numero de elementos de los
      vectores pasados solo se escribiran en el fichero los nPuntos primeros
      elementos de los vectores.
\note Esta funcion empieza a escribir en el lugar donde se encuentre el puntero
      de L/E del fichero pasado.
\note Esta funcion acaba de escribir en el fichero indicado con una nueva linea
      y un retorno de carro.
*/
void DeformacionAlturaGMT(FILE* idF,
                          int nPuntos,
                          char** cod,
                          double* lat,
                          double* lon,
                          double* du,
                          double* sdu);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Escribe en un fichero de ordenes de GMT los comandos necesarios para
       dibujar mapas de deformacion planimetrica y altimetrica de una serie de
       puntos.
\param[in] idFichero Identificador de fichero abierto para escribir.
\param[in] fichero Parte comun del nombre de los ficheros de dibujo de
                   deformacion en planimetria y altimetria.
\param[in] nPuntos Numero de puntos a dibujar.
\param[in] cod Vector que almacena los codigos de los puntos a dibujar.
\param[in] lat Vector que almacena la latitud (en grados sexagesimales, formato
               decimal) de los puntos a dibujar.
\param[in] lon Vector que almacena la longitud (en grados sexagesimales, formato
               decimal) de los puntos a dibujar.
\param[in] de Vector que almacena el desplazamiento a lo largo del eje east de
              los puntos a dibujar.
\param[in] dn Vector que almacena el desplazamiento a lo largo del eje north de
              los puntos a dibujar.
\param[in] du Vector que almacena el desplazamiento a lo largo del eje up de los
              puntos a dibujar.
\param[in] sde Vector que almacena la desviacion tipica del desplazamiento a lo
               largo del eje east de los puntos a dibujar.
\param[in] sdn Vector que almacena la desviacion tipica del desplazamiento a lo
               largo del eje north de los puntos a dibujar.
\param[in] cdedn Vector que almacena el coeficiente de correlacion entre las
                 desviaciones tipicas de los desplazamientos a lo largo de los
                 ejes east y north de los puntos a dibujar.
\param[in] sdu Vector que almacena la desviacion tipica del desplazamiento a lo
               largo del eje up de los puntos a dibujar.
\return Codigo de error.
\note Todos los vectores pasados han de tener la misma longitud.
\note Si el argumento nPuntos es mayor que el numero de elementos de los
      vectores pasados se produciran errores al intentar acceder a elementos que
      no existen.
\note Si el argumento nPuntos es menor que el numero de elementos de los
      vectores pasados solo se escribiran en el fichero los nPuntos primeros
      elementos de los vectores.
\note Esta funcion empieza a escribir en el lugar donde se encuentre el puntero
      de L/E del fichero pasado.
\note Esta funcion acaba de escribir en el fichero indicado con una nueva linea
      y un retorno de carro.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por los vectores de entrada no es
      liberada.
*/
int DibujaDeformacionGMT(FILE* idFichero,
                         char fichero[],
                         int nPuntos,
                         char** cod,
                         double* lat,
                         double* lon,
                         double* de,
                         double* dn,
                         double* du,
                         double* sde,
                         double* sdn,
                         double* cdedn,
                         double* sdu);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
