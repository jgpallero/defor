/**
\addtogroup dibgmt
@{
\file dibgmt.c
\brief Definicion de funciones para la escritura de ficheros de ordenes para
       GMT.

En este fichero se definen las funciones necesarias para la escritura de
ficheros de ordenes para la representacion grafica de datos con GMT.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\author Pablo Jose Gonzalez Mendez, pjgonzal@mat.ucm.es
\date 21 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"general.h"
#include"geodesia.h"
#include"paramgmt.h"
#include"dibgmt.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void CabeceraGMT(FILE* idF,
                 char fichSalP[],
                 char fichSalH[],
                 double latNW,
                 double lonNW,
                 double latSE,
                 double lonSE,
                 double latNWOrig,
                 double lonNWOrig,
                 double latSEOrig,
                 double lonSEOrig,
                 double pasoCoor,
                 double tamEscala,
                 double escalaRefH,
                 double escalaRefV)
{
    //formato de escritura
    char ft[LONFORMGMT+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos la shell a utilizar
    fprintf(idF,"#!%s\n",INTERPRETE);
    fprintf(idF,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //encerramos en un recuadro las variables a modificar por el usuario
    fprintf(idF,"########################################");
    fprintf(idF,"########################################\n");
    fprintf(idF,"#LOS SIGUIENTES PARAMETROS CONTROLAN LA APARIENCIA DEL ");
    fprintf(idF,"DIBUJO\n");
    fprintf(idF,"\n");
    //escribimos los nombres de los ficheros de salida
    fprintf(idF,"#Nombres de los ficheros de salida (planimetria y ");
    fprintf(idF,"altimetria)\n");
    fprintf(idF,"%s=%s\n",NVARFSALP,fichSalP);
    fprintf(idF,"%s=%s\n",NVARFSALH,fichSalH);
    fprintf(idF,"\n");
    //escribimos el indicador de dibujo de los codigos de las estaciones
    fprintf(idF,"#Indicador para dibujar los codigos de las estaciones ");
    fprintf(idF,"(%d=si, distinto de %d=no)\n",DIBCOD,DIBCOD);
    fprintf(idF,"%s=%d\n",NVARDIBCOD,DIBCOD);
    fprintf(idF,"\n");
    //escribimos el nivel de confianza de los errores
    fprintf(idF,"#Nivel de confianza para las elipses y los intervalos de ");
    fprintf(idF,"error\n");
    sprintf(ft,"%%s=%s\n",FORMNCONFGMT);
    fprintf(idF,ft,NVARNCONF,NCONF);
    fprintf(idF,"\n");
    //escribimos los limites del dibujo
    fprintf(idF,"#Limites del dibujo ampliados\n");
    fprintf(idF,"#Estos limites pueden contener errores debido a las\n");
    fprintf(idF,"#singularidades en los limites de la coordenada longitud\n");
    fprintf(idF,"#geodesica, especialmente cuando nuestra zona de trabajo\n");
    fprintf(idF,"#se situa en torno al antimeridiano 0\n");
    sprintf(ft,"%%s=%%s%s/%s/%s/%s\n",
            FORMLDIBGMT,FORMLDIBGMT,FORMLDIBGMT,FORMLDIBGMT);
    fprintf(idF,ft,NVARLIMIT,LIMIT,lonNW,lonSE,latSE,latNW);
    fprintf(idF,"#Limites del dibujo originales\n");
    sprintf(ft,"#%%s=%%s%s/%s/%s/%s\n",
            FORMLDIBGMT,FORMLDIBGMT,FORMLDIBGMT,FORMLDIBGMT);
    fprintf(idF,ft,NVARLIMIT,LIMIT,lonNWOrig,lonSEOrig,latSEOrig,latNWOrig);
    fprintf(idF,"\n");
    //escribimos el estilo de las barras laterales del dibujo
    fprintf(idF,"#Barras laterales del dibujo\n");
    sprintf(ft,"%%s=%%s%s\n",FORMPASOCOOR);
    fprintf(idF,ft,NVARBARLAT,BARLAT,pasoCoor);
    fprintf(idF,"\n");
    //escribimos la proyeccion cartografica utilizada
    fprintf(idF,"#Proyeccion cartografica (M: Mercator)\n");
    fprintf(idF,"%s=%s\n",NVARPROYEC,PROYEC);
    fprintf(idF,"\n");
    //escribimos la orientacion de la pagina
    fprintf(idF,"#Orientacion de pagina (%s: vertical, -L: horizontal)\n",
            ORIPAG);
    fprintf(idF,"%s=%s\n",NVARORIPAG,ORIPAG);
    fprintf(idF,"\n");
    //escribimos el logotipo de GMT
    fprintf(idF,"#Dibujo con el logotipo de GMT\n");
    fprintf(idF,"%s=%s\n",NVARLOGO,LOGO);
    fprintf(idF,"#Dibujo sin el logotipo de GMT\n");
    fprintf(idF,"#%s=\n",NVARLOGO);
    fprintf(idF,"\n");
    //escribimos la resolucion de las lineas de costa
    fprintf(idF,"#Resolucion de las lineas de costa (%s: full, -Dh: ",RESCOS);
    fprintf(idF,"high, -Di: intermediate)\n");
    fprintf(idF,"%s=%s\n",NVARRESCOS,RESCOS);
    fprintf(idF,"\n");
    //escribimos la escala grafica del dibujo
    fprintf(idF,"#Escala grafica del dibujo\n");
    fprintf(idF,"%s=%s%.1lfk\n",NVAREGRAF,EGRAF,tamEscala);
    fprintf(idF,"\n");
    //escribimos las escalas de referencia para las deformaciones
    fprintf(idF,"#Escala de referencia para las deformaciones horizontales\n");
    sprintf(ft,"%s=\"%s%s%s%s0.0%s%lf%s0.0%s0.0%s0.0%s%.1lfcm\"\n",
            NVAREDEFH,FORMLONGMT,SEPARADGMT,FORMLATGMT,SEPARADGMT,SEPARADGMT,
            escalaRefH/100.0,SEPARADGMT,SEPARADGMT,SEPARADGMT,SEPARADGMT,
            escalaRefH);
    fprintf(idF,ft,lonNWOrig,latSEOrig);
    fprintf(idF,"#Escala de referencia para las deformaciones verticales\n");
    sprintf(ft,"%s=\"%s%s%s%s0.0%s%lf%s0.0%s0.0%s0.0%s%.1lfcm\"\n",
            NVAREDEFV,FORMLONGMT,SEPARADGMT,FORMLATGMT,SEPARADGMT,SEPARADGMT,
            escalaRefV/100.0,SEPARADGMT,SEPARADGMT,SEPARADGMT,SEPARADGMT,
            escalaRefV);
    fprintf(idF,ft,lonNWOrig,latSEOrig);
    fprintf(idF,"\n");
    //escribimos los rios
    fprintf(idF,"#DIbujo con rios\n");
    fprintf(idF,"%s=%s\n",NVARRIOS,RIOS);
    fprintf(idF,"#DIbujo sin rios\n");
    fprintf(idF,"#%s=\n",NVARRIOS);
    fprintf(idF,"\n");
    //escribimos el color de las partes solidas de la Tierra
    fprintf(idF,"#Color de las partes solidas\n");
    fprintf(idF,"%s=%s\n",NVARCTIERR,TIERR);
    fprintf(idF,"\n");
    //escribimos el color de las partes liquidas de la Tierra
    fprintf(idF,"#Color de las partes liquidas\n");
    fprintf(idF,"%s=%s\n",NVARCAGUA,AGUA);
    fprintf(idF,"\n");
    //parámetros de dibujo de las flechas
    fprintf(idF,"#Parametros de dibujo de las flechas\n");
    fprintf(idF,"%s=%s\n",NVARPFLECH,PFLECH);
    fprintf(idF,"\n");
    //escribimos los parametros de dibujo de las elipses de error
    fprintf(idF,"#Parametros de dibujo de las elipses de error\n");
    fprintf(idF,"%s=%s$%s%s\n",NVARPARELI,PARELI1,NVARNCONF,PARELI3);
    fprintf(idF,"\n");
    //escribimos los parametros de dibujo de las barras de error
    fprintf(idF,"#Parametros de dibujo de las barras de error\n");
    fprintf(idF,"%s=%s\n",NVARPBERR,PBERR);
    fprintf(idF,"\n");
    //escribimos el color de las elipses de error
    fprintf(idF,"#Color de las elipses y barras de error\n");
    fprintf(idF,"%s=%s\n",NVARCELIP,CELIP);
    fprintf(idF,"\n");
    //escribimos el factor de multiplicacion para el intervalo de confianza
    //del error en altimetria
    fprintf(idF,"#Factor de multiplicacion para el calculo del intervalo\n");
    fprintf(idF,"#de confianza del error en la deformacion altimetrica\n");
    fprintf(idF,"#Si el nivel de confianza pasado no se encuantra entre los\n");
    fprintf(idF,"#definidos a continuacion se asigna el valor\n");
    fprintf(idF,"#correspondiante a un nivel de confianza estándar 68.27%%\n");
    sprintf(ft,"%%s=%s\n",FORMFMULGMT);
    fprintf(idF,ft,NVARFACMUL,1.0);
    fprintf(idF,"if [ $%s == 0.394 ]; then\n",NVARNCONF);
    fprintf(idF,"    %s=1.0\n",NVARFACMUL);
    fprintf(idF,"elif [ $%s == 0.900 ]; then\n",NVARNCONF);
    fprintf(idF,"    %s=1.28155\n",NVARFACMUL);
    sprintf(ft,"elif [ $%%s == %s ]; then\n",FORMNCONFGMT);
    fprintf(idF,ft,NVARNCONF,NCONF);
    sprintf(ft,"    %%s=%s\n",FORMFMULGMT);
    fprintf(idF,ft,NVARFACMUL,FACMUL);
    fprintf(idF,"elif [ $%s == 0.975 ]; then\n",NVARNCONF);
    fprintf(idF,"    %s=1.95996\n",NVARFACMUL);
    fprintf(idF,"elif [ $%s == 0.990 ]; then\n",NVARNCONF);
    fprintf(idF,"    %s=2.32635\n",NVARFACMUL);
    fprintf(idF,"elif [ $%s == 0.999 ]; then\n",NVARNCONF);
    fprintf(idF,"    %s=3.09023\n",NVARFACMUL);
    fprintf(idF,"else\n");
    fprintf(idF,"    echo \"No hay definido ningun factor de\"\n");
    fprintf(idF,"    echo \"multiplicacion para el nivel de confianza\"\n");
    fprintf(idF,"    echo \"seleccionado. Se toma el factor para el nivel\"\n");
    fprintf(idF,"    echo \"de confianza estándar (1.0) para la expansion\"\n");
    fprintf(idF,"    echo \"del error en altimetria\"\n");
    fprintf(idF,"fi\n");
    fprintf(idF,"\n");
    //escribimos los parametros generales de GMT
    fprintf(idF,"#Parametros generales de GMT\n");
    fprintf(idF,"%s\n",GMTSET);
    fprintf(idF,"\n");
    fprintf(idF,"########################################");
    fprintf(idF,"########################################\n");
    fprintf(idF,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void DeformacionPlanoGMT(FILE* idF,
                         int nPuntos,
                         char** cod,
                         double* lat,
                         double* lon,
                         double* de,
                         double* dn,
                         double* sde,
                         double* sdn,
                         double* cdedn)
{
    //indice para recorrer bucles
    int i=0;
    //formato de escritura
    char ft[LONFORMGMT+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //dibujamos el mapa de planimetria
    fprintf(idF,"########################################");
    fprintf(idF,"########################################\n");
    fprintf(idF,"#LOS SIGUIENTES COMANDOS DIBUJAN LA DEFORMACION EN ");
    fprintf(idF,"PLANIMETRIA\n");
    fprintf(idF,"\n");
    fprintf(idF,"#Mapa base: limites y lineas de costa\n");
    //dibujamos los limites del mapa
    fprintf(idF,"psbasemap ${%s} ${%s} ${%s} ${%s} ${%s} -K > ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARBARLAT,NVARORIPAG,NVARLOGO,NVARFSALP);
    //dibujamos las lineas de costa
    fprintf(idF,"pscoast ${%s} ${%s} ${%s} ${%s} ${%s} ${%s} ${%s} -K -O ",
            NVARPROYEC,NVARLIMIT,NVARRESCOS,NVAREGRAF,NVARRIOS,NVARCTIERR,
            NVARCAGUA);
    fprintf(idF,">> ${%s}\n",NVARFSALP);
    //dibujamos las flechas de deformacion y las elipses de error
    fprintf(idF,"#Desplazamientos y elipses de error\n");
    //creamos la cadena de formato de escritura de los datos a dibujar
    sprintf(ft,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMLONGMT,SEPARADGMT,FORMLATGMT,SEPARADGMT,
            FORMDEGMT,SEPARADGMT,FORMDNGMT,SEPARADGMT,
            FORMSEGMT,SEPARADGMT,FORMSNGMT,SEPARADGMT,FORMCORRGMT,SEPARADGMT,
            FORMCODGMT);
    //dibujo sin codigo de estacion
    fprintf(idF,"if [ $%s -ne %d ]; then\n",NVARDIBCOD,DIBCOD);
    //escribimos los datos a dibujar
    fprintf(idF,"psvelo << END ${%s} ${%s} ${%s} ${%s} ${%s} -K -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPARELI,NVARCELIP,NVARPFLECH,NVARFSALP);
    for(i=0;i<nPuntos;i++)
    {
        //escribimos los datos
        fprintf(idF,ft,
                lon[i],lat[i],
                de[i],dn[i],
                sde[i],sdn[i],cdedn[i],
                "");
    }
    fprintf(idF,"END\n");
    //dibujo con codigo de estacion
    fprintf(idF,"else\n");
    //escribimos los datos a dibujar
    fprintf(idF,"psvelo << END ${%s} ${%s} ${%s} ${%s} ${%s} -K -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPARELI,NVARCELIP,NVARPFLECH,NVARFSALP);
    for(i=0;i<nPuntos;i++)
    {
        //escribimos los datos
        fprintf(idF,ft,
                lon[i],lat[i],
                de[i],dn[i],
                sde[i],sdn[i],cdedn[i],
                cod[i]);
    }
    fprintf(idF,"END\n");
    fprintf(idF,"fi\n");
    //dibujamos la escala de referencia para las deformaciones
    fprintf(idF,"#Escala de referencia para las deformaciones\n");
    //escribimos la escala de referencia para las deformaciones
    fprintf(idF,"psvelo << END ${%s} ${%s} ${%s} ${%s} ${%s} -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPARELI,NVARCELIP,NVARPFLECH,NVARFSALP);
    fprintf(idF,"${%s}\n",NVAREDEFH);
    fprintf(idF,"END\n");
    fprintf(idF,"\n");
    fprintf(idF,"########################################");
    fprintf(idF,"########################################\n");
    fprintf(idF,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void DeformacionAlturaGMT(FILE* idF,
                          int nPuntos,
                          char** cod,
                          double* lat,
                          double* lon,
                          double* du,
                          double* sdu)
{
    //indice para recorrer bucles
    int i=0;
    //formato de escritura
    char ft[LONFORMGMT+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //dibujamos el mapa de altimetria
    fprintf(idF,"########################################");
    fprintf(idF,"########################################\n");
    fprintf(idF,"#LOS SIGUIENTES COMANDOS DIBUJAN LA DEFORMACION EN ");
    fprintf(idF,"ALTIMETRIA\n");
    fprintf(idF,"\n");
    fprintf(idF,"#Mapa base: limites y lineas de costa\n");
    //dibujamos los limites del mapa
    fprintf(idF,"psbasemap ${%s} ${%s} ${%s} ${%s} ${%s} -K > ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARBARLAT,NVARORIPAG,NVARLOGO,NVARFSALH);
    //dibujamos las lineas de costa
    fprintf(idF,"pscoast ${%s} ${%s} ${%s} ${%s} ${%s} ${%s} ${%s} -K -O ",
            NVARPROYEC,NVARLIMIT,NVARRESCOS,NVAREGRAF,NVARRIOS,NVARCTIERR,
            NVARCAGUA);
    fprintf(idF,">> ${%s}\n",NVARFSALH);
    //dibujamos las flechas de deformacion
    fprintf(idF,"#Desplazamientos\n");
    //cremos la cadena de formato de escritura de los datos a dibujar
    sprintf(ft,"%s%s%s%s0.0%s%s%s0.0%s0.0%s0.0%s%s\n",
            FORMLONGMT,SEPARADGMT,FORMLATGMT,SEPARADGMT,
            SEPARADGMT,
            FORMDUGMT,SEPARADGMT,
            SEPARADGMT,SEPARADGMT,SEPARADGMT,
            FORMCODGMT);
    //dibujo sin codigo de estacion
    fprintf(idF,"if [ $%s -ne %d ]; then\n",NVARDIBCOD,DIBCOD);
    //escribimos los desplazamientos
    fprintf(idF,"psvelo << END ${%s} ${%s} ${%s} ${%s} ${%s} -K -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPARELI,NVARCELIP,NVARPFLECH,NVARFSALH);
    for(i=0;i<nPuntos;i++)
    {
        fprintf(idF,ft,
                lon[i],lat[i],
                du[i],
                "");
    }
    fprintf(idF,"END\n");
    //dibujo con codigo de estacion
    fprintf(idF,"else\n");
    //escribimos los desplazamientos
    fprintf(idF,"psvelo << END ${%s} ${%s} ${%s} ${%s} ${%s} -K -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPARELI,NVARCELIP,NVARPFLECH,NVARFSALH);
    for(i=0;i<nPuntos;i++)
    {
        fprintf(idF,ft,
                lon[i],lat[i],
                du[i],
                cod[i]);
    }
    fprintf(idF,"END\n");
    fprintf(idF,"fi\n");
    //dibujamos las barras de error
    fprintf(idF,"#Barras de error\n");
    fprintf(idF,"awk '{print $1,$2,$3,$4,$5,\"'\"$facmul\"'\"*$6,$7 }' << "
                "END | \\\n");
    fprintf(idF,"psvelo ${%s} ${%s} ${%s} ${%s} -K -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPBERR,NVARCELIP,NVARFSALH);
    //cremos la cadena de formato de escritura de los datos a dibujar
    sprintf(ft,"%s%s%s%s0.0 0.0 1.0e-7 %s 0.0\n",
            FORMLONGMT,SEPARADGMT,FORMLATGMT,SEPARADGMT,
            FORMSUGMT);
    //escribimos las barras de error
    for(i=0;i<nPuntos;i++)
    {
        fprintf(idF,ft,
                lon[i],lat[i],
                sdu[i]);
    }
    fprintf(idF,"END\n");
    //dibujamos la escala de referencia para las deformaciones
    fprintf(idF,"#Escala de referencia para las deformaciones\n");
    //escribimos la escala de referencia para las deformaciones
    fprintf(idF,"psvelo << END ${%s} ${%s} ${%s} ${%s} ${%s} -O >> ${%s}\n",
            NVARPROYEC,NVARLIMIT,NVARPARELI,NVARCELIP,NVARPFLECH,NVARFSALH);
    fprintf(idF,"${%s}\n",NVAREDEFV);
    fprintf(idF,"END\n");
    fprintf(idF,"\n");
    fprintf(idF,"########################################");
    fprintf(idF,"########################################\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int DibujaDeformacionGMT(FILE* idFichero,
                         char fichero[],
                         int nPuntos,
                         char** cod,
                         double* lat,
                         double* lon,
                         double* de,
                         double* dn,
                         double* du,
                         double* sde,
                         double* sdn,
                         double* cdedn,
                         double* sdu)
{
    //indice para recorrer bucles
    int i=0;
    //coordenadas geodesicas de los puntos extremos
    double latNWOrig=0.0,lonNWOrig=0.0,latSEOrig=0.0,lonSEOrig=0.0;
    double latNW=0.0,lonNW=0.0,latSE=0.0,lonSE=0.0;
    //incrementos en las coordenadas geodesicas
    double dLat=0.0,dLon=0.0,dMax=0.0,dMedio=0.0;
    //variable para almacenar el paso en coordenadas para las barras laterales
    //del dibujo
    double pasoCoor=0.0;
    //deformaciones en valor absoluto
    double* absDef=NULL;
    //variables para almacenar posiciones de valores extremos
    int posMaxLat=0,posMinLat=0,posMaxLon=0,posMinLon=0;
    //tamanyo de las escalas
    double tamEscala=0.0,tamEscDefH=0.0,tamEscDefV=0.0;
    //nombre de los dibujos de salida
    char psp[strlen(fichero)+strlen(INDP)+strlen(EXTPS)+2];
    char psh[strlen(fichero)+strlen(INDH)+strlen(EXTPS)+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos el nombre del fichero de planimetria
    strcpy(psp,fichero);
    strcat(psp,INDP);
    strcat(psp,EXTPS);
    //creamos el nombre del fichero de altimetria
    strcpy(psh,fichero);
    strcat(psh,INDH);
    strcat(psh,EXTPS);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos los valores extremos de la latitud y la longitud
    MaxMin(lat,nPuntos,&posMaxLat,&posMinLat);
    MaxMin(lon,nPuntos,&posMaxLon,&posMinLon);
    //asignamos los valores a los puntos extremos
    latNWOrig = lat[posMaxLat];
    lonNWOrig = lon[posMinLon];
    latSEOrig = lat[posMinLat];
    lonSEOrig = lon[posMaxLon];
    //calculamos los incrementos en latitud y longitud
    dLat = latNWOrig-latSEOrig;
    dLon = lonSEOrig-lonNWOrig;
    //ampliamos los limites en coordenadas dependiendo del numero de puntos de
    //trabajo
    if(nPuntos!=1)
    {
        //ampliamos los limites de la latitud
        latNW = latNWOrig+dLat*AMPLIALAT;
        latSE = latSEOrig-dLat*AMPLIALAT;
        //ampliamos los limites de la longitud
        lonNW = lonNWOrig-dLon*AMPLIALON;
        lonSE = lonSEOrig+dLon*AMPLIALON;
    }
    else
    {
        //ampliamos los limites de la latitud
        latNW = latNWOrig+AMPLIAC1P;
        latSE = latSEOrig-AMPLIAC1P;
        //ampliamos los limites de la longitud
        lonNW = lonNWOrig-AMPLIAC1P;
        lonSE = lonSEOrig+AMPLIAC1P;
        //asignamos los incrementos de latitudes y longitudes
        dLat = 2.0*AMPLIAC1P;
        dLon = 2.0*AMPLIAC1P;
    }
    //verificamos que los limites en latitud esten en dominio: ]-90 90[
    if(latNW>=POLON)
    {
        latNW = POLON-fabs(DIFPOLO);
    }
    if(latNW<=POLOS)
    {
        latNW = POLOS+fabs(DIFPOLO);
    }
    if(latSE>=POLON)
    {
        latSE = POLON-fabs(DIFPOLO);
    }
    if(latSE<=POLOS)
    {
        latSE = POLOS+fabs(DIFPOLO);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escogemos el mayor incremento de coordenadas
    dMax = (dLat>=dLon) ? dLat : dLon;
    //calculamos el paso en coordenadas para las barras laterales
    pasoCoor = dMax/NPASOSCOOR;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos el incremento medio de coordenadas
    dMedio = (dLat+dLon)/2.0;
    //calculamos una primera aproximacion al tamanyo de la escala como una
    //fraccion del incremento medio de coordenadas
    tamEscala = dMedio/FRAESCGRAF;
    //pasamos el tamanyo a unidades de distancia para una esfera de radio el
    //semieje mayor del elipsoide
    tamEscala *= DR*SEMI_MAYOR;
    //redondeamos las unidades dependiendo del tamanyo y las pasamos a km
    if(tamEscala<=100.0)
    {
        //el tamanyo minimo de la escala grafica sera de 100 metros
        tamEscala = 0.1;
    }
    else if(tamEscala<=1000.0)
    {
        //si la escala grafica va a tener menos de 1 km redondeamos a la centena
        //de metros
        tamEscala /= 100.0;
        tamEscala = floor(tamEscala);
        tamEscala /= 10.0;
    }
    else if(tamEscala<=10000.0)
    {
        //si la escala grafica va a tener menos de 10 km redondeamos al km
        tamEscala /= 1000.0;
        tamEscala = floor(tamEscala);
    }
    else if(tamEscala<=100000.0)
    {
        //si la escala grafica va a tener menos de 100 km redondeamos cada 10 km
        tamEscala /= 10000.0;
        tamEscala = floor(tamEscala);
        tamEscala *= 10.0;
    }
    else
    {
        //para escalas mayores de 100 km redondeamos cada 100 km
        tamEscala /= 100000.0;
        tamEscala = floor(tamEscala);
        tamEscala *= 100.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para el vector de deformaciones
    absDef = (double*)malloc((size_t)nPuntos*sizeof(double));
    //comprobamos si ha ocurrido algun error en la asignacion de memoria
    if(absDef==NULL)
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    //copiamos las deformaciones horizontales en centimetros
    for(i=0;i<nPuntos;i++)
    {
        absDef[i] = sqrt(de[i]*de[i]+dn[i]*dn[i])*100.0;
    }
    //calculamos la mediana
    tamEscDefH = floor(Mediana(absDef,(size_t)nPuntos));
    //copiamos el valor absoluto de las deformaciones verticales en centimetros
    for(i=0;i<nPuntos;i++)
    {
        absDef[i] = fabs(du[i])*100.0;
    }
    //calculamos la mediana
    tamEscDefV = floor(Mediana(absDef,(size_t)nPuntos));
    //si alguna de las medianas es 0, asignamos 1
    if(tamEscDefH==0.0)
    {
        tamEscDefH = 1.0;
    }
    if(tamEscDefV==0.0)
    {
        tamEscDefV = 1.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos la cabecera del fichero
    CabeceraGMT(idFichero,
                psp,psh,
                latNW,lonNW,latSE,lonSE,
                latNWOrig,lonNWOrig,latSEOrig,lonSEOrig,
                pasoCoor,tamEscala,tamEscDefH,tamEscDefV);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos la deformacion en planimetria
    DeformacionPlanoGMT(idFichero,
                        nPuntos,
                        cod,
                        lat,lon,
                        de,dn,
                        sde,sdn,cdedn);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos la deformacion en altimetria
    DeformacionAlturaGMT(idFichero,
                         nPuntos,
                         cod,
                         lat,lon,
                         du,
                         sdu);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria utilizada
    free(absDef);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
