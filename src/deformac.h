/**
\defgroup deformacion Modulo DEFORMACION
\brief En este modulo se reunen los ficheros necesarios para realizar el calculo
       de la deformacion en un punto a lo largo del tiempo.

Los ficheros descritos a continuacion son el material imprescindible para
realizar el calculo de la deformacion en un punto a lo largo del tiempo.
@{
\file deformac.h
\brief Declaracion de la estructura y funciones para el calculo de deformaciones
       entre puntos.

En este fichero se declaran la estructura y todas las funciones necesarias para
el calculo de deformaciones en puntos singulares a partir de sus coordenadas
cartesianas tridimensionales geocentricas.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 24 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _DEFORMAC_H_
#define _DEFORMAC_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include"ptos.h"
#include"tran.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** \def SITSR
    \brief Indicador de que se ha realizado transformacion entre sistemas de
           referencia. */
#define SITSR 1
/** \def NOTSR
    \brief Indicador de que no se ha realizado transformacion entre sistemas de
           referencia. */
#define NOTSR 0
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\struct deformac
\brief Estructura contenedora de los datos de deformacion de una serie de
       puntos.

En esta estructura se almacenan todos los elementos referentes a la deformacion
de una serie de puntos singulares evaluada en el plano del horizonte local de
cada uno de ellos, asi como los parametros de error correspondientes (matriz de
varianza-covarianza y elipses de error).
*/
typedef struct
{
    /** \brief Numero de puntos en los que se ha calculado deformacion que
               contiene la estructura. */
    int nPuntos;
    /** \brief Codigos de los puntos en los que se ha calculado deformacion que
               hay en la estructura. */
    /**
    Array de cadenas de texto para almacenar los codigos de los puntos en los
    que se ha calculado deformacion que hay en la estructura. La longitud
    asignada a cada cadena del array es igual a #LONMAXLINPTOS+1. Este array
    contiene tantas cadenas de texto como elementos tenga deformac::nPuntos. La
    numeracion de elementos comienza en 0.
    */
    char** puntos;
    /** \brief Codigo del sistema de referencia en el que se enmarca cada punto
               de la estructura en el que se ha calculado deformacion antes de
               la deformacion. */
    /**
    Array de cadenas de texto para almacenar los codigos del sistema de
    referencia de cada punto en el que se ha calculado deformacion que hay en
    la estructura antes de la deformacion. La longitud asignada a cada cadena
    del array es igual a #LONMAXLINTRAN+1. Este array contiene tantas cadenas de
    texto como elementos tenga deformac::nPuntos. Cada fila corresponde al punto
    almacenado en el mismo indice del array deformac::puntos. La numeracion de
    elementos comienza en 0.
    */
    char** sistAntes;
    /** \brief Codigo del sistema de referencia en el que se enmarca cada punto
               de la estructura en el que se ha calculado deformacion despues de
               la deformacion. */
    /**
    Array de cadenas de texto para almacenar los codigos del sistema de
    referencia de cada punto en el que se ha calculado deformacion que hay en la
    estructura despues de la deformacion. La longitud asignada a cada cadena del
    array es igual a #LONMAXLINTRAN+1. Este array contiene tantas cadenas de
    texto como elementos tenga deformac::nPuntos. Cada fila corresponde al punto
    almacenado en el mismo indice del array deformac::puntos. La numeracion de
    elementos comienza en 0.
    */
    char** sistDespues;
    /** \brief Indices en una lista de los puntos implicados en cada
               deformacion. */
    /**
    Matriz de dos columnas que almacena los indices en las listas originales en
    que se encuantran los puntos implicados en cada deformacion. Esta matriz
    contiene tantas filas como elementos tenga deformac::nPuntos. Cada fila
    corresponde al punto almacenado en el mismo indice del array
    deformac::puntos. La numeracion de elementos comienza en 0. Las columnas
    corresponden a:
    - Col. 0: Indice del punto implicado antes de la deformacion.
    - Col. 1: indice del punto implicado despues de la deformacion.
    */
    int** indices;
    /** \brief Epocas base de origen y final para cada punto, en formato
               anyo.decimal_de_anyo. */
    /**
    Matriz de dos columnas que almacena la epoca base en de origen y final para
    cada punto en el que se ha calculado deformacion. Esta matriz contiene
    tantas filas como elementos tenga deformac::nPuntos. Cada fila corresponde
    al punto almacenado en el mismo indice del array deformac::puntos. La
    numeracion de elementos comienza en 0. Las columnas corresponden a:
    - Col. 0: Epoca del punto antes de la deformacion.
    - Col. 1: Epoca del punto despues de la deformacion.
    */
    double** epocas;
    /** \brief Coordenadas de los puntos. */
    /**
    Matriz de tres columnas que almacena las coordenadas de los puntos en la
    epoca anterior a la deformacion. Esta matriz contiene tantas filas como
    elementos tenga deformac::nPuntos. Cada fila corresponde al punto almacenado
    en el mismo indice del array deformac::puntos. La numeracion de elementos
    comienza en 0. Las columnas corresponden a:
    - Col. 0: Coordenada X del punto.
    - Col. 1: Coordenada Y del punto.
    - Col. 2: Coordenada Z del punto.
    */
    double** pos;
    /** \brief Deformacion de los puntos. */
    /**
    Matriz de tres columnas que almacena los valores de deformacion de los
    puntos en el plano horizonte local de cada uno (en el sistema de coordenadas
    cartesiano topocentrico: east, north, up). Esta matriz contiene tantas filas
    como elementos tenga deformac::nPuntos. Cada fila corresponde al punto
    almacenado en el mismo indice del array deformac::puntos. La numeracion de
    elementos comienza en 0. Las columnas corresponden a:
    - Col. 0: Deformacion a lo largo del eje east.
    - Col. 1: Deformacion a lo largo del eje horth.
    - Col. 2: Deformacion a lo largo del eje up.

    \note Las deformaciones en las tres coordenadas resultan de la resta:
          (coordenada despues de la deformacion) - (coordenada antes de la
          deformacion)
    \note En el caso en que las coordenadas antes y despues de la deformacion
          esten referidas a dos sistemas de referencia distintos el calculo de
          la deformacion se hara en el sistema de referencia correspondiente al
          punto antes de la deformacion.
    */
    double** def;
    /** \brief Matriz de error asociada a las deformaciones de los puntos. */
    /**
    Matriz de seis columnas que almacena las varianzas y covarianzas asociadas a
    las deformaciones de los puntos de la estructura. Esta matriz contiene
    tantas filas como elementos tenga deformac::nPuntos. Cada fila corresponde
    al punto almacenado en el mismo indice del array deformac::puntos. La
    numeracion de elementos comienza en 0. Las columnas corresponden a:
    - Col. 0: Varianza de la componente east de la deformacion.
    - Col. 1: Covarianza entre la componentes east y north de la deformacion.
    - Col. 2: Covarianza entre la componentes east y up de la deformacion.
    - Col. 3: Varianza de la componente north de la deformacion.
    - Col. 4: Covarianza entre la componentes north y up de la deformacion.
    - Col. 5: Varianza de la componente up de la deformacion.
    */
    double** vcdef;
    /** \brief Elipse de error estandar planimetrica de la deformacion. */
    /**
    Matriz de tres columnas que almacena la elipse de error estandar
    planimetrica (en el plano horizonte local y en el sistema de coordenadas
    cartesianas topocentricas) asociada a la deformacion de cada punto. Esta
    matriz contiene tantas filas como elementos tenga deformac::nPuntos. Cada
    fila corresponde al punto almacenado en el mismo indice del array
    deformac::puntos. La numeracion de elementos comienza en 0. Las columnas
    corresponden a:
    - Col. 0: Semieje mayor de la elipse.
    - Col. 1: Semieje menor de la elipse.
    - Col. 2: Acimut del semieje mayor de la elipse, en radianes.
    */
    double** elipse;
    /** \brief Indicador de transformacion entre sistemas de referencia. */
    /**
    Vector que almacena un indicador de si se ha hecho o no una transformacion
    entre sistemas de referencia al calcular cada deformacion. Este vector
    contiene tantos elementos como valor tenga deformac::nPuntos. Cada elemento
    corresponde al punto almacenado en el mismo indice del array
    deformac::puntos. La numeracion de elementos comienza en 0. Cada elemento
    del vector puede tener dos posibles valores:
    - #NOTSR: No se ha realizado transformacion alguna.
    - #SITSR: Si se ha realizado transformacion entre los sistemas de referencia
              de los puntos implicados en la deformacion.
    */
    int* tsr;
}deformac;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Inicializa una estructura deformac.

Pone el campo deformac::nPuntos de la estructura a 0 e inicializa a NULL todos
los elementos de la estructura que sean punteros.
\param[in] defor Puntero a una estructura deformac.
\note Es necesario inicializar una estructura deformac antes de su uso porque
      muchas funciones comprueban el estado del campo deformac::nPuntos para su
      correcto funcionamiento y si la estructura no se ha inicializado podrian
      producirse errores.
*/
void InicializaDeformac(deformac* defor);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Asigna memoria para un numero de elementos dado en una estructura
       deformac, anyadiendolos a los que ya existan previamente.
\param[in] defor Puntero a una estructura deformac.
\param[in] elementos Numero de elementos para los cuales se asignara memoria.
\return Codigo de error.
\note La estructura deformac pasada ha de estar inicializada.
\note Esta funcion actualiza el campo deformac::nPuntos de la estructura.
*/
int AsignaMemoriaDeformac(deformac* defor,
                          int elementos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Libera la memoria ocupada por una estructura deformac.
\param[in] defor Puntero a una estructura deformac.
\note Ademas de liberar la memoria ocupada por los elementos que son punteros de
      la estructura, esta funcion pone a cero el campo deformac::nPuntos.
*/
void LiberaMemoriaDeformac(deformac* defor);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Realiza un cambio de sistema de referencia de un punto almacenado en una
       estructura posptos.
\param[in] antes Estructura posptos que contiene la posicion de una serie de
                 puntos antes de producirse la deformacion.
\param[in] despues Estructura posptos que contiene la posicion de una serie de
                   puntos despues de producirse la deformacion.
\param[in] transf Estructura partran que contiene parametros de transformacion
                  entre sistemas de referencia.
\param[in] posA Posicion del punto de trabajo en la estructura posptos antes de
                producirse la deformacion.
\param[in] posD Posicion del punto de trabajo en la estructura posptos despues
                de producirse la deformacion.
\param[out] hecho Indicador de si se ha realizado transformacion o no. Dos
                  posibles valores:
                  - #SITSR: Si se ha realizado transformacion.
                  - #NOTSR: No se ha realizado transformacion.
\param[out] x Coordenada X del punto despues de la deformacion en el sistema
              antes de la deformacion, en la epoca antes de la deformacion.
\param[out] y Coordenada Y del punto despues de la deformacion en el sistema
              antes de la deformacion, en la epoca antes de la deformacion.
\param[out] z Coordenada Z del punto despues de la deformacion en el sistema
              antes de la deformacion, en la epoca antes de la deformacion.
\param[out] varx Varianza de la coordenada X en el sistema antes de la
                 deformacion, en la epoca antes de la deformacion.
\param[out] varxy Covarianza entre las coordenadas X e Y en el sistema antes de
                  la deformacion, en la epoca antes de la deformacion.
\param[out] varxz Covarianza entre las coordenadas X y Z en el sistema antes de
                  la deformacion, en la epoca antes de la deformacion.
\param[out] vary Varianza de la coordenada Y en el sistema antes de la
                 deformacion, en la epoca antes de la deformacion.
\param[out] varyz Covarianza entre las coordenadas Y y Z en el sistema antes de
                  la deformacion, en la epoca antes de la deformacion.
\param[out] varz Varianza de la coordenada Z en el sistema antes de la
                 deformacion, en la epoca antes de la deformacion.
\note En el caso en que no se pueda hacer la transformacion pertinente, las
      coordenadas devueltas son las mismas que las originales antes de la
      transformacion.
*/
void CambiaSistRef(posptos* antes,
                   posptos* despues,
                   partran* transf,
                   int posA,
                   int posD,
                   int* hecho,
                   double* x,
                   double* y,
                   double* z,
                   double* varx,
                   double* varxy,
                   double* varxz,
                   double* vary,
                   double* varyz,
                   double* varz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la deformacion existente en una serie de puntos singulares
       sin tener en cuenta la posible transformacion entre los sistemas de
       referencia de cada punto antes y despues de la deformacion.
\param[in] antes Estructura posptos que contiene la posicion de una serie de
                 puntos antes de producirse la deformacion.
\param[in] despues Estructura posptos que contiene la posicion de una serie de
                   puntos despues de producirse la deformacion.
\param[out] defor Puntero a una estructura deformac para almacenar resultados.
\return Codigo de error.
\note La estructura deformac pasada ha de estar inicializada.
\note No es necesario que los puntos almacenados en las estructuras antes y
      despues sean los mismos ni que esten ordenados para que coincidan las
      posiciones en las matrices entre los puntos comunes. Las deformaciones se
      calculan del siguiente modo:
      Se coge el primer punto de la estructura despues y se van recorriendo los
      puntos de la estructura antes. Cada vez que haya coincidencia en los
      codigos se calcula la deformacion y se anyade a la estructura de salida.
      Si en la estructura antes hay mas de una posicion para el mismo punto se
      calcula la deformacion para todas ellas. Se repite la misma operacion para
      los restantes puntos de la estructura despues. En los puntos que no sean
      comunes a las estructuras antes y despues no se calcula deformacion y, por
      consiguiente, no se almacena informacion alguna referente a ellos en la
      estructura de salida.
\note Aunque esta funcion no tenga en cuenta la posible necesidad de
      transformacion entre los sistemas de referencia de cada punto implicado en
      el calculo de la deformacion, a los indices del vector deformac::tsr se
      les asigna el valor #SITSR.
*/
int CalcDeforDeformac(posptos* antes,
                      posptos* despues,
                      deformac* defor);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la deformacion existente en una serie de puntos singulares
       teniendo en cuenta la posible transformacion entre los sistemas de
       referencia de cada punto antes y despues de la deformacion.
\param[in] antes Estructura posptos que contiene la posicion de una serie de
                 puntos antes de producirse la deformacion.
\param[in] despues Estructura posptos que contiene la posicion de una serie de
                 puntos despues de producirse la deformacion.
\param[in] transf Estructura partran que contiene parametros de transformacion
                  entre sistemas de referencia.
\param[out] defor Puntero a una estructura deformac para almacenar resultados.
\return Codigo de error.
\note La estructura deformac pasada ha de estar inicializada.
\note No es necesario que los puntos almacenados en las estructuras antes y
      despues sean los mismos ni que esten ordenados para que coincidan las
      posiciones en las matrices entre los puntos comunes. Las deformaciones se
      calculan del siguiente modo:
      Se coge el primer punto de la estructura despues y se van recorriendo los
      puntos de la estructura antes. Cada vez que haya coincidencia en los
      codigos se calcula la deformacion y se anyade a la estructura de salida.
      Si en la estructura antes hay mas de una posicion para el mismo punto se
      calcula la deformacion para todas ellas. Se repite la misma operacion para
      los restantes puntos de la estructura despues. En los puntos que no sean
      comunes a las estructuras antes y despues no se calcula deformacion y, por
      consiguiente, no se almacena informacion alguna referente a ellos en la
      estructura de salida.
*/
int CalcDeforTransfDeformac(posptos* antes,
                            posptos* despues,
                            partran* transf,
                            deformac* defor);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
