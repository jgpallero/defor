/**
\addtogroup transf
\ingroup deformacion
@{
\file paramtran.h
\brief Archivo de definicion de constantes para el trabajo con ficheros de
       parametros de transformacion entre sistemas de referencia.

En este fichero se definen las constantes simbolicas necesarias para identificar
los distintos bloques de datos que componen un fichero de parametros de
transformacion entre sistemas de referencia.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com,jlgpallero@pdi.ucm.es
\date 11 de julio de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF).
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves de
las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _PARAMTRAN_H_
#define _PARAMTRAN_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//parametros generales aplicables a todo el fichero SINEX
/** \def LONMAXLINTRAN
    \brief Longitud maxima de las lineas de un fichero de parametros de
           transformacion entre sistemas de referencia. */
#define LONMAXLINTRAN  80
/** \def CADINIBLOQTRAN
    \brief Cadena identificadora de comienzo de bloque de un fichero de
           parametros de transformacion entre sistemas de referencia. */
#define CADINIBLOQTRAN "+"
/** \def CADFINBLOQTRAN
    \brief Cadena identificadora de final de bloque de un fichero de
           parametros de transformacion entre sistemas de referencia. */
#define CADFINBLOQTRAN "-"
/** \def SEPCADINIBLOQETIQBLOQTRAN
    \brief Cadena de separacion entre la cadena identificadora de inicio de
           bloque y la cadena identificadora del bloque. */
#define SEPCADINIBLOQETIQBLOQTRAN ""
/** \def SEPCADFINBLOQETIQBLOQTRAN
    \brief Cadena de separacion entre la cadena identificadora de fin de bloque
           y la cadena identificadora del bloque. */
#define SEPCADFINBLOQETIQBLOQTRAN ""
/** \def SEPELEMETIQBLOQTRAN
    \brief Cadena de separacion entre la cadena identificadora de bloque y otros
           elementos informativos del bloque, si existen */
#define SEPELEMETIQBLOQTRAN " "
/** \def CADIDTRANTRAN
    \brief Cadena identificadora de inicio de parametros de transformacion en un
           bloque de un fichero de parametros de transformacion entre sistemas
           de referencia. */
#define CADIDTRANTRAN ">"
/** \def POSINIIDTRANTRAN
    \brief Posicion en una linea en la que comienza el indicador de inicio de
           parametros de transformacion en un bloque de un fichero de parametros
           de transformacion entre sistemas de referencia (las posiciones
           comienzan en 0). */
#define POSINIIDTRANTRAN  0
/** \def SEPCADIDTRANINITRANTRAN
    \brief Cadena de separacion entre la cadena identificadora de inicio de
           parametros de transformacion y el comienzo de la linea de definicion
           de parametros de transformacion. */
#define SEPCADIDTRANINITRANTRAN ""
/** \def SEPCAMPOSPARTRAN
    \brief Cadena de separacion entre los campos de las lineas de definicion de
           parametros de transformacion. */
#define SEPCAMPOSPARTRAN " "
/** \def CADCOMENTTRAN
    \brief Cadena identificadora de linea de comentario de un fichero de
           parametros de transformacion entre sistemas de referencia. */
#define CADCOMENTTRAN  "*"
/** \def POSINICOMENTTRAN
    \brief Posicion en una linea en la que comienza el indicador de comentario
           (las posiciones comienzan en 0). */
#define POSINICOMENTTRAN  0
/** \def IDNOBLOQUETRAN
    \brief Identificador de la posicion de un bloque cuando este no existe en el
           fichero de parametros de transformacion entre sistemas de
           referencia. */
#define IDNOBLOQUETRAN -1
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
