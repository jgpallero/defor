/**
\addtogroup sinex
@{
\file bloqsnx.c
\brief Definicion de funciones asociadas a la creacion de estructuras bloqsnx.

En este fichero se definen todas las funciones necesarias para la creacion de
estructuras bloqsnx y, por consiguiente, para la extraccion del nombre de los
bloques de datos existentes en un fichero SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"errores.h"
#include"general.h"
#include"paramsnx.h"
#include"bloqsnx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void InicializaBloqSnx(bloqsnx* bloques)
{
    //inicializamos el contador de bloques a 0
    bloques->nBloques = 0;
    //inicializamos los miembros que son punteros a NULL
    bloques->bloques = NULL;
    bloques->posicion = NULL;
    bloques->lineas = NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int AsignaMemoriaBloqSnx(bloqsnx* bloques,
                         int elementos)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de elementos es correcto
    if(elementos<=0)
    {
        //escribimos el mensaje de error
        MensajeErrorNELEMMENORCERO((char*)__func__);
        //salimos de la funcion
        return ERRNELEMMENORCERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aumentamos el contador de bloques
    bloques->nBloques += elementos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las filas a las matrices de datos y de etiquetas
    bloques->bloques=(char**)realloc(bloques->bloques,
                                       (size_t)bloques->nBloques*sizeof(char*));
    bloques->posicion=(long int**)realloc(bloques->posicion,
                                   (size_t)bloques->nBloques*sizeof(long int*));
    bloques->lineas=(int**)realloc(bloques->lineas,
                                        (size_t)bloques->nBloques*sizeof(int*));
    //controlamos los posibles errores en la asignacion de memoria
    if((bloques->bloques==NULL)||(bloques->posicion==NULL)||
       (bloques->lineas==NULL))
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las columnas a las matrices de datos y de etiquetas
    for(i=0;i<elementos;i++)
    {
        bloques->bloques[bloques->nBloques-i-1]=(char*)malloc(LONMAXLINSNX+1);
        bloques->posicion[bloques->nBloques-i-1]=
                                          (long int*)malloc(2*sizeof(long int));
        bloques->lineas[bloques->nBloques-i-1]=(int*)malloc(2*sizeof(int));
        //controlamos los posibles errores en la asignacion de memoria
        if((bloques->bloques[bloques->nBloques-i-1]==NULL)||
           (bloques->posicion[bloques->nBloques-i-1]==NULL)||
           (bloques->lineas[bloques->nBloques-i-1]==NULL))
        {
            //escribimos el mensaje de error
            MensajeErrorNOMEMORIA((char*)__func__);
            //salimos de la funcion
            return ERRNOMEMORIA;
        }
        //vamos inicializando a 0 las posiciones de inicio y final de los
        //bloques en el fichero
        bloques->posicion[bloques->nBloques-i-1][0] = 0;
        bloques->posicion[bloques->nBloques-i-1][1] = 0;
        //vamos inicializando a 0 los numeros de linea de inicio y final de los
        //bloques en el fichero
        bloques->lineas[bloques->nBloques-i-1][0] = 0;
        bloques->lineas[bloques->nBloques-i-1][1] = 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LiberaMemoriaBloqSnx(bloqsnx* bloques)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //verificamos si hemos pasado una estructura vacia
    if(!bloques->nBloques)
    {
        //salimos de la funcion
        return;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada a las matrices "posicion" y "lineas" y al
    //array de etiquetas
    for(i=0;i<bloques->nBloques;i++)
    {
        free(bloques->bloques[i]);
        free(bloques->posicion[i]);
        free(bloques->lineas[i]);
    }
    free(bloques->bloques);
    free(bloques->posicion);
    free(bloques->lineas);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el contador de bloques a 0
    bloques->nBloques = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeBloquesBloqSnx(FILE* idFichero,
                      char inicioBloque[],
                      char finBloque[],
                      bloqsnx* bloques)
{
    //codigo de error
    int cError=ERRNOERROR;
    //longitud de cada linea
    int lonLinea=LONMAXLINSNX;
    //linea leida
    char lineaLeida[lonLinea+2];
    //longitud de la etiqueta de comienzo de bloque
    int lonComBloque=(int)strlen(inicioBloque);
    //etiqueta de inicio de bloque
    char cadIniBloque[lonComBloque+1];
    //longitud de la etiqueta de fin de bloque
    int lonFinBloque=(int)strlen(finBloque);
    //etiqueta de fin de bloque
    char cadFinBloque[lonFinBloque+1];
    //longitud de la cadena separadora de la etiqueta de fin de bloque y la
    //etiqueta identificadora del bloque
    int lonSepFinBloque=(int)strlen(SEPCADFINBLOQETIQBLOQ);
    //posicion inicial en el fichero
    long int posOriginal=ftell(idFichero);
    //posicion del comienzo de la linea actual
    long int posLinea=0;
    //contador de lineas
    int nLinea=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //posicion del comienzo de la linea
    posLinea = posOriginal;
    //vamos leyendo linea a linea
    while(fgets(lineaLeida,lonLinea+2,idFichero))
    {
        //aumentamos el contador de lineas
        nLinea++;
        //extraemos el posible indicador de inicio de bloque
        ExtraeSubcadena(lineaLeida,0,lonComBloque,cadIniBloque);
        //extraemos el posible indicador de fin de bloque
        ExtraeSubcadena(lineaLeida,0,lonFinBloque,cadFinBloque);
        //comprobamos si hemos leido un indicador de inicio de bloque
        if(!strcmp(inicioBloque,cadIniBloque))
        {
            //asignamos memoria para el nuevo elemento
            //esta funcion actualiza el campo bloques.nBloques
            cError = AsignaMemoriaBloqSnx(bloques,1);
            //comprobamos el codigo de error devuelto
            if(cError!=ERRNOERROR)
            {
                //lanzamos el mensaje de error
                MensajeErrorPropagado((char*)__func__,
                                      "AsignaMemoriaBloqSnx()");
                //salimos de la funcion
                return cError;
            }
            //almacenamos la posicion del comienzo de bloque
            bloques->posicion[bloques->nBloques-1][0] = posLinea;
            //almacenamos el numero de linea
            bloques->lineas[bloques->nBloques-1][0] = nLinea;
        }
        //comprobamos si hemos leido un final de bloque
        if(!strcmp(finBloque,cadFinBloque))
        {
            //almacenamos la posicion del comienzo de bloque
            bloques->posicion[bloques->nBloques-1][1] = posLinea;
            //almacenamos el numero de linea
            bloques->lineas[bloques->nBloques-1][1] = nLinea;
            //extraemos la etiqueta identificadora del bloque
            ExtraeSubcadenaDelim(lineaLeida,
                                 lonFinBloque+lonSepFinBloque,
                                 SEPELEMETIQBLOQ,
                                 bloques->bloques[bloques->nBloques-1]);
        }
        //calculamos la nueva posicion del comienzo de linea
        posLinea = ftell(idFichero);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el puntero de L/E en su posicion original en el fichero
    fseek(idFichero,posOriginal,SEEK_SET);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int PosicionBloqueBloqSnx(bloqsnx* bloques,
                          char idBloque[])
{
    //variable auxiliar
    int i=0;
    //posicion del bloque
    int posicion=IDNOBLOQUESNX;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos toda la matriz de nombres de bloque
    for(i=0;i<bloques->nBloques;i++)
    {
        //comparamos los nombres extraidos con la cadena pasada
        if(!strcmp(bloques->bloques[i],idBloque))
        {
            //asignamos la posicion encontrada
            posicion = i;
            //salimos del bucle
            break;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return posicion;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
