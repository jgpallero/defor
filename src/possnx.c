/**
\addtogroup sinex
@{
\file possnx.c
\brief Definicion de la estructura y funciones para la extraccion de datos de
       posicion de ficheros SINEX.

En este fichero se definen la estructura y todas las funciones necesarias para
la extraccion de datos de posicion y sus errores asociados de los puntos
contenidos en un fichero SINEX.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>
#include"errores.h"
#include"general.h"
#include"paramsnx.h"
#include"bloqsnx.h"
#include"possnx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void InicializaPosSnx(possnx* posicion)
{
    //inicializamos el contador de puntos a 0
    posicion->nPuntos = 0;
    //inicializamos los miembros que son punteros a NULL
    posicion->puntos = NULL;
    posicion->epocas = NULL;
    posicion->pt = NULL;
    posicion->idsol = NULL;
    posicion->epos = NULL;
    posicion->evel = NULL;
    posicion->pos = NULL;
    posicion->vel = NULL;
    posicion->vcpos = NULL;
    posicion->vcvel = NULL;
    posicion->nincpos = NULL;
    posicion->nincvel = NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int AsignaMemoriaPosSnx(possnx* posicion,
                        int elementos)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de elementos es correcto
    if(elementos<=0)
    {
        //escribimos el mensaje de error
        MensajeErrorNELEMMENORCERO((char*)__func__);
        //salimos de la funcion
        return ERRNELEMMENORCERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aumentamos el contador de puntos
    posicion->nPuntos += elementos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las filas a las matrices de datos y de puntos y
    //epocas
    posicion->puntos=(char**)realloc(posicion->puntos,
                                       (size_t)posicion->nPuntos*sizeof(char*));
    posicion->epocas=(char**)realloc(posicion->epocas,
                                       (size_t)posicion->nPuntos*sizeof(char*));
    posicion->pt=(char**)realloc(posicion->pt,
                                       (size_t)posicion->nPuntos*sizeof(char*));
    posicion->idsol=(char**)realloc(posicion->idsol,
                                       (size_t)posicion->nPuntos*sizeof(char*));
    posicion->epos=(int**)realloc(posicion->epos,
                                        (size_t)posicion->nPuntos*sizeof(int*));
    posicion->evel=(int**)realloc(posicion->evel,
                                        (size_t)posicion->nPuntos*sizeof(int*));
    posicion->pos=(double**)realloc(posicion->pos,
                                     (size_t)posicion->nPuntos*sizeof(double*));
    posicion->vel=(double**)realloc(posicion->vel,
                                     (size_t)posicion->nPuntos*sizeof(double*));
    posicion->vcpos=(double**)realloc(posicion->vcpos,
                                     (size_t)posicion->nPuntos*sizeof(double*));
    posicion->vcvel=(double**)realloc(posicion->vcvel,
                                     (size_t)posicion->nPuntos*sizeof(double*));
    posicion->nincpos=(int**)realloc(posicion->nincpos,
                                        (size_t)posicion->nPuntos*sizeof(int*));
    posicion->nincvel=(int**)realloc(posicion->nincvel,
                                        (size_t)posicion->nPuntos*sizeof(int*));
    //controlamos los posibles errores en la asignacion de memoria
    if((posicion->puntos==NULL)||(posicion->epocas==NULL)||
       (posicion->pt==NULL)||(posicion->idsol==NULL)||(posicion->epos==NULL)||
       (posicion->evel==NULL)||(posicion->pos==NULL)||(posicion->vel==NULL)||
       (posicion->vcpos==NULL)||(posicion->vcvel==NULL)||
       (posicion->nincpos==NULL)||(posicion->nincvel==NULL))
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las columnas a las matrices de datos y de puntos y
    //epocas
    for(i=0;i<elementos;i++)
    {
        posicion->puntos[posicion->nPuntos-i-1]=(char*)malloc(LONMAXLINSNX+1);
        posicion->epocas[posicion->nPuntos-i-1]=(char*)malloc(LONMAXLINSNX+1);
        posicion->pt[posicion->nPuntos-i-1]=(char*)malloc(LONMAXLINSNX+1);
        posicion->idsol[posicion->nPuntos-i-1]=(char*)malloc(LONMAXLINSNX+1);
        posicion->epos[posicion->nPuntos-i-1]=(int*)malloc(3*sizeof(int));
        posicion->evel[posicion->nPuntos-i-1]=(int*)malloc(3*sizeof(int));
        posicion->pos[posicion->nPuntos-i-1]=(double*)malloc(3*sizeof(double));
        posicion->vel[posicion->nPuntos-i-1]=(double*)malloc(3*sizeof(double));
        posicion->vcpos[posicion->nPuntos-i-1]=
                                              (double*)malloc(6*sizeof(double));
        posicion->vcvel[posicion->nPuntos-i-1]=
                                              (double*)malloc(6*sizeof(double));
        posicion->nincpos[posicion->nPuntos-i-1]=(int*)malloc(3*sizeof(int));
        posicion->nincvel[posicion->nPuntos-i-1]=(int*)malloc(3*sizeof(int));
        //controlamos los posibles errores en la asignacion de memoria
        if((posicion->puntos[posicion->nPuntos-i-1]==NULL)||
           (posicion->epocas[posicion->nPuntos-i-1]==NULL)||
           (posicion->pt[posicion->nPuntos-i-1]==NULL)||
           (posicion->idsol[posicion->nPuntos-i-1]==NULL)||
           (posicion->epos[posicion->nPuntos-i-1]==NULL)||
           (posicion->evel[posicion->nPuntos-i-1]==NULL)||
           (posicion->pos[posicion->nPuntos-i-1]==NULL)||
           (posicion->vel[posicion->nPuntos-i-1]==NULL)||
           (posicion->vcpos[posicion->nPuntos-i-1]==NULL)||
           (posicion->vcvel[posicion->nPuntos-i-1]==NULL)||
           (posicion->nincpos[posicion->nPuntos-i-1]==NULL)||
           (posicion->nincvel[posicion->nPuntos-i-1]==NULL))
        {
            //escribimos el mensaje de error
            MensajeErrorNOMEMORIA((char*)__func__);
            //salimos de la funcion
            return ERRNOMEMORIA;
        }
        //vamos inicializando a 0 el indicador de existencia de posicion
        posicion->epos[posicion->nPuntos-i-1][0] = 0;
        posicion->epos[posicion->nPuntos-i-1][1] = 0;
        posicion->epos[posicion->nPuntos-i-1][2] = 0;
        //vamos inicializando a 0 el indicador de existencia de velocidad
        posicion->evel[posicion->nPuntos-i-1][0] = 0;
        posicion->evel[posicion->nPuntos-i-1][1] = 0;
        posicion->evel[posicion->nPuntos-i-1][2] = 0;
        //vamos inicializando a 0 la matriz de coordenadas
        posicion->pos[posicion->nPuntos-i-1][0] = 0.0;
        posicion->pos[posicion->nPuntos-i-1][1] = 0.0;
        posicion->pos[posicion->nPuntos-i-1][2] = 0.0;
        //vamos inicializando a 0 la matriz de velocidades
        posicion->vel[posicion->nPuntos-i-1][0] = 0.0;
        posicion->vel[posicion->nPuntos-i-1][1] = 0.0;
        posicion->vel[posicion->nPuntos-i-1][2] = 0.0;
        //vamos inicializando a 0 la matriz de varianza-covarianza de la
        //posicion
        posicion->vcpos[posicion->nPuntos-i-1][0] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][1] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][2] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][3] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][4] = 0.0;
        posicion->vcpos[posicion->nPuntos-i-1][5] = 0.0;
        //vamos inicializando a 0 la matriz de varianza-covarianza de la
        //velocidad
        posicion->vcvel[posicion->nPuntos-i-1][0] = 0.0;
        posicion->vcvel[posicion->nPuntos-i-1][1] = 0.0;
        posicion->vcvel[posicion->nPuntos-i-1][2] = 0.0;
        posicion->vcvel[posicion->nPuntos-i-1][3] = 0.0;
        posicion->vcvel[posicion->nPuntos-i-1][4] = 0.0;
        posicion->vcvel[posicion->nPuntos-i-1][5] = 0.0;
        //vamos inicializando a 0 la matriz de numero de incognita de las
        //coordenadas
        posicion->nincpos[posicion->nPuntos-i-1][0] = 0;
        posicion->nincpos[posicion->nPuntos-i-1][1] = 0;
        posicion->nincpos[posicion->nPuntos-i-1][2] = 0;
        //vamos inicializando a 0 la matriz de numero de incognita de las
        //velocidades
        posicion->nincvel[posicion->nPuntos-i-1][0] = 0;
        posicion->nincvel[posicion->nPuntos-i-1][1] = 0;
        posicion->nincvel[posicion->nPuntos-i-1][2] = 0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LiberaMemoriaPosSnx(possnx* posicion)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //verificamos si hemos pasado una estructura vacia
    if(!posicion->nPuntos)
    {
        //salimos de la funcion
        return;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada a las matrices y a los array de cadenas de
    //caracteres
    for(i=0;i<posicion->nPuntos;i++)
    {
        free(posicion->puntos[i]);
        free(posicion->epocas[i]);
        free(posicion->pt[i]);
        free(posicion->idsol[i]);
        free(posicion->epos[i]);
        free(posicion->evel[i]);
        free(posicion->pos[i]);
        free(posicion->vel[i]);
        free(posicion->vcpos[i]);
        free(posicion->vcvel[i]);
        free(posicion->nincpos[i]);
        free(posicion->nincvel[i]);
    }
    free(posicion->puntos);
    free(posicion->epocas);
    free(posicion->pt);
    free(posicion->idsol);
    free(posicion->epos);
    free(posicion->evel);
    free(posicion->pos);
    free(posicion->vel);
    free(posicion->vcpos);
    free(posicion->vcvel);
    free(posicion->nincpos);
    free(posicion->nincvel);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el contador de puntos a 0
    posicion->nPuntos = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeParametrosErrorPosSnx(FILE* idFichero,
                             int extraePosicion,
                             int extraeVelocidad,
                             int numeroIncognitas,
                             long int posFichSOLMATES,
                             int lineaIniSOLMATES,
                             int lineaFinSOLMATES,
                             int* infoS02,
                             possnx* posicion)
{
    //codigo de error
    int cError=ERRNOERROR;
    //indice para recorrer bucles
    int i=0;
    //numero de lineas con datos
    int nLineas=0;
    //longitud de cada linea
    int lonLinea=LONMAXLINSNX;
    //linea leida
    char lineaLeida[lonLinea+2];
    //cadena de formato de lectura
    char formato[lonLinea+2];
    //indicador de clase de matriz
    char clMat[lonLinea+2];
    //campos no validos de la linea leida
    char noValido[lonLinea+2];
    //numero de elementos leidos de una linea
    int nElementos=0;
    //variable que almacenan la fila y columna leidas
    int fil=0,col=0;
    //variables que almacenan las varianzas y covarianzas/correlaciones leidas
    double var1=0.0,var2=0.0,var3=0.0;
    //numero de elementos que contiene el vector que almacena la matriz de error
    int nElemVec=0;
    //vector que almacenara la matriz de error
    double* varCov=NULL;
    //posicion en el vector que almacenara la matriz de error
    int posVector=0;
    //posicion en el vector que almacenara la matriz de error de los elementos
    //de una matriz triangular inferior de 3x3
    int posx=0,posy=0,posz=0,posxy=0,posxz=0,posyz=0;
    //posicion inicial en el fichero
    long int posOriginal=ftell(idFichero);
    //estructura bloqsnx
    bloqsnx bloques;
    //estructura statsnx
    statsnx estadisticas;
    //posiciones del bloque de estadisticas
    int posSOLSTAT=0;
    //Inicializo el valor de infoS02 a 0
    *infoS02=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //si no hay que extraer posicion ni velocidad salimos de la funcion
    if((!extraePosicion)&&(!extraeVelocidad))
    {
        //salimos de la funcion
        return ERRNOERROR;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos el numero de elementos del vector que almacena la matriz de
    //error
    nElemVec = numeroIncognitas*(numeroIncognitas+1)/2;
    //asignamos memoria para el vector que almacenara la matriz de error
    //anyadimos un elemento de mas para que en la posicion 0 se almacene el
    //valor 0.0, que sera el indicador de no existencia de dato
    //los elementos que contienen dato valido se indexan a partir de la posicion
    //numero 1
    if((varCov=(double*)malloc((size_t)(nElemVec+1)*sizeof(double)))==NULL)
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    //inicializamos el vector a 0.0
    for(i=0;i<nElemVec+1;i++)
    {
        varCov[i] = 0.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //nos colocamos al comienzo de la linea que contiene la cabecera del bloque
    //SOLUTION/MATRIX_ESTIMATE
    fseek(idFichero,posFichSOLMATES,SEEK_SET);
    //leemos la linea que contiene la cabecera
    fgets(lineaLeida,lonLinea+2,idFichero);
    //creamos la cadena de formato de lectura para los indicadores de clase y
    //tipo de matriz
    sprintf(formato,"%%s%s%%s%s%%s",SEPELEMETIQBLOQ,SEPELEMETIQBLOQ);
    //leemos los indicadores de clase y tipo de matriz
    nElementos = sscanf(lineaLeida,formato,noValido,clMat,posicion->tipoMat);
    //comprobamos si se han leido todos los elementos
    if(nElementos!=3)
    {
        //escribimos el mensaje en la salida de error
        MensajeErrorNELEMLEIDOSFICH((char*)__func__,lineaIniSOLMATES);
        //liberamos la memoria asignada al vector de matriz de errores
        free(varCov);
        //salimos de la funcion
        return ERRNELEMLEIDOSFICH;
    }
    //comprobamos si la clase de matriz es valida
    if(strcmp(clMat,TRISUPSOLMATES)&&strcmp(clMat,TRIINFSOLMATES))
    {
        //lanzamos el mensaje de error
        MensajeErrorCLASMATNOVALIDA((char*)__func__,clMat);
        //liberamos la memoria asignada al vector de matriz de errores
        free(varCov);
        //salimos de la funcion
        return ERRCLASMATNOVALIDA;
    }
    //comprobamos si el tipo de matriz es valido
    if(strcmp(posicion->tipoMat,MATCOVASOLMATES)&&
       strcmp(posicion->tipoMat,MATCORRSOLMATES)&&
       strcmp(posicion->tipoMat,MATINFOSOLMATES))
    {
        //lanzamos el mensaje de error
        MensajeErrorTIPOMATNOVALIDO((char*)__func__,posicion->tipoMat);
        //liberamos la memoria asignada al vector de matriz de errores
        free(varCov);
        //salimos de la funcion
        return ERRTIPOMATNOVALIDO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //actualizamos el valor de clMat a un valor valido para la funcion
    //PosicionVectorTriangular()
    if(!strcmp(clMat,TRISUPSOLMATES))
    {
        strcpy(clMat,TRISUP);
    }
    if(!strcmp(clMat,TRIINFSOLMATES))
    {
        strcpy(clMat,TRIINF);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //numero de lineas con datos en el bloque SOLUTION/MATRIX_ESTIMATE
    nLineas = lineaFinSOLMATES-lineaIniSOLMATES-1;
    //vamos almacenando la matriz de error en el vector
    for(i=lineaIniSOLMATES+1;i<lineaIniSOLMATES+nLineas+1;i++)
    {
        //leemos la siguiente linea del fichero
        fgets(lineaLeida,lonLinea+2,idFichero);
        //trabajamos con ella si no es un comentario
        if(!ExisteSubcadena(lineaLeida,POSINICOMENTSNX,CADCOMENTSNX))
        {
            //creamos la cadena de formato de lectura
            sprintf(formato,"%s%%d%s%%d%s%%lf%s%%lf%s%%lf",SEPCAMPOSSOLMATES,
                                                           SEPCAMPOSSOLMATES,
                                                           SEPCAMPOSSOLMATES,
                                                           SEPCAMPOSSOLMATES,
                                                           SEPCAMPOSSOLMATES);
            //leemos los elementos necesarios
            nElementos = sscanf(lineaLeida,formato,&fil,&col,&var1,&var2,&var3);
            //comprobamos si se han leido todos los elementos
            if(nElementos<3)
            {
                //escribimos el mensaje en la salida de error
                MensajeErrorNELEMLEIDOSFICH((char*)__func__,i);
                //liberamos la memoria asignada al vector de matriz de errores
                free(varCov);
                //salimos de la funcion
                return ERRNELEMLEIDOSFICH;
            }
            //vamos asignando los valores a las posiciones correspondientes en
            //el vector
            if(nElementos>=3)
            {
                //calculamos la posicion en el vector
                posVector = PosicionVectorTriangular(clMat,numeroIncognitas,
                                                     fil,col);
                //asignamos el valor correspondiente
                varCov[posVector] = var1;
                //asignamos otro valor si hay mas datos
                if(nElementos>=4)
                {
                    //calculamos la posicion en el vector
                    posVector = PosicionVectorTriangular(clMat,numeroIncognitas,
                                                         fil,col+1);
                    //asignamos el valor correspondiente
                    varCov[posVector] = var2;
                    //asignamos otro valor si hay mas datos
                    if(nElementos==5)
                    {
                        //calculamos la posicion en el vector
                        posVector = PosicionVectorTriangular(clMat,
                                                             numeroIncognitas,
                                                             fil,col+2);
                        //asignamos el valor correspondiente
                        varCov[posVector] = var3;
                    }
                }
            }
        }
    }
    //compruebo si lo que he extraido es la matriz normal
    if(!strcmp(posicion->tipoMat,MATINFOSOLMATES))
    {
        //factorización de Cholesky de la matriz normal
        if(gclapack_dpptrf(CblasColMajor,CblasLower,numeroIncognitas,
                           &varCov[1])>0)
        {
            //escribimos el mensaje en la salida de error
            MensajeErrorERRCHOL((char*)__func__);
            //liberamos la memoria asignada al vector de matriz de errores
            free(varCov);
            //salimos de la funcion
            return ERRCHOL;
        }
        //inversion de la matriz a partir del factor de Cholesky
        gclapack_dpptri(CblasColMajor,CblasLower,numeroIncognitas,&varCov[1]);
        //nos ponemos al inicio del fichero
        fseek(idFichero,0,SEEK_SET);
        //inicializamos la estructura bloqsnx
        InicializaBloqSnx(&bloques);
        //leemos los bloques del fichero
        cError = LeeBloquesBloqSnx(idFichero,CADINIBLOQSNX,CADFINBLOQSNX,
                                   &bloques);
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPropagado((char*)__func__,"LeeBloquesBloqSnx()");
            //liberamos la memoria asignada
            LiberaMemoriaBloqSnx(&bloques);
            free(varCov);
            //salimos de la funcion
            return cError;
        }
        //buscamos las posiciones de los bloques de trabajo
        posSOLSTAT = PosicionBloqueBloqSnx(&bloques,ETIQUETASOLSTAT);
        //sólo continúo si hay bloque de estadísticas
        if(posSOLSTAT!=IDNOBLOQUESNX)
        {
            //inicializamos la estructura estadisticas
            InicializaStatSnx(&estadisticas);
            //leo las estadísticas
            LeeEstadisticasStatSnx(idFichero,bloques.posicion[posSOLSTAT][0],
                                   bloques.lineas[posSOLSTAT][0],
                                   bloques.lineas[posSOLSTAT][1],
                                   &estadisticas);
            //sólo continúo si hay información de la varianza del observable de
            //peso unidad a posteriori
            if(estadisticas.evarfact)
            {
                //multiplico todos los valores de la matriz cofactor por el
                // valor de la varianza del observable de peso unidad a
                //posteriori
                for(i=1;i<(nElemVec+1);i++)
                {
                    varCov[i] *= estadisticas.varfact;
                }
            }
            else
            {
                //Indco que he considerado s02=1
                *infoS02 = 1;
            }
        }
        else
        {
            //Indco que he considerado s02=1
            *infoS02 = 1;
        }
        //libero la memoria asignada a la estructura de bloques
        LiberaMemoriaBloqSnx(&bloques);
        //como ya he convertido a matriz de varianzas-covarianzas, indico este
        //tipo en la estructura
        strcpy(posicion->tipoMat,MATCOVASOLMATES);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //vamos recorriendo todos los puntos de trabajo
    for(i=0;i<posicion->nPuntos;i++)
    {
        //extraemos los elementos de error para las posiciones
        if(extraePosicion)
        {
            //extraemos las posiciones en el vector que almacena la matriz de
            //todos los elementos de la submatriz de 3x3 de parametros de error
            PosVectorMatrizTriInf(numeroIncognitas,
                                  posicion->nincpos[i][0],
                                  posicion->nincpos[i][1],
                                  posicion->nincpos[i][2],
                                  &posx,&posy,&posz,
                                  &posxy,&posxz,&posyz);
            //asignamos los valores
            posicion->vcpos[i][0] = varCov[posx];
            posicion->vcpos[i][1] = varCov[posxy];
            posicion->vcpos[i][2] = varCov[posxz];
            posicion->vcpos[i][3] = varCov[posy];
            posicion->vcpos[i][4] = varCov[posyz];
            posicion->vcpos[i][5] = varCov[posz];
        }
        //extraemos los elementos de error para las velocidades
        if(extraeVelocidad)
        {
            //extraemos las posiciones en el vector que almacena la matriz de
            //todos los elementos de la submatriz de 3x3 de parametros de error
            PosVectorMatrizTriInf(numeroIncognitas,
                                  posicion->nincvel[i][0],
                                  posicion->nincvel[i][1],
                                  posicion->nincvel[i][2],
                                  &posx,&posy,&posz,
                                  &posxy,&posxz,&posyz);
            //asignamos los valores
            posicion->vcvel[i][0] = varCov[posx];
            posicion->vcvel[i][1] = varCov[posxy];
            posicion->vcvel[i][2] = varCov[posxz];
            posicion->vcvel[i][3] = varCov[posy];
            posicion->vcvel[i][4] = varCov[posyz];
            posicion->vcvel[i][5] = varCov[posz];
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada al vector de errores
    free(varCov);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el puntero de L/E en su posicion original en el fichero
    fseek(idFichero,posOriginal,SEEK_SET);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeSolucionEstimadaPosSnx(FILE* idFichero,
                              int extraePosicion,
                              int extraeVelocidad,
                              long int posFichSOLES,
                              int lineaIniSOLES,
                              int lineaFinSOLES,
                              long int posFichSOLMATES,
                              int lineaIniSOLMATES,
                              int lineaFinSOLMATES,
                              int* infoS02,
                              possnx* posicion)
{
    //codigo de error
    int cError=ERRNOERROR;
    //indice para recorrer bucle
    int i=0;
    //longitud de cada linea
    int lonLinea=LONMAXLINSNX;
    //linea leida
    char lineaLeida[lonLinea+2];
    //numero de lineas con datos
    int nLineas=0;
    //numero de elementos leidos
    int nElementos=0;
    //cadena de formato de lectura
    char formato[lonLinea+2];
    //numero de incognita del parametro leido
    int nIncognita=0;
    //tipo de incognita del parametro leido
    char tipoIncognita[lonLinea+2];
    //codigo de la incognita leida
    char codIncognita[lonLinea+2];
    //identificador de la solucion a la que pertenece la incognita leida
    char solIncognita[lonLinea+2];
    //identificador del monumento incognita
    char ptIncognita[lonLinea+2];
    //epoca de la incognita leida
    char epocaIncognita[lonLinea+2];
    //valor de la coordenada leida,de su desviación típica y de su varianza
    double coordenada=0.0,std=0.0,var=0.0;
    //campos no validos de la linea leida
    char noValido[lonLinea+2];
    //indicador de existencia de punto
    int existePunto=0;
    //contador para almacenar el numero de incognitas
    int numeroIncognitas=0;
    //posicion inicial en el fichero
    long int posOriginal=ftell(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //LECTURA DE POSICIONES Y VELOCIDADES ESTIMADAS
    //nos colocamos al comienzo de la linea que contiene la cabecera del bloque
    //SOLUTION/ESTIMATE
    fseek(idFichero,posFichSOLES,SEEK_SET);
    //leemos la linea que contiene la cabecera del bloque
    fgets(lineaLeida,lonLinea+2,idFichero);
    //numero de lineas con datos en el bloque SOLUTION/ESTIMATE
    nLineas = lineaFinSOLES-lineaIniSOLES-1;
    //vamos recorriendo todas las lineas
    for(i=lineaIniSOLES+1;i<lineaIniSOLES+nLineas+1;i++)
    {
        //leemos la siguiente linea del fichero
        fgets(lineaLeida,lonLinea+2,idFichero);
        //trabajamos con ella si no es un comentario
        if(!ExisteSubcadena(lineaLeida,POSINICOMENTSNX,CADCOMENTSNX))
        {
            //aumentamos en una unidad el contador de incognitas contenidas en
            //el fichero sinex
            numeroIncognitas++;
            //creamos la cadena de formato de lectura
            sprintf(formato,"%s%%d%s%%s%s%%s%s%%s%s%%s%s%%s%s%%s%s%%s%s%%lf%s"
                            "%%lf",
                    SEPCAMPOSSOLES,SEPCAMPOSSOLES,SEPCAMPOSSOLES,SEPCAMPOSSOLES,
                    SEPCAMPOSSOLES,SEPCAMPOSSOLES,SEPCAMPOSSOLES,SEPCAMPOSSOLES,
                    SEPCAMPOSSOLES,SEPCAMPOSSOLES);
            //leemos los elementos necesarios
            nElementos = sscanf(lineaLeida,formato,&nIncognita,
                                                   tipoIncognita,
                                                   codIncognita,
                                                   ptIncognita,
                                                   solIncognita,
                                                   epocaIncognita,
                                                   noValido,
                                                   noValido,
                                                   &coordenada,
                                                   &std);
            //comprobamos si se han leido todos los elementos
            if(nElementos!=10)
            {
                //escribimos el mensaje en la salida de error
                MensajeErrorNELEMLEIDOSFICH((char*)__func__,i);
                //salimos de la funcion
                return ERRNELEMLEIDOSFICH;
            }
            //calculamos la varianza
            var = pow(std,2.0);
            //solo trabajamos si es una incógnita de posición o velocidad
            if(strcmp(tipoIncognita,COORXSOLES)&&
               strcmp(tipoIncognita,COORYSOLES)&&
               strcmp(tipoIncognita,COORZSOLES)&&
               strcmp(tipoIncognita,VELXSOLES)&&
               strcmp(tipoIncognita,VELYSOLES)&&
               strcmp(tipoIncognita,VELZSOLES))
            {
                //continuamos en la siguiente vuelta del bucle
                continue;
            }
            //comprobamos si el punto extraido ya existe en nuestra lista
            existePunto = ExisteCadena(posicion->puntos,posicion->pt,
                                       posicion->idsol,
                                       posicion->nPuntos,
                                       codIncognita,ptIncognita,solIncognita);
            //si no existe anyadimos un nuevo punto
            if(existePunto==-1)
            {
                //asignamos memoria para un nuevo punto
                cError = AsignaMemoriaPosSnx(posicion,1);
                //comprobamos el codigo de error devuelto
                if(cError!=ERRNOERROR)
                {
                    //lanzamos el mensaje de error
                    MensajeErrorPropagado((char*)__func__,
                                          "AsignaMemoriaPosSnx()");
                    //salimos de la funcion
                    return cError;
                }
                //ahora el contador de indice es existePunto
                existePunto = posicion->nPuntos-1;
                //asignamos el nombre, el identificador de solucion y la epoca
                //del nuevo punto
                strcpy(posicion->puntos[existePunto],codIncognita);
                strcpy(posicion->epocas[existePunto],epocaIncognita);
                strcpy(posicion->pt[existePunto],ptIncognita);
                strcpy(posicion->idsol[existePunto],solIncognita);
            }
            //comprobamos si hay que extraer resultados de posicion
            if(extraePosicion)
            {
                //comprobamos si hemos leido una coordenada
                if(!strcmp(tipoIncognita,COORXSOLES))
                {
                    //marcamos la coordenada como existente
                    posicion->epos[existePunto][0] = 1;
                    //almacenamos la coordenada
                    posicion->pos[existePunto][0] = coordenada;
                    //almacenamos la varianza
                    posicion->vcpos[existePunto][0] = var;
                    //asignamos el numero de incognita
                    posicion->nincpos[existePunto][0] = nIncognita;
                }
                else if(!strcmp(tipoIncognita,COORYSOLES))
                {
                    //marcamos la coordenada como existente
                    posicion->epos[existePunto][1] = 1;
                    //almacenamos la coordenada
                    posicion->pos[existePunto][1] = coordenada;
                    //almacenamos la varianza
                    posicion->vcpos[existePunto][3] = var;
                    //asignamos el numero de incognita
                    posicion->nincpos[existePunto][1] = nIncognita;
                }
                else if(!strcmp(tipoIncognita,COORZSOLES))
                {
                    //marcamos la coordenada como existente
                    posicion->epos[existePunto][2] = 1;
                    //almacenamos la coordenada
                    posicion->pos[existePunto][2] = coordenada;
                    //almacenamos la varianza
                    posicion->vcpos[existePunto][5] = var;
                    //asignamos el numero de incognita
                    posicion->nincpos[existePunto][2] = nIncognita;
                }
            }
            //comprobamos si hay que extraer resultados de velocidad
            if(extraeVelocidad)
            {
                //comprobamos si hemos leido una velocidad
                if(!strcmp(tipoIncognita,VELXSOLES))
                {
                    //marcamos la coordenada como existente
                    posicion->evel[existePunto][0] = 1;
                    //almacenamos la coordenada
                    posicion->vel[existePunto][0] = coordenada;
                    //almacenamos la varianza
                    posicion->vcvel[existePunto][0] = var;
                    //asignamos el numero de incognita
                    posicion->nincvel[existePunto][0] = nIncognita;
                }
                else if(!strcmp(tipoIncognita,VELYSOLES))
                {
                    //marcamos la coordenada como existente
                    posicion->evel[existePunto][1] = 1;
                    //almacenamos la coordenada
                    posicion->vel[existePunto][1] = coordenada;
                    //almacenamos la varianza
                    posicion->vcvel[existePunto][3] = var;
                    //asignamos el numero de incognita
                    posicion->nincvel[existePunto][1] = nIncognita;
                }
                else if(!strcmp(tipoIncognita,VELZSOLES))
                {
                    //marcamos la coordenada como existente
                    posicion->evel[existePunto][2] = 1;
                    //almacenamos la coordenada
                    posicion->vel[existePunto][2] = coordenada;
                    //almacenamos la varianza
                    posicion->vcvel[existePunto][5] = var;
                    //asignamos el numero de incognita
                    posicion->nincvel[existePunto][2] = nIncognita;
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamo si hay que leer parámetros de error
    if((posFichSOLMATES==-1)||(lineaIniSOLMATES==-1)||(lineaFinSOLMATES==-1))
    {
        //vale con las varianzas leídas en SOLUTION/ESTIMATE
        //indicamos que las matrices son de varianzas-covarianzas
        strcpy(posicion->tipoMat,MATCOVASOLMATES);
    }
    else
    {
        //extraemos los parametros de error
        cError = LeeParametrosErrorPosSnx(idFichero,extraePosicion,
                                          extraeVelocidad,numeroIncognitas,
                                          posFichSOLMATES,lineaIniSOLMATES,
                                          lineaFinSOLMATES,infoS02,posicion);
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPropagado((char*)__func__,
                                  "LeeParametrosErrorPosSnx()");
            //salimos de la funcion
            return cError;
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el puntero de L/E en su posicion original en el fichero
    fseek(idFichero,posOriginal,SEEK_SET);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int EsBisiesto(const int anyo)
{
    //calculamos y salimos de la función
    return ((!(int)fmod((double)anyo,4.0))&&((int)fmod((double)anyo,100.0)))||
           (!(int)fmod((double)anyo,400.0));
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double EpocaSnxFechaAnyo(char epoca[])
{
    //anyo, dia, segundo
    int anyo=0,dia=0,segundo=0;
    //numero de elementos leidos
    int nElementos=0;
    //fecha de salida
    double fecha=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los datos de anyo, dia y segundo
    nElementos = sscanf(epoca,FORMATOEPOCASNX,&anyo,&dia,&segundo);
    //comprobamos si se han leido todos los elementos
    if(nElementos!=3)
    {
        //escribimos el mensaje en la salida de error
        fprintf(stderr,"\n");
        fprintf(stderr,"*****ERROR: En la funcion %s\n",__func__);
        fprintf(stderr,"            Numero de elementos leidos incorrecto\n");
        fprintf(stderr,"            Formato incorrecto de la epoca de ");
        fprintf(stderr,"            entrada\n");
        fprintf(stderr,"            El programa finalizara\n");
        fprintf(stderr,"\n");
        //salimos del programa
        exit(1);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //actualizamos el valor del día, ya que en el formato SINEX se almacena un
    //día de más (el 1 de enero a las 0 horas se almacena como día 001 en vez de
    //000)
    dia--;
    //comprobamos si el anyo es del siglo XX o XXI
    if(anyo<=SIGLO21SNX)
    {
        fecha = 2000.0+(double)anyo;
    }
    else
    {
        fecha = 1900.0+(double)anyo;
    }
    //distinguimos entre anyo normal y bisiesto
    if(EsBisiesto((int)fecha))
    {
        //anyadimos el dia y los segundos a la fecha
        fecha +=((double)dia/366.0+(double)segundo/(366.0*SEGUNDOSDIASNX));
    }
    else
    {
        //anyadimos el dia y los segundos a la fecha
        fecha +=((double)dia/365.0+(double)segundo/(365.0*SEGUNDOSDIASNX));
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return fecha;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeePosVelFicheroSinex(char fichero[],
                          int extraePosicion,
                          int extraeVelocidad,
                          int extraeError,
                          int convierteVC,
                          int* infoS02,
                          possnx* posicion)
{
    //codigo de error
    int cError=ERRNOERROR;
    //variable para recorrer bucles
    int i=0;
    //fichero de trabajo
    FILE* idFichero=NULL;
    //posiciones del bloque de matriz de varianzas-covarianzas
    long int posMemVC=0;
    int posLinVCIni=0,posLinVCFin=0;
    //estructura bloqsnx
    bloqsnx bloques;
    //posiciones de los bloques necesarios
    int posSOLES=0,posSOLMATES=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura bloqsnx
    InicializaBloqSnx(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //abrimos el fichero de trabajo para leer
    if((idFichero=fopen(fichero,"rb"))==NULL)
    {
        //lanzamos un mensaje de error
        MensajeErrorABRIRFICHERO((char*)__func__,fichero);
        //salimos de la funcion
        return ERRABRIRFICHERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los bloques del fichero
    cError = LeeBloquesBloqSnx(idFichero,CADINIBLOQSNX,CADFINBLOQSNX,&bloques);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,"LeeBloquesBloqSnx()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqSnx(&bloques);
        //salimos de la funcion
        return cError;
    }
    //buscamos las posiciones de los bloques de trabajo
    posSOLES = PosicionBloqueBloqSnx(&bloques,ETIQUETASOLES);
    if(posSOLES==IDNOBLOQUESNX)
    {
        //lanzamos un mensaje de error
        MensajeErrorNOBLOQUEFICH((char*)__func__,ETIQUETASOLES,fichero);
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqSnx(&bloques);
        //salimos de la funcion
        return ERRNOBLOQUEFICH;
    }
    posSOLMATES = PosicionBloqueBloqSnx(&bloques,ETIQUETASOLMATES);
    if((posSOLMATES==IDNOBLOQUESNX)||(!extraeError))
    {
        //no hay matriz de varianzas-covarianzas
        posMemVC = -1;
        posLinVCIni = -1;
        posLinVCFin = -1;
    }
    else
    {
        //posiciones de inicio de la matriz de varianzas-covarianzas
        posMemVC = bloques.posicion[posSOLMATES][0];
        posLinVCIni = bloques.lineas[posSOLMATES][0];
        posLinVCFin = bloques.lineas[posSOLMATES][1];
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos la solucion estimada
    cError = LeeSolucionEstimadaPosSnx(idFichero,extraePosicion,extraeVelocidad,
                                       bloques.posicion[posSOLES][0],
                                       bloques.lineas[posSOLES][0],
                                       bloques.lineas[posSOLES][1],
                                       posMemVC,posLinVCIni,posLinVCFin,
                                       infoS02,posicion);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,
                              "LeeSolucionEstimadaPosSnx()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqSnx(&bloques);
        //salimos de la funcion
        return cError;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //pasamos a matriz de varianza-covarianza si el fichero SINEX contiene
    //matriz de correlacion
    if(!strcmp(posicion->tipoMat,MATCORRSOLMATES))
    {
        if(convierteVC)
        {
            //actualizamos la etiqueta identificadora del tipo de matriz
            strcpy(posicion->tipoMat,MATCOVASOLMATES);
            //convertimos las matrices de error
            for(i=0;i<posicion->nPuntos;i++)
            {
                //calculamos las covarianzas de la matriz de error en posicion
                MatrizCorrMatrizVarCov(posicion->vcpos[i][0],
                                       posicion->vcpos[i][3],
                                       posicion->vcpos[i][5],
                                       posicion->vcpos[i][1],
                                       posicion->vcpos[i][2],
                                       posicion->vcpos[i][4],
                                       &posicion->vcpos[i][1],
                                       &posicion->vcpos[i][2],
                                       &posicion->vcpos[i][4]);
                //convertimos las desviaciones tipicas en varianzas
                posicion->vcpos[i][0] = pow(posicion->vcpos[i][0],2.0);
                posicion->vcpos[i][3] = pow(posicion->vcpos[i][3],2.0);
                posicion->vcpos[i][5] = pow(posicion->vcpos[i][5],2.0);
                //calculamos las covarianzas de la matriz de error en velocidad
                MatrizCorrMatrizVarCov(posicion->vcvel[i][0],
                                       posicion->vcvel[i][3],
                                       posicion->vcvel[i][5],
                                       posicion->vcvel[i][1],
                                       posicion->vcvel[i][2],
                                       posicion->vcvel[i][4],
                                       &posicion->vcvel[i][1],
                                       &posicion->vcvel[i][2],
                                       &posicion->vcvel[i][4]);
                //convertimos las desviaciones tipicas en varianzas
                posicion->vcvel[i][0] = pow(posicion->vcvel[i][0],2.0);
                posicion->vcvel[i][3] = pow(posicion->vcvel[i][3],2.0);
                posicion->vcvel[i][5] = pow(posicion->vcvel[i][5],2.0);
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //cerramos el fichero de trabajo
    fclose(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaBloqSnx(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
