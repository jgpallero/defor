/**
\ingroup stemp
@{
\file series.c
\brief Definicion de las funciones para el calculo de series temporales de las
       posiciones de puntos.

En este fichero se definen todas las funciones necesarias para el calculo de
series temporales de las posiciones de una serie de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 13 de septiembre de 2009
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include"series.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double** CoordPtosSerie(const posptos* posicion,
                        const char codPto[],
                        int* nEpocas)
{
    //indices para recorrer bucles
    int i=0,j=0;
    //variables auxiliares
    double aux=0.0,epMasModerna=-0.0;
    //coordenadas
    double** coord=NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos a 0 el contador de epocas
    *nEpocas = 0;
    //contamos el numero de epocas para el punto de trabajo y buscamos la epoca
    //mas moderna
    for(i=0;i<posicion->nPuntos;i++)
    {
        if(!strcmp(codPto,posicion->puntos[i]))
        {
            //aumentamos el contador de epocas
            (*nEpocas)++;
            //buscamos la epoca mas moderna
            if(i==0)
            {
                //asignamos como epoca mas moderna la primera de la lista
                epMasModerna = posicion->epocas[i];
            }
            else
            {
                //comprobamos si el resto de epocas son mas modernas
                if(posicion->epocas[i]>epMasModerna)
                {
                    epMasModerna = posicion->epocas[i];
                }
            }
        }
    }
    //hacemos una copia de la epoca mas moderna
    aux = epMasModerna;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para la matriz de salida
    coord = AsigMemMatrizC((size_t)(*nEpocas),10);
    //comprobamos si ha ocurrido algun error
    if(coord==NULL)
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //buscamos la posicion de la epoca mas antigua
    for(i=0;i<posicion->nPuntos;i++)
    {
        //trabajamos con el punto correcto
        if(!strcmp(codPto,posicion->puntos[i]))
        {
            //comprobamos si la epoca de trabajo es mas antigua o igual que la
            //anterior
            if(posicion->epocas[i]<=aux)
            {
                //extraemos la epoca
                aux = posicion->epocas[i];
                //asignamos los datos a la matriz de salida
                coord[0][0] = aux;
                coord[0][1] = posicion->pos[i][0];
                coord[0][2] = posicion->pos[i][1];
                coord[0][3] = posicion->pos[i][2];
                coord[0][4] = posicion->vcpos[i][0];
                coord[0][5] = posicion->vcpos[i][1];
                coord[0][6] = posicion->vcpos[i][2];
                coord[0][7] = posicion->vcpos[i][3];
                coord[0][8] = posicion->vcpos[i][4];
                coord[0][9] = posicion->vcpos[i][5];
            }
        }
    }
    //recorremos el resto de epocas
    for(i=1;i<(*nEpocas);i++)
    {
        //inicializamos la variable auxiliar
        aux = epMasModerna;
        //recorremos las posiciones de la estructura
        for(j=0;j<posicion->nPuntos;j++)
        {
            //trabajamos con el punto correcto
            if(!strcmp(codPto,posicion->puntos[j]))
            {
                //trabajamos solo con epocas mas actuales que la anterior
                if(posicion->epocas[j]>coord[i-1][0])
                {
                    //comprobamos si la epoca de trabajo es mas antigua que la
                    //anterior
                    if(posicion->epocas[j]<=aux)
                    {
                        //extraemos la epoca
                        aux = posicion->epocas[j];
                        //asignamos los datos
                        coord[i][0] = aux;
                        coord[i][1] = posicion->pos[j][0];
                        coord[i][2] = posicion->pos[j][1];
                        coord[i][3] = posicion->pos[j][2];
                        coord[i][4] = posicion->vcpos[j][0];
                        coord[i][5] = posicion->vcpos[j][1];
                        coord[i][6] = posicion->vcpos[j][2];
                        coord[i][7] = posicion->vcpos[j][3];
                        coord[i][8] = posicion->vcpos[j][4];
                        coord[i][9] = posicion->vcpos[j][5];
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return coord;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double** ConvierteUtm(double** coor,
                      const int filas)
{
    //indice para recorrer bucles
    int i=0;
    //coordenadas UTM
    double** utm=NULL;
    //variables auxiliares
    double lat=0.0,lon=0.0,h=0.0,varLat=0.0,varLatLon=0.0,varLatH=0.0;
    double varLon=0.0,varLonH=0.0,varH=0.0,x=0.0,y=0.0,varx=0.0,varxy=0.0;
    double vary=0.0;
    int huso=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para la matriz de salida
    utm = AsigMemMatrizC((size_t)filas,10);
    //comprobamos si ha ocurrido algun error
    if(utm==NULL)
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos las filas de la matriz de entrada
    for(i=0;i<filas;i++)
    {
        //asignamos la fecha a la matriz de salida
        utm[i][0] = coor[i][0];
        //convertimos las coordenadas geocentricas a geodesicas
        TriGeoVC(SEMI_MAYOR,APL,coor[i][1],coor[i][2],coor[i][3],
                 coor[i][4],coor[i][5],coor[i][6],coor[i][7],coor[i][8],
                 coor[i][9],
                 &lat,&lon,&h,&varLat,&varLatLon,&varLatH,&varLon,&varLonH,
                 &varH);
        //convertimos las coordenadas en UTM
        GeoUtmVC(lat,lon,varLat,varLatLon,varLon,SEMI_MAYOR,APL,
                 &x,&y,&varx,&varxy,&vary,&huso);
        //asignamos las coordenadas a la matriz de salida
        utm[i][1] = x;
        utm[i][2] = y;
        utm[i][3] = h;
        utm[i][4] = varx;
        utm[i][5] = varxy;
        utm[i][6] = 0.0;
        utm[i][7] = vary;
        utm[i][8] = 0.0;
        utm[i][9] = varH;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return utm;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
double** ConvierteEnu(double** coor,
                      const int filas)
{
    //indice para recorrer bucles
    int i=0;
    //coordenadas ENU
    double** enu=NULL;
    //variables auxiliares
    double lat0=0.0,lon0=0.0,h=0.0,e=0.0,n=0.0,u=0.0,vare=0.0,varen=0.0;
    double vareu=0.0,varn=0.0,varnu=0.0,varu=0.0,x0=0.0,y0=0.0,z0=0.0,varx0=0.0;
    double varx0y0=0.0,varx0z0=0.0,vary0=0.0,vary0z0=0.0,varz0=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos memoria para la matriz de salida
    enu = AsigMemMatrizC((size_t)filas,10);
    //comprobamos si ha ocurrido algun error
    if(enu==NULL)
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return NULL;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos las coordenadas geodesicas para la primera epoca
    TriGeo(SEMI_MAYOR,APL,coor[0][1],coor[0][2],coor[0][3],&lat0,&lon0,&h);
    //extraemos las coordenadas geocentricas de la primera epoca
    x0 = coor[0][1];
    y0 = coor[0][2];
    z0 = coor[0][3];
    varx0 = coor[0][4];
    varx0y0 = coor[0][5];
    varx0z0 = coor[0][6];
    vary0 = coor[0][7];
    vary0z0 = coor[0][8];
    varz0 = coor[0][9];
    //hacemos el calculo para el primer punto
    //asignamos la fecha a la matriz de salida
    enu[0][0] = coor[0][0];
    //calculamos las componentes ENU
    TriEnuVC(lat0,lon0,0.0,0.0,0.0,varx0,varx0y0,varx0z0,vary0,vary0z0,varz0,
             &e,&n,&u,&vare,&varen,&vareu,&varn,&varnu,&varu);
    //asignamos las coordenadas a la matriz de salida
    enu[0][1] = e;
    enu[0][2] = n;
    enu[0][3] = u;
    enu[0][4] = vare;
    enu[0][5] = vareu;
    enu[0][6] = varen;
    enu[0][7] = varn;
    enu[0][8] = varnu;
    enu[0][9] = varu;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos el resto de las filas de la matriz de entrada
    for(i=1;i<filas;i++)
    {
        //asignamos la fecha a la matriz de salida
        enu[i][0] = coor[i][0];
        //calculamos las componentes ENU
        TriEnuVC(lat0,lon0,coor[i][1]-x0,coor[i][2]-y0,coor[i][3]-z0,
                 coor[i][4]+varx0,coor[i][5]+varx0y0,coor[i][6]+varx0z0,
                 coor[i][7]+vary0,coor[i][8]+vary0z0,coor[i][9]+varz0,
                 &e,&n,&u,&vare,&varen,&vareu,&varn,&varnu,&varu);
        //asignamos las coordenadas a la matriz de salida
        enu[i][1] = e;
        enu[i][2] = n;
        enu[i][3] = u;
        enu[i][4] = vare;
        enu[i][5] = vareu;
        enu[i][6] = varen;
        enu[i][7] = varn;
        enu[i][8] = varnu;
        enu[i][9] = varu;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return enu;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void TextoScriptTipoCoor(char** ptosDist,
                         const int nPtosDist,
                         const char tipoSeries[],
                         char texto[])
{
    //indice para recorrer bucles
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    strcpy(texto,"");
    //ETIQUETA DE COMIENZO DE PARAMETROS DE LOS PUNTOS DE TRABAJO
    strcat(texto,"%%PUNTOS DE TRABAJO, TIPOS DE COORDENADAS Y ESCALAS");
    //LISTA DE CODIGOS DE PUNTOS
    strcat(texto,"\nptos = {");
    //anyadimos los codigos de los puntos
    for(i=0;i<nPtosDist;i++)
    {
        //vamos anyadiendo
        strcat(texto,"'");
        strcat(texto,ptosDist[i]);
        strcat(texto,"'");
        if(i!=(nPtosDist-1))
        {
            strcat(texto,",");
        }
    }
    //cerramos la lista de codigos de puntos
    strcat(texto,"}; %%lista de estaciones de trabajo");
    //sistema ENU
    if(!strcmp(tipoSeries,FLAGTSERIEENU))
    {
        //LISTA DE TIPOS DE COORDENADAS
        strcat(texto,"\ntipoCoor = {'"EXTFICHENU"'};   %%tipos de coordenadas");
        //TIPOS DE COORDENADAS A USAR
        strcat(texto,"\nusaCoor = [1];   %%identificadores de uso de los "
                     "elementos de 'tipoCoor' 0/1");
        //FACTOR DE MULTIPLICACION PARA LAS COORDENADAS
        strcat(texto,"\nfactMult = [1000.0]; %%factor de multiplicación para "
                     "las coordenadas");
        //LIMITES DE LOS EJES
        //limite X inferior
        strcat(texto,"\nlimInfX = [0];           %%límite inferior eje X del "
                     "dibujo");
        //limite X superior
        strcat(texto,"\nlimSupX = [0];           %%límite superior eje X del "
                     "dibujo");
        //limite Y inferior
        strcat(texto,"\nlimInfY = [0];           %%límite inferior eje Y del "
                     "dibujo");
        //limite Y superior
        strcat(texto,"\nlimSupY = [0];           %%límite superior eje Y del "
                     "dibujo");
        //FORMATO DE LAS COORDENADAS DE LOS EJES
        //eje X
        strcat(texto,"\nformEjeX = {'%%.3f'}; %%formato de las coordenadas del "
                     "eje X");
        //eje Y
        strcat(texto,"\nformEjeY = {'%%g'}; %%formato de las coordenadas del "
                     "eje Y");
        //FORMATO PARA ESCRIBIR LAS VELOCIDADES EN EL TÍTULO DE LA FIGURA
        strcat(texto,"\n%%formato para escribir las velocidades ajustadas en "
                     "el título del dibujo\n"
                     "%%ha de estar preparado para recibir 3 valores reales\n"
                     "etiqTitVel = {', Vel.E=%%.1f+-%%.1f, Vel.N=%%.1f+-%%.1f, "
                     "Vel.U=%%.1f+-%%.1f (mm/yr, %%.1f%%%%)'};");
        //ETIQUETAS DE LOS EJES
        //eje este
        strcat(texto,"\netiqEjeYEste = {'ESTE (mm)'};       %%etiq. Y (primer "
                     "dibujo)");
        //eje norte
        strcat(texto,"\netiqEjeYNorte = {'NORTE (mm)'};     %%etiq. Y (seg. "
                     "dibujo)");
        //eje arriba
        strcat(texto,"\netiqEjeYArriba = {'ARRIBA (mm)'}; %%etiq. Y (tercer "
                     "dibujo)");
    }
    //sistema cartesiano tridimensional geocentrico
    if(!strcmp(tipoSeries,FLAGTSERIEGEOC))
    {
        //LISTA DE TIPOS DE COORDENADAS
        strcat(texto,"\ntipoCoor = {'"EXTFICHGEOC
                     "'};   %%tipos de coordenadas");
        //TIPOS DE COORDENADAS A USAR
        strcat(texto,"\nusaCoor = [1];   %%identificadores de uso de los "
                     "elementos de 'tipoCoor' 0/1");
        //FACTOR DE MULTIPLICACION PARA LAS COORDENADAS
        strcat(texto,"\nfactMult = [1.0]; %%factor de multiplicación para las "
                     "coordenadas");
        //LIMITES DE LOS EJES
        //limite X inferior
        strcat(texto,"\nlimInfX = [0];           %%límite inferior eje X del "
                     "dibujo");
        //limite X superior
        strcat(texto,"\nlimSupX = [0];           %%límite superior eje X del "
                     "dibujo");
        //limite Y inferior
        strcat(texto,"\nlimInfY = [0];           %%límite inferior eje Y del "
                     "dibujo");
        //limite Y superior
        strcat(texto,"\nlimSupY = [0];           %%límite superior eje Y del "
                     "dibujo");
        //FORMATO DE LAS COORDENADAS DE LOS EJES
        //eje X
        strcat(texto,"\nformEjeX = {'%%.3f'}; %%formato de las coordenadas del "
                     "eje X");
        //eje Y
        strcat(texto,"\nformEjeY = {'%%.3f'}; %%formato de las coordenadas del "
                     "eje Y");
        //FORMATO PARA ESCRIBIR LAS VELOCIDADES EN EL TÍTULO DE LA FIGURA
        strcat(texto,"\n%%formato para escribir las velocidades ajustadas en "
                     "el título del dibujo\n"
                     "%%ha de estar preparado para recibir 3 valores reales\n"
                     "etiqTitVel = {', Vel.X=%%.4f+-%%.4f, Vel.Y=%%.4f+-%%.4f, "
                     "Vel.Z=%%.4f+-%%.4f (m/yr, %%.1f%%%%)'};");
        //ETIQUETAS DE LOS EJES
        //eje este
        strcat(texto,"\netiqEjeYEste = {'X (m)'};       %%etiq. Y (primer "
                     "dibujo)");
        //eje norte
        strcat(texto,"\netiqEjeYNorte = {'Y (m)'};     %%etiq. Y (seg. "
                     "dibujo)");
        //eje arriba
        strcat(texto,"\netiqEjeYArriba = {'Z (m)'}; %%etiq. Y (tercer dibujo)");
    }
    //sistema UTM
    if(!strcmp(tipoSeries,FLAGTSERIEUTM))
    {
        //LISTA DE TIPOS DE COORDENADAS
        strcat(texto,"\ntipoCoor = {'"EXTFICHUTM"'};   %%tipos de coordenadas");
        //TIPOS DE COORDENADAS A USAR
        strcat(texto,"\nusaCoor = [1];   %%identificadores de uso de los "
                     "elementos de 'tipoCoor' 0/1");
        //FACTOR DE MULTIPLICACION PARA LAS COORDENADAS
        strcat(texto,"\nfactMult = [1.0]; %%factor de multiplicación para las "
                     "coordenadas");
        //LIMITES DE LOS EJES
        //limite X inferior
        strcat(texto,"\nlimInfX = [0];           %%límite inferior eje X del "
                     "dibujo");
        //limite X superior
        strcat(texto,"\nlimSupX = [0];           %%límite superior eje X del "
                     "dibujo");
        //limite Y inferior
        strcat(texto,"\nlimInfY = [0];           %%límite inferior eje Y del "
                     "dibujo");
        //limite Y superior
        strcat(texto,"\nlimSupY = [0];           %%límite superior eje Y del "
                     "dibujo");
        //FORMATO DE LAS COORDENADAS DE LOS EJES
        //eje X
        strcat(texto,"\nformEjeX = {'%%.3f'}; %%formato de las coordenadas del "
                     "eje X");
        //eje Y
        strcat(texto,"\nformEjeY = {'%%.3f'}; %%formato de las coordenadas del "
                     "eje Y");
        //FORMATO PARA ESCRIBIR LAS VELOCIDADES EN EL TÍTULO DE LA FIGURA
        strcat(texto,"\n%%formato para escribir las velocidades ajustadas en "
                     "el título del dibujo\n"
                     "%%ha de estar preparado para recibir 3 valores reales\n"
                     "etiqTitVel = {', Vel.X=%%.4f+-%%.4f, Vel.Y=%%.4f+-%%.4f, "
                     "Vel.H=%%.4f+-%%.4f (m/yr, %%.1f%%%%)'};");
        //ETIQUETAS DE LOS EJES
        //eje este
        strcat(texto,"\netiqEjeYEste = {'X UTM (m)'};       %%etiq. Y (primer "
                     "dibujo)");
        //eje norte
        strcat(texto,"\netiqEjeYNorte = {'Y UTM (m)'};     %%etiq. Y (seg. "
                     "dibujo)");
        //eje arriba
        strcat(texto,"\netiqEjeYArriba = {'h elip. (m)'}; %%etiq. Y (tercer "
                     "dibujo)");
    }
    //sistema plano
    if(!strcmp(tipoSeries,FLAGTSERIEPLANO))
    {
        //LISTA DE TIPOS DE COORDENADAS
        strcat(texto,"\ntipoCoor = {'"EXTFICHENU"','"EXTFICHUTM"'};   %%tipos "
                     "de coordenadas");
        //TIPOS DE COORDENADAS A USAR
        strcat(texto,"\nusaCoor = [1 1];   %%identificadores de uso de los "
                     "elementos de 'tipoCoor' 0/1");
        //FACTOR DE MULTIPLICACION PARA LAS COORDENADAS
        strcat(texto,"\nfactMult = [1000.0 1.0]; %%factor de multiplicación "
        "para las coordenadas");
        //LIMITES DE LOS EJES
        //limite X inferior
        strcat(texto,"\nlimInfX = [0 0];           %%límite inferior eje X del "
                     "dibujo");
        //limite X superior
        strcat(texto,"\nlimSupX = [0 0];           %%límite superior eje X del "
                     "dibujo");
        //limite Y inferior
        strcat(texto,"\nlimInfY = [0 0];           %%límite inferior eje Y del "
                     "dibujo");
        //limite Y superior
        strcat(texto,"\nlimSupY = [0 0];           %%límite superior eje Y del "
                     "dibujo");
        //FORMATO DE LAS COORDENADAS DE LOS EJES
        //eje X
        strcat(texto,"\nformEjeX = {'%%.3f','%%.3f'}; %%formato de las "
                     "coordenadas del eje X");
        //eje Y
        strcat(texto,"\nformEjeY = {'%%g','%%.3f'}; %%formato de las "
                     "coordenadas del eje Y");
        //FORMATO PARA ESCRIBIR LAS VELOCIDADES EN EL TÍTULO DE LA FIGURA
        strcat(texto,"\n%%formato para escribir las velocidades ajustadas en "
                     "el título del dibujo\n"
                     "%%ha de estar preparado para recibir 3 valores reales\n"
                     "etiqTitVel = {', Vel.E=%%.1f+-%%.1f, Vel.N=%%.1f+-%%.1f, "
                     "Vel.U=%%.1f+-%%.1f (mm/yr, %%.1f%%%%)',...\n"
                     "              ', Vel.X=%%.4f+-%%.4f, Vel.Y=%%.4f+-%%.4f, "
                     "Vel.H=%%.4f+-%%.4f (m/yr, %%.1f%%%%)'};");
        //ETIQUETAS DE LOS EJES
        //eje este
        strcat(texto,"\netiqEjeYEste = {'ESTE (mm)','X UTM (m)'};       "
                     "%%etiq. Y (primer dibujo)");
        //eje norte
        strcat(texto,"\netiqEjeYNorte = {'NORTE (mm)','Y UTM (m)'};     "
                     "%%etiq. Y (seg. dibujo)");
        //eje arriba
        strcat(texto,"\netiqEjeYArriba = {'ARRIBA (mm)','h elip. (m)'}; "
                     "%%etiq. Y (tercer dibujo)");
    }
    //todos los sistemas
    if(!strcmp(tipoSeries,FLAGTSERIETODO))
    {
        //LISTA DE TIPOS DE COORDENADAS
        strcat(texto,"\ntipoCoor = {'"EXTFICHENU"','"EXTFICHGEOC"','"EXTFICHUTM
                     "'};   %%tipos de coordenadas");
        //TIPOS DE COORDENADAS A USAR
        strcat(texto,"\nusaCoor = [1 1 1];   %%identificadores de uso de los "
                     "elementos de 'tipoCoor' 0/1");
        //FACTOR DE MULTIPLICACION PARA LAS COORDENADAS
        strcat(texto,"\nfactMult = [1000.0 1.0 1.0]; %%factor de multiplicación "
                     "para las coordenadas");
        //LIMITES DE LOS EJES
        //limite X inferior
        strcat(texto,"\nlimInfX = [0 0 0];           %%límite inferior eje X "
                      "del dibujo");
        //limite X superior
        strcat(texto,"\nlimSupX = [0 0 0];           %%límite superior eje X "
                     "del dibujo");
        //limite Y inferior
        strcat(texto,"\nlimInfY = [0 0 0];           %%límite inferior eje Y "
                     "del dibujo");
        //limite Y superior
        strcat(texto,"\nlimSupY = [0 0 0];           %%límite superior eje Y "
                     "del dibujo");
        //FORMATO DE LAS COORDENADAS DE LOS EJES
        //eje X
        strcat(texto,"\nformEjeX = {'%%.3f','%%.3f','%%.3f'}; %%formato de las "
                     "coordenadas del eje X");
        //eje Y
        strcat(texto,"\nformEjeY = {'%%g','%%.3f','%%.3f'};   %%formato de las "
                     "coordenadas del eje Y");
        //FORMATO PARA ESCRIBIR LAS VELOCIDADES EN EL TÍTULO DE LA FIGURA
        strcat(texto,"\n%%formato para escribir las velocidades ajustadas en "
                     "el título del dibujo\n"
                     "%%ha de estar preparado para recibir 3 valores reales\n"
                     "etiqTitVel = {', Vel.E=%%.1f+-%%.1f, Vel.N=%%.1f+-%%.1f, "
                     "Vel.U=%%.1f+-%%.1f (mm/yr, %%.1f%%%%)',...\n"
                     "              ', Vel.X=%%.4f+-%%.4f, Vel.Y=%%.4f+-%%.4f, "
                     "Vel.H=%%.4f+-%%.4f (m/yr, %%.1f%%%%)',...\n"
                     "              ', Vel.X=%%.4f+-%%.4f, Vel.Y=%%.4f+-%%.4f, "
                     "Vel.Z=%%.4f+-%%.4f (m/yr, %%.1f%%%%)'};");
        //ETIQUETAS DE LOS EJES
        //eje este
        strcat(texto,"\netiqEjeYEste = {'ESTE (mm)','X (m)','X UTM (m)'};      "
                     " %%etiq. Y (primer dibujo)");
        //eje norte
        strcat(texto,"\netiqEjeYNorte = {'NORTE (mm)','Y (m)','Y UTM (m)'};    "
                     " %%etiq. Y (seg. dibujo)");
        //eje arriba
        strcat(texto,"\netiqEjeYArriba = {'ARRIBA (mm)','Z (m)','h elip. (m)'};"
                     " %%etiq. Y (tercer dibujo)");
    }
    //ETIQUETA DEL EJE X
    strcat(texto,"\netiqEjeX = 'TIEMPO (años)';                             "
                 "%%etiqueta eje X");
    //TEXTOS DE LA LEYENDA
    //serie
    strcat(texto,"\ntextoLeySerieError = sprintf('Serie temporal "
                 "(err. %%.1f%%%%)',intConf*100.0);");
    //serie con errorbar()
    strcat(texto,"\ntextoLeySerie = 'Serie temporal'; %%texto de la leyenda "
                 "para la serie");
    //error
    strcat(texto,"\ntextoLeyError = sprintf('Error al %%.1f%%%%',"
                 "intConf*100.0);");
    //velocidad
    strcat(texto,"\ntextoLeyVel = 'Velocidad ajustada'; "
                 "%%texto de la leyenda para la velocidad");
    //comentarios finales
    strcat(texto,"\n%%cuando los límites superiores o inferiores valen 0, se "
                 "calculan a partir de los\n");
    strcat(texto,"%%datos");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int FicherosSeriesTempOctave(const posptos* posicion,
                             char** ptosDist,
                             const int nPtosDist,
                             const char tipoSeries[],
                             const char directorio[],
                             char texto[])
{
    //indices para recorrer bucles
    int i=0,j=0;
    //numero de epocas implicadas en cada punto
    int nEpocas=0;
    //matrices para almacenar las coordenadas de trabajo
    double** enu=NULL;
    double** utm=NULL;
    double** geoc=NULL;
    //puntero a fichero
    FILE* idFich=NULL;
    //variables auxiliares
    char punto[LONMAXLINSTEMP+1];
    char nFich[LONMAXNOMFICH+1];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salida rapida si no hay ninguna estacion de trabajo
    if(!nPtosDist)
    {
        //lanzamos el mensaje de error
        MensajeErrorERRNOPTOSSTEMP((char*)__func__);
        //salimos de la funcion
        return ERRNOPTOSSTEMP;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos las coordenadas de los puntos de trabajo
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //recorremos los puntos de trabajo
    for(i=0;i<nPtosDist;i++)
    {
        //extraemos el nombre del punto de trabajo
        strcpy(punto,ptosDist[i]);
        //extraemos las epocas implicadas para el punto
        geoc = CoordPtosSerie(posicion,punto,&nEpocas);
        //comprobamos si ha ocurrido algun error
        if(geoc==NULL)
        {
            //escribimos el mensaje de error
            MensajeErrorNOMEMORIA((char*)__func__);
            //salimos de la funcion
            return ERRNOMEMORIA;
        }
        //calculamos para un sistema ENU
        if((!strcmp(tipoSeries,FLAGTSERIEENU))||
           (!strcmp(tipoSeries,FLAGTSERIEPLANO))||
           (!strcmp(tipoSeries,FLAGTSERIETODO)))
        {
            //convertimos las coordenadas al sistema ENU
            enu = ConvierteEnu(geoc,nEpocas);
            //comprobamos si ha ocurrido algun error
            if(enu==NULL)
            {
                //escribimos el mensaje de error
                MensajeErrorNOMEMORIA((char*)__func__);
                //liberamos la memoria asignada a otras matrices
                LibMemMatrizC(geoc,(size_t)nEpocas);
                //salimos de la funcion
                return ERRNOMEMORIA;
            }
            //creamos el nombre del fichero de salida
            sprintf(nFich,"%s/%s%s",directorio,punto,EXTFICHENU);
            //abrimos el fichero de salida
            idFich = fopen(nFich,"wb");
            //comprobamos si ha ocurrido algun error
            if(idFich==NULL)
            {
                //escribimos el mensaje de error
                MensajeErrorNOMEMORIA((char*)__func__);
                MensajeErrorABRIRFICHERO((char*)__func__,nFich);
                //liberamos la memoria asignada a las matrices
                LibMemMatrizC(enu,(size_t)nEpocas);
                LibMemMatrizC(geoc,(size_t)nEpocas);
                //salimos de la funcion
                return ERRABRIRFICHERO;
            }
            //escribimos las coordenadas en el fichero
            for(j=0;j<nEpocas;j++)
            {
                fprintf(idFich,FORMSERIEENU,enu[j][0],enu[j][1],enu[j][2],
                                            enu[j][3],sqrt(enu[j][4]),
                                            sqrt(enu[j][7]),
                                            sqrt(enu[j][9]));
            }
            //cerramos el fichero de trabajo
            fclose(idFich);
        }
        //calculamos para un sistema UTM
        if((!strcmp(tipoSeries,FLAGTSERIEUTM))||
           (!strcmp(tipoSeries,FLAGTSERIEPLANO))||
           (!strcmp(tipoSeries,FLAGTSERIETODO)))
        {
            //convertimos las coordenadas a UTM
            utm = ConvierteUtm(geoc,nEpocas);
            //comprobamos si ha ocurrido algun error
            if(utm==NULL)
            {
                //escribimos el mensaje de error
                MensajeErrorNOMEMORIA((char*)__func__);
                //liberamos la memoria asignada a otras matrices
                LibMemMatrizC(enu,(size_t)nEpocas);
                LibMemMatrizC(geoc,(size_t)nEpocas);
                //salimos de la funcion
                return ERRNOMEMORIA;
            }
            //creamos el nombre del fichero de salida
            sprintf(nFich,"%s/%s%s",directorio,punto,EXTFICHUTM);
            //abrimos el fichero de salida
            idFich = fopen(nFich,"wb");
            //comprobamos si ha ocurrido algun error
            if(idFich==NULL)
            {
                //escribimos el mensaje de error
                MensajeErrorNOMEMORIA((char*)__func__);
                MensajeErrorABRIRFICHERO((char*)__func__,nFich);
                //liberamos la memoria asignada a las matrices
                LibMemMatrizC(enu,(size_t)nEpocas);
                LibMemMatrizC(utm,(size_t)nEpocas);
                LibMemMatrizC(geoc,(size_t)nEpocas);
                //salimos de la funcion
                return ERRABRIRFICHERO;
            }
            //escribimos las coordenadas en el fichero
            for(j=0;j<nEpocas;j++)
            {
                fprintf(idFich,FORMSERIEUTM,utm[j][0],utm[j][1],utm[j][2],
                                            utm[j][3],sqrt(utm[j][4]),
                                            sqrt(utm[j][7]),
                                            sqrt(utm[j][9]));
            }
            //cerramos el fichero de trabajo
            fclose(idFich);
        }
        //calculamos para un sistema cartesiano tridimensional geocentrico
        if((!strcmp(tipoSeries,FLAGTSERIEGEOC))||
           (!strcmp(tipoSeries,FLAGTSERIETODO)))
        {
            //creamos el nombre del fichero de salida
            sprintf(nFich,"%s/%s%s",directorio,punto,EXTFICHGEOC);
            //abrimos el fichero de salida
            idFich = fopen(nFich,"wb");
            //comprobamos si ha ocurrido algun error
            if(idFich==NULL)
            {
                //escribimos el mensaje de error
                MensajeErrorNOMEMORIA((char*)__func__);
                MensajeErrorABRIRFICHERO((char*)__func__,nFich);
                //liberamos la memoria asignada a las matrices
                LibMemMatrizC(enu,(size_t)nEpocas);
                LibMemMatrizC(utm,(size_t)nEpocas);
                LibMemMatrizC(geoc,(size_t)nEpocas);
                //salimos de la funcion
                return ERRABRIRFICHERO;
            }
            //escribimos las coordenadas en el fichero
            for(j=0;j<nEpocas;j++)
            {
                fprintf(idFich,FORMSERIEGEOC,geoc[j][0],geoc[j][1],geoc[j][2],
                                             geoc[j][3],sqrt(geoc[j][4]),
                                             sqrt(geoc[j][7]),
                                             sqrt(geoc[j][9]));
            }
            //cerramos el fichero de trabajo
            fclose(idFich);
        }
        //liberamos la posible memoria utilizada
        LibMemMatrizC(enu,(size_t)nEpocas);
        LibMemMatrizC(utm,(size_t)nEpocas);
        LibMemMatrizC(geoc,(size_t)nEpocas);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos el texto del script
    TextoScriptTipoCoor(ptosDist,nPtosDist,tipoSeries,texto);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int SerieTempPtosOctave(const posptos* posicion,
                        const char tipoSeries[],
                        const char directorio[],
                        FILE* idFichero,
                        char fichero[])
{
    //codigo de error
    int codErr=ERRNOERROR;
    //indice para recorrer bucles
    int i=0;
    //numero de codigos distintos
    size_t nCodDist=0;
    //vector para almacenar los codigos distintos
    char** codigos=NULL;
    //cadena de texto para almacenar los parametros del dibujo dependientes del
    //numero de sistemas de coordenadas
    char texto[LONMAXTEXTOSCOORDSTEMP+1];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salida rapida si no hay ninguna estacion de trabajo
    if(!posicion->nPuntos)
    {
        //lanzamos el mensaje de error
        MensajeErrorERRNOPTOSSTEMP((char*)__func__);
        //salimos de la funcion
        return ERRNOPTOSSTEMP;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraigo los codigos de los puntos distintos
    codigos = CodigosIndividuales(posicion->puntos,(size_t)posicion->nPuntos,
                                  &nCodDist);
    //compruebo si ha ocurrido algun error
    if(codigos==NULL)
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos las coordenadas a los ficheros de trabajo
    codErr = FicherosSeriesTempOctave(posicion,codigos,(int)nCodDist,tipoSeries,
                                      directorio,texto);
    //comprobamos si ha ocurrido algun error
    if(codErr!=ERRNOERROR)
    {
        //libero la memoria utilizada
        for(i=0;i<(int)nCodDist;i++)
        {
            free(codigos[i]);
        }
        free(codigos);
        //salimos de la funcion
        return codErr;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay que escribir el script de GNU Octave
    if(idFichero!=NULL)
    {
        //escribimos la primera linea del fichero
        if(fprintf(idFichero,"%s\n\n",LLAMADAOCTAVESTEMP)<0)
        {
            //libero la memoria utilizada
            for(i=0;i<(int)nCodDist;i++)
            {
                free(codigos[i]);
            }
            free(codigos);
            //lanzamos el mensaje de error
            MensajeErrorERRESCFICH((char*)__func__,fichero);
            //salimos de la funcion
            return ERRESCFICH;
        }
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //escribimos la ayuda
        fprintf(idFichero,AYUDASCRIPTSTEMP);
        fprintf(idFichero,"\n\n");
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //escribimos las lineas de opciones
        fprintf(idFichero,PARAMSSTEMP1);
        fprintf(idFichero,"\n");
        fprintf(idFichero,"%s",texto);
        fprintf(idFichero,"\n");
        fprintf(idFichero,PARAMSSTEMP2);
        fprintf(idFichero,"\n");
        ////////////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //escribimos las lineas de dibujo
        fprintf(idFichero,DIBUJOSTEMP1);
        fprintf(idFichero,DIBUJOSTEMP2);
        fprintf(idFichero,DIBUJOSTEMP3);
        fprintf(idFichero,DIBUJOSTEMP4);
        fprintf(idFichero,DIBUJOSTEMP5);
        fprintf(idFichero,DIBUJOSTEMP6);
        fprintf(idFichero,DIBUJOSTEMP7);
        fprintf(idFichero,DIBUJOSTEMP8);
        fprintf(idFichero,DIBUJOSTEMP9);
        fprintf(idFichero,DIBUJOSTEMP10);
        fprintf(idFichero,DIBUJOSTEMP11);
        fprintf(idFichero,DIBUJOSTEMP12);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //libero la memoria utilizada
    for(i=0;i<(int)nCodDist;i++)
    {
        free(codigos[i]);
    }
    free(codigos);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
