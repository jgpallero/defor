/**
\addtogroup transf
\ingroup deformacion
@{
\file partran.c
\brief Definicion de la estructura y funciones para la extraccion de datos de
       parametros de tranformacion de ficheros de parametros de transformacion
       entre sistemas de referencia.

En este fichero se definen la estructura y todas las funciones necesarias para
la extraccion de parametros de transformacion de los bloques contenidos en un
fichero de parametros de transformacion entre sistemas de referencia.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 30 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"errores.h"
#include"general.h"
#include"geodesia.h"
#include"paramtran.h"
#include"bloqtran.h"
#include"partran.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void InicializaParTran(partran* parametros)
{
    //inicializamos el contador de puntos a 0
    parametros->nTransf = 0;
    //inicializamos los miembros que son punteros a NULL
    parametros->sistIni = NULL;
    parametros->sistFin = NULL;
    parametros->epocas = NULL;
    parametros->param = NULL;
    parametros->variac = NULL;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int AsignaMemoriaParTran(partran* parametros,
                         int elementos)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de elementos es correcto
    if(elementos<=0)
    {
        //escribimos el mensaje de error
        MensajeErrorNELEMMENORCERO((char*)__func__);
        //salimos de la funcion
        return ERRNELEMMENORCERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //aumentamos el contador de transformaciones
    parametros->nTransf += elementos;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las filas a las matrices de datos
    parametros->sistIni=(char**)realloc(parametros->sistIni,
                                     (size_t)parametros->nTransf*sizeof(char*));
    parametros->sistFin=(char**)realloc(parametros->sistFin,
                                     (size_t)parametros->nTransf*sizeof(char*));
    parametros->epocas=(double**)realloc(parametros->epocas,
                                   (size_t)parametros->nTransf*sizeof(double*));
    parametros->param=(double**)realloc(parametros->param,
                                   (size_t)parametros->nTransf*sizeof(double*));
    parametros->variac=(double**)realloc(parametros->variac,
                                   (size_t)parametros->nTransf*sizeof(double*));
    //controlamos los posibles errores en la asignacion de memoria
    if((parametros->sistIni==NULL)||(parametros->sistFin==NULL)||
       (parametros->epocas==NULL)||(parametros->param==NULL)||
       (parametros->variac==NULL))
    {
        //escribimos el mensaje de error
        MensajeErrorNOMEMORIA((char*)__func__);
        //salimos de la funcion
        return ERRNOMEMORIA;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //asignamos espacio para las columnas a las matrices de datos
    for(i=0;i<elementos;i++)
    {
        parametros->sistIni[parametros->nTransf-i-1]=
                                                 (char*)malloc(LONMAXLINTRAN+1);
        parametros->sistFin[parametros->nTransf-i-1]=
                                                 (char*)malloc(LONMAXLINTRAN+1);
        parametros->epocas[parametros->nTransf-i-1]=
                                              (double*)malloc(2*sizeof(double));
        parametros->param[parametros->nTransf-i-1]=
                                              (double*)malloc(7*sizeof(double));
        parametros->variac[parametros->nTransf-i-1]=
                                              (double*)malloc(7*sizeof(double));
        //controlamos los posibles errores en la asignacion de memoria
        if((parametros->sistIni[parametros->nTransf-i-1]==NULL)||
           (parametros->sistFin[parametros->nTransf-i-1]==NULL)||
           (parametros->epocas[parametros->nTransf-i-1]==NULL)||
           (parametros->param[parametros->nTransf-i-1]==NULL)||
           (parametros->variac[parametros->nTransf-i-1]==NULL))
        {
            //escribimos el mensaje de error
            MensajeErrorNOMEMORIA((char*)__func__);
            //salimos de la funcion
            return ERRNOMEMORIA;
        }
        //vamos inicializando a 0 la matriz de epocas
        parametros->epocas[parametros->nTransf-i-1][0] = 0.0;
        parametros->epocas[parametros->nTransf-i-1][1] = 0.0;
        //vamos inicializando a 0 la matriz de parametros de transformacion
        parametros->param[parametros->nTransf-i-1][0] = 0.0;
        parametros->param[parametros->nTransf-i-1][1] = 0.0;
        parametros->param[parametros->nTransf-i-1][2] = 0.0;
        parametros->param[parametros->nTransf-i-1][3] = 0.0;
        parametros->param[parametros->nTransf-i-1][4] = 0.0;
        parametros->param[parametros->nTransf-i-1][5] = 0.0;
        parametros->param[parametros->nTransf-i-1][6] = 0.0;
        //vamos inicializando a 0 la matriz de variacion de los parametros de
        parametros->variac[parametros->nTransf-i-1][0] = 0.0;
        parametros->variac[parametros->nTransf-i-1][1] = 0.0;
        parametros->variac[parametros->nTransf-i-1][2] = 0.0;
        parametros->variac[parametros->nTransf-i-1][3] = 0.0;
        parametros->variac[parametros->nTransf-i-1][4] = 0.0;
        parametros->variac[parametros->nTransf-i-1][5] = 0.0;
        parametros->variac[parametros->nTransf-i-1][6] = 0.0;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void LiberaMemoriaParTran(partran* parametros)
{
    //variable auxiliar
    int i=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //verificamos si hemos pasado una estructura vacia
    if(!parametros->nTransf)
    {
        //salimos de la funcion
        return;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos la memoria asignada a las matrices y a los array de cadenas de
    //caracteres
    for(i=0;i<parametros->nTransf;i++)
    {
        free(parametros->sistIni[i]);
        free(parametros->sistFin[i]);
        free(parametros->epocas[i]);
        free(parametros->param[i]);
        free(parametros->variac[i]);
    }
    free(parametros->sistIni);
    free(parametros->sistFin);
    free(parametros->epocas);
    free(parametros->param);
    free(parametros->variac);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el contador de puntos a 0
    parametros->nTransf = 0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeBloqueParTran(FILE* idFichero,
                     int extraeVariacion,
                     long int posFich,
                     int lineaIni,
                     int lineaFin,
                     partran* parametros)
{
    //codigo de error
    int cError=ERRNOERROR;
    //indice para recorrer bucle
    int i=0;
    //longitud de cada linea
    int lonLinea=LONMAXLINTRAN;
    //linea leida
    char lineaLeida[lonLinea+2];
    //numero de lineas con datos
    int nLineas=0;
    //numero de elementos leidos
    int nElementos=0;
    //cadena de formato de lectura
    char formato[lonLinea+2];
    //codigo de los sistemas de referencia leidos
    char sistIni[lonLinea+2];
    char sistFin[lonLinea+2];
    //epocas base de los sistemas de referencia leidos
    double epocaIni=0.0;
    double epocaFin=0.0;
    //valor de los parametros
    double tx=0.0,ty=0.0,tz=0.0,d=0.0,rx=0.0,ry=0.0,rz=0.0;
    //valor de la variacion de los parametros
    double vtx=0.0,vty=0.0,vtz=0.0,vd=0.0,vrx=0.0,vry=0.0,vrz=0.0;
    //posicion inicial en el fichero
    long int posOriginal=ftell(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //LECTURA DE POSICIONES Y PARAMETROS DE ERROR
    //nos colocamos al comienzo de la linea que contiene el bloque de
    //transformaciones
    fseek(idFichero,posFich,SEEK_SET);
    //leemos la linea que contiene la cabecera del bloque
    fgets(lineaLeida,lonLinea+2,idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //numero de lineas con datos en el bloque
    nLineas = lineaFin-lineaIni-1;
    //vamos recorriendo todas las lineas
    for(i=lineaIni+1;i<lineaIni+nLineas+1;i++)
    {
        //leemos la siguiente linea del fichero
        fgets(lineaLeida,lonLinea+2,idFichero);
        //trabajamos con ella si no es un comentario
        if(!ExisteSubcadena(lineaLeida,POSINICOMENTTRAN,CADCOMENTTRAN))
        {
            //comprobamos si es una linea identificadora de transformacion
            if(ExisteSubcadena(lineaLeida,POSINIIDTRANTRAN,CADIDTRANTRAN))
            {
                //creamos la cadena de formato de lectura
                sprintf(formato,
           "%s%s%%s%s%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                        CADIDTRANTRAN,SEPCADIDTRANINITRANTRAN,
                        SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,
                        SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,
                        SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,
                        SEPCAMPOSPARTRAN);
                //leemos los elementos necesarios
                nElementos = sscanf(lineaLeida,formato,
                                    sistIni,sistFin,
                                    &epocaIni,&epocaFin,
                                    &tx,&ty,&tz,
                                    &d,
                                    &rx,&ry,&rz);
                //comprobamos si se han leido todos los elementos
                if(nElementos!=11)
                {
                    //escribimos el mensaje en la salida de error
                    MensajeErrorNELEMLEIDOSFICH((char*)__func__,i);
                    //salimos de la funcion
                    return ERRNELEMLEIDOSFICH;
                }
                //asignamos memoria para una nueva transformacion
                cError = AsignaMemoriaParTran(parametros,1);
                //comprobamos el codigo de error devuelto
                if(cError!=ERRNOERROR)
                {
                    //lanzamos el mensaje de error
                    MensajeErrorPropagado((char*)__func__,
                                          "AsignaMemoriaParTran()");
                    //salimos de la funcion
                    return cError;
                }
                //transformamos los datos a sus unidades correctas y los
                //asignamos a los campos de la estructura
                strcpy(parametros->sistIni[parametros->nTransf-1],sistIni);
                strcpy(parametros->sistFin[parametros->nTransf-1],sistFin);
                parametros->epocas[parametros->nTransf-1][0] = epocaIni;
                parametros->epocas[parametros->nTransf-1][1] = epocaFin;
                parametros->param[parametros->nTransf-1][0] = tx*CMM;
                parametros->param[parametros->nTransf-1][1] = ty*CMM;
                parametros->param[parametros->nTransf-1][2] = tz*CMM;
                parametros->param[parametros->nTransf-1][3] = d;
                parametros->param[parametros->nTransf-1][4] = rx*MSR;
                parametros->param[parametros->nTransf-1][5] = ry*MSR;
                parametros->param[parametros->nTransf-1][6] = rz*MSR;
            }
            else
            {
                //extraemos las variaciones si se han solicitado
                if(extraeVariacion)
                {
                    //creamos la cadena de formato de lectura
                    sprintf(formato,
                            "%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                            SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,
                            SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,SEPCAMPOSPARTRAN,
                            SEPCAMPOSPARTRAN);
                    //leemos los elementos necesarios
                    nElementos = sscanf(lineaLeida,formato,
                                        &vtx,&vty,&vtz,&vd,&vrx,&vry,&vrz);
                    //comprobamos si se han leido todos los elementos
                    if(nElementos!=7)
                    {
                        //escribimos el mensaje en la salida de error
                        MensajeErrorNELEMLEIDOSFICH((char*)__func__,i);
                        //salimos de la funcion
                        return ERRNELEMLEIDOSFICH;
                    }
                    //la matriz leida se considera perteneciente la ultima
                    //transformacion creada, por lo que hay que comprobar si
                    //existe alguna
                    //si no existe ninguna, la linea leida no se tiene en cuenta
                    if(parametros->nTransf)
                    {
                        //transformamos los datos a sus unidades correctas y los
                        //asignamos a los campos de la estructura
                        parametros->variac[parametros->nTransf-1][0] = vtx*CMM;
                        parametros->variac[parametros->nTransf-1][1] = vty*CMM;
                        parametros->variac[parametros->nTransf-1][2] = vtz*CMM;
                        parametros->variac[parametros->nTransf-1][3] = vd;
                        parametros->variac[parametros->nTransf-1][4] = vrx*MSR;
                        parametros->variac[parametros->nTransf-1][5] = vry*MSR;
                        parametros->variac[parametros->nTransf-1][6] = vrz*MSR;
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //ponemos el puntero de L/E en su posicion original en el fichero
    fseek(idFichero,posOriginal,SEEK_SET);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int LeeParFicheroTransf(char fichero[],
                        char bloque[],
                        int extraeVariacion,
                        partran* parametros)
{
    //codigo de error
    int cError=ERRNOERROR;
    //puntero al fichero de trabajo
    FILE* idFichero=NULL;
    //estructura bloqtran
    bloqtran bloques;
    //posicion del bloque de trabajo
    int posBLOQUE=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura bloqtran
    InicializaBloqTran(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //abrimos el fichero de trabajo para leer
    if((idFichero=fopen(fichero,"rb"))==NULL)
    {
        //lanzamos un mensaje de error
        MensajeErrorABRIRFICHERO((char*)__func__,fichero);
        //salimos de la funcion
        return ERRABRIRFICHERO;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los bloques del fichero
    cError = LeeBloquesBloqTran(idFichero,
                                CADINIBLOQTRAN,CADFINBLOQTRAN,
                                &bloques);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,"LeeBloquesBloqTran()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqTran(&bloques);
        //salimos de la funcion
        return cError;
    }
    //buscamos la posicion del bloque de trabajo
    posBLOQUE = PosicionBloqueBloqTran(&bloques,bloque);
    if(posBLOQUE==IDNOBLOQUETRAN)
    {
        //lanzamos un mensaje de error
        MensajeErrorNOBLOQUEFICH((char*)__func__,bloque,fichero);
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqTran(&bloques);
        //salimos de la funcion
        return ERRNOBLOQUEFICH;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos el bloque elegido
    cError = LeeBloqueParTran(idFichero,extraeVariacion,
                              bloques.posicion[posBLOQUE][0],
                              bloques.lineas[posBLOQUE][0],
                              bloques.lineas[posBLOQUE][1],
                              parametros);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPropagado((char*)__func__,"LeeBloqueParTran()");
        //cerramos el fichero de trabajo
        fclose(idFichero);
        //liberamos la posible memoria asignada a la estructura bloqsnx
        LiberaMemoriaBloqTran(&bloques);
        //salimos de la funcion
        return cError;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //cerramos el fichero de trabajo
    fclose(idFichero);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaBloqTran(&bloques);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return ERRNOERROR;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int PuntoSistRefTransf(partran* parametros,
                       char sistRef1[],
                       char sistRef2[],
                       double t1,
                       double x1,
                       double y1,
                       double z1,
                       double vx1,
                       double vx1y1,
                       double vx1z1,
                       double vy1,
                       double vy1z1,
                       double vz1,
                       double* x2,
                       double* y2,
                       double* z2,
                       double* vx2,
                       double* vx2y2,
                       double* vx2z2,
                       double* vy2,
                       double* vy2z2,
                       double* vz2)
{
    //codigo de error
    int cError=ERRNOERROR;
    //variables intermedias para una transformacion compuesta
    double xi=0.0,yi=0.0,zi=0.0;
    double vxi=0.0,vxiyi=0.0,vxizi=0.0,vyi=0.0,vyizi=0.0,vzi=0.0;
    //variables de posicion para la transformacion de sistema de referencia
    int pos=0,pos1=0,pos2=0;
    //variables de orden para la transformacion de sistema de referencia
    int orden=0,orden1=0,orden2=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si los sistemas de partida y llegada son el mismo
    if(!strcmp(sistRef1,sistRef2))
    {
        //las coordenadas del punto no cambian
        *x2 = x1;
        *y2 = y1;
        *z2 = z1;
        *vx2 = vx1;
        *vx2y2 = vx1y1;
        *vx2z2 = vx1z1;
        *vy2 = vy1;
        *vy2z2 = vy1z1;
        *vz2 = vz1;
        //salimos de la funcion
        return cError;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay transformacion directa
    ExistenCadenasEnDosListas(parametros->sistIni,parametros->sistFin,
                              parametros->nTransf,
                              sistRef1,sistRef2,
                              &pos,&orden);
    //comprobamos si hay transformacion compuesta
    Cadena1ACadena2(parametros->sistIni,parametros->sistFin,parametros->nTransf,
                    sistRef1,sistRef2,
                    &pos1,&orden1,&pos2,&orden2);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //realizamos la transformacion pertinente
    if(pos!=-1)
    {
        //comprobamos si el orden de transformacion es directo o inverso
        if(orden==1)
        {
            //realizamos la transformacion para orden directo
            TranSistRefVC(parametros->epocas[pos][1],
                          parametros->param[pos][0],parametros->param[pos][1],
                          parametros->param[pos][2],parametros->param[pos][3],
                          parametros->param[pos][4],parametros->param[pos][5],
                          parametros->param[pos][6],
                          parametros->variac[pos][0],parametros->variac[pos][1],
                          parametros->variac[pos][2],parametros->variac[pos][3],
                          parametros->variac[pos][4],parametros->variac[pos][5],
                          parametros->variac[pos][6],
                          t1,
                          x1,y1,z1,
                          vx1,vx1y1,vx1z1,vy1,vy1z1,vz1,
                          x2,y2,z2,
                          vx2,vx2y2,vx2z2,vy2,vy2z2,vz2);
        }
        else
        {
            //realizamos la transformacion para orden inverso
            //solo se diferencia de la transformacion directa en que los
            //parametros de transformacion y sus velocidades tienen signo
            //opuesto
            TranSistRefVC(parametros->epocas[pos][1],
                          -parametros->param[pos][0],-parametros->param[pos][1],
                          -parametros->param[pos][2],-parametros->param[pos][3],
                          -parametros->param[pos][4],-parametros->param[pos][5],
                          -parametros->param[pos][6],
                        -parametros->variac[pos][0],-parametros->variac[pos][1],
                        -parametros->variac[pos][2],-parametros->variac[pos][3],
                        -parametros->variac[pos][4],-parametros->variac[pos][5],
                          -parametros->variac[pos][6],
                          t1,
                          x1,y1,z1,
                          vx1,vx1y1,vx1z1,vy1,vy1z1,vz1,
                          x2,y2,z2,
                          vx2,vx2y2,vx2z2,vy2,vy2z2,vz2);
        }
    }
    else if((pos1!=-1)&&(pos2!=-1))
    {
        //transformamos del sistema origen al sistema comun
        if(orden1==1)
        {
            //realizamos la transformacion para orden directo
            TranSistRefVC(parametros->epocas[pos1][1],
                          parametros->param[pos1][0],parametros->param[pos1][1],
                          parametros->param[pos1][2],parametros->param[pos1][3],
                          parametros->param[pos1][4],parametros->param[pos1][5],
                          parametros->param[pos1][6],
                        parametros->variac[pos1][0],parametros->variac[pos1][1],
                        parametros->variac[pos1][2],parametros->variac[pos1][3],
                        parametros->variac[pos1][4],parametros->variac[pos1][5],
                        parametros->variac[pos1][6],
                          t1,
                          x1,y1,z1,
                          vx1,vx1y1,vx1z1,vy1,vy1z1,vz1,
                          &xi,&yi,&zi,
                          &vxi,&vxiyi,&vxizi,&vyi,&vyizi,&vzi);
        }
        else
        {
            //realizamos la transformacion para orden inverso
            //solo se diferencia de la transformacion directa en que los
            //parametros de transformacion y sus velocidades tienen signo
            //opuesto
            TranSistRefVC(parametros->epocas[pos1][1],
                        -parametros->param[pos1][0],-parametros->param[pos1][1],
                        -parametros->param[pos1][2],-parametros->param[pos1][3],
                        -parametros->param[pos1][4],-parametros->param[pos1][5],
                        -parametros->param[pos1][6],
                      -parametros->variac[pos1][0],-parametros->variac[pos1][1],
                      -parametros->variac[pos1][2],-parametros->variac[pos1][3],
                      -parametros->variac[pos1][4],-parametros->variac[pos1][5],
                      -parametros->variac[pos1][6],
                          t1,
                          x1,y1,z1,
                          vx1,vx1y1,vx1z1,vy1,vy1z1,vz1,
                          &xi,&yi,&zi,
                          &vxi,&vxiyi,&vxizi,&vyi,&vyizi,&vzi);
        }
        //transformamos del sistema comun al sistema destino
        if(orden2==1)
        {
            //realizamos la transformacion para orden directo
            TranSistRefVC(parametros->epocas[pos2][1],
                          parametros->param[pos2][0],parametros->param[pos2][1],
                          parametros->param[pos2][2],parametros->param[pos2][3],
                          parametros->param[pos2][4],parametros->param[pos2][5],
                          parametros->param[pos2][6],
                        parametros->variac[pos2][0],parametros->variac[pos2][1],
                        parametros->variac[pos2][2],parametros->variac[pos2][3],
                        parametros->variac[pos2][4],parametros->variac[pos2][5],
                          parametros->variac[pos2][6],
                          t1,
                          xi,yi,zi,
                          vxi,vxiyi,vxizi,vyi,vyizi,vzi,
                          x2,y2,z2,
                          vx2,vx2y2,vx2z2,vy2,vy2z2,vz2);
        }
        else
        {
            //realizamos la transformacion para orden inverso
            //solo se diferencia de la transformacion directa en que los
            //parametros de transformacion y sus velocidades tienen signo
            //opuesto
            TranSistRefVC(parametros->epocas[pos2][1],
                        -parametros->param[pos2][0],-parametros->param[pos2][1],
                        -parametros->param[pos2][2],-parametros->param[pos2][3],
                        -parametros->param[pos2][4],-parametros->param[pos2][5],
                        -parametros->param[pos2][6],
                      -parametros->variac[pos2][0],-parametros->variac[pos2][1],
                      -parametros->variac[pos2][2],-parametros->variac[pos2][3],
                      -parametros->variac[pos2][4],-parametros->variac[pos2][5],
                      -parametros->variac[pos2][6],
                          t1,
                          xi,yi,zi,
                          vxi,vxiyi,vxizi,vyi,vyizi,vzi,
                          x2,y2,z2,
                          vx2,vx2y2,vx2z2,vy2,vy2z2,vz2);
        }
    }
    else
    {
        //las coordenadas del punto no cambian
        *x2 = x1;
        *y2 = y1;
        *z2 = z1;
        *vx2 = vx1;
        *vx2y2 = vx1y1;
        *vx2z2 = vx1z1;
        *vy2 = vy1;
        *vy2z2 = vy1z1;
        *vz2 = vz1;
        //actuelizamos el indicador de error
        cError = ERRNOTRANSF;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return cError;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
