/* -*- coding: utf-8 -*- */
/**
\ingroup gcblas
@{
\file errgcblas.h
\brief Definición de las macros de control de errores para \p GCBLAS.
\author José Luis García Pallero, jgpallero@gmail.com
\date 06 de julio de 2009
\version 1.0
\copyright
Copyright (c) 2009-2014, José Luis García Pallero. All rights reserved.
\par
Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:
\par
- Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.
- Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.
- Neither the name of the copyright holders nor the names of its contributors
  may be used to endorse or promote products derived from this software without
  specific prior written permission.
\par
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT,
INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/******************************************************************************/
/******************************************************************************/
#ifndef _ERRGCBLAS_H_
#define _ERRGCBLAS_H_
/******************************************************************************/
/******************************************************************************/
#include"auxgcblas.h"
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/**
\def ERR_GCBLAS_MAX
\brief Macro para seleccionar el valor máximo entre dos escalares.
\param[in] a Un número.
\param[in] b Otro número.
\return El mayor de los dos argumentos de entrada.
\date 16 de enero de 2014: Creación de la macro.
*/
#define ERR_GCBLAS_MAX(a,b) ((a)>(b) ? (a) : (b))
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_GEMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dgemv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dgemv.
\param[in] Order Argumento \em Order de la función \ref cblas_dgemv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dgemv.
\param[in] M Argumento \em M de la función \ref cblas_dgemv.
\param[in] N Argumento \em N de la función \ref cblas_dgemv.
\param[in] lda Argumento \em lda de la función \ref cblas_dgemv.
\param[in] incX Argumento \em incX de la función \ref cblas_dgemv.
\param[in] incY Argumento \em incY de la función \ref cblas_dgemv.
\todo Esta macro no está probada.
\date 06 de julio de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_GEMV(pos,Order,TransA,M,N,lda,incX,incY) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
          ((TransA)!=CblasConjTrans)) { \
    (pos) = 2; \
} else if((M)<0) { \
    (pos) = 3; \
} else if((N)<0) { \
    (pos) = 4; \
} else if((Order)==CblasRowMajor) { \
    if((lda)<ERR_GCBLAS_MAX(1,(N))) { \
        (pos) = 7; \
    } \
} else if((Order)==CblasColMajor) { \
    if((lda)<ERR_GCBLAS_MAX(1,(M))) { \
        (pos) = 7; \
    } \
} else if((incX)==0) { \
    (pos) = 9; \
} else if((incY)==0) { \
    (pos) = 12; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_GBMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dgbmv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dgbmv.
\param[in] Order Argumento \em Order de la función \ref cblas_dgbmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dgbmv.
\param[in] M Argumento \em M de la función \ref cblas_dgbmv.
\param[in] N Argumento \em N de la función \ref cblas_dgbmv.
\param[in] KL Argumento \em KL de la función \ref cblas_dgbmv.
\param[in] KU Argumento \em KU de la función \ref cblas_dgbmv.
\param[in] lda Argumento \em lda de la función \ref cblas_dgbmv.
\param[in] incX Argumento \em incX de la función \ref cblas_dgbmv.
\param[in] incY Argumento \em incY de la función \ref cblas_dgbmv.
\todo Esta macro no está probada.
\date 19 de septiembre de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_GBMV(pos,Order,TransA,M,N,KL,KU,lda,incX,incY) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
          ((TransA)!=CblasConjTrans)) { \
    (pos) = 2; \
} else if((M)<0) { \
    (pos) = 3; \
} else if((N)<0) { \
    (pos) = 4; \
} else if((KL)<0) { \
    (pos) = 5; \
} else if((KU)<0) { \
    (pos) = 6; \
} else if((lda)<ERR_GCBLAS_MAX(1,((KL)+(KU)+1))) { \
    (pos) = 9; \
} else if((incX)==0) { \
    (pos) = 11; \
} else if((incY)==0) { \
    (pos) = 14; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SYMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsymv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsymv.
\param[in] Order Argumento \em Order de la función \ref cblas_dsymv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsymv.
\param[in] N Argumento \em N de la función \ref cblas_dsymv.
\param[in] lda Argumento \em lda de la función \ref cblas_dsymv.
\param[in] incX Argumento \em incX de la función \ref cblas_dsymv.
\param[in] incY Argumento \em incY de la función \ref cblas_dsymv.
\todo Esta macro no está probada.
\date 09 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SYMV(pos,Order,Uplo,N,lda,incX,incY) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((lda)<ERR_GCBLAS_MAX(1,(N))) { \
    (pos) = 6; \
} else if((incX)==0) { \
    (pos) = 8; \
} else if((incX)==0) { \
    (pos) = 11; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SBMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsbmv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsbmv.
\param[in] Order Argumento \em Order de la función \ref cblas_dsbmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsbmv.
\param[in] N Argumento \em N de la función \ref cblas_dsbmv.
\param[in] K Argumento \em K de la función \ref cblas_dsbmv.
\param[in] lda Argumento \em lda de la función \ref cblas_dsbmv.
\param[in] incX Argumento \em incX de la función \ref cblas_dsbmv.
\param[in] incY Argumento \em incY de la función \ref cblas_dsbmv.
\todo Esta macro no está probada.
\date 11 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SBMV(pos,Order,Uplo,N,K,lda,incX,incY) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((K)<0) { \
    (pos) = 4; \
} else if((lda)<ERR_GCBLAS_MAX(1,((K)+1))) { \
    (pos) = 7; \
} else if((incX)==0) { \
    (pos) = 9; \
} else if((incX)==0) { \
    (pos) = 12; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SPMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dspmv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dspmv.
\param[in] Order Argumento \em Order de la función \ref cblas_dspmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dspmv.
\param[in] N Argumento \em N de la función \ref cblas_dspmv.
\param[in] incX Argumento \em incX de la función \ref cblas_dspmv.
\param[in] incY Argumento \em incY de la función \ref cblas_dspmv.
\todo Esta macro no está probada.
\date 11 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SPMV(pos,Order,Uplo,N,incX,incY) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((incX)==0) { \
    (pos) = 7; \
} else if((incX)==0) { \
    (pos) = 10; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TRMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtrmv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtrmv.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrmv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrmv.
\param[in] N Argumento \em N de la función \ref cblas_dtrmv.
\param[in] lda Argumento \em lda de la función \ref cblas_dtrmv.
\param[in] incX Argumento \em incX de la función \ref cblas_dtrmv.
\todo Esta macro no está probada.
\date 04 de agosto de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_TRMV(pos,Order,Uplo,TransA,Diag,N,lda,incX) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
          ((TransA)!=CblasConjTrans)) { \
    (pos) = 3; \
} else if(((Diag)!=CblasNonUnit)&&((Diag)!=CblasUnit)) { \
    (pos) = 4; \
} else if((N)<0) { \
    (pos) = 5; \
} else if((lda)<ERR_GCBLAS_MAX(1,(N))) { \
    (pos) = 7; \
} else if((incX)==0) { \
    (pos) = 9; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TBMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtbmv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtbmv.
\param[in] Order Argumento \em Order de la función \ref cblas_dtbmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtbmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtbmv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtbmv.
\param[in] N Argumento \em N de la función \ref cblas_dtbmv.
\param[in] K Argumento \em K de la función \ref cblas_dtbmv.
\param[in] lda Argumento \em lda de la función \ref cblas_dtbmv.
\param[in] incX Argumento \em incX de la función \ref cblas_dtbmv.
\todo Esta macro no está probada.
\date 26 de septiembre de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_TBMV(pos,Order,Uplo,TransA,Diag,N,K,lda,incX) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
          ((TransA)!=CblasConjTrans)) { \
    (pos) = 3; \
} else if(((Diag)!=CblasNonUnit)&&((Diag)!=CblasUnit)) { \
    (pos) = 4; \
} else if((N)<0) { \
    (pos) = 5; \
} else if((K)<0) { \
    (pos) = 6; \
} else if((lda)<ERR_GCBLAS_MAX(1,((K)+1))) { \
    (pos) = 8; \
} else if((incX)==0) { \
    (pos) = 10; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TPMV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtpmv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtpmv.
\param[in] Order Argumento \em Order de la función \ref cblas_dtpmv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtpmv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtpmv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtpmv.
\param[in] N Argumento \em N de la función \ref cblas_dtpmv.
\param[in] incX Argumento \em incX de la función \ref cblas_dtpmv.
\todo Esta macro no está probada.
\date 03 de agosto de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_TPMV(pos,Order,Uplo,TransA,Diag,N,incX) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
          ((TransA)!=CblasConjTrans)) { \
    (pos) = 3; \
} else if(((Diag)!=CblasNonUnit)&&((Diag)!=CblasUnit)) { \
    (pos) = 4; \
} else if((N)<0) { \
    (pos) = 5; \
} else if((incX)==0) { \
    (pos) = 8; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TRSV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtrsv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtrsv.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrsv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrsv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrsv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrsv.
\param[in] N Argumento \em N de la función \ref cblas_dtrsv.
\param[in] lda Argumento \em lda de la función \ref cblas_dtrsv.
\param[in] incX Argumento \em incX de la función \ref cblas_dtrsv.
\todo Esta macro no está probada.
\date 05 de agosto de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_TRSV(pos,Order,Uplo,TransA,Diag,N,lda,incX) \
    GCBLAS_ERROR_TRMV((pos),(Order),(Uplo),(TransA),(Diag),(N),(lda),(incX))
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TBSV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtbsv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtbsv.
\param[in] Order Argumento \em Order de la función \ref cblas_dtbsv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtbsv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtbsv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtbsv.
\param[in] N Argumento \em N de la función \ref cblas_dtbsv.
\param[in] K Argumento \em K de la función \ref cblas_dtbsv.
\param[in] lda Argumento \em lda de la función \ref cblas_dtbsv.
\param[in] incX Argumento \em incX de la función \ref cblas_dtbsv.
\todo Esta macro no está probada.
\date 06 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_TBSV(pos,Order,Uplo,TransA,Diag,N,K,lda,incX) \
  GCBLAS_ERROR_TBMV((pos),(Order),(Uplo),(TransA),(Diag),(N),(K),(lda),(incX))
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TPSV
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtpsv.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtpsv.
\param[in] Order Argumento \em Order de la función \ref cblas_dtpsv.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtpsv.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtpsv.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtpsv.
\param[in] N Argumento \em N de la función \ref cblas_dtpsv.
\param[in] incX Argumento \em incX de la función \ref cblas_dtpsv.
\todo Esta macro no está probada.
\date 05 de agosto de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_TPSV(pos,Order,Uplo,TransA,Diag,N,incX) \
    GCBLAS_ERROR_TPMV((pos),(Order),(Uplo),(TransA),(Diag),(N),(incX))
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_GER
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dger.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dger.
\param[in] Order Argumento \em Order de la función \ref cblas_dger.
\param[in] M Argumento \em M de la función \ref cblas_dger.
\param[in] N Argumento \em N de la función \ref cblas_dger.
\param[in] incX Argumento \em incX de la función \ref cblas_dger.
\param[in] incY Argumento \em incY de la función \ref cblas_dger.
\param[in] lda Argumento \em lda de la función \ref cblas_dger.
\todo Esta macro no está probada.
\date 06 de julio de 2009: Creación de la macro.
*/
#define GCBLAS_ERROR_GER(pos,Order,M,N,incX,incY,lda) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if((M)<0) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((incX)==0) { \
    (pos) = 6; \
} else if((incY)==0) { \
    (pos) = 8; \
} else if((Order)==CblasRowMajor) { \
    if((lda)<ERR_GCBLAS_MAX(1,(N))) { \
        (pos) = 10; \
    } \
} else if((Order)==CblasColMajor) { \
    if((lda)<ERR_GCBLAS_MAX(1,(M))) { \
        (pos) = 10; \
    } \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SYR
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsyr.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsyr.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyr.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyr.
\param[in] N Argumento \em N de la función \ref cblas_dsyr.
\param[in] incX Argumento \em incX de la función \ref cblas_dsyr.
\param[in] lda Argumento \em lda de la función \ref cblas_dsyr.
\todo Esta macro no está probada.
\date 12 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SYR(pos,Order,Uplo,N,incX,lda) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((incX)==0) { \
    (pos) = 6; \
} else if((lda)<ERR_GCBLAS_MAX(1,(N))) { \
    (pos) = 8; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SPR
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dspr.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dspr.
\param[in] Order Argumento \em Order de la función \ref cblas_dspr.
\param[in] Uplo Argumento \em M de la función \ref cblas_dspr.
\param[in] N Argumento \em N de la función \ref cblas_dspr.
\param[in] incX Argumento \em incX de la función \ref cblas_dspr.
\todo Esta macro no está probada.
\date 12 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SPR(pos,Order,Uplo,N,incX) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((incX)==0) { \
    (pos) = 6; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SYR2
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsyr2.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsyr2.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyr2.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyr2.
\param[in] N Argumento \em N de la función \ref cblas_dsyr2.
\param[in] incX Argumento \em incX de la función \ref cblas_dsyr2.
\param[in] incY Argumento \em incY de la función \ref cblas_dsyr2.
\param[in] lda Argumento \em lda de la función \ref cblas_dsyr2.
\todo Esta macro no está probada.
\date 12 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SYR2(pos,Order,Uplo,N,incX,incY,lda) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((incX)==0) { \
    (pos) = 6; \
} else if((incY)==0) { \
    (pos) = 8; \
} else if((lda)<ERR_GCBLAS_MAX(1,(N))) { \
    (pos) = 10; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SPR2
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dspr2.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dspr2.
\param[in] Order Argumento \em Order de la función \ref cblas_dspr2.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dspr2.
\param[in] N Argumento \em N de la función \ref cblas_dspr2.
\param[in] incX Argumento \em incX de la función \ref cblas_dspr2.
\param[in] incY Argumento \em incY de la función \ref cblas_dspr2.
\todo Esta macro no está probada.
\date 12 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SPR2(pos,Order,Uplo,N,incX,incY) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
} else if((incX)==0) { \
    (pos) = 6; \
} else if((incY)==0) { \
    (pos) = 8; \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_GEMM
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dgemm.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dgemm.
\param[in] Order Argumento \em Order de la función \ref cblas_dgemm.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dgemm.
\param[in] TransB Argumento \em TransB de la función \ref cblas_dgemm.
\param[in] M Argumento \em M de la función \ref cblas_dgemm.
\param[in] N Argumento \em N de la función \ref cblas_dgemm.
\param[in] K Argumento \em K de la función \ref cblas_dgemm.
\param[in] lda Argumento \em lda de la función \ref cblas_dgemm.
\param[in] ldb Argumento \em ldb de la función \ref cblas_dgemm.
\param[in] ldc Argumento \em ldc de la función \ref cblas_dgemm.
\todo Esta macro no está probada.
\date 06 de julio de 2009: Creación de la macro.
\date 12 de octubre de 2010: Corrección de error en el chequeo de \em ldb.
*/
#define GCBLAS_ERROR_GEMM(pos,Order,TransA,TransB,M,N,K,lda,ldb,ldc) \
{ \
    enum CBLAS_TRANSPOSE __trasF=CblasNoTrans,__trasG=CblasNoTrans; \
    if((Order)==CblasRowMajor) { \
        __trasF = ((TransA)!=CblasConjTrans) ? (TransA) : CblasTrans; \
        __trasG = ((TransB)!=CblasConjTrans) ? (TransB) : CblasTrans; \
    } else { \
        __trasF = ((TransB)!=CblasConjTrans) ? (TransB) : CblasTrans; \
        __trasG = ((TransA)!=CblasConjTrans) ? (TransA) : CblasTrans; \
    } \
    (pos) = 0; \
    if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
        (pos) = 1; \
    } else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
              ((TransA)!=CblasConjTrans)) { \
        (pos) = 2; \
    } else if(((TransB)!=CblasNoTrans)&&((TransB)!=CblasTrans)&& \
              ((TransB)!=CblasConjTrans)) { \
        (pos) = 3; \
    } else if((M)<0) { \
        (pos) = 4; \
    } else if((N)<0) { \
        (pos) = 5; \
    } else if((K)<0) { \
        (pos) = 6; \
    } else if((Order)==CblasRowMajor) { \
        if(__trasF==CblasNoTrans) { \
            if((lda)<ERR_GCBLAS_MAX(1,(K))) { \
                (pos) = 9; \
            } \
        } else { \
            if((lda)<ERR_GCBLAS_MAX(1,(M))) { \
                (pos) = 9; \
            } \
        } \
        if(__trasG==CblasNoTrans) { \
            if((ldb)<ERR_GCBLAS_MAX(1,(N))) { \
                (pos) = 11; \
            } \
        } else { \
            if((ldb)<ERR_GCBLAS_MAX(1,(K))) { \
                (pos) = 11; \
            } \
        } \
        if((ldc)<ERR_GCBLAS_MAX(1,(N))) { \
            (pos) = 14; \
        } \
    } else if((Order)==CblasColMajor) { \
        if(__trasF==CblasNoTrans) { \
            if((ldb)<ERR_GCBLAS_MAX(1,(K))) { \
                (pos) = 11; \
            } \
        } else { \
            if((ldb)<ERR_GCBLAS_MAX(1,(N))) { \
                (pos) = 11; \
            } \
        } \
        if(__trasG==CblasNoTrans) { \
            if((lda)<ERR_GCBLAS_MAX(1,(M))) { \
                (pos) = 9; \
            } \
        } else { \
            if((lda)<ERR_GCBLAS_MAX(1,(K))) { \
                (pos) = 9; \
            } \
        } \
        if((ldc)<ERR_GCBLAS_MAX(1,(M))) { \
            (pos) = 14; \
        } \
    } \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SYMM
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsymm.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsymm.
\param[in] Order Argumento \em Order de la función \ref cblas_dsymm.
\param[in] Side Argumento \em Side de la función \ref cblas_dsymm.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsymm.
\param[in] M Argumento \em M de la función \ref cblas_dsymm.
\param[in] N Argumento \em N de la función \ref cblas_dsymm.
\param[in] lda Argumento \em lda de la función \ref cblas_dsymm.
\param[in] ldb Argumento \em ldb de la función \ref cblas_dsymm.
\param[in] ldc Argumento \em ldc de la función \ref cblas_dsymm.
\todo Esta macro no está probada.
\date 13 de marzo de 2010: Creación de la macro.
\date 12 de octubre de 2010: Corrección de error en el chequeo de las
      dimensiones de \em A.
*/
#define GCBLAS_ERROR_SYMM(pos,Order,Side,Uplo,M,N,lda,ldb,ldc) \
{ \
    gblas_int __dimA=0; \
    if((Side)==CblasLeft) { \
        __dimA = (M); \
    } else { \
        __dimA = (N); \
    } \
    (pos) = 0; \
    if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
        (pos) = 1; \
    } else if(((Side)!=CblasLeft)&&((Side)!=CblasRight)) { \
        (pos) = 2; \
    } else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
        (pos) = 3; \
    } else if((M)<0) { \
        (pos) = 4; \
    } else if((N)<0) { \
        (pos) = 5; \
    } else if((lda)<ERR_GCBLAS_MAX(1,__dimA)) { \
        (pos) = 8; \
    } else if((Order)==CblasRowMajor) { \
        if((ldb)<ERR_GCBLAS_MAX(1,(N))) { \
                (pos) = 10; \
        } \
        if((ldc)<ERR_GCBLAS_MAX(1,(N))) { \
                (pos) = 13; \
        } \
    } else if((Order)==CblasColMajor) { \
        if((ldb)<ERR_GCBLAS_MAX(1,(M))) { \
                (pos) = 10; \
        } \
        if((ldc)<ERR_GCBLAS_MAX(1,(M))) { \
                (pos) = 13; \
        } \
    } \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SYRK
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsyrk.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsyrk.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyrk.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyrk.
\param[in] Trans Argumento \em Trans de la función \ref cblas_dsyrk.
\param[in] N Argumento \em N de la función \ref cblas_dsyrk.
\param[in] K Argumento \em K de la función \ref cblas_dsyrk.
\param[in] lda Argumento \em lda de la función \ref cblas_dsyrk.
\param[in] ldc Argumento \em ldc de la función \ref cblas_dsyrk.
\todo Esta macro no está probada.
\date 13 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SYRK(pos,Order,Uplo,Trans,N,K,lda,ldc) \
{ \
    gblas_int __dimA=0; \
    if((Order)==CblasRowMajor) { \
        if((Trans)==CblasNoTrans) { \
            __dimA = (K); \
        } else { \
            __dimA = (N); \
        } \
    } else { \
        if((Trans)==CblasNoTrans) { \
            __dimA = (N); \
        } else { \
            __dimA = (K); \
        } \
    } \
    (pos) = 0; \
    if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
        (pos) = 1; \
    } else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
        (pos) = 2; \
    } else if(((Trans)!=CblasNoTrans)&&((Trans)!=CblasTrans)&& \
              ((Trans)!=CblasConjTrans)) { \
        (pos) = 3; \
    } else if((N)<0) { \
        (pos) = 4; \
    } else if((K)<0) { \
        (pos) = 5; \
    } \
    if((lda)<ERR_GCBLAS_MAX(1,__dimA)) { \
        (pos) = 8; \
    } \
    if((ldc)<ERR_GCBLAS_MAX(1,(N))) { \
        (pos) = 11; \
    } \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_SYR2K
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dsyr2k.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dsyr2k.
\param[in] Order Argumento \em Order de la función \ref cblas_dsyr2k.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dsyr2k.
\param[in] Trans Argumento \em Trans de la función \ref cblas_dsyr2k.
\param[in] N Argumento \em N de la función \ref cblas_dsyr2k.
\param[in] K Argumento \em K de la función \ref cblas_dsyr2k.
\param[in] lda Argumento \em lda de la función \ref cblas_dsyr2k.
\param[in] ldb Argumento \em ldb de la función \ref cblas_dsyr2k.
\param[in] ldc Argumento \em ldc de la función \ref cblas_dsyr2k.
\todo Esta macro no está probada.
\date 13 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_SYR2K(pos,Order,Uplo,Trans,N,K,lda,ldb,ldc) \
{ \
    gblas_int __dim=0; \
    if((Order)==CblasRowMajor) { \
        if((Trans)==CblasNoTrans) { \
            __dim = (K); \
        } else { \
            __dim = (N); \
        } \
    } else { \
        if((Trans)==CblasNoTrans) { \
            __dim = (N); \
        } else { \
            __dim = (K); \
        } \
    } \
    (pos) = 0; \
    if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
        (pos) = 1; \
    } else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
        (pos) = 2; \
    } else if(((Trans)!=CblasNoTrans)&&((Trans)!=CblasTrans)&& \
              ((Trans)!=CblasConjTrans)) { \
        (pos) = 3; \
    } else if((N)<0) { \
        (pos) = 4; \
    } else if((K)<0) { \
        (pos) = 5; \
    } \
    if((lda)<ERR_GCBLAS_MAX(1,__dim)) { \
        (pos) = 8; \
    } \
    if((ldb)<ERR_GCBLAS_MAX(1,__dim)) { \
        (pos) = 11; \
    } \
    if((ldc)<ERR_GCBLAS_MAX(1,(N))) { \
        (pos) = 14; \
    } \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TRMM
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtrmm.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtrmm.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrmm.
\param[in] Side Argumento \em Side de la función \ref cblas_dtrmm.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrmm.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrmm.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrmm.
\param[in] M Argumento \em M de la función \ref cblas_dtrmm.
\param[in] N Argumento \em N de la función \ref cblas_dtrmm.
\param[in] lda Argumento \em lda de la función \ref cblas_dtrmm.
\param[in] ldb Argumento \em ldb de la función \ref cblas_dtrmm.
\todo Esta macro no está probada.
\date 13 de marzo de 2010: Creación de la macro.
\date 12 de octubre de 2010: Corrección de error en el chequeo de \em lda.
*/
#define GCBLAS_ERROR_TRMM(pos,Order,Side,Uplo,TransA,Diag,M,N,lda,ldb) \
{ \
    gblas_int __dim=0; \
    if((Side)==CblasLeft) { \
        __dim = (M); \
    } else { \
        __dim = (N); \
    } \
    (pos) = 0; \
    if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
        (pos) = 1; \
    } else if(((Side)!=CblasLeft)&&((Side)!=CblasRight)) { \
        (pos) = 2; \
    } else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
        (pos) = 3; \
    } else if(((TransA)!=CblasNoTrans)&&((TransA)!=CblasTrans)&& \
              ((TransA)!=CblasConjTrans)) { \
        (pos) = 4; \
    } else if(((Diag)!=CblasNonUnit)&&((Diag)!=CblasUnit)) { \
        (pos) = 5; \
    } else if((M)<0) { \
        (pos) = 6; \
    } else if((N)<0) { \
        (pos) = 7; \
    } \
    if((lda)<ERR_GCBLAS_MAX(1,__dim)) { \
        (pos) = 10; \
    } \
    if((Order)==CblasRowMajor) { \
        if((ldb)<ERR_GCBLAS_MAX(1,(N))) { \
            (pos) = 12; \
        } \
    } else { \
        if((ldb)<ERR_GCBLAS_MAX(1,(M))) { \
            (pos) = 12; \
        } \
    } \
}
/******************************************************************************/
/******************************************************************************/
/**
\def GCBLAS_ERROR_TRSM
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref cblas_dtrsm.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref cblas_dtrsm.
\param[in] Order Argumento \em Order de la función \ref cblas_dtrsm.
\param[in] Side Argumento \em Side de la función \ref cblas_dtrsm.
\param[in] Uplo Argumento \em Uplo de la función \ref cblas_dtrsm.
\param[in] TransA Argumento \em TransA de la función \ref cblas_dtrsm.
\param[in] Diag Argumento \em Diag de la función \ref cblas_dtrsm.
\param[in] M Argumento \em M de la función \ref cblas_dtrsm.
\param[in] N Argumento \em N de la función \ref cblas_dtrsm.
\param[in] lda Argumento \em lda de la función \ref cblas_dtrsm.
\param[in] ldb Argumento \em ldb de la función \ref cblas_dtrsm.
\todo Esta macro no está probada.
\date 18 de marzo de 2010: Creación de la macro.
*/
#define GCBLAS_ERROR_TRSM(pos,Order,Side,Uplo,TransA,Diag,M,N,lda,ldb) \
    GCBLAS_ERROR_TRMM((pos),(Order),(Side),(Uplo),(TransA),(Diag),(M),(N), \
                      (lda),(ldb))
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/** @} */
/******************************************************************************/
/******************************************************************************/
/* kate: encoding utf-8; end-of-line unix; syntax c; indent-mode cstyle; */
/* kate: replace-tabs on; space-indent on; tab-indents off; indent-width 4; */
/* kate: line-numbers on; folding-markers on; remove-trailing-space on; */
/* kate: backspace-indents on; show-tabs on; */
/* kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off; */
