/**
\addtogroup puntos
\ingroup deformacion
@{
\file posptos.h
\brief Declaracion de la estructura y funciones para la extraccion de datos de
       posicion de ficheros de base de datos de puntos.

En este fichero se declaran la estructura y todas las funciones necesarias para
la extraccion de datos de posicion de los bloques de puntos contenidos en un
fichero de base de datos de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _POSPTOS_H_
#define _POSPTOS_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\struct posptos
\brief Estructura contenedora de los datos de posicion de los puntos almacenados
       en un bloque de un fichero de base de datos de puntos.

En esta estructura se almacenan todos los elementos referentes a la posicion de
los puntos almacenados en un bloque de un fichero de base de datos de puntos. Se
almacenan el codigo del bloque de puntos elegidos, el codigo del sistema de
referencia al que pertenecen, la epoca a la que estan referidos, las coordenadas
y, si existen, las correspondientes matrices de varianza-covarianza.
*/
typedef struct
{
    /** \brief Numero de puntos que contiene el bloque elegido. */
    int nPuntos;
    /** \brief Codigos de los puntos que hay en el bloque elegido. */
    /**
    Array de cadenas de texto para almacenar los codigos de los puntos que hay
    en el bloque elegido del fichero de base de datos de puntos. La longitud
    asignada a cada cadena del array es igual a #LONMAXLINPTOS+1. Este array
    contiene tantas cadenas de texto como elementos tenga posptos::nPuntos. La
    numeracion de elementos comienza en 0.
    */
    char** puntos;
    /** \brief Codigo del sistema de referencia en el que se enmarca cada punto
               del bloque de trabajo. */
    /**
    Array de cadenas de texto para almacenar los codigos del sistema de
    referencia de cada punto que hay en el bloque elegido del fichero de base de
    datos de puntos. La longitud asignada a cada cadena del array es igual a
    #LONMAXLINPTOS+1. Este array contiene tantas cadenas de texto como elementos
    tenga posptos::nPuntos. Cada fila corresponde al punto almacenado en el
    mismo indice del array posptos::puntos. La numeracion de elementos comienza
    en 0.
    */
    char** sistRef;
    /** \brief Epoca de calculo de los puntos en formato anyo.decimal_de_anyo */
    /**
    Vector para almacenar la epoca a la que esta referido cada punto. Este array
    contiene tantas cadenas de texto como elementos tenga posptos::nPuntos. La
    numeracion de elementos comienza en 0.
    */
    double* epocas;
    /** \brief Coordenadas de los puntos. */
    /**
    Matriz de tres columnas que almacena las coordenadas de los puntos que hay
    en el bloque elegido de un fichero de base de datos de puntos. Esta matriz
    contiene tantas filas como elementos tenga posptos::nPuntos. Cada fila
    corresponde al punto almacenado en el mismo indice del array
    posptos::puntos. La numeracion de elementos comienza en 0.
    Las columnas corresponden a:
    - Col. 0: Coordenada X del punto.
    - Col. 1: Coordenada Y del punto.
    - Col. 2: Coordenada Z del punto.
    */
    double** pos;
    /** \brief Matriz de error asociada a las coordenadas de los puntos. */
    /**
    Matriz de seis columnas que almacena las varianzas y covarianzas asociadas a
    las coordenadas del punto de trabajo. Esta matriz contiene tantas filas como
    elementos tenga posptos::nPuntos. Cada fila corresponde al punto almacenado
    en el mismo indice del array posptos::puntos. La numeracion de elementos
    comienza en 0. Las columnas corresponden a:
    - Col. 0: Varianza de la coordenada X del punto.
    - Col. 1: Covarianza entre las coordenadas XY.
    - Col. 2: Covarianza entre las coordenadas XZ.
    - Col. 3: Varianza de la coordenada Y del punto.
    - Col. 4: Covarianza entre las coordenadas YZ.
    - Col. 5: Varianza de la coordenada Z del punto.

    \note Si para un punto determinado no existe matriz de varianza-covarianza,
          los elementos correspondientes en esta matriz almacenan el valor 0.0.
    */
    double** vcpos;
}posptos;
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Inicializa una estructura posptos.

Pone el campo posptos::nPuntos de la estructura a 0 e inicializa a NULL todos
los elementos de la estructura que sean punteros.
\param[in] posicion Puntero a una estructura posptos.
\note Es necesario inicializar una estructura posptos antes de su uso porque
      muchas funciones comprueban el estado del campo posptos::nPuntos para su
      correcto funcionamiento y si la estructura no se ha inicializado podrian
      producirse errores.
*/
void InicializaPosPtos(posptos* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Asigna memoria para un numero de elementos dado en una estructura
       posptos, anyadiendolos a los que ya existan previamente.
\param[in] posicion Puntero a una estructura posptos.
\param[in] elementos Numero de elementos para los cuales se asignara memoria.
\return Codigo de error.
\note La estructura posptos pasada ha de estar inicializada.
\note Esta funcion actualiza el campo posptos::nPuntos de la estructura.
*/
int AsignaMemoriaPosPtos(posptos* posicion,
                         int elementos);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Libera la memoria ocupada por una estructura posptos.
\param[in] posicion Puntero a una estructura posptos.
\note Ademas de liberar la memoria ocupada por los elementos que son punteros de
      la estructura, esta funcion pone a cero el campo posptos::nPuntos.
*/
void LiberaMemoriaPosPtos(posptos* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee las posiciones de un bloque de puntos almacenado en un fichero de
       base de datos de puntos.
\param[in] idFichero Identificador de fichero abierto para leer.
\param[in] extraeVarCov Indicador de extraccion de la matriz de
                        varianza-covarianza de las coordenadas de los puntos
                        almacenados en el fichero. Dos posibles valores:
                        - 0: No se extrae la matriz.
                        - 1: Si se extrae la matriz.
\param[in] posFich Direccion de memoria del inicio de la linea de comienzo del
                   bloque de puntos elegido.
\param[in] lineaIni Numero de linea del fichero en la que se encuentra la linea
                    de comienzo del bloque de puntos elegido.
\param[in] lineaFin Numero de linea del fichero en la que se encuentra la linea
                    de final del bloque de puntos elegido.
\param[out] posicion Puntero a una estructura posptos. Se almacenaran todos los
                     datos referentes al bloque de puntos solicitado.
\return Codigo de error.
\note Esta funcion no comprueba si los parametros de posicion y lineas de
      fichero son validos, por lo que es necesario que se le pasen valores
      correctos.
\note El puntero de L/E es devuelto a su posicion original en el fichero tras la
      ejecucion de esta funcion.
\note La estructura posptos pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura posptos pasada
      no es liberada y el fichero apuntado por idFichero no es cerrado.
*/
int LeeBloquePosPtos(FILE* idFichero,
                     int extraeVarCov,
                     long int posFich,
                     int lineaIni,
                     int lineaFin,
                     posptos* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lee las posiciones de un bloque de puntos almacenado en un fichero de
       base de datos de puntos.
\param[in] fichero Ruta completa del fichero de trabajo.
\param[in] bloque Nombre del bloque de puntos a extraer.
\param[in] extraeVarCov Indicador de extraccion de la matriz de
                        varianza-covarianza de las coordenadas de los puntos
                        almacenados en el fichero. Dos posibles valores:
                        - 0: No se extrae la matriz.
                        - 1: Si se extrae la matriz.
\param[out] posicion Puntero a una estructura posptos. Se almacenaran todos los
                     datos referentes al bloque de puntos solicitado.
\return Codigo de error.
\note La estructura posptos pasada ha de estar inicializada.
\note En el caso de producirse un error (codigo de error devuelto distinto de
      #ERRNOERROR) la posible memoria ocupada por la estructura posptos pasada
      no es liberada.
*/
int LeePosFicheroPuntos(char fichero[],
                        char bloque[],
                        int extraeVarCov,
                        posptos* posicion);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
