/**
\addtogroup general
@{
\file matriz.h
\brief Declaración de funciones para la inversión de una matriz simétrica
       definida positiva por el método de Cholesky según Lapack.

En este fichero se declaran las funciones necesarias para invertir una matriz
simétrica definida positiva almacenada en fomato empaquetado. Este código es
copiapega del que tengo hecho en LIBGEOC
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlg.pallero@upm.es
\date 7 de mayo de 2021
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifndef _MATRIZ_H_
#define _MATRIZ_H_
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdarg.h>
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"gcblas.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\def GEOCLPK_ERROR_PPTRF
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref gclapack_dpptrf.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref gclapack_dpptrf.
\param[in] Order Argumento \em Order de la función \ref gclapack_dpptrf.
\param[in] Uplo Argumento \em Uplo de la función \ref gclapack_dpptrf.
\param[in] N Argumento \em N de la función \ref gclapack_dpptrf.
\todo Esta macro no está probada.
\date 20 de marzo de 2010: Creación de la macro.
*/
#define GEOCLPK_ERROR_PPTRF(pos,Order,Uplo,N) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if((N)<0) { \
    (pos) = 3; \
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\def GEOCLPK_ERROR_PPTRI
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref gclapack_dpptri.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref gclapack_dpptri.
\param[in] Order Argumento \em Order de la función \ref gclapack_dpptri.
\param[in] Uplo Argumento \em Uplo de la función \ref gclapack_dpptri.
\param[in] N Argumento \em N de la función \ref gclapack_dpptri.
\todo Esta macro no está probada.
\date 24 de marzo de 2010: Creación de la macro.
*/
#define GEOCLPK_ERROR_PPTRI(pos,Order,Uplo,N) \
    GEOCLPK_ERROR_PPTRF((pos),(Order),(Uplo),(N))
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\def GEOCLPK_ERROR_TPTRI
\brief Macro para comprobar si es incorrecto alguno de los argumentos de entrada
       de la función \ref gclapack_dtptri.
\param[in,out] pos Variable para almacenar el valor calculado por la macro. Dos
               posibles valores:
               - 0, si todos los argumentos son correctos.
               - Distinto de 0, si algún argumento es incorrecto. El valor
                 devuelto es la posición del argumento incorrecto en la lista de
                 argumentos de entrada de la función \ref gclapack_dtptri.
\param[in] Order Argumento \em Order de la función \ref gclapack_dtptri.
\param[in] Uplo Argumento \em Uplo de la función \ref gclapack_dtptri.
\param[in] Diag Argumento \em Diag de la función \ref gclapack_dtptri.
\param[in] N Argumento \em N de la función \ref gclapack_dtptri.
\todo Esta macro no está probada.
\date 24 de marzo de 2010: Creación de la macro.
*/
#define GEOCLPK_ERROR_TPTRI(pos,Order,Uplo,Diag,N) \
(pos) = 0; \
if(((Order)!=CblasRowMajor)&&((Order)!=CblasColMajor)) { \
    (pos) = 1; \
} else if(((Uplo)!=CblasUpper)&&((Uplo)!=CblasLower)) { \
    (pos) = 2; \
} else if(((Diag)!=CblasNonUnit)&&((Diag)!=CblasUnit)) { \
    (pos) = 3; \
} else if((N)<0) { \
    (pos) = 4; \
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Lanza un mensaje de error y detiene el programa en ejecución.
\param[in] p Posición en la lista de argumentos de entrada del argumento que ha
           hecho que la función correspondiente produzca un error.
\param[in] rout Nombre de la función donde se ha producido el error.
\param[in] form Cadena de formato para la impresión de los argumentos
           opcionales.
\param[in] ... Argumentos opcionales, separados por comas y acordes con la
           cadena de formato pasada en el argumento \em form.
\note Esta función detiene la ejecución del programa en curso mediante la
      lamada a <p>exit(EXIT_FAILURE);</p>
\date 14 de agosto de 2010: Creación de la función.
*/
void gclapack_xerbla(gblas_int p,
                     const char *rout,
                     const char *form,
                     ...);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la factorización de Cholesky de una matriz simétrica almacenada
       en formato empaquetado.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz referenciada,
           referido a \em A. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em A (filas==columnas).
\param[in,out] Ap Puntero a la dirección de memoria donde comienza la matriz
               \em Ap, almacenada en formato empaquetado. Al término de la
               ejecución de la función almacena el factor de Cholesky indicado
               en \em Uplo, tal que:
               - Si \em Uplo es #CblasLower: El factor es \f$L\f$ y la matriz
                 original se reconstruye como \f$LL^T\f$.
               - Si \em Uplo es #CblasUpper: El factor es \f$U\f$ y la matriz
                 original se reconstruye como \f$U^TU\f$.
\return Código de error. Varias posibilidades:
        - Menor que 0: Uno de los argumentos de entrada es incorrecto. La
                       posición en la lista de entrada del argumento incorrecto
                       es el valor absoluto de la variable de salida de la
                       función.
        - 0: No ha ocurrido ningún error.
        - Mayor que 0: La matriz no es definida positiva y la factorización no
                       ha podido ser completada. El argumento de salida indica
                       el orden del menor de la matriz que no es definido
                       positivo.
\note Esta función ha sido adaptada de Lapack 3.2.1, dpptrf.f,
      (http://www.netlib.org/lapack).
\note Esta función pertenece al nivel \em computational \em routines de Lapack.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si el tamaño de la matriz de
      trabajo es congruente con las dimensiones indicadas.
\note Las causas de error pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que la matriz \em Ap no sea definida positiva.
\date 20 de marzo de 2010: Creación de la función.
\todo Esta función todavía no está probada.
*/
int gclapack_dpptrf(const enum CBLAS_ORDER Order,
                    const enum CBLAS_UPLO Uplo,
                    const gblas_int N,
                    double* Ap);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la inversa de una matriz simétrica, almacenada en formato
       empaquetado, definida positiva, a partir de su factorización de Cholesky.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz referenciada,
           referido a \em Ap. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_UPLO.
\param[in] N Dimensiones de la matriz \em Ap (filas==columnas).
\param[in,out] Ap Factor U o L , dependiendo de lo indicado en \em Uplo, de
               Cholesky de la matriz simétrica original. Al término de la
               ejecución de la función almacena la parte triangular indicada en
               \em Uplo de la matriz inversa.
\return Código de error. Varias posibilidades:
        - Menor que 0: Uno de los argumentos de entrada es incorrecto. La
                       posición en la lista de entrada del argumento incorrecto
                       es el valor absoluto de la variable de salida de la
                       función.
        - 0: No ha ocurrido ningún error.
        - Mayor que 0: El elemento de la diagonal indicado en la variable de
                       salida es exactamente 0.0, por lo que la matriz de
                       entrada es singular y no se puede invertir.
\note Esta función ha sido adaptada de Lapack 3.2.1, dpptri.f,
      (http://www.netlib.org/lapack).
\note Esta función pertenece al nivel \em computational \em routines de Lapack.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si el tamaño de la matriz de
      trabajo es congruente con las dimensiones indicadas.
\note Las causas de error pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em N sea menor que 0.
      - Que la matriz \em Ap no sea definida positiva.
\date 24 de marzo de 2010: Creación de la función.
\todo Esta función todavía no está probada.
*/
int gclapack_dpptri(const enum CBLAS_ORDER Order,
                    const enum CBLAS_UPLO Uplo,
                    const gblas_int N,
                    double* Ap);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/**
\brief Calcula la inversa de una matriz triangular, almacenada en formato
       empaquetado.
\param[in] Order Identificador del tipo de almacenamiento matricial utilizado.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_ORDER.
\param[in] Uplo Identificador de la parte triangular de la matriz referenciada,
           referido a \em Ap. Ha de ser un elemento perteneciente al tipo
           enumerado #CBLAS_UPLO.
\param[in] Diag Identificador del contenido de la diagonal de la matriz \em Ap.
           Ha de ser un elemento perteneciente al tipo enumerado #CBLAS_DIAG.
\param[in] N Dimensiones de la matriz \em Ap (filas==columnas).
\param[in,out] Ap Puntero a la dirección de memoria donde comienza la matriz
               \em Ap. Al término de la ejecución de la función almacena la
               inversa de la matriz original en la parte triangular indicada en
               \em Uplo.
\return Código de error. Varias posibilidades:
        - Menor que 0: Uno de los argumentos de entrada es incorrecto. La
                       posición en la lista de entrada del argumento incorrecto
                       es el valor absoluto de la variable de salida de la
                       función.
        - 0: No ha ocurrido ningún error.
        - Mayor que 0: El elemento de la diagonal indicado en la variable de
                       salida es exactamente 0.0, por lo que la matriz es
                       singular y no se puede invertir.
\note Si la variable \em N es igual a 0, no se realiza ninguna operación.
\note Esta función no comprueba internamente si el tamaño de la matriz de
      trabajo es congruente con las dimensiones indicadas.
\note Las causas de error pueden ser:
      - Que \em Order no pertenezca al tipo enumerado #CBLAS_ORDER.
      - Que \em Uplo no pertenezca al tipo enumerado #CBLAS_UPLO.
      - Que \em Diag no pertenezca al tipo enumerado #CBLAS_DIAG.
      - Que \em N sea menor que 0.
      - Que la matriz \em Ap sea singular.
\note Esta función ha sido adaptada de Lapack 3.2.1, dtptri.f,
      (http://www.netlib.org/lapack).
\note Esta función pertenece al nivel \em computational \em routines de Lapack.
\date 24 de marzo de 2010: Creación de la función.
\todo Esta función todavía no está probada.
*/
int gclapack_dtptri(const enum CBLAS_ORDER Order,
                    const enum CBLAS_UPLO Uplo,
                    const enum CBLAS_DIAG Diag,
                    const gblas_int N,
                    double* Ap);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#ifdef __cplusplus
}
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#endif
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
/** @} */
