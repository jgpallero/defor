#directorio base
# DIR:=$(shell pwd)
DIR=.
#compilador
CC=gcc
#flags generales del compilador
CFLAGS=-std=c99 -pedantic -Wall -Wextra -Wconversion -Wmissing-prototypes \
       -Wstrict-prototypes -Wshadow -Wpointer-arith -Wcast-align \
       -Wnested-externs -fno-common -fshort-enums
#flags de optimización del código
CFLAGS+=-O3 -funroll-loops
#flags para el centrino
# CFLAGS+=-mtune=pentium-m -mieee-fp
#flags de optimización para el ibook G4
# CFLAGS+=-mtune=G4
#empaquetador para crear la biblioteca
AR=ar
#flags del empaquetador
ARFLAGS=rcs
#doxygen
DOXY=doxygen
DOXYF=Doxyfile
#directorios de datos
DIRINC=$(DIR)/src/
DIRLIB=$(DIR)/src/
DIRSRC=$(DIR)/src/
DIRPRO=$(DIR)/prog/
DIRBIN=$(DIR)/
DIRDOC=$(DIR)/doc/
DIRDOO=$(DIRDOC)otradoc/
DIRDAT=$(DIR)/datos/
#flags de includes y bibliotecas
CFDIRINC=-I$(DIRINC)
CFDIRLIB=-L$(DIRLIB) -ldefor -lm

#ficheros objeto de la biblioteca defor
.PHONY: OBJS
OBJS=$(DIRLIB)errores.o $(DIRLIB)general.o $(DIRLIB)bloqsnx.o \
	 $(DIRLIB)possnx.o $(DIRLIB)statsnx.o $(DIRLIB)bloqptos.o \
	 $(DIRLIB)posptos.o $(DIRLIB)snxptos.o $(DIRLIB)bloqtran.o \
	 $(DIRLIB)partran.o $(DIRLIB)geodesia.o $(DIRLIB)deformac.o \
	 $(DIRLIB)dibgmt.o $(DIRLIB)gcblasd.o $(DIRLIB)gcblasx.o \
	 $(DIRLIB)coblasfx.o $(DIRLIB)coblasfd.o $(DIRLIB)matriz.o $(DIRLIB)series.o

.PHONY: all
all: $(DIRBIN)psnx $(DIRBIN)esnx $(DIRBIN)cptos $(DIRBIN)pptos $(DIRBIN)fptos \
	 $(DIRBIN)ptran $(DIRBIN)geotri $(DIRBIN)trigeo $(DIRBIN)geoutm \
	 $(DIRBIN)utmgeo $(DIRBIN)enutri $(DIRBIN)trienu $(DIRBIN)defor \
	 $(DIRBIN)dgmt $(DIRBIN)tsr $(DIRBIN)pvel1 $(DIRBIN)pvel2 $(DIRBIN)stemp

.PHONY: libdefor
libdefor: $(DIRLIB)libdefor.a

#dependencias de los ficheros de cabecera
$(DIRINC)bloqsnx.h: $(DIRINC)paramsnx.h
$(DIRINC)possnx.h: $(DIRINC)paramsnx.h $(DIRINC)matriz.h
$(DIRINC)statsnx.h: $(DIRINC)paramsnx.h
$(DIRINC)bloqtran.h: $(DIRINC)paramtran.h
$(DIRINC)partran.h: $(DIRINC)paramtran.h
$(DIRINC)snxptos.h: $(DIRINC)possnx.h $(DIRINC)posptos.h
$(DIRINC)snx.h: $(DIRINC)errores.h $(DIRINC)general.h $(DIRINC)paramsnx.h \
				$(DIRINC)bloqsnx.h $(DIRINC)possnx.h $(DIRINC)statsnx.h
$(DIRINC)ptos.h: $(DIRINC)errores.h $(DIRINC)general.h $(DIRINC)paramptos.h \
				 $(DIRINC)bloqptos.h $(DIRINC)posptos.h
$(DIRINC)tran.h: $(DIRINC)errores.h $(DIRINC)general.h $(DIRINC)geodesia.h \
				 $(DIRINC)paramtran.h $(DIRINC)bloqtran.h $(DIRINC)partran.h
$(DIRINC)deformac.h: $(DIRINC)ptos.h $(DIRINC)tran.h
$(DIRINC)dibgmt.h: $(DIRINC)errores.h
$(DIRINC)series.h: $(DIRINC)posptos.h $(DIRINC)geodesia.h $(DIRINC)general.h \
				   $(DIRINC)errores.h $(DIRINC)paramprg.h

#compilacion de la biblioteca defor
$(DIRLIB)libdefor.a: $(OBJS)
	$(AR) $(ARFLAGS) $(DIRLIB)libdefor.a $(OBJS)

$(DIRLIB)errores.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRSRC)errores.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)errores.c -o $(DIRLIB)errores.o

$(DIRLIB)general.o: $(DIR)/Makefile $(DIRINC)general.h $(DIRSRC)general.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)general.c -o $(DIRLIB)general.o

$(DIRLIB)bloqsnx.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					$(DIRINC)paramsnx.h $(DIRINC)bloqsnx.h $(DIRSRC)bloqsnx.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)bloqsnx.c -o $(DIRLIB)bloqsnx.o

$(DIRLIB)possnx.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
				   $(DIRINC)paramsnx.h $(DIRINC)bloqsnx.h $(DIRINC)possnx.h \
				   $(DIRSRC)possnx.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)possnx.c -o $(DIRLIB)possnx.o

$(DIRLIB)statsnx.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					$(DIRINC)paramsnx.h $(DIRINC)bloqsnx.h $(DIRINC)statsnx.h \
					$(DIRSRC)statsnx.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)statsnx.c -o $(DIRLIB)statsnx.o

$(DIRLIB)bloqptos.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					 $(DIRINC)paramptos.h $(DIRINC)bloqptos.h \
					 $(DIRSRC)bloqptos.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)bloqptos.c -o $(DIRLIB)bloqptos.o

$(DIRLIB)posptos.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					$(DIRINC)paramptos.h $(DIRINC)bloqptos.h \
					$(DIRINC)posptos.h $(DIRSRC)posptos.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)posptos.c -o $(DIRLIB)posptos.o

$(DIRLIB)snxptos.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					$(DIRINC)paramsnx.h $(DIRINC)possnx.h $(DIRINC)posptos.h \
					$(DIRINC)snxptos.h $(DIRSRC)snxptos.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)snxptos.c -o $(DIRLIB)snxptos.o

$(DIRLIB)bloqtran.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					 $(DIRINC)paramtran.h $(DIRINC)bloqtran.h \
					 $(DIRSRC)bloqtran.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)bloqtran.c -o $(DIRLIB)bloqtran.o

$(DIRLIB)partran.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					$(DIRINC)paramtran.h $(DIRINC)bloqtran.h \
					$(DIRINC)partran.h $(DIRSRC)partran.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)partran.c -o $(DIRLIB)partran.o

$(DIRLIB)geodesia.o: $(DIR)/Makefile $(DIRINC)geodesia.h $(DIRSRC)geodesia.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)geodesia.c -o $(DIRLIB)geodesia.o

$(DIRLIB)deformac.o: $(DIR)/Makefile $(DIRINC)errores.h $(DIRINC)general.h \
					 $(DIRINC)ptos.h $(DIRINC)tran.h $(DIRINC)geodesia.h \
					 $(DIRINC)deformac.h $(DIRSRC)deformac.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)deformac.c -o $(DIRLIB)deformac.o

$(DIRLIB)dibgmt.o: $(DIR)/Makefile $(DIRINC)general.h $(DIRINC)paramgmt.h \
				   $(DIRINC)dibgmt.h $(DIRSRC)dibgmt.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)dibgmt.c -o $(DIRLIB)dibgmt.o

$(DIRLIB)series.o: $(DIR)/Makefile $(DIRINC)posptos.h $(DIRINC)geodesia.h \
				   $(DIRINC)general.h $(DIRINC)errores.h $(DIRINC)paramprg.h \
				   $(DIRINC)series.h $(DIRSRC)series.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)series.c -o $(DIRLIB)series.o

$(DIRLIB)coblasfd.o: $(DIR)/Makefile $(DIRSRC)mnfblas.h $(DIRSRC)tdblas.h \
					 $(DIRSRC)coblasf.h $(DIRSRC)coblasfd.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)coblasfd.c -o $(DIRLIB)coblasfd.o

$(DIRLIB)coblasfx.o: $(DIR)/Makefile $(DIRSRC)coblasf.h $(DIRSRC)coblasfx.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)coblasfx.c -o $(DIRLIB)coblasfx.o

$(DIRLIB)gcblasd.o: $(DIR)/Makefile $(DIRSRC)auxgcblas.h $(DIRSRC)errgcblas.h \
					$(DIRSRC)gcoblasf.h $(DIRSRC)vcmoblas.h $(DIRSRC)gcblas.h \
					$(DIRSRC)gcblasd.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)gcblasd.c -o $(DIRLIB)gcblasd.o

$(DIRLIB)gcblasx.o: $(DIR)/Makefile $(DIRSRC)gcblas.h $(DIRSRC)gcblasx.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)gcblasx.c -o $(DIRLIB)gcblasx.o

$(DIRLIB)matriz.o: $(DIR)/Makefile $(DIRINC)matriz.h $(DIRSRC)matriz.c
	$(CC) $(CFLAGS) $(CFDIRINC) -c $(DIRSRC)matriz.c -o $(DIRLIB)matriz.o

#compilacion de los programas basados en la biblioteca defor
$(DIRBIN)psnx: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
			   $(DIRINC)snx.h $(DIRPRO)psnx.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)psnx.c -o $(DIRBIN)psnx $(CFDIRLIB)

$(DIRBIN)esnx: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
			   $(DIRINC)snx.h $(DIRPRO)esnx.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)esnx.c -o $(DIRBIN)esnx $(CFDIRLIB)

$(DIRBIN)cptos: $(DIRPRO)cptos.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)cptos.c -o $(DIRBIN)cptos

$(DIRBIN)pptos: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
				$(DIRINC)ptos.h $(DIRPRO)pptos.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)pptos.c -o $(DIRBIN)pptos $(CFDIRLIB)

$(DIRBIN)fptos: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)ptos.h \
				$(DIRPRO)fptos.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)fptos.c -o $(DIRBIN)fptos $(CFDIRLIB)

$(DIRBIN)ptran: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
				$(DIRINC)tran.h $(DIRPRO)ptran.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)ptran.c -o $(DIRBIN)ptran $(CFDIRLIB)

$(DIRBIN)geotri: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)geodesia.h \
				 $(DIRPRO)geotri.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)geotri.c -o $(DIRBIN)geotri $(CFDIRLIB)

$(DIRBIN)trigeo: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)geodesia.h \
				 $(DIRPRO)trigeo.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)trigeo.c -o $(DIRBIN)trigeo $(CFDIRLIB)

$(DIRBIN)geoutm: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)geodesia.h \
				 $(DIRPRO)geoutm.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)geoutm.c -o $(DIRBIN)geoutm $(CFDIRLIB)

$(DIRBIN)utmgeo: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)geodesia.h \
				 $(DIRPRO)utmgeo.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)utmgeo.c -o $(DIRBIN)utmgeo $(CFDIRLIB)

$(DIRBIN)enutri: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)geodesia.h \
				 $(DIRPRO)enutri.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)enutri.c -o $(DIRBIN)enutri $(CFDIRLIB)

$(DIRBIN)trienu: $(DIRLIB)libdefor.a $(DIRINC)paramprg.h $(DIRINC)geodesia.h \
				 $(DIRPRO)trienu.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)trienu.c -o $(DIRBIN)trienu $(CFDIRLIB)

$(DIRBIN)defor: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
				$(DIRINC)snx.h $(DIRINC)ptos.h $(DIRINC)tran.h \
				$(DIRINC)snxptos.h $(DIRINC)deformac.h $(DIRINC)geodesia.h \
				$(DIRINC)dibgmt.h $(DIRPRO)defor.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)defor.c -o $(DIRBIN)defor $(CFDIRLIB)

$(DIRBIN)dgmt: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
			   $(DIRINC)dibgmt.h $(DIRPRO)dgmt.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)dgmt.c -o $(DIRBIN)dgmt $(CFDIRLIB)

$(DIRBIN)tsr: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
			  $(DIRINC)snx.h $(DIRINC)ptos.h $(DIRINC)tran.h \
			  $(DIRINC)snxptos.h $(DIRINC)deformac.h $(DIRINC)geodesia.h \
			  $(DIRINC)dibgmt.h $(DIRPRO)tsr.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)tsr.c -o $(DIRBIN)tsr $(CFDIRLIB)

$(DIRBIN)pvel1: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
				$(DIRINC)geodesia.h $(DIRPRO)pvel1.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)pvel1.c -o $(DIRBIN)pvel1 $(CFDIRLIB)

$(DIRBIN)pvel2: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
				$(DIRINC)geodesia.h $(DIRPRO)pvel2.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)pvel2.c -o $(DIRBIN)pvel2 $(CFDIRLIB)

$(DIRBIN)stemp: $(DIRLIB)libdefor.a $(DIRINC)errores.h $(DIRINC)paramprg.h \
				$(DIRINC)ptos.h $(DIRINC)series.h $(DIRPRO)stemp.c
	$(CC) $(CFLAGS) $(CFDIRINC) $(DIRPRO)stemp.c -o $(DIRBIN)stemp $(CFDIRLIB)

#generacion de documentacion
.PHONY: doc
doc:
	cd $(DIRDOC); \
	$(DOXY) $(DOXYF); \
	cd ..

#limpieza de basura
.PHONY: clean
clean:
	rm -rf $(DIR)/*~
	rm -rf $(DIRINC)*~
	rm -rf $(DIRLIB)*.o $(DIRLIB)*~
	rm -rf $(DIRSRC)*~
	rm -rf $(DIRPRO)*~
	rm -rf $(DIRDOC)*~
	rm -rf $(DIRDOO)*~
	rm -rf $(DIRDAT)*~

#borrado de la biblioteca
.PHONY: cleanlib
cleanlib:
	rm -rf $(DIRLIB)*.o $(DIRLIB)*.a $(DIRLIB)*~

#borrado de los ejecutables
.PHONY: cleanprog
cleanprog:
	rm -rf $(DIRBIN)psnx*
	rm -rf $(DIRBIN)esnx*
	rm -rf $(DIRBIN)cptos*
	rm -rf $(DIRBIN)pptos*
	rm -rf $(DIRBIN)fptos*
	rm -rf $(DIRBIN)ptran*
	rm -rf $(DIRBIN)geotri*
	rm -rf $(DIRBIN)trigeo*
	rm -rf $(DIRBIN)geoutm*
	rm -rf $(DIRBIN)utmgeo*
	rm -rf $(DIRBIN)enutri*
	rm -rf $(DIRBIN)trienu*
	rm -rf $(DIRBIN)defor*
	rm -rf $(DIRBIN)dgmt*
	rm -rf $(DIRBIN)tsr*
	rm -rf $(DIRBIN)pvel1*
	rm -rf $(DIRBIN)pvel2*
	rm -rf $(DIRBIN)stemp*

#borrado de la documentacion
.PHONY: cleandoc
cleandoc:
	rm -rf $(DIRDOC)avisos
	rm -rf $(DIRDOC)html
	rm -rf $(DIRDOC)latex

#limpieza de toda la basura
.PHONY: cleanall
cleanall: clean cleanlib cleanprog cleandoc
#-------------------------------------------------------------------------------
# kate: encoding utf-8; end-of-line unix; syntax makefile; indent-mode cstyle;
# kate: replace-tabs off; space-indent off; tab-indents on; indent-width 4;
# kate: tab-width 4;
# kate: line-numbers on; folding-markers on; remove-trailing-space on;
# kate: backspace-indents on; show-tabs on;
# kate: word-wrap-column 80; word-wrap-marker-color #D2D2D2; word-wrap off;
