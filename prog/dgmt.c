/**
\file dgmt.c
\brief Programa para dibujar con la herramienta GMT (Generic Mapping Tools)
       deformaciónes en puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"dibgmt.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "dgmt"
//numero minimo de argumentos
#define MINARG 3
//numero maximo de argumentos
#define MAXARG 12
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //Código de error
    int cError=ERRNOERROR;
    //variable para recorrer bucles
    int i=0;
    //cadenas de formato para entrada
    char fte[LONFORMATO+2];
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //numero de puntos leidos
    int nPuntos=0;
    //parametros de entrada en formato vector
    char** vcod=NULL;
    double* vlat=NULL;
    double* vlon=NULL;
    double* vde=NULL;
    double* vdn=NULL;
    double* vdu=NULL;
    double* vse=NULL;
    double* vsn=NULL;
    double* vcen=NULL;
    double* vsu=NULL;
    //parametros de entrada en formato escalar
    char ecod[LONCOD+2];
    double elat=0.0,elon=0.0;
    double ede=0.0,edn=0.0,edu=0.0;
    double ese=0.0,esn=0.0,ecen=0.0,esu=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=MINARG)&&(argc!=MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if(argc==MINARG)
    {
        //compruebo si el argumento indicador de entrada por pipe es correcto
        if(strcmp(argv[1],FLAGPIPE))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Argumento %s incorrecto\n",argv[1]);
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //construyo el formato de entrada
        sprintf(fte,
                "%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           ecod,
                           &elat,&elon,
                           &ede,&edn,&edu,
                           &ese,&esn,&ecen,&esu);
            //compruebo que el numero de elementos leidos es correcto
            if(nElem==5)
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //actualizo el contador de puntos
            nPuntos++;
            //asigno memoria a los vectores para un punto mas
            //distingo entre asignar y reasignar memoria
            if(nPuntos==1)
            {
                vcod = (char**)malloc(sizeof(char*));
                vlat = (double*)malloc(sizeof(double));
                vlon = (double*)malloc(sizeof(double));
                vde = (double*)malloc(sizeof(double));
                vdn = (double*)malloc(sizeof(double));
                vdu = (double*)malloc(sizeof(double));
                vse = (double*)malloc(sizeof(double));
                vsn = (double*)malloc(sizeof(double));
                vcen = (double*)malloc(sizeof(double));
                vsu = (double*)malloc(sizeof(double));
            }
            else
            {
                vcod = (char**)realloc(vcod,(size_t)nPuntos*sizeof(char*));
                vlat = (double*)realloc(vlat,(size_t)nPuntos*sizeof(double));
                vlon = (double*)realloc(vlon,(size_t)nPuntos*sizeof(double));
                vde = (double*)realloc(vde,(size_t)nPuntos*sizeof(double));
                vdn = (double*)realloc(vdn,(size_t)nPuntos*sizeof(double));
                vdu = (double*)realloc(vdu,(size_t)nPuntos*sizeof(double));
                vse = (double*)realloc(vse,(size_t)nPuntos*sizeof(double));
                vsn = (double*)realloc(vsn,(size_t)nPuntos*sizeof(double));
                vcen = (double*)realloc(vcen,(size_t)nPuntos*sizeof(double));
                vsu = (double*)realloc(vsu,(size_t)nPuntos*sizeof(double));
            }
            vcod[nPuntos-1] = (char*)malloc(LONCOD+2);
            //compruebo si ha ocurrido algun error en la asignacion de memoria
            if((vcod==NULL)||(vlat==NULL)||(vlon==NULL)||(vde==NULL)||
               (vdn==NULL)||(vdu==NULL)||(vse==NULL)||(vsn==NULL)||
               (vcen==NULL)||(vsu==NULL))
            {
                //lanzamos un mensaje de error
                fprintf(stderr,"\n");
                fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
                fprintf(stderr,"            Error en la asignación dinámica\n");
                fprintf(stderr,"            de memoria\n");
                fprintf(stderr,"            El programa finalizará\n");
                fprintf(stderr,"\n");
                //libero la posible memoria asignada
                for(i=0;i<nPuntos;i++)
                {
                    free(vcod[i]);
                }
                free(vcod);
                free(vlat);
                free(vlon);
                free(vde);
                free(vdn);
                free(vdu);
                free(vse);
                free(vsn);
                free(vcen);
                free(vsu);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            if(vcod[nPuntos-1]==NULL)
            {
                //lanzamos un mensaje de error
                fprintf(stderr,"\n");
                fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
                fprintf(stderr,"            Error en la asignación dinámica\n");
                fprintf(stderr,"            de memoria\n");
                fprintf(stderr,"            El programa finalizará\n");
                fprintf(stderr,"\n");
                //libero la posible memoria asignada
                for(i=0;i<nPuntos;i++)
                {
                    free(vcod[i]);
                }
                free(vcod);
                free(vlat);
                free(vlon);
                free(vde);
                free(vdn);
                free(vdu);
                free(vse);
                free(vsn);
                free(vcen);
                free(vsu);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //asigno la entrada a los vectores
            strcpy(vcod[nPuntos-1],ecod);
            vlat[nPuntos-1] = elat;
            vlon[nPuntos-1] = elon;
            vde[nPuntos-1] = ede;
            vdn[nPuntos-1] = edn;
            vdu[nPuntos-1] = edu;
            vse[nPuntos-1] = ese;
            vsn[nPuntos-1] = esn;
            vcen[nPuntos-1] = ecen;
            vsu[nPuntos-1] = esu;
        }
        //creamos el script de dibujo
        cError = DibujaDeformacionGMT(stdout,
                                      argv[2],
                                      nPuntos,
                                      vcod,
                                      vlat,vlon,
                                      vde,vdn,vdu,
                                      vse,vsn,vcen,vsu);
        //comprobamos el código de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            for(i=0;i<nPuntos;i++)
            {
                free(vcod[i]);
            }
            free(vcod);
            free(vlat);
            free(vlon);
            free(vde);
            free(vdn);
            free(vdu);
            free(vse);
            free(vsn);
            free(vcen);
            free(vsu);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        //marcamos el contador de puntos a 1
        nPuntos = 1;
        //asignamos memoria a los vectores para un elemento
        vcod = (char**)malloc(sizeof(char*));
        vlat = (double*)malloc(sizeof(double));
        vlon = (double*)malloc(sizeof(double));
        vde = (double*)malloc(sizeof(double));
        vdn = (double*)malloc(sizeof(double));
        vdu = (double*)malloc(sizeof(double));
        vse = (double*)malloc(sizeof(double));
        vsn = (double*)malloc(sizeof(double));
        vcen = (double*)malloc(sizeof(double));
        vsu = (double*)malloc(sizeof(double));
        vcod[0] = (char*)malloc(LONCOD+2);
        //compruebo si ha ocurrido algun error en la asignacion de memoria
        if((vcod==NULL)||(vlat==NULL)||(vlon==NULL)||(vde==NULL)||
           (vdn==NULL)||(vdu==NULL)||(vse==NULL)||(vsn==NULL)||
           (vcen==NULL)||(vsu==NULL))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Error en la asignación dinámica\n");
            fprintf(stderr,"            de memoria\n");
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //libero la posible memoria asignada
            for(i=0;i<nPuntos;i++)
            {
                free(vcod[i]);
            }
            free(vcod);
            free(vlat);
            free(vlon);
            free(vde);
            free(vdn);
            free(vdu);
            free(vse);
            free(vsn);
            free(vcen);
            free(vsu);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        if(vcod[0]==NULL)
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Error en la asignación dinámica\n");
            fprintf(stderr,"            de memoria\n");
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //libero la posible memoria asignada
            for(i=0;i<nPuntos;i++)
            {
                free(vcod[i]);
            }
            free(vcod);
            free(vlat);
            free(vlon);
            free(vde);
            free(vdn);
            free(vdu);
            free(vse);
            free(vsn);
            free(vcen);
            free(vsu);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //asignamos los elementos de entrada
        strcpy(vcod[0],argv[2]);
        vlat[0] = atof(argv[3]);
        vlon[0] = atof(argv[4]);
        vde[0] = atof(argv[5]);
        vdn[0] = atof(argv[6]);
        vdu[0] = atof(argv[7]);
        vse[0] = atof(argv[8]);
        vsn[0] = atof(argv[9]);
        vcen[0] = atof(argv[10]);
        vsu[0] = atof(argv[11]);
        //creamos el script de dibujo
        cError = DibujaDeformacionGMT(stdout,
                                      argv[1],
                                      nPuntos,
                                      vcod,
                                      vlat,vlon,
                                      vde,vdn,vdu,
                                      vse,vsn,vcen,vsu);
        //comprobamos el código de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            for(i=0;i<nPuntos;i++)
            {
                free(vcod[i]);
            }
            free(vcod);
            free(vlat);
            free(vlon);
            free(vde);
            free(vdn);
            free(vdu);
            free(vse);
            free(vsn);
            free(vcen);
            free(vsu);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    for(i=0;i<nPuntos;i++)
    {
        free(vcod[i]);
    }
    free(vcod);
    free(vlat);
    free(vlon);
    free(vde);
    free(vdn);
    free(vdu);
    free(vse);
    free(vsn);
    free(vcen);
    free(vsu);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Crea los comandos necesarios  de GMT para representar\n");
    fprintf(stderr,"gráficamente información de deformación de puntos\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s [%s] fichero cod lat lon de dn du Sde Sdn Cdedn Sdu\n\n",
            nombre,FLAGPIPE);
    fprintf(stderr,"\t%s: Indicador de que la entrada es a través ",FLAGPIPE);
    fprintf(stderr,"de una tubería (pipe)\n");
    fprintf(stderr,"\tfichero: Parte común del nombre de los ficheros de\n");
    fprintf(stderr,"\t         dibujo de deformación en planimetría y ");
    fprintf(stderr,"altimetría\n\t         que se crearán con los comandos ");
    fprintf(stderr,"generados\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tlat: Latitud del punto estación. En grados ");
    fprintf(stderr,"sexagesimales, formato decimal\n");
    fprintf(stderr,"\tlon: Longitud del punto estación. En grados ");
    fprintf(stderr,"sexagesimales, formato decimal\n");
    fprintf(stderr,"\tde: Desplazamiento a lo largo del eje east\n");
    fprintf(stderr,"\tdn: Desplazamiento a lo largo del eje north\n");
    fprintf(stderr,"\tdu: Desplazamiento a lo largo del eje up\n");
    fprintf(stderr,"\tSde: Desviación típica del desplazamiento a lo largo ");
    fprintf(stderr,"del eje east\n");
    fprintf(stderr,"\tSdn: Desviación típica del desplazamiento a lo largo ");
    fprintf(stderr,"del eje north\n");
    fprintf(stderr,"\tCdedn: Coeficiente de correlación entre las\n");
    fprintf(stderr,"\t       desviaciones típicas de los desplazamientos a\n");
    fprintf(stderr,"\t       lo largo de los ejes east y north\n");
    fprintf(stderr,"\tSdu: Desviación típica del desplazamiento a lo largo ");
    fprintf(stderr,"del eje up\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería solo\n");
    fprintf(stderr,"  deben pasarse los argumentos %s y fichero, ninguno más\n",
            FLAGPIPE);
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la ");
    fprintf(stderr,"salida de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
