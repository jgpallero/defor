/**
\file defor.c
\brief Programa para el calculo de deformaciónes de puntos a partir de sus
       coordenadas cartesianas tridimensionales geocentricas.
\author José Luis García Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"snx.h"
#include"ptos.h"
#include"tran.h"
#include"snxptos.h"
#include"deformac.h"
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "defor"
//numero minimo de argumentos de entrada
#define MINARG 6
//numero intermedio de argumentos de entrada
#define INTARG 7
//numero maximo de argumentos de entrada
#define MAXARG 9
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //codigo de error
    int cError=ERRNOERROR;
    //variable para recorrer bucles
    int i=0;
    //estructura possnx
    possnx pSNX;
    //identificador de no existencia de s02
    int infoS02=0;
    //estructuras posptos
    posptos pPTOS1,pPTOS2;
    //estructura partran
    partran pTRAN;
    //estructura deformac
    deformac defor;
    //flag de tipo de salida
    char flag[LONMAXNOMBREARG+2];
    //formato de escritura
    char fte[LONFORMATO+2];
    //coordenadas geodesicas
    double lat=0.0,lon=0.0,h=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura possnx
    InicializaPosSnx(&pSNX);
    //inicializamos las estructuras posptos
    InicializaPosPtos(&pPTOS1);
    InicializaPosPtos(&pPTOS2);
    //inicializamos la estructura partran
    InicializaParTran(&pTRAN);
    //inicializamos la estructura deformac
    InicializaDeformac(&defor);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=MINARG)&&(argc!=INTARG)&&(argc!=MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos el flag de escritura en el fichero de resultados numericos
    if(argc==MINARG)
    {
        strcpy(flag,argv[argc-1]);
    }
    else if(argc==INTARG)
    {
        strcpy(flag,argv[argc-1]);
    }
    else
    {
        strcpy(flag,argv[argc-1]);
    }
    //comprobamos que el flag es correcto
    if(strcmp(flag,FLAGDEFORGMT)&&
       strcmp(flag,FLAGDEFORCOMP)&&
       strcmp(flag,FLAGDEFORRES))
    {
        //lanzamos un mensaje de error
        fprintf(stderr,"\n");
        fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"            El argumento %s es incorrecto\n",flag);
        fprintf(stderr,"            El programa finalizará\n");
        fprintf(stderr,"\n");
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los puntos de trabajo antes de la deformación
    cError = LeePosFicheroPuntos(argv[2],argv[3],1,&pPTOS1);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaPosPtos(&pPTOS1);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos los puntos de trabajo después de la deformación
    if(!strcmp(argv[1],FLAGFICHSINEX))
    {
        //extraemos la información necesaria del fichero SINEX
        cError = LeePosVelFicheroSinex(argv[4],1,0,1,1,&infoS02,&pSNX);
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            LiberaMemoriaPosPtos(&pPTOS1);
            LiberaMemoriaPosSnx(&pSNX);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //Comprobamos si ho hay s02 si se trabaja con matriz normal
        if(infoS02)
        {
            //lanzamos un mensaje de aviso
            fprintf(stderr,"\n");
            fprintf(stderr,"*****WARNING: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"              El fichero %s contiene matriz\n"
                           "              normal pero no información del\n"
                           "              valor de s02. Se ha utilizado el\n"
                           "              valor s02=1 para el cálculo de la"
                           "              matriz de varianzas-covarianzas\n",
                    argv[4]);
            fprintf(stderr,"\n");
        }
        //pasamos la estructura possnx a posptos
        if(argc==MINARG)
        {
            //pasamos la etiqueta 'SIN-DATO' porque no hay sistema de referencia
            cError = CopiaPosSnxEnPosPtos(&pSNX,"SIN-DATO",&pPTOS2);
        }
        else
        {
            //copiamos el sistema de referencia pasado
            cError = CopiaPosSnxEnPosPtos(&pSNX,argv[5],&pPTOS2);
        }
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            LiberaMemoriaPosPtos(&pPTOS1);
            LiberaMemoriaPosSnx(&pSNX);
            LiberaMemoriaPosPtos(&pPTOS2);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argv[1],FLAGFICHPUNTOS))
    {
        //extraemos los puntos después de la deformación
        cError = LeePosFicheroPuntos(argv[4],argv[5],1,&pPTOS2);
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            LiberaMemoriaPosPtos(&pPTOS1);
            LiberaMemoriaPosPtos(&pPTOS2);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //liberamos memoria
        LiberaMemoriaPosPtos(&pPTOS1);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos el fichero de parametros de transformación, si ha lugar
    if(argc==MAXARG)
    {
        cError = LeeParFicheroTransf(argv[6],argv[7],1,&pTRAN);
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            LiberaMemoriaPosPtos(&pPTOS1);
            LiberaMemoriaPosPtos(&pPTOS2);
            LiberaMemoriaPosSnx(&pSNX);
            LiberaMemoriaParTran(&pTRAN);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la deformación
    if((argc==MINARG)||(argc==INTARG))
    {
        cError = CalcDeforDeformac(&pPTOS1,&pPTOS2,&defor);
    }
    else
    {
        cError = CalcDeforTransfDeformac(&pPTOS1,&pPTOS2,&pTRAN,&defor);
    }
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaPosPtos(&pPTOS1);
        LiberaMemoriaPosPtos(&pPTOS2);
        LiberaMemoriaPosSnx(&pSNX);
        LiberaMemoriaParTran(&pTRAN);
        LiberaMemoriaDeformac(&defor);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si se ha calculado alguna deformación
    if(defor.nPuntos==0)
    {
        //lanzamos un mensaje de error
        fprintf(stderr,"\n");
        fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"            No hay puntos comunes para calcular ");
        fprintf(stderr,"deformaciónes\n");
        fprintf(stderr,"            El programa finalizará\n");
        fprintf(stderr,"\n");
        //liberamos memoria
        LiberaMemoriaPosPtos(&pPTOS1);
        LiberaMemoriaPosPtos(&pPTOS2);
        LiberaMemoriaPosSnx(&pSNX);
        LiberaMemoriaParTran(&pTRAN);
        LiberaMemoriaDeformac(&defor);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si se ha omitido alguna transformación entre sistemas de
    //referencia
    for(i=0;i<defor.nPuntos;i++)
    {
        if(defor.tsr[i]==NOTSR)
        {
            fprintf(stderr,"***Para la deformación numero %d, punto %s\n",
                    i+1,defor.puntos[i]);
            fprintf(stderr,"   no se ha podido calcular la transformación\n");
            fprintf(stderr,"   entre los sistemas %s -> %s\n",
                    defor.sistDespues[i],defor.sistAntes[i]);
        }
    }
    //escribo los resultados en la salida estándar
    if(!strcmp(flag,FLAGDEFORGMT))
    {
        //creamos la cadena de formato
        sprintf(fte,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
                FORMCODPTOS,SEPARADOR,
                FORMLAT,SEPARADOR,FORMLON,SEPARADOR,
                FORMDE,SEPARADOR,FORMDN,SEPARADOR,FORMDU,SEPARADOR,
                FORMSIGMA,SEPARADOR,FORMSIGMA,SEPARADOR,
                FORMCOEFCORR,SEPARADOR,
                FORMSIGMA);
        //escribimos el resultado
        for(i=0;i<defor.nPuntos;i++)
        {
            //calculamos las coordenadas geodesicas del punto antes de la
            //deformación
            TriGeo(SEMI_MAYOR,APL,
                   defor.pos[i][0],defor.pos[i][1],defor.pos[i][2],
                   &lat,&lon,&h);
            //escribimos los resultados
            fprintf(stdout,fte,
                    defor.puntos[i],
                    RD*lat,RD*lon,
                    defor.def[i][0],defor.def[i][1],defor.def[i][2],
                    sqrt(defor.vcdef[i][0]),sqrt(defor.vcdef[i][3]),
            defor.vcdef[i][1]/(sqrt(defor.vcdef[i][0])*sqrt(defor.vcdef[i][3])),
                    sqrt(defor.vcdef[i][5]));
        }
    }
    else if(!strcmp(flag,FLAGDEFORCOMP))
    {
        //creamos la cadena de formato
        sprintf(fte,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s"
                    "%s%s%s%s%s%s%s%s%s%s%s\n",
                FORMCODPTOS,SEPARADOR,
                FORMSISTREF,SEPARADOR,FORMSISTREF,SEPARADOR,
                FORMEPOCAPTOS,SEPARADOR,FORMEPOCAPTOS,SEPARADOR,
                FORMLAT,SEPARADOR,FORMLON,SEPARADOR,FORMH,SEPARADOR,FORMDE,
                SEPARADOR,FORMDN,SEPARADOR,FORMDU,SEPARADOR,FORMVC,SEPARADOR,
                FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,
                SEPARADOR,FORMVC,SEPARADOR,FORMSEMIMAYOR,SEPARADOR,
                FORMSEMIMENOR,SEPARADOR,FORMACIELIP);
        //escribimos el resultado
        for(i=0;i<defor.nPuntos;i++)
        {
            //calculamos las coordenadas geodesicas del punto antes de la
            //deformación
            TriGeo(SEMI_MAYOR,APL,
                   defor.pos[i][0],defor.pos[i][1],defor.pos[i][2],
                   &lat,&lon,&h);
            //escribimos los resultados
            fprintf(stdout,fte,
                    defor.puntos[i],
                    defor.sistAntes[i],
                    defor.sistDespues[i],
                    defor.epocas[i][0],
                    defor.epocas[i][1],
                    RD*lat,RD*lon,h,
                    defor.def[i][0],defor.def[i][1],defor.def[i][2],
                    defor.vcdef[i][0],defor.vcdef[i][1],defor.vcdef[i][2],
                    defor.vcdef[i][3],defor.vcdef[i][4],defor.vcdef[i][5],
                   defor.elipse[i][0],defor.elipse[i][1],RD*defor.elipse[i][2]);
        }
    }
    else
    {
        //creamos la cadena de formato
        sprintf(fte,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
                FORMCODPTOS,SEPARADOR,
                FORMEPOCAPTOS,SEPARADOR,
                FORMLAT,SEPARADOR,FORMLON,SEPARADOR,FORMH,SEPARADOR,
                FORMDE,SEPARADOR,FORMDN,SEPARADOR,FORMDU,SEPARADOR,
                FORMSEMIMAYOR,SEPARADOR,FORMSEMIMENOR,SEPARADOR,
                FORMACIELIP,SEPARADOR,
                FORMSIGMA);
        //escribimos el resultado
        for(i=0;i<defor.nPuntos;i++)
        {
            //calculamos las coordenadas geodesicas del punto antes de la
            //deformación
            TriGeo(SEMI_MAYOR,APL,
                   defor.pos[i][0],defor.pos[i][1],defor.pos[i][2],
                   &lat,&lon,&h);
            //escribimos los resultados
            fprintf(stdout,fte,
                    defor.puntos[i],
                    defor.epocas[i][1]-defor.epocas[i][0],
                    RD*lat,RD*lon,h,
                    defor.def[i][0],defor.def[i][1],defor.def[i][2],
                    defor.elipse[i][0],defor.elipse[i][1],RD*defor.elipse[i][2],
                    sqrt(defor.vcdef[i][5]));
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaPosPtos(&pPTOS1);
    LiberaMemoriaPosPtos(&pPTOS2);
    LiberaMemoriaPosSnx(&pSNX);
    LiberaMemoriaParTran(&pTRAN);
    LiberaMemoriaDeformac(&defor);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Calcula deformaciónes de puntos a partir de sus\n");
    fprintf(stderr,"coordenadas cartesianas tridimensionales geocéntricas\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s %s fichPUNT1 bloqPUNT1 fichSINEX2 %s|%s|%s\n",
            nombre,FLAGFICHSINEX,FLAGDEFORGMT,FLAGDEFORCOMP,FLAGDEFORRES);
    fprintf(stderr,"%s %s fichPUNT1 bloqPUNT1 fichSINEX2 sistREF2 ",nombre,
            FLAGFICHSINEX);
    fprintf(stderr,"fichTRANSF bloqTRANSF %s|%s|%s\n",
            FLAGDEFORGMT,FLAGDEFORCOMP,FLAGDEFORRES);
    fprintf(stderr,"%s %s fichPUNT1 bloqPUNT1 fichPUNT2 bloqPUNT2 %s|%s|%s\n",
            nombre,FLAGFICHPUNTOS,FLAGDEFORGMT,FLAGDEFORCOMP,FLAGDEFORRES);
    fprintf(stderr,"%s %s fichPUNT1 bloqPUNT1 fichPUNT2 bloqPUNT2 ",nombre,
            FLAGFICHPUNTOS);
    fprintf(stderr,"fichTRANSF bloqTRANSF %s|%s|%s\n\n",
            FLAGDEFORGMT,FLAGDEFORCOMP,FLAGDEFORRES);
    fprintf(stderr,"\t%s: Indicador de origen de datos después ",FLAGFICHSINEX);
    fprintf(stderr,"de la deformación de fichero SINEX\n");
    fprintf(stderr,"\t%s: Indicador de origen de datos ",FLAGFICHPUNTOS);
    fprintf(stderr,"después de la deformación de fichero de puntos\n");
    fprintf(stderr,"\tfichPUNT1: Nombre del fichero de base de datos de "
                   "puntos para extraer las\n"
                   "\t           coordenadas antes de la deformación\n");
    fprintf(stderr,"\tbloqPUNT1: Bloque de trabajo en el fichero de puntos "
                   "antes de la deformación\n");
    fprintf(stderr,"\tfichSINEX2: Nombre del fichero SINEX de trabajo que "
                   "contiene los datos después\n"
                   "\t            de la deformación\n");
    fprintf(stderr,"\tsistREF2: Identificador del sistema de referencia de "
                   "los puntos almacenados en\n"
                   "\t          el fichero SINEX después de la deformación\n");
    fprintf(stderr,"\tfichPUNT2: Nombre del fichero de base de datos de puntos "
                   "para extraer las\n"
                   "\t           coordenadas después de la deformación\n");
    fprintf(stderr,"\tbloqPUNT2: Bloque de trabajo en el fichero de puntos "
                   "después de la deformación\n");
    fprintf(stderr,"\tfichTRANSF: Nombre del fichero de parametros de "
                   "transformación\n");
    fprintf(stderr,"\tbloqTRANSF: Bloque de trabajo en el fichero de "
                   "parametros de transformación\n");
    fprintf(stderr,"\t%s: Escribe en la salida estándar la ",FLAGDEFORGMT);
    fprintf(stderr,"información necesaria para crear un\n\t    dibujo de los "
                   "resultados con GMT: cod lat lon de dn du Se Sn Cen Su\n");
    fprintf(stderr,"\t%s: Escribe en la salida estándar la ",FLAGDEFORCOMP);
    fprintf(stderr,"información completa acerca de la\n\t    deformación "
                   "calculada:\n"
                   "\t    cod sA sD tA tD lat lon h de dn du Se2 Sen Seu Su2 "
                   "Snu Su a b aci\n");
    fprintf(stderr,"\t%s: Escribe en la salida estándar la ",FLAGDEFORRES);
    fprintf(stderr,"información resumida acerca de la\n\t    deformación "
                   "calculada: cod dt lat lon h de dn du a b aci Su\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Donde:\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"*cod: Código de la estación\n");
    fprintf(stderr,"*sA: Sistema de referencia del punto antes de la ");
    fprintf(stderr,"deformación\n");
    fprintf(stderr,"*sD: Sistema de referencia del punto después de la ");
    fprintf(stderr,"deformación\n");
    fprintf(stderr,"*tA: Época antes de la deformación, en años\n");
    fprintf(stderr,"*tD: Época después de la deformación, en años\n");
    fprintf(stderr,"*dt: Intervalo de tiempo tD-tA, en años\n");
    fprintf(stderr,"*lat: Latitud geodésica del punto de trabajo antes de la "
                   "deformación,\n"
                   "      en grados sexagesimales (formato decimal)\n");
    fprintf(stderr,"*lon: Longitud geodésica del punto de trabajo antes de la "
                   "deformación,\n"
                   "      en grados sexagesimales (formato decimal)\n");
    fprintf(stderr,"*h: Altitud elipsoidal del punto de trabajo antes de la "
                   "deformación\n");
    fprintf(stderr,"*de: deformación a lo largo del eje east\n");
    fprintf(stderr,"*dn: deformación a lo largo del eje north\n");
    fprintf(stderr,"*du: deformación a lo largo del eje up\n");
    fprintf(stderr,"*Se: Desviacion típica de la deformación a lo largo del "
                   "eje east\n");
    fprintf(stderr,"*Sn: Desviacion típica de la deformación a lo largo del "
                   "eje north\n");
    fprintf(stderr,"*Cen: Coeficiente de correlación entre las desviaciones "
                   "típicas de las\n"
                   "      deformaciónes a lo largo de los east y north\n");
    fprintf(stderr,"*Su: Desviación típica de la deformación a lo largo del "
                   "eje up\n");
    fprintf(stderr,"*Se2: Varianza de la deformación a lo largo del eje "
                   "east\n");
    fprintf(stderr,"*Sen: Covarianza de la deformación a lo largo de los ejes "
                   "east y north\n");
    fprintf(stderr,"*Seu: Covarianza de la deformación a lo largo de los ejes "
                   "east y up\n");
    fprintf(stderr,"*Sn2: Varianza de la deformación a lo largo del eje "
                   "north\n");
    fprintf(stderr,"*Snu: Covarianza de la deformación a lo largo de los ejes "
                   "north y up\n");
    fprintf(stderr,"*Su2: Varianza de la deformación a lo largo del eje up\n");
    fprintf(stderr,"*a: Semieje mayor de la elipse de error estándar de la "
                   "deformación\n");
    fprintf(stderr,"*b: Semieje menor de la elipse de error estándar de la "
                   "deformación\n");
    fprintf(stderr,"*aci: Acimut del semieje mayor de la elipse, en grados "
                   "sexagesimales (formato decimal)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar "
                   "(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida "
                   "de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
