/**
\file fptos.c
\brief Programa para la escritura de coordenadas cartesianas tridimensionales
       geocéntricas en el formato adecuado para anyadir a un fichero de base de
       datos de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"paramprg.h"
#include"ptos.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "fptos"
//numero minimo de argumentos de entrada
#define MINARG 2
//numero intermedio de argumentos de entrada
#define INTARG 7
//numero maximo de argumentos de entrada
#define MAXARG 13
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadena de formato para entrada
    char fte[LONFORMATO+2];
    //cadenas de formato para la salida
    char ftc[LONFORMATO+2],ftvc[LONFORMATO+2],ftcom[LONFORMATO+2];
    //parametros de entrada por pipe
    char cod[LONCOD+2],sr[LONCOD+2];
    double t=0.0;
    double x=0.0,y=0.0,z=0.0;
    double varx=0.0,varxy=0.0,varxz=0.0,vary=0.0,varyz=0.0,varz=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=MINARG)&&(argc!=INTARG)&&(argc!=MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos el formato de salida para las coordenadas
    sprintf(ftc,"%s%s%s%s%s%s%s%s%s%s%s%s\n",
            CADIDPUNTOPTOS,
            FORMCODPTOS,SEPARADOR,FORMSISTREF,SEPARADOR,FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC);
    //creamos el formato de salida para la matriz varianza-covarianza
    sprintf(ftvc,"%s%s%s%s%s%s%s%s%s%s%s%s\n",
            SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    //creamos el formato de salida para la linea de comentario
    sprintf(ftcom,"%s\n",CADCOMENTPTOS);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if(argc==MINARG)
    {
        //compruebo si el argumento indicador de entrada por pipe es correcto
        if(strcmp(argv[1],FLAGPIPE))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Argumento %s incorrecto\n",argv[1]);
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //construyo el formato de entrada
        sprintf(fte,
         "%%s%s%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           cod,sr,&t,
                           &x,&y,&z,
                           &varx,&varxy,&varxz,&vary,&varyz,&varz);
            //compruebo el numero de elementos leido
            if((nElem!=INTARG-1)&&(nElem!=MAXARG-1))
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //asigno ceros a la matriz de varianza-covarianza si no se ha
            //introducido
            if(nElem==INTARG-1)
            {
                varx = 0.0;
                varxy = 0.0;
                varxz = 0.0;
                vary = 0.0;
                varyz = 0.0;
                varz = 0.0;
            }
            //imprimimos la linea con las coordenadas
            fprintf(stdout,ftc,cod,sr,t,x,y,z);
            //imprimimos la linea con la matriz varianza-covarianza
            fprintf(stdout,ftvc,varx,varxy,varxz,vary,varyz,varz);
            //imprimimos la linea con el caracter de comentario
            fprintf(stdout,"%s",ftcom);
        }
    }
    else
    {
        //asignamos los elementos de entrada
        strcpy(cod,argv[1]);
        strcpy(sr,argv[2]);
        t = atof(argv[3]);
        x = atof(argv[4]);
        y = atof(argv[5]);
        z = atof(argv[6]);
        if(argc==INTARG)
        {
            varx = 0.0;
            varxy = 0.0;
            varxz = 0.0;
            vary = 0.0;
            varyz = 0.0;
            varz = 0.0;
        }
        else
        {
            varx = atof(argv[7]);
            varxy = atof(argv[8]);
            varxz = atof(argv[9]);
            vary = atof(argv[10]);
            varyz = atof(argv[11]);
            varz = atof(argv[12]);
        }
        //imprimimos la linea con las coordenadas
        fprintf(stdout,ftc,cod,sr,t,x,y,z);
        //imprimimos la linea con la matriz varianza-covarianza
        fprintf(stdout,ftvc,varx,varxy,varxz,vary,varyz,varz);
        //imprimimos la linea con el caracter de comentario
        fprintf(stdout,"%s",ftcom);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Escribe datos de posición de un punto en formato de\n");
    fprintf(stderr,"fichero de base de datos de puntos\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s [%s] cod sr t x y z [Sx2 Sxy Sxz Sy2 Syz Sz2]\n\n",
            nombre,FLAGPIPE);
    fprintf(stderr,"\t%s: Indicador de que la entrada es a través ",FLAGPIPE);
    fprintf(stderr,"de una tubería (pipe)\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tsr: Código del sistema de referencia de la estación\n");
    fprintf(stderr,"\tt: Época a la que están referidas las coordenadas\n");
    fprintf(stderr,"\tx: Coordenada X geocéntrica\n");
    fprintf(stderr,"\ty: Coordenada Y geocéntrica\n");
    fprintf(stderr,"\tz: Coordenada Z geocéntrica\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxz: Covarianza entre las coordenadas X y Z\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyz: Covarianza entre las coordenadas Y y Z\n");
    fprintf(stderr,"\tSz2: Varianza de la coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería sólo\n");
    fprintf(stderr,"  debe pasarse el argumento %s, ninguno más\n",FLAGPIPE);
    fprintf(stderr,"**Después de la escritura del punto se añade una\n");
    fprintf(stderr,"  línea de comentario para una mejor claridad del\n");
    fprintf(stderr,"  fichero de base de datos de puntos\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la ");
    fprintf(stderr,"salida de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
