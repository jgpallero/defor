/**
\file pvel1.c
\brief Programa para la aplicación de velocidades de desplazamiento a puntos
       expresados en coordenadas cartesianas tridimensionales geocéntricas.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 10 de septiembre de 2009
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "pvel1"
//numero de argumentos de entrada segun el uso del programa
#define ARGNORM1 9
#define ARGNORM2 11
#define ARGNORM3 15
#define ARGNORM4 17
#define ARGNORM5 21
#define ARGNORM6 23
#define ARGPIPE1 6
#define ARGPIPE2 12
#define ARGRESTOPIPE1 4
#define ARGRESTOPIPE2 6
#define ARGRESTOPIPE3 10
#define ARGRESTOPIPE4 12
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
void ImprimeResultados(int formato,
                       char cod[],
                       char sr[],
                       double t,
                       double x,
                       double y,
                       double z,
                       double sx2,
                       double sxy,
                       double sxz,
                       double sy2,
                       double syz,
                       double sz2);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //indicador de entrada por pipe
    int entPipe=0;
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadena de formato para entrada
    char fte[LONFORMATO+2];
    //indicador de impresion de la Época original en la salida
    int impEpOri=0;
    //variable para almacenar el flag de impresion de Época
    char flagImpEp[LONMAXNOMBREARG+1];
    char epDestTexto[LONMAXVALORARG+1];
    //parametros de entrada
    char aux1[LONMAXVALORARG+2],aux2[LONMAXVALORARG+2];
    double aux3=0.0,aux4=0.0,aux5=0.0,aux6=0.0,aux7=0.0,aux8=0.0,aux9=0.0;
    double aux10=0.0,aux11=0.0,aux12=0.0;
    char cod[LONCOD+2],sr[LONCOD+2];
    double epDest=0.0,vx=0.0,vy=0.0,vz=0.0,svx2=0.0,svxvy=0.0,svxvz=0.0;
    double svy2=0.0,svyvz=0.0,svz2=0.0;
    double t=0.0;
    double x=0.0,y=0.0,z=0.0;
    double sx2=0.0,sxy=0.0,sxz=0.0,sy2=0.0,syz=0.0,sz2=0.0;
    //parametros de salida
    double xt=0.0,yt=0.0,zt=0.0;
    double sxt2=0.0,sxtyt=0.0,sxtzt=0.0,syt2=0.0,sytzt=0.0,szt2=0.0;
    //indicador de formato de salida
    int formato=0;
    //posicion del primer argumento de entrada
    size_t posIni=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=ARGNORM1)&&(argc!=ARGNORM2)&&(argc!=ARGNORM3)&&(argc!=ARGNORM4)&&
       (argc!=ARGNORM5)&&(argc!=ARGNORM6)&&(argc!=ARGPIPE1)&&(argc!=ARGPIPE2))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    else
    {
        //compruebo si la entrada es por pipe
        if((argc==ARGPIPE1)||(argc==ARGPIPE2))
        {
            //compruebo si el argumento de indicador de pipe es correcto
            if(strcmp(argv[1],FLAGPIPE))
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            else
            {
                //asigno el indicador de entrada por pipe
                entPipe = 1;
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //calculamos la posicion del primer argumento de entrada
    if(entPipe)
    {
        posIni = 2;
    }
    else
    {
        posIni = 1;
    }
    //extraigo el argumento de la Época final
    if(strlen(argv[posIni])>2)
    {
        //extraigo el flag
        strncpy(flagImpEp,argv[posIni],2*sizeof(char));
        flagImpEp[2] = '\0';
        //compruebo si el flag es correcto
        if(!strcmp(flagImpEp,FLAGEPOR))
        {
            //indico que hay que imprimir la época original
            impEpOri = 1;
        }
        else if(!strcmp(flagImpEp,FLAGEPDES))
        {
            //indico que hay que imprimir la época final
            impEpOri = 0;
        }
        else
        {
            //lanzamos un mensaje de ayuda
            MensajeAyuda(PROGRAMA);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //extraigo la Época de destino
        strncpy(epDestTexto,&argv[posIni][2],strlen(argv[posIni])-2);
        epDestTexto[strlen(argv[posIni])-2] = '\0';
        //la convierto en numero
        epDest = atof(epDestTexto);
    }
    else
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    //extraigo los parametros de velocidades
    vx = atof(argv[posIni+1]);
    vy = atof(argv[posIni+2]);
    vz = atof(argv[posIni+3]);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if(entPipe)
    {
        //inicializo los parametros de error de las velocidades
        svx2 = 0.0;
        svxvy = 0.0;
        svxvz = 0.0;
        svy2 = 0.0;
        svyvz = 0.0;
        svz2 = 0.0;
        //los extraigo, si ha lugar
        if(argc==ARGPIPE2)
        {
            svx2 = atof(argv[6]);
            svxvy = atof(argv[7]);
            svxvz = atof(argv[8]);
            svy2 = atof(argv[9]);
            svyvz = atof(argv[10]);
            svz2 = atof(argv[11]);
            //actualizo el indicador de formato de salida
            formato = 1;
        }
        //construyo el formato de entrada completo
        sprintf(fte,
         "%%s%s%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada completa para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           aux1,aux2,&aux3,&aux4,&aux5,&aux6,&aux7,&aux8,&aux9,
                           &aux10,&aux11,&aux12);
            //compruebo el numero de elementos pasado y leo la entrada de forma
            //correcta
            if((nElem!=ARGRESTOPIPE1)&&(nElem!=ARGRESTOPIPE2)&&
               (nElem!=ARGRESTOPIPE3)&&(nElem!=ARGRESTOPIPE4))
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //leo la entrada segun el numero de argumentos
            strcpy(cod,"");
            strcpy(sr,"");
            sx2 = 0.0;
            sxy = 0.0;
            sxz = 0.0;
            sy2 = 0.0;
            syz = 0.0;
            sz2 = 0.0;
            if(nElem==ARGRESTOPIPE1)
            {
                t = atof(aux1);
                x = atof(aux2);
                y = aux3;
                z = aux4;
            }
            else if(nElem==ARGRESTOPIPE2)
            {
                strcpy(cod,aux1);
                strcpy(sr,aux2);
                t = aux3;
                x = aux4;
                y = aux5;
                z = aux6;
            }
            else if(nElem==ARGRESTOPIPE3)
            {
                t = atof(aux1);
                x = atof(aux2);
                y = aux3;
                z = aux4;
                sx2 = aux5;
                sxy = aux6;
                sxz = aux7;
                sy2 = aux8;
                syz = aux9;
                sz2 = aux10;
                //actualizo el indicador de formato de salida
                formato = 1;
            }
            else
            {
                strcpy(cod,aux1);
                strcpy(sr,aux2);
                t = aux3;
                x = aux4;
                y = aux5;
                z = aux6;
                sx2 = aux7;
                sxy = aux8;
                sxz = aux9;
                sy2 = aux10;
                syz = aux11;
                sz2 = aux12;
                //actualizo el indicador de formato de salida
                formato = 1;
            }
            //realizamos la transformacion
            AplicaVelocidadVC(x,y,z,sx2,sxy,sxz,sy2,syz,sz2,
                              vx,vy,vz,svx2,svxvy,svxvz,svy2,svyvz,svz2,
                              t,epDest,
                              &xt,&yt,&zt,
                              &sxt2,&sxtyt,&sxtzt,&syt2,&sytzt,&szt2);
            //imprimimos el resultado
            if(impEpOri)
            {
                ImprimeResultados(formato,cod,sr,t,xt,yt,zt,
                                  sxt2,sxtyt,sxtzt,syt2,sytzt,szt2);
            }
            else
            {
                ImprimeResultados(formato,cod,sr,epDest,xt,yt,zt,
                                  sxt2,sxtyt,sxtzt,syt2,sytzt,szt2);
            }
        }
    }
    else
    {
        //inicializamos a 0 los elementos de entrada opcionales
        strcpy(cod,"");
        strcpy(sr,"");
        svx2 = 0.0;
        svxvy = 0.0;
        svxvz = 0.0;
        svy2 = 0.0;
        svyvz = 0.0;
        svz2 = 0.0;
        sx2 = 0.0;
        sxy = 0.0;
        sxz = 0.0;
        sy2 = 0.0;
        syz = 0.0;
        sz2 = 0.0;
        //extraemos los elementos de entrada, segun convenga
        if(argc==ARGNORM1)
        {
            t = atof(argv[5]);
            x = atof(argv[6]);
            y = atof(argv[7]);
            z = atof(argv[8]);
        }
        else if(argc==ARGNORM2)
        {
            strcpy(cod,argv[5]);
            strcpy(sr,argv[6]);
            t = atof(argv[7]);
            x = atof(argv[8]);
            y = atof(argv[9]);
            z = atof(argv[10]);
        }
        else if(argc==ARGNORM3)
        {
            t = atof(argv[5]);
            x = atof(argv[6]);
            y = atof(argv[7]);
            z = atof(argv[8]);
            sx2 = atof(argv[9]);
            sxy = atof(argv[10]);
            sxz = atof(argv[11]);
            sy2 = atof(argv[12]);
            syz = atof(argv[13]);
            sz2 = atof(argv[14]);
            //actualizo el indicador de formato de salida
            formato = 1;
        }
        else if(argc==ARGNORM4)
        {
            strcpy(cod,argv[5]);
            strcpy(sr,argv[6]);
            t = atof(argv[7]);
            x = atof(argv[8]);
            y = atof(argv[9]);
            z = atof(argv[10]);
            sx2 = atof(argv[11]);
            sxy = atof(argv[12]);
            sxz = atof(argv[13]);
            sy2 = atof(argv[14]);
            syz = atof(argv[15]);
            sz2 = atof(argv[16]);
            //actualizo el indicador de formato de salida
            formato = 1;
        }
        else if(argc==ARGNORM5)
        {
            svx2 = atof(argv[5]);
            svxvy = atof(argv[6]);
            svxvz = atof(argv[7]);
            svy2 = atof(argv[8]);
            svyvz = atof(argv[9]);
            svz2 = atof(argv[10]);
            t = atof(argv[11]);
            x = atof(argv[12]);
            y = atof(argv[13]);
            z = atof(argv[14]);
            sx2 = atof(argv[15]);
            sxy = atof(argv[16]);
            sxz = atof(argv[17]);
            sy2 = atof(argv[18]);
            syz = atof(argv[19]);
            sz2 = atof(argv[20]);
            //actualizo el indicador de formato de salida
            formato = 1;
        }
        else
        {
            svx2 = atof(argv[5]);
            svxvy = atof(argv[6]);
            svxvz = atof(argv[7]);
            svy2 = atof(argv[8]);
            svyvz = atof(argv[9]);
            svz2 = atof(argv[10]);
            strcpy(cod,argv[11]);
            strcpy(sr,argv[12]);
            t = atof(argv[13]);
            x = atof(argv[14]);
            y = atof(argv[15]);
            z = atof(argv[16]);
            sx2 = atof(argv[17]);
            sxy = atof(argv[18]);
            sxz = atof(argv[19]);
            sy2 = atof(argv[20]);
            syz = atof(argv[21]);
            sz2 = atof(argv[22]);
            //actualizo el indicador de formato de salida
            formato = 1;
        }
        //realizamos la transformacion
        AplicaVelocidadVC(x,y,z,sx2,sxy,sxz,sy2,syz,sz2,
                          vx,vy,vz,svx2,svxvy,svxvz,svy2,svyvz,svz2,
                          t,epDest,
                          &xt,&yt,&zt,
                          &sxt2,&sxtyt,&sxtzt,&syt2,&sytzt,&szt2);
        //imprimimos el resultado
        if(impEpOri)
        {
            ImprimeResultados(formato,cod,sr,t,xt,yt,zt,
                              sxt2,sxtyt,sxtzt,syt2,sytzt,szt2);
        }
        else
        {
            ImprimeResultados(formato,cod,sr,epDest,xt,yt,zt,
                              sxt2,sxtyt,sxtzt,syt2,sytzt,szt2);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Aplicación de velocidades de desplazamiento a puntos\n");
    fprintf(stderr,"expresados en coordenadas cartesianas tridimensionales ");
    fprintf(stderr,"geocéntricas\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s [%s] %sEpDes|%sEpDes vx vy vz [cod sr] t x y z\n",
            nombre,FLAGPIPE,FLAGEPOR,FLAGEPDES);
    fprintf(stderr,"%s [%s] %sEpDes|%sEpDes vx vy vz [cod sr] t x y z ",
            nombre,FLAGPIPE,FLAGEPOR,FLAGEPDES);
    fprintf(stderr,"Sx2 Sxy Sxz Sy2 Syz Sz2\n");
    fprintf(stderr,"%s [%s] %sEpDes|%sEpDes vx vy vz ",
            nombre,FLAGPIPE,FLAGEPOR,FLAGEPDES);
    fprintf(stderr,"Svx2 Svxvy Svxvz Svy2 Svyvz Svz2 [cod sr] t x y z ");
    fprintf(stderr,"Sx2 Sxy Sxz Sy2 Syz Sz2\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"\t%s: Indicador de que la entrada es a través ",FLAGPIPE);
    fprintf(stderr,"de una tubería (pipe)\n");
    fprintf(stderr,"\tepDes: Época final. Si este argumento se pasa con el\n");
    fprintf(stderr,"\t       flag %s, en la salida se imprime la Época a la\n",
            FLAGEPOR);
    fprintf(stderr,"\t       que viene referido originalmente el punto de\n");
    fprintf(stderr,"\t       trabajo. Si se pasa con el flag %s, en la\n",
            FLAGEPDES);
    fprintf(stderr,"\t       salida se imprime la Época final\n");
    fprintf(stderr,"\tvx: Velocidad de la coordenada X\n");
    fprintf(stderr,"\tvy: Velocidad de la coordenada Y\n");
    fprintf(stderr,"\tvz: Velocidad de la coordenada Z\n");
    fprintf(stderr,"\tSvx2: Varianza de la velocidad de la coordenada X\n");
    fprintf(stderr,"\tSvxvy: Covarianza entre las velocidades en X e Y\n");
    fprintf(stderr,"\tSvxvz: Covarianza entre las velocidades en X y Z\n");
    fprintf(stderr,"\tSvy2: Varianza de la velocidad de la coordenada Y\n");
    fprintf(stderr,"\tSvyvz: Covarianza entre las velocidades en Y y Z\n");
    fprintf(stderr,"\tSvz2: Varianza de la velocidad de la coordenada Z\n");
    fprintf(stderr,"\tcod: Código del punto de trabajo\n");
    fprintf(stderr,"\tsr: Código del sistema de referencia del punto de ");
    fprintf(stderr,"trabajo\n");
    fprintf(stderr,"\tt: Época a la que vienen referidas las coordenadas\n");
    fprintf(stderr,"\t   del punto de trabajo\n");
    fprintf(stderr,"\tx: Coordenada X geocéntrica del punto de trabajo\n");
    fprintf(stderr,"\ty: Coordenada Y geocéntrica del punto de trabajo\n");
    fprintf(stderr,"\tz: Coordenada Z geocéntrica del punto de trabajo\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxz: Covarianza entre las coordenadas X y Z\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyz: Covarianza entre las coordenadas Y y Z\n");
    fprintf(stderr,"\tSz2: Varianza de la coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El formato de salida es el siguiente:\n\n");
    fprintf(stderr,"\t[cod sr] t|epDes x y z [Sx2 Sxy Sxz Sy2 Syz Sz2]\n\n");
    fprintf(stderr,"\tcod: Código de la estacion\n");
    fprintf(stderr,"\tsr: Código del sistema de referencia del punto de ");
    fprintf(stderr,"trabajo\n");
    fprintf(stderr,"\tt|epDes: Época origen del punto de trabajo o época\n");
    fprintf(stderr,"\t         final, segun se haya pasado el flag %s o %s\n",
            FLAGEPOR,FLAGEPDES);
    fprintf(stderr,"\t         en el argumento de entrada referido a la\n");
    fprintf(stderr,"\t         época final\n");
    fprintf(stderr,"\tx: Coordenada X geocéntrica del punto transformado\n");
    fprintf(stderr,"\ty: Coordenada Y geocéntrica del punto transformado\n");
    fprintf(stderr,"\tz: Coordenada Z geocéntrica del punto transformado\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxz: Covarianza entre las coordenadas X y Z\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyz: Covarianza entre las coordenadas Y y Z\n");
    fprintf(stderr,"\tSz2: Varianza de la coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería (pipe)\n");
    fprintf(stderr,"  solo deben pasarse los argumentos %s, vx, vy, vz y\n",
            FLAGPIPE);
    fprintf(stderr,"  (en su caso) Svx2, Svxvy, Svxvz, Svy2, Svyvz y Svz2,\n");
    fprintf(stderr,"  ninguno más\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2009, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeResultados(int formato,
                       char cod[],
                       char sr[],
                       double t,
                       double x,
                       double y,
                       double z,
                       double sx2,
                       double sxy,
                       double sxz,
                       double sy2,
                       double syz,
                       double sz2)
{
    //cadenas de formato para la salida
    char ftrc[LONFORMATO+2],ftcc[LONFORMATO+2];
    char ftri[LONFORMATO+2],ftci[LONFORMATO+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos el formato de salida resumida (sin informacion de errores) con
    //Código y sistema de referencia
    sprintf(ftrc,"%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCODPTOS,SEPARADOR,FORMSISTREF,SEPARADOR,FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC);
    //creamos el formato de salida completa (con informacion de errores) con
    //Código y sistema de referencia
    sprintf(ftcc,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCODPTOS,SEPARADOR,FORMSISTREF,SEPARADOR,FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    //creamos el formato de salida resumida (sin informacion de errores) sin
    //Código y sistema de referencia
    sprintf(ftri,"%s%s%s%s%s%s%s\n",
            FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC);
    //creamos el formato de salida completa (con informacion de errores) sin
    //Código y sistema de referencia
    sprintf(ftci,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos la salida en pantalla
    if(!strcmp(cod,""))
    {
        if(formato==0)
        {
            //escribimos la salida resumida
            fprintf(stdout,ftri,t,x,y,z);
        }
        else
        {
            //escribimos la salida completa
            fprintf(stdout,ftci,t,x,y,z,sx2,sxy,sxz,sy2,syz,sz2);
        }
    }
    else
    {
        if(formato==0)
        {
            //escribimos la salida resumida
            fprintf(stdout,ftrc,cod,sr,t,x,y,z);
        }
        else
        {
            //escribimos la salida completa
            fprintf(stdout,ftcc,cod,sr,t,x,y,z,sx2,sxy,sxz,sy2,syz,sz2);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
