/**
\file geoutm.c
\brief Programa para la transformacion de coordenadas geodésicas en coordenadas
       UTM.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 28 de noviembre de 2010
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"paramprg.h"
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "geoutm"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //identificador de entrada por pipe
    int entPipe=0;
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadenas de formato para entrada y salidas incompleta y completa
    char fte[LONFORMATO+2],ftsi[LONFORMATO+2],ftsc[LONFORMATO+2];
    char ftsih[LONFORMATO+2],ftsch[LONFORMATO+2];
    //parametros de entrada por pipe
    char cod[LONCOD+2];
    double lat=0.0,lon=0.0,h=0.0;
    double varlat=0.0,varlatlon=0.0,varlath=0.0,varlon=0.0,varlonh=0.0,varh=0.0;
    //parametros de salida
    int huso=0,hemis=0;
    double x=0.0,y=0.0;
    double varx=0.0,varxy=0.0,vary=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=2)&&(argc!=3)&&(argc!=4)&&(argc!=5)&&(argc!=6)&&(argc!=8)&&
       (argc!=10)&&(argc!=12))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos los formatos de salida incompleta
    sprintf(ftsi,"%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,FORMXUTM,SEPARADOR,FORMYUTM,SEPARADOR,FORMHUSOUTM,
            SEPARADOR,FORMHEMISF);
    sprintf(ftsih,"%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,FORMXUTM,SEPARADOR,FORMYUTM,SEPARADOR,FORMH,
            SEPARADOR,FORMHUSOUTM,SEPARADOR,FORMHEMISF);
    //creamos los formatos de salida completa
    sprintf(ftsc,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,FORMXUTM,SEPARADOR,FORMYUTM,SEPARADOR,FORMHUSOUTM,
            SEPARADOR,SEPARADOR,FORMHEMISF,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC);
    sprintf(ftsch,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,FORMXUTM,SEPARADOR,FORMYUTM,SEPARADOR,FORMH,
            SEPARADOR,FORMHUSOUTM,SEPARADOR,SEPARADOR,FORMHEMISF,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si la entrada es por pipe o en la linea de argumentos
    if(argc==2)
    {
        //con dos argumentos de entrada se supone que la entrada es por pipe
        entPipe = 1;
    }
    else if(argc==3)
    {
        //con tres argumentos de entrada puede haber entrada por pipe
        if(!strcmp(argv[2],FLAGCOD))
        {
            entPipe = 1;
        }
    }
    //distingo entre entrada por pipe o en la linea de argumentos
    if(entPipe)
    {
        //compruebo si el argumento indicador de entrada por pipe es correcto
        if(strcmp(argv[1],FLAGPIPE))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Argumento %s incorrecto\n",argv[1]);
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //construyo el formato de entrada con el maximo numero de argumentos
        sprintf(fte,"%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           cod,
                           &lat,&lon,&h,
                           &varlat,&varlatlon,&varlath,&varlon,&varlonh,&varh);
            //inicializamos todos los elementos opcionales a 0
            strcpy(cod,"");
            h = 0.0;
            varlat = 0.0;
            varlatlon = 0.0;
            varlath = 0.0;
            varlon = 0.0;
            varlonh = 0.0;
            varh = 0.0;
            //dependiendo del numero de argumentos hago la lectura correcta
            if(nElem==2)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lon);
                //convertimos en numero la cadena de texto leida como coordenda
                lat = atof(cod);
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==3)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lat,&lon);
            }
            else if(nElem==3)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lon,&h);
                //convertimos en numero la cadena de texto leida como coordenda
                lat = atof(cod);
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==4)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lat,&lon,&h);
            }
            else if(nElem==5)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lon,&varlat,&varlatlon,&varlon);
                //convertimos en numero la cadena de texto leida como coordenda
                lat = atof(cod);
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==6)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lat,&lon,&varlat,&varlatlon,&varlon);
            }
            else if(nElem==9)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lon,&h,
                       &varlat,&varlatlon,&varlath,&varlon,&varlonh,&varh);
                //convertimos en numero la cadena de texto leida como coordenda
                lat = atof(cod);
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==10)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&lat,&lon,&h,
                       &varlat,&varlatlon,&varlath,&varlon,&varlonh,&varh);
            }
            else
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //convertimos los angulos a radianes
            lat *= DR;
            lon *= DR;
            varlat *= (DR*DR);
            varlatlon *= (DR*DR);
            varlath *= DR;
            varlon *= (DR*DR);
            varlonh *= DR;
            //calculamos
            GeoUtmVC(lat,lon,varlat,varlatlon,varlon,
                     SEMI_MAYOR,APL,
                     &x,&y,&varx,&varxy,&vary,&huso);
            //comprobamos el hemisferio
            if(lat>=0.0)
            {
                hemis = 1;
            }
            else
            {
                hemis = 0;
            }
            //presentamos los resultados en la salida estandar
            if((nElem==2)||((nElem==3)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD))))
            {
                //imprimimos resultados
                fprintf(stdout,ftsi,cod,x,y,huso,hemis);
            }
            else if((nElem==3)||
                    ((nElem==4)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD))))
            {
                //imprimimos resultados
                fprintf(stdout,ftsih,cod,x,y,h,huso,hemis);
            }
            else if((nElem==5)||
                    ((nElem==6)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD))))
            {
                //imprimimos resultados
                fprintf(stdout,ftsc,cod,x,y,huso,hemis,varx,varxy,vary);
            }
            else
            {
                //imprimimos resultados
                fprintf(stdout,ftsch,cod,x,y,h,huso,hemis,
                        varx,varxy,0.0,vary,0.0,varh);
            }
        }
    }
    else
    {
        //inicializamos todos los elementos opcionales a 0
        strcpy(cod,"");
        h = 0.0;
        varlat = 0.0;
        varlatlon = 0.0;
        varlath = 0.0;
        varlon = 0.0;
        varlonh = 0.0;
        varh = 0.0;
        //asignamos los elementos de entrada dependiendo del numero
        if(argc==3)
        {
            //extraemos los datos de trabajo
            lat = atof(argv[1]);
            lon = atof(argv[2]);
        }
        else if(argc==4)
        {
            //extraemos los datos de trabajo
            lat = atof(argv[1]);
            lon = atof(argv[2]);
            h = atof(argv[3]);
        }
        else if((argc==5)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            lat = atof(argv[3]);
            lon = atof(argv[4]);
        }
        else if((argc==6)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            lat = atof(argv[3]);
            lon = atof(argv[4]);
            h = atof(argv[5]);
        }
        else if(argc==6)
        {
            //extraemos los datos de trabajo
            lat = atof(argv[1]);
            lon = atof(argv[2]);
            varlat = atof(argv[3]);
            varlatlon = atof(argv[4]);
            varlon = atof(argv[5]);
        }
        else if((argc==8)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            lat = atof(argv[3]);
            lon = atof(argv[4]);
            varlat = atof(argv[5]);
            varlatlon = atof(argv[6]);
            varlon = atof(argv[7]);
        }
        else if(argc==10)
        {
            //extraemos los datos de trabajo
            lat = atof(argv[1]);
            lon = atof(argv[2]);
            h = atof(argv[3]);
            varlat = atof(argv[4]);
            varlatlon = atof(argv[5]);
            varlath = atof(argv[6]);
            varlon = atof(argv[7]);
            varlonh = atof(argv[8]);
            varh = atof(argv[9]);
        }
        else if((argc==12)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            lat = atof(argv[3]);
            lon = atof(argv[4]);
            h = atof(argv[5]);
            varlat = atof(argv[6]);
            varlatlon = atof(argv[7]);
            varlath = atof(argv[8]);
            varlon = atof(argv[9]);
            varlonh = atof(argv[10]);
            varh = atof(argv[11]);
        }
        else
        {
            //lanzamos un mensaje de ayuda
            MensajeAyuda(PROGRAMA);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //convertimos los angulos a radianes
        lat *= DR;
        lon *= DR;
        varlat *= (DR*DR);
        varlatlon *= (DR*DR);
        varlath *= DR;
        varlon *= (DR*DR);
        varlonh *= DR;
        //calculamos
        GeoUtmVC(lat,lon,varlat,varlatlon,varlon,
                 SEMI_MAYOR,APL,
                 &x,&y,&varx,&varxy,&vary,&huso);
        //comprobamos el hemisferio
        if(lat>=0.0)
        {
            hemis = 1;
        }
        else
        {
            hemis = 0;
        }
        //presentamos los resultados en la salida estandar
        if((argc==3)||((argc==5)&&(!strcmp(argv[1],FLAGCOD))))
        {
            //imprimimos resultados
            fprintf(stdout,ftsi,cod,x,y,huso,hemis);
        }
        else if((argc==4)||((argc==6)&&(!strcmp(argv[1],FLAGCOD))))
        {
            //imprimimos resultados
            fprintf(stdout,ftsih,cod,x,y,h,huso,hemis);
        }
        else if((argc==6)||((argc==8)&&(!strcmp(argv[1],FLAGCOD))))
        {
            //imprimimos resultados
            fprintf(stdout,ftsc,cod,x,y,huso,hemis,varx,varxy,vary);
        }
        else
        {
            //imprimimos resultados
            fprintf(stdout,ftsch,cod,x,y,h,huso,hemis,
                    varx,varxy,0.0,vary,0.0,varh);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Pasa de coordenadas geodésicas a coordenadas UTM\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,
     "%s [%s] [%s [cod]] lat lon [h] [Slat2 Slatlon Slath Slon2 Slonh Sh2]\n\n",
            nombre,FLAGPIPE,FLAGCOD);
    fprintf(stderr,"\t%s: Indicador de que la entrada es a través ",FLAGPIPE);
    fprintf(stderr,"de una tubería (pipe)\n");
    fprintf(stderr,
            "\t%s: Indicador de que el primer dato es un código de punto\n",
            FLAGCOD);
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tlat: Latitud geodésica. En grados sexagesimales, ");
    fprintf(stderr,"formato decimal\n");
    fprintf(stderr,"\tlon: Longitud geodésica. En grados sexagesimales, ");
    fprintf(stderr,"formato decimal\n");
    fprintf(stderr,"\th: Altitud elipsoidal\n");
    fprintf(stderr,"\tSlat2: Varianza de la latitud\n");
    fprintf(stderr,"\tSlatlon: Covarianza entre la latitud y la longitud\n");
    fprintf(stderr,"\tSlath: Covarianza entre la latitud y la altitud\n");
    fprintf(stderr,"\tSlon2: Varianza de la longitud\n");
    fprintf(stderr,"\tSlonh: Covarianza entre la longitud y la altitud\n");
    fprintf(stderr,"\tSh2: Varianza de la altitud elipsoidal\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El formato de salida es el siguiente:\n\n");
    fprintf(stderr,"\t[cod] x y [h] Huso Hemis [Sx2 Sxy 0.0 Sy2 0.0 Sh2]\n\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tx: Coordenada X UTM\n");
    fprintf(stderr,"\ty: Coordenada Y UTM\n");
    fprintf(stderr,"\th: Altitud elipsoidal\n");
    fprintf(stderr,"\tHuso: Huso UTM\n");
    fprintf(stderr,"\tHemis: Hemisferio en el que se encuentra el punto:\n");
    fprintf(stderr,"\t       - 1: Norte\n");
    fprintf(stderr,"\t       - 0: Sur\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSh2: Varianza de la altitud elipsoidal\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería sólo\n");
    fprintf(stderr,"  deben pasarse los argumentos %s (obligatorio)  y\n"
                   "  %s (opcional), ninguno más\n",FLAGPIPE,FLAGCOD);
    fprintf(stderr,"**Las varianzas y covarianzas de las unidades angulares\n");
    fprintf(stderr,"  se expresan en grados sexagesimales elevadas a la\n");
    fprintf(stderr,"  potencia correspondiente\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la ");
    fprintf(stderr,"salida de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2010, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
