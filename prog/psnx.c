/**
\file psnx.c
\brief Programa para la extraccion de datos de posicion y velocidad de un
       fichero SINEX (Solution INdependent EXchange).
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"snx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "psnx"
//numero minimo de argumentos de entrada
#define MINARG 2
//numero maximo de argumentos de entrada
#define MAXARG 9
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
void ImprimeParametro(char argumento[],
                      possnx* posicion,
                      int nPunto);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //codigo de error
    int cError=ERRNOERROR;
    //variable para recorrer un bucle
    int i=0;
    //indicador de extraccion de posicion
    int extraePosicion=0;
    //indicador de extraccion de velocidad
    int extraeVelocidad=0;
    //indicador de conversion de matrices de error
    int convierteVC=0;
    //cadenas para almacenar el nombre de los argumentos de entrada
    char argv1[LONMAXNOMBREARG+2];
    char argv2[LONMAXNOMBREARG+2];
    char argv3[LONMAXNOMBREARG+2];
    char argv4[LONMAXNOMBREARG+2];
    char argv5[LONMAXNOMBREARG+2];
    char argv6[LONMAXNOMBREARG+2];
    char argv7[LONMAXNOMBREARG+2];
    char argv8[LONMAXNOMBREARG+2];
    //nombre del fichero de trabajo
    char fichero[LONMAXVALORARG+2];
    //estructura possnx
    possnx posicion;
    //identificador de no existencia de s02
    int infoS02=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura possnx
    InicializaPosSnx(&posicion);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc<MINARG)||(argc>MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos los argumentos de entrada a valor vacio
    strcpy(argv1,FLAGVACIO);
    strcpy(argv2,FLAGVACIO);
    strcpy(argv3,FLAGVACIO);
    strcpy(argv4,FLAGVACIO);
    strcpy(argv5,FLAGVACIO);
    strcpy(argv6,FLAGVACIO);
    strcpy(argv7,FLAGVACIO);
    strcpy(argv8,FLAGVACIO);
    //extraemos el nombre del fichero SINEX
    strcpy(argv1,"-f");
    strcpy(fichero,argv[1]);
    //extraccion del resto de argumentos de entrada
    if(argc==2)
    {
        //si solo se ha pasado el nombre del fichero la salida son los codigos
        //de las estaciones
        strcpy(argv2,FLAGCOD);
    }
    //vamos extrayendo el resto de argumentos si ha lugar
    if(argc>=3)
    {
        //extraemos el segundo argumento de entrada
        strcpy(argv2,argv[2]);
        //comprobamos si hay mas argumentos
        if(argc>=4)
        {
            //extraemos el tercer argumento de entrada
            strcpy(argv3,argv[3]);
            //comprobamos si hay mas argumentos
            if(argc>=5)
            {
                //extraemos el cuarto argumento de entrada
                strcpy(argv4,argv[4]);
                //comprobamos si hay mas argumentos
                if(argc>=6)
                {
                    //extraemos el quinto argumento de entrada
                    strcpy(argv5,argv[5]);
                    //comprobamos si hay mas argumentos
                    if(argc>=7)
                    {
                        //extraemos el sexto argumento de entrada
                        strcpy(argv6,argv[6]);
                        //comprobamos si hay mas argumentos
                        if(argc>=8)
                        {
                            //extraemos el septimo argumento de entrada
                            strcpy(argv7,argv[7]);
                            //comprobamos si hay mas argumentos
                            if(argc==9)
                            {
                            //extraemos el septimo argumento de entrada
                                strcpy(argv8,argv[8]);
                            }
                        }
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay que extraer posicion y/o velocidad
    if((!strcmp(argv2,FLAGCOORSNX))||(!strcmp(argv2,FLAGVCCOORSNX))||
       (!strcmp(argv3,FLAGCOORSNX))||(!strcmp(argv3,FLAGVCCOORSNX))||
       (!strcmp(argv4,FLAGCOORSNX))||(!strcmp(argv4,FLAGVCCOORSNX))||
       (!strcmp(argv5,FLAGCOORSNX))||(!strcmp(argv5,FLAGVCCOORSNX))||
       (!strcmp(argv6,FLAGCOORSNX))||(!strcmp(argv6,FLAGVCCOORSNX))||
       (!strcmp(argv7,FLAGCOORSNX))||(!strcmp(argv7,FLAGVCCOORSNX))||
       (!strcmp(argv8,FLAGCOORSNX))||(!strcmp(argv8,FLAGVCCOORSNX)))
    {
        extraePosicion = 1;
    }
    if((!strcmp(argv2,FLAGVELSNX))||(!strcmp(argv2,FLAGVCVELSNX))||
       (!strcmp(argv3,FLAGVELSNX))||(!strcmp(argv3,FLAGVCVELSNX))||
       (!strcmp(argv4,FLAGVELSNX))||(!strcmp(argv4,FLAGVCVELSNX))||
       (!strcmp(argv5,FLAGVELSNX))||(!strcmp(argv5,FLAGVCVELSNX))||
       (!strcmp(argv6,FLAGVELSNX))||(!strcmp(argv6,FLAGVCVELSNX))||
       (!strcmp(argv7,FLAGVELSNX))||(!strcmp(argv7,FLAGVCVELSNX))||
       (!strcmp(argv8,FLAGVELSNX))||(!strcmp(argv8,FLAGVCVELSNX)))
    {
        extraeVelocidad = 1;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay que extraer las matrices de error
    if((!strcmp(argv2,FLAGVCCOORSNX))||(!strcmp(argv2,FLAGVCVELSNX))||
       (!strcmp(argv3,FLAGVCCOORSNX))||(!strcmp(argv3,FLAGVCVELSNX))||
       (!strcmp(argv4,FLAGVCCOORSNX))||(!strcmp(argv4,FLAGVCVELSNX))||
       (!strcmp(argv5,FLAGVCCOORSNX))||(!strcmp(argv5,FLAGVCVELSNX))||
       (!strcmp(argv6,FLAGVCCOORSNX))||(!strcmp(argv6,FLAGVCVELSNX))||
       (!strcmp(argv7,FLAGVCCOORSNX))||(!strcmp(argv7,FLAGVCVELSNX))||
       (!strcmp(argv8,FLAGVCCOORSNX))||(!strcmp(argv8,FLAGVCVELSNX)))
    {
        convierteVC = 1;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos la informacion necesaria del fichero
    cError = LeePosVelFicheroSinex(fichero,
                                   extraePosicion,extraeVelocidad,
                                   convierteVC,convierteVC,
                                   &infoS02,&posicion);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaPosSnx(&posicion);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    //Comprobamos si ho hay s02 si se trabaja con matriz normal
    if(infoS02)
    {
        //lanzamos un mensaje de aviso
        fprintf(stderr,"\n");
        fprintf(stderr,"*****WARNING: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"              El fichero %s contiene matriz normal\n"
                       "              pero no información del valor de s02.\n"
                       "              Se ha utilizado el valor s02=1 para el\n"
                       "              cálculo de la matriz de "
                       "varianzas-covarianzas\n",fichero);
        fprintf(stderr,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //imprimimos los resultados en la salida estandar
    for(i=0;i<posicion.nPuntos;i++)
    {
        ImprimeParametro(argv1,&posicion,i);
        ImprimeParametro(argv2,&posicion,i);
        ImprimeParametro(argv3,&posicion,i);
        ImprimeParametro(argv4,&posicion,i);
        ImprimeParametro(argv5,&posicion,i);
        ImprimeParametro(argv6,&posicion,i);
        ImprimeParametro(argv7,&posicion,i);
        ImprimeParametro(argv8,&posicion,i);
        fprintf(stdout,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaPosSnx(&posicion);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Extrae datos de posición y velocidad de un fichero\n");
    fprintf(stderr,"SINEX (Solution INdependent EXchange)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s fichSINEX [%s] [%sSREF] [%s] [%s] [%s] [%s] [%s]\n\n",
            nombre,FLAGCOD,FLAGSISTREF,FLAGEPOCA,FLAGCOORSNX,FLAGVCCOORSNX,
            FLAGVELSNX,FLAGVCVELSNX);
    fprintf(stderr,"\tfichSINEX: Nombre del fichero SINEX de trabajo\n");
    fprintf(stderr,"\t%s: Imprime el código de las estaciones\n",FLAGCOD);
    fprintf(stderr,"\t%sSREF: Imprime el código del sistema de ",FLAGSISTREF);
    fprintf(stderr,"referencia,\n\t         introducido como SREF\n");
    fprintf(stderr,"\t%s: Imprime la época de cálculo de las estaciones\n",
            FLAGEPOCA);
    fprintf(stderr,"\t%s: Imprime las coordenadas X Y Z de las estaciones\n",
            FLAGCOORSNX);
    fprintf(stderr,"\t%s: Imprime matriz varianza-covarianza ",FLAGVCCOORSNX);
    fprintf(stderr,"de las coordenadas\n\t     (Sx2 Sxy Sxz Sy2 Syz Sz2)\n");
    fprintf(stderr,"\t%s: Imprime las velocidades en X Y Z de las ",FLAGVELSNX);
    fprintf(stderr,"estaciones\n");
    fprintf(stderr,"\t%s: Imprime la matriz varianza-covarianza de las ",
            FLAGVCVELSNX);
    fprintf(stderr,"velocidades\n\t     (Svx2 Svxvy Svxvz Svy2 Svyvz Svz2)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El argumento fichSINEX es obligatorio, mientras que\n");
    fprintf(stderr,"  el resto es opcional\n");
    fprintf(stderr,"**Si sólo se pasa fichSINEX como argumento de entrada,\n");
    fprintf(stderr,"  el programa devuelve el código de las estaciones\n");
    fprintf(stderr,"  contenidas en el fichero\n");
    fprintf(stderr,"**En el caso de no existir el bloque correspondiente a\n");
    fprintf(stderr,"  las matrices de varianzas-covarianzas o correlaciones\n");
    fprintf(stderr,"  en el fichero SINEX original, los parametros de error\n");
    fprintf(stderr,"  son extraidos del bloque SOLUTION/ESTIMATE, que sólo\n");
    fprintf(stderr,"  contiene varianzas (almacena desviaciones típicas,\n");
    fprintf(stderr,"  que se elevan al cuadrado), pero no covarianzas\n");
    fprintf(stderr,"**Los datos solicitados se imprimen en la salida ");
    fprintf(stderr,"estándar (stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeParametro(char argumento[],
                      possnx* posicion,
                      int nPunto)
{
    //formato de la presentacion en pantalla
    char formato[LONFORMATO+2];
    //flag de sistema de referencia
    char flagsr[strlen(FLAGSISTREF)+2];
    //identificador de sistema de referencia
    char sistRef[strlen(argumento)+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos el posible flag y el codigo de sistema de referencia
    if(strlen(argumento)>=strlen(FLAGSISTREF))
    {
        //extraemos el flag de sistema de referencia
        ExtraeSubcadena(argumento,0,strlen(FLAGSISTREF),flagsr);
        //extraemos el codigo del sistema de referencia
        ExtraeSubcadena(argumento,
                        (int)strlen(FLAGSISTREF),
                        (int)(strlen(argumento)-strlen(FLAGSISTREF))+1,
                        sistRef);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //vamos comprobando el argumento a imprimir
    if(!strcmp(argumento,FLAGCOD))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMCOD,SEPARADOR);
        //imprimo el codigo de la estacion
        fprintf(stdout,formato,posicion->puntos[nPunto]);
    }
    else if(!strcmp(argumento,FLAGEPOCA))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMEPOCA,SEPARADOR);
        //imprimo la epoca de observacion
        fprintf(stdout,formato,EpocaSnxFechaAnyo(posicion->epocas[nPunto]));
    }
    else if(!strcmp(argumento,FLAGCOORSNX))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMCOORSNX,SEPARADOR);
        //imprimo las coordenadas de la estacion
        fprintf(stdout,formato,posicion->pos[nPunto][0]);
        fprintf(stdout,formato,posicion->pos[nPunto][1]);
        fprintf(stdout,formato,posicion->pos[nPunto][2]);
    }
    else if(!strcmp(argumento,FLAGVELSNX))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMVELSNX,SEPARADOR);
        //imprimo las coordenadas de la estacion
        fprintf(stdout,formato,posicion->vel[nPunto][0]);
        fprintf(stdout,formato,posicion->vel[nPunto][1]);
        fprintf(stdout,formato,posicion->vel[nPunto][2]);
    }
    else if(!strcmp(argumento,FLAGVCCOORSNX))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMVCCOORSNX,SEPARADOR);
        //imprimo las coordenadas de la estacion
        fprintf(stdout,formato,posicion->vcpos[nPunto][0]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][1]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][2]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][3]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][4]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][5]);
    }
    else if(!strcmp(argumento,FLAGVCVELSNX))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMVCVELSNX,SEPARADOR);
        //imprimo las coordenadas de la estacion
        fprintf(stdout,formato,posicion->vcvel[nPunto][0]);
        fprintf(stdout,formato,posicion->vcvel[nPunto][1]);
        fprintf(stdout,formato,posicion->vcvel[nPunto][2]);
        fprintf(stdout,formato,posicion->vcvel[nPunto][3]);
        fprintf(stdout,formato,posicion->vcvel[nPunto][4]);
        fprintf(stdout,formato,posicion->vcvel[nPunto][5]);
    }
    else if((!strcmp(argumento,FLAGVACIO))||(!strcmp(argumento,FLAGFICHERO)))
    {
        //salimos de la funcion
        return;
    }
    else if(!strcmp(flagsr,FLAGSISTREF))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMSISTREF,SEPARADOR);
        //imprimimos el codigo del sistema de referencia
        fprintf(stdout,formato,sistRef);
    }
    else
    {
        fprintf(stderr,"\n");
        fprintf(stderr,"*****AVISO: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"            El argumento %s es incorrecto\n",argumento);
        fprintf(stderr,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
