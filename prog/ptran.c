/**
\file ptran.c
\brief Programa para la extraccion de parámetros de transformación de un fichero
       de parámetros de transformación entre sistemas de referencia.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 08 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"geodesia.h"
#include"tran.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "ptran"
//numero minimo de argumentos de entrada
#define MINARG 3
//numero maximo de argumentos de entrada
#define MAXARG 7
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
void ImprimeParametro(char argumento[],
                      partran* parametros,
                      int nPunto);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //codigo de error
    int cError=ERRNOERROR;
    //variable para recorrer un bucle
    int i=0;
    //indicador de extraccion de variacion de parámetros
    int extraeVariacion=0;
    //cadenas para almacenar el nombre de los argumentos de entrada
    char argv1[LONMAXNOMBREARG+2];
    char argv2[LONMAXNOMBREARG+2];
    char argv3[LONMAXNOMBREARG+2];
    char argv4[LONMAXNOMBREARG+2];
    char argv5[LONMAXNOMBREARG+2];
    char argv6[LONMAXNOMBREARG+2];
    //nombre del fichero de trabajo
    char fichero[LONMAXVALORARG+2];
    //nombre del bloque de trabajo
    char bloque[LONMAXVALORARG+2];
    //estructura partran
    partran parametros;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura posptos
    InicializaParTran(&parametros);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc<MINARG)||(argc>MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos los argumentos de entrada a valor vacio
    strcpy(argv1,FLAGVACIO);
    strcpy(argv2,FLAGVACIO);
    strcpy(argv3,FLAGVACIO);
    strcpy(argv4,FLAGVACIO);
    strcpy(argv5,FLAGVACIO);
    strcpy(argv6,FLAGVACIO);
    //extraemos el nombre del fichero de trabajo
    strcpy(argv1,"-f");
    strcpy(fichero,argv[1]);
    //extraemos el nombre del bloque de trabajo
    strcpy(argv2,"-b");
    strcpy(bloque,argv[2]);
    //extraccion del resto de argumentos de entrada
    if(argc==3)
    {
        //si solo se ha pasado el nombre del fichero y el bloque de trabajo la
        //salida son los códigos de las transformaciones
        strcpy(argv3,FLAGCODTRAN);
    }
    //vamos extrayendo el resto de argumentos si ha lugar
    if(argc>=4)
    {
        //extraemos el tercer argumentto de entrada
        strcpy(argv3,argv[3]);
        //comprobamos si hay mas argumentos
        if(argc>=5)
        {
            //extraemos el cuarto argumento de entrada
            strcpy(argv4,argv[4]);
            //comprobamos si hay mas argumentos
            if(argc>=6)
            {
                //extraemos el quinto argumento de entrada
                strcpy(argv5,argv[5]);
                //comprobamos si hay mas argumentos
                if(argc==7)
                {
                    //extraemos el sexto argumento de entrada
                    strcpy(argv6,argv[6]);
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay que extraer variacion de los parámetros
    if((!strcmp(argv3,FLAGVPARTRAN))||(!strcmp(argv4,FLAGVPARTRAN))||
       (!strcmp(argv5,FLAGVPARTRAN))||(!strcmp(argv6,FLAGVPARTRAN)))
    {
        extraeVariacion = 1;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos los parámetros del fichero de trabajo
    cError = LeeParFicheroTransf(fichero,bloque,extraeVariacion,&parametros);
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaParTran(&parametros);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //imprimimos los resultados en la salida estandar
    for(i=0;i<parametros.nTransf;i++)
    {
        ImprimeParametro(argv1,&parametros,i);
        ImprimeParametro(argv2,&parametros,i);
        ImprimeParametro(argv3,&parametros,i);
        ImprimeParametro(argv4,&parametros,i);
        ImprimeParametro(argv5,&parametros,i);
        ImprimeParametro(argv6,&parametros,i);
        fprintf(stdout,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaParTran(&parametros);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Extrae parámetros de transformación de un fichero de\n");
    fprintf(stderr,"parámetros de transformación entre sistemas de\n");
    fprintf(stderr,"referencia.\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s fichPTRAN BLOQUE [%s] [%s] [%s] [%s]\n\n",nombre,
            FLAGCODTRAN,FLAGEPOCATRAN,FLAGPARTRAN,FLAGVPARTRAN);
    fprintf(stderr,"\tfichPTRAN: Nombre del fichero de parámetros de ");
    fprintf(stderr,"transformación de trabajo\n");
    fprintf(stderr,"\tBLOQUE: Identificador del bloque de transformaciones\n");
    fprintf(stderr,"\t        a extraer del fichero\n");
    fprintf(stderr,"\t%s: Imprime los códigos de los sistemas de referencia\n",
            FLAGCODTRAN);
    fprintf(stderr,"\t    implicados en las transformaciones\n");
    fprintf(stderr,"\t%s: Imprime las épocas base de los sistemas de\n",
            FLAGEPOCATRAN);
    fprintf(stderr,"\t    referencia implicados en las transformaciones\n");
    fprintf(stderr,"\t%s: Imprime los parámetros de transformación\n",
            FLAGPARTRAN);
    fprintf(stderr,"\t%s: Imprime la variacion de los parámetros de ",
            FLAGVPARTRAN);
    fprintf(stderr,"transformación\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos fichPTRAN y BLOQUE son obligatorios,\n");
    fprintf(stderr,"  mientras que el resto es opcional\n");
    fprintf(stderr,"**Si sólo se pasan fichPTRAN y BLOQUE como argumentos\n");
    fprintf(stderr,"  de entrada, el programa devuelve los códigos de los\n");
    fprintf(stderr,"  sistemas de referencia\n");
    fprintf(stderr,"**Los datos solicitados se imprimen en la salida ");
    fprintf(stderr,"estándar (stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeParametro(char argumento[],
                      partran* parametros,
                      int nPunto)
{
    //formato de la presentacion en pantalla
    char formato[LONFORMATO+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //vamos comprobando el argumento a imprimir
    if(!strcmp(argumento,FLAGCODTRAN))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s%s",FORMCODTRAN,SEPARADOR,FORMCODTRAN,SEPARADOR);
        //imprimo los códigos de los sistemas de referencia
        fprintf(stdout,formato,
                parametros->sistIni[nPunto],
                parametros->sistFin[nPunto]);
    }
    else if(!strcmp(argumento,FLAGEPOCATRAN))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s%s",FORMEPOCATRAN,SEPARADOR,
                                   FORMEPOCATRAN,SEPARADOR);
        //imprimo las epocas base
        fprintf(stdout,formato,parametros->epocas[nPunto][0],
                               parametros->epocas[nPunto][1]);
    }
    else if(!strcmp(argumento,FLAGPARTRAN))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s",FORMTX,SEPARADOR,
                                                       FORMTY,SEPARADOR,
                                                       FORMTZ,SEPARADOR,
                                                       FORMD,SEPARADOR,
                                                       FORMRX,SEPARADOR,
                                                       FORMRY,SEPARADOR,
                                                       FORMRZ,SEPARADOR);
        //imprimo los parámetros de transformación
        fprintf(stdout,formato,parametros->param[nPunto][0]/CMM,
                               parametros->param[nPunto][1]/CMM,
                               parametros->param[nPunto][2]/CMM,
                               parametros->param[nPunto][3],
                               parametros->param[nPunto][4]/MSR,
                               parametros->param[nPunto][5]/MSR,
                               parametros->param[nPunto][6]/MSR);
    }
    else if(!strcmp(argumento,FLAGVPARTRAN))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s",FORMVTX,SEPARADOR,
                                                       FORMVTY,SEPARADOR,
                                                       FORMVTZ,SEPARADOR,
                                                       FORMVD,SEPARADOR,
                                                       FORMVRX,SEPARADOR,
                                                       FORMVRY,SEPARADOR,
                                                       FORMVRZ,SEPARADOR);
        //imprimo las valocidades de cambio de los parámetros de transformación
        fprintf(stdout,formato,parametros->variac[nPunto][0]/CMM,
                               parametros->variac[nPunto][1]/CMM,
                               parametros->variac[nPunto][2]/CMM,
                               parametros->variac[nPunto][3],
                               parametros->variac[nPunto][4]/MSR,
                               parametros->variac[nPunto][5]/MSR,
                               parametros->variac[nPunto][6]/MSR);
    }
    else if((!strcmp(argumento,FLAGVACIO))||(!strcmp(argumento,FLAGFICHERO))||
            (!strcmp(argumento,FLAGBLOQUE)))
    {
        //salimos de la funcion
        return;
    }
    else
    {
        fprintf(stderr,"\n");
        fprintf(stderr,"*****AVISO: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"            El argumento %s es incorrecto\n",argumento);
        fprintf(stderr,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
