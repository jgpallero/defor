/**
\file cptos.c
\brief Programa para la impresion de la cabecera de informacion de un fichero de
       base de datos de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 27 de febrero de 2010
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<stdlib.h>
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "cptos"
//numero minimo de argumentos de entrada
#define NARG 1
//cabecera del fichero
#define TEXTO "\
********************************************************************************\n\
* DEFINICIÓN DE UN FICHERO DE BASE DE DATOS DE PUNTOS EN FORMATO DEFOR\n\
*\n\
* Cada bloque de puntos tiene una etiqueta identificadora de inicio y final,\n\
* cada una en una línea individual del fichero\n\
*\n\
* En la primera columna de la línea que contiene la etiqueta de inicio de bloque\n\
* ha de aparecer el carácter \"+\", mientras que para la etiqueta de fin el\n\
* carácter sera \"-\"\n\
*\n\
* Cada definición de punto consta de un máximo de dos líneas: la primera para el\n\
* código, el sistema de referencia y las coordenadas y la segunda (que puede\n\
* aparecer o no) para la matriz de varianza-covarianza\n\
*\n\
* Cadenas de formato en lenguaje C de almacenamiento de puntos:\n\
* Línea de coordenadas:      \">%8s %8s %13.8lf %13.4lf %13.4lf %13.4lf\\n\"\n\
* Línea de matriz var.-cov.: \" %12.5E %12.5E %12.5E %12.5E %12.5E %12.5E\\n\"\n\
*\n\
* Las columnas correspondientes son:\n\
* - Primera línea:\n\
*   - 1. Código del punto\n\
*   - 2. Código del sistema de referencia del punto\n\
*   - 3. Época del punto, en año y decimal de año\n\
*   - 4. Coordenada X cartesiana geocéntrica\n\
*   - 5. Coordenada Y cartesiana geocéntrica\n\
*   - 6. Coordenada Z cartesiana geocéntrica\n\
* - Segunda línea:\n\
*   - 1. Varianza de la coordenada X\n\
*   - 2. Covarianza XY\n\
*   - 3. Covarianza XZ\n\
*   - 4. Varianza de la coordenada Y\n\
*   - 5. Covarianza YZ\n\
*   - 6. Varianza de la coordenada Z\n\
*\n\
* Para crear puntos en el formato descrito puede utilizarse el programa \"fptos\"\n\
********************************************************************************"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //sentencia tonta para que no de warning el compilador
    argv = argv;
    //comprobamos si el numero de argumentos de entrada es correcto
    if(argc!=NARG)
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //imprimimos el texto
    fprintf(stdout,"%s\n",TEXTO);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Imprime una cabecera con información sobre el formato\n");
    fprintf(stderr,"de un fichero de base de datos de puntos\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s\n\n",nombre);
    fprintf(stderr,"**Los datos solicitados se imprimen en la salida ");
    fprintf(stderr,"estándar (stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2010, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
