/**
\file tsr.c
\brief Programa para la transformación entre sistemas de referencia de puntos
       expresados en coordenadas cartesianas tridimensionales geocéntricas.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 29 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"tran.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "tsr"
//numero minimo de argumentos de entrada
#define MINARG 4
//numero intermedio de argumentos de entrada
#define INTARG 10
//numero maximo de argumentos de entrada
#define MAXARG 16
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
void ImprimeResultados(int formato,
                       char cod[],
                       char sr[],
                       double t,
                       double x,
                       double y,
                       double z,
                       double vx,
                       double vxy,
                       double vxz,
                       double vy,
                       double vyz,
                       double vz);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //Código de error
    int cError=ERRNOERROR;
    //contador de puntos
    int contador=0;
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadena de formato para entrada
    char fte[LONFORMATO+2];
    //parametros de entrada por pipe
    char cod[LONCOD+2],sr[LONCOD+2];
    double t=0.0;
    double x=0.0,y=0.0,z=0.0;
    double vx=0.0,vxy=0.0,vxz=0.0,vy=0.0,vyz=0.0,vz=0.0;
    //parametros de salida
    double xt=0.0,yt=0.0,zt=0.0;
    double vxt=0.0,vxtyt=0.0,vxtzt=0.0,vyt=0.0,vytzt=0.0,vzt=0.0;
    //indicador de formato de salida
    int formato=0;
    //estructura partran
    partran pTRAN;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura partran
    InicializaParTran(&pTRAN);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=MINARG)&&(argc!=INTARG)&&(argc!=MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //leemos el fichero de parametros de transformación
    cError = LeeParFicheroTransf(argv[1],argv[2],1,&pTRAN);
    //comprobamos el Código de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos memoria
        LiberaMemoriaParTran(&pTRAN);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if(argc==MINARG)
    {
        //construyo el formato de entrada
        sprintf(fte,
         "%%s%s%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           cod,sr,&t,
                           &x,&y,&z,
                           &vx,&vxy,&vxz,&vy,&vyz,&vz);
            //compruebo el numero de elementos pasado
            if((nElem!=6)&&(nElem!=12))
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //liberamos memoria
                LiberaMemoriaParTran(&pTRAN);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //actualizamos el contador de puntos
            contador++;
            //asigno ceros a la matriz de varianza-covarianza si no se ha
            //introducido y actualizo la variable indicadora de formato de
            //salida
            if(nElem==6)
            {
                vx = 0.0;
                vxy = 0.0;
                vxz = 0.0;
                vy = 0.0;
                vyz = 0.0;
                vz = 0.0;
                formato = 0;
            }
            else
            {
                formato = 1;
            }
            //realizamos la transformación
            cError = PuntoSistRefTransf(&pTRAN,
                                        sr,argv[3],
                                        t,
                                        x,y,z,
                                        vx,vxy,vxz,vy,vyz,vz,
                                        &xt,&yt,&zt,
                                        &vxt,&vxtyt,&vxtzt,&vyt,&vytzt,&vzt);
            //comprobamos si se ha podido hacer la transformación
            if(cError==ERRNOTRANSF)
            {
                fprintf(stderr,"***Para el punto pasado en la posición ");
                fprintf(stderr,"%d (%s)\n",contador,cod);
                fprintf(stderr,"   no se ha podido calcular la ");
                fprintf(stderr,"transformación\n");
                fprintf(stderr,"   entre los sistemas %s -> %s\n",sr,argv[3]);
                fprintf(stderr,"   Las coordenadas transformadas son las\n");
                fprintf(stderr,"   mismas que las coordenadas iniciales\n");
            }
            //imprimimos el resultado
            ImprimeResultados(formato,
                              cod,argv[3],t,
                              xt,yt,zt,
                              vxt,vxtyt,vxtzt,vyt,vytzt,vzt);
        }
    }
    else
    {
        //asignamos los elementos de entrada
        strcpy(cod,argv[4]);
        strcpy(sr,argv[5]);
        t = atof(argv[6]);
        x = atof(argv[7]);
        y = atof(argv[8]);
        z = atof(argv[9]);
        if(argc==INTARG)
        {
            vx = 0.0;
            vxy = 0.0;
            vxz = 0.0;
            vy = 0.0;
            vyz = 0.0;
            vz = 0.0;
            formato = 0;
        }
        else
        {
            vx = atof(argv[10]);
            vxy = atof(argv[11]);
            vxz = atof(argv[12]);
            vy = atof(argv[13]);
            vyz = atof(argv[14]);
            vz = atof(argv[15]);
            formato = 1;
        }
        //realizamos la transformación
        cError = PuntoSistRefTransf(&pTRAN,
                                     sr,argv[3],
                                     t,
                                     x,y,z,
                                     vx,vxy,vxz,vy,vyz,vz,
                                     &xt,&yt,&zt,
                                     &vxt,&vxtyt,&vxtzt,&vyt,&vytzt,&vzt);
            //comprobamos si se ha podido hacer la transformación
        if(cError==ERRNOTRANSF)
        {
            fprintf(stderr,"***Para el punto pasado: %s\n",cod);
            fprintf(stderr,"   no se ha podido calcular la ");
            fprintf(stderr,"transformación\n");
            fprintf(stderr,"   entre los sistemas %s -> %s\n",sr,argv[3]);
            fprintf(stderr,"   Las coordenadas transformadas son las\n");
            fprintf(stderr,"   mismas que las coordenadas iniciales\n");
        }
        //imprimimos el resultado
        ImprimeResultados(formato,
                          cod,argv[3],t,
                          xt,yt,zt,
                          vxt,vxtyt,vxtzt,vyt,vytzt,vzt);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaParTran(&pTRAN);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Transformación entre sistemas de referencia de puntos\n");
    fprintf(stderr,"en coordenadas cartesianas tridimensionales ");
    fprintf(stderr,"geocéntricas\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s fichTRANSF bloqTRANSF srDEST ",nombre);
    fprintf(stderr,"cod srORIG t x y z [Sx2 Sxy Sxz Sy2 Syz Sz2]\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"\tfichTRANSF: Nombre del fichero de parametros de\n");
    fprintf(stderr,"\t            transformación\n");
    fprintf(stderr,"\tbloqTRANSF: Bloque de trabajo en el fichero de\n");
    fprintf(stderr,"\t            parametros de transformación\n");
    fprintf(stderr,"\tsrDEST: Código del sistema de referencia destino de\n");
    fprintf(stderr,"\t        la transformación\n");
    fprintf(stderr,"\tcod: Código del punto de trabajo\n");
    fprintf(stderr,"\tsrORIG: Código del sistema de referencia origen del\n");
    fprintf(stderr,"\t        punto de trabajo\n");
    fprintf(stderr,"\tt: Época a la que vienen referidas las coordenadas\n");
    fprintf(stderr,"\t   del punto de trabajo\n");
    fprintf(stderr,"\tx: Coordenada X geocéntrica del punto de trabajo\n");
    fprintf(stderr,"\ty: Coordenada Y geocéntrica del punto de trabajo\n");
    fprintf(stderr,"\tz: Coordenada Z geocéntrica del punto de trabajo\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxz: Covarianza entre las coordenadas X y Z\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyz: Covarianza entre las coordenadas Y y Z\n");
    fprintf(stderr,"\tSz2: Varianza de la coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El formato de salida es el siguiente:\n\n");
    fprintf(stderr,"\tcod srDEST t x y z [Sx2 Sxy Sxz Sy2 Syz Sz2]\n\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tsrDEST: Código del sistema de referencia destino del\n");
    fprintf(stderr,"\t        punto de trabajo\n");
    fprintf(stderr,"\tt: Época a la que vienen referidas las coordenadas\n");
    fprintf(stderr,"\t   del punto de trabajo\n");
    fprintf(stderr,"\tx: Coordenada X geocéntrica del punto transformado\n");
    fprintf(stderr,"\ty: Coordenada Y geocéntrica del punto transformado\n");
    fprintf(stderr,"\tz: Coordenada Z geocéntrica del punto transformado\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxz: Covarianza entre las coordenadas X y Z\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyz: Covarianza entre las coordenadas Y y Z\n");
    fprintf(stderr,"\tSz2: Varianza de la coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería (pipe)\n");
    fprintf(stderr,"  sólo deben pasarse los argumentos fichTRANSF, ");
    fprintf(stderr,"bloqTRANSF y srDEST,\n  ninguno más\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeResultados(int formato,
                       char cod[],
                       char sr[],
                       double t,
                       double x,
                       double y,
                       double z,
                       double vx,
                       double vxy,
                       double vxz,
                       double vy,
                       double vyz,
                       double vz)
{
    //cadenas de formato para la salida
    char ftr[LONFORMATO+2],ftc[LONFORMATO+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos el formato de salida resumida (sin informacion de errores)
    sprintf(ftr,"%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCODPTOS,SEPARADOR,FORMSISTREF,SEPARADOR,FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC);
    //creamos el formato de salida completa (con informacion de errores)
    sprintf(ftc,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCODPTOS,SEPARADOR,FORMSISTREF,SEPARADOR,FORMEPOCAPTOS,SEPARADOR,
            FORMXGEOC,SEPARADOR,FORMYGEOC,SEPARADOR,FORMZGEOC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //escribimos la salida en pantalla
    if(formato==0)
    {
        //escribimos la salida resumida
        fprintf(stdout,ftr,cod,sr,t,x,y,z);
    }
    else
    {
        //escribimos la salida completa
        fprintf(stdout,ftc,cod,sr,t,x,y,z,vx,vxy,vxz,vy,vyz,vz);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
