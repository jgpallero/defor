/**
\file enutri.c
\brief Programa para pasar de un punto expresado en coordenadas cartesianas
       topocéntricas (east, north, up) a un vector expresado en incrementos de
       coordenadas cartesianas tridimensionales geocéntricas con origen en el
       origen del sistema topocentrico.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"paramprg.h"
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "enutri"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadenas de formato para entrada y salidas incompleta y completa
    char fte[LONFORMATO+2],ftsi[LONFORMATO+2],ftsc[LONFORMATO+2];
    //parametros de entrada por pipe
    char cod[LONCOD+2];
    double lat=0.0,lon=0.0;
    double e=0.0,n=0.0,u=0.0;
    double vare=0.0,varen=0.0,vareu=0.0,varn=0.0,varnu=0.0,varu=0.0;
    //parametros de salida
    double x=0.0,y=0.0,z=0.0;
    double varx=0.0,varxy=0.0,varxz=0.0,vary=0.0,varyz=0.0,varz=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=2)&&(argc!=6)&&(argc!=7)&&(argc!=12)&&(argc!=13))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos el formato de salida incompleta
    sprintf(ftsi,"%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,
            FORMLAT,SEPARADOR,FORMLON,SEPARADOR,
            FORMDXGEOC,SEPARADOR,FORMDYGEOC,SEPARADOR,FORMDZGEOC);
    //creamos el formato de salida completa
    sprintf(ftsc,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,
            FORMLAT,SEPARADOR,FORMLON,SEPARADOR,
            FORMDXGEOC,SEPARADOR,FORMDYGEOC,SEPARADOR,FORMDZGEOC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if(argc==2)
    {
        //compruebo si el argumento indicador de entrada por pipe es correcto
        if(strcmp(argv[1],FLAGPIPE))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Argumento %s incorrecto\n",argv[1]);
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //construyo el formato de entrada con el maximo numero de argumentos
        sprintf(fte,
        "%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           cod,
                           &lat,&lon,
                           &e,&n,&u,
                           &vare,&varen,&vareu,&varn,&varnu,&varu);
            //dependiendo del numero de argumentos hago la lectura correcta
            if(nElem==5)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,&lon,
                       &e,&n,&u);
                //convertimos en numero la cadena de texto leida como coordenda
                lat = atof(cod);
                //asignamos 0.0 a los datos no pasados
                vare = 0.0;
                varen = 0.0;
                vareu = 0.0;
                varn = 0.0;
                varnu = 0.0;
                varu = 0.0;
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if(nElem==6)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,
                       &lat,&lon,
                       &e,&n,&u);
                //asignamos 0.0 a los datos no pasados
                vare = 0.0;
                varen = 0.0;
                vareu = 0.0;
                varn = 0.0;
                varnu = 0.0;
                varu = 0.0;
            }
            else if(nElem==11)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,&lon,
                       &e,&n,&u,
                       &vare,&varen,&vareu,&varn,&varnu,&varu);
                //convertimos en numero la cadena de texto leida como coordenada
                lat = atof(cod);
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if(nElem==12)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,
                       &lat,&lon,
                       &e,&n,&u,
                       &vare,&varen,&vareu,&varn,&varnu,&varu);
            }
            else
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //calculamos (las coordenadas geodesicas se pasan a radianes)
            EnuTriVC(lat*DR,lon*DR,
                     e,n,u,
                     vare,varen,vareu,varn,varnu,varu,
                     &x,&y,&z,
                     &varx,&varxy,&varxz,&vary,&varyz,&varz);
            //presentamos los resultados en la salida estandar
            if((nElem==5)||(nElem==6))
            {
                //imprimimos resultados
                fprintf(stdout,ftsi,
                        cod,
                        lat,lon,
                        x,y,z);
            }
            else
            {
                //imprimimos resultados
                fprintf(stdout,ftsc,
                        cod,
                        lat,lon,
                        x,y,z,
                        varx,varxy,varxz,vary,varyz,varz);
            }
        }
    }
    else
    {
        //asignamos los elementos de entrada dependiendo del numero
        if(argc==6)
        {
            //asignamos al codigo una cadena vacia
            strcpy(cod,"");
            //extraemos los datos de trabajo
            lat = atof(argv[1]);
            lon = atof(argv[2]);
            e = atof(argv[3]);
            n = atof(argv[4]);
            u = atof(argv[5]);
            //asignamos 0.0 a los datos no pasados
            vare = 0.0;
            varen = 0.0;
            vareu = 0.0;
            varn = 0.0;
            varnu = 0.0;
            varu = 0.0;
        }
        else if(argc==7)
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[1]);
            lat = atof(argv[2]);
            lon = atof(argv[3]);
            e = atof(argv[4]);
            n = atof(argv[5]);
            u = atof(argv[6]);
            //asignamos 0.0 a los datos no pasados
            vare = 0.0;
            varen = 0.0;
            vareu = 0.0;
            varn = 0.0;
            varnu = 0.0;
            varu = 0.0;
        }
        else if(argc==12)
        {
            //asignamos al codigo una cadena vacia
            strcpy(cod,"");
            //extraemos los datos de trabajo
            lat = atof(argv[1]);
            lon = atof(argv[2]);
            e = atof(argv[3]);
            n = atof(argv[4]);
            u = atof(argv[5]);
            vare = atof(argv[6]);
            varen = atof(argv[7]);
            vareu = atof(argv[8]);
            varn = atof(argv[9]);
            varnu = atof(argv[10]);
            varu = atof(argv[11]);
        }
        else
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[1]);
            lat = atof(argv[2]);
            lon = atof(argv[3]);
            e = atof(argv[4]);
            n = atof(argv[5]);
            u = atof(argv[6]);
            vare = atof(argv[7]);
            varen = atof(argv[8]);
            vareu = atof(argv[9]);
            varn = atof(argv[10]);
            varnu = atof(argv[11]);
            varu = atof(argv[12]);
        }
        //calculamos (las coordenadas geodesicas se pasan a radianes)
        EnuTriVC(lat*DR,lon*DR,
                 e,n,u,
                 vare,varen,vareu,varn,varnu,varu,
                 &x,&y,&z,
                 &varx,&varxy,&varxz,&vary,&varyz,&varz);
        //presentamos los resultados en la salida estandar
        if((argc==6)||(argc==7))
        {
            //imprimimos resultados
            fprintf(stdout,ftsi,
                    cod,
                    lat,lon,
                    x,y,z);
        }
        else
        {
            //imprimimos resultados
            fprintf(stdout,ftsc,
                    cod,
                    lat,lon,
                    x,y,z,
                    varx,varxy,varxz,vary,varyz,varz);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Pasa de un punto expresado en coordenadas cartesianas\n");
    fprintf(stderr,"topocéntricas (east, north, up) a un vector expresado\n");
    fprintf(stderr,"en incrementos de coordenadas cartesianas\n");
    fprintf(stderr,"tridimensionales geocéntricas con origen en el origen\n");
    fprintf(stderr,"del sistema topocéntrico\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s [%s] [cod] lat lon e n u [Se2 Sen Seu Sn2 Snu Su2]\n\n",
            nombre,FLAGPIPE);
    fprintf(stderr,"\t%s: Indicador de que la entrada es a través ",FLAGPIPE);
    fprintf(stderr,"de una tubería (pipe)\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tlat: Latitud del punto estación. En grados ");
    fprintf(stderr,"sexagesimales, formato decimal\n");
    fprintf(stderr,"\tlon: Longitud del punto estación. En grados ");
    fprintf(stderr,"sexagesimales, formato decimal\n");
    fprintf(stderr,"\te: Coordenada east\n");
    fprintf(stderr,"\tn: Coordenada north\n");
    fprintf(stderr,"\tu: Coordenada up\n");
    fprintf(stderr,"\tSe2: Varianza de la coordenada east\n");
    fprintf(stderr,"\tSen: Covarianza entre las coordenadas east y north\n");
    fprintf(stderr,"\tSeu: Covarianza entre las coordenadas east y up\n");
    fprintf(stderr,"\tSn2: Varianza de la coordenada north\n");
    fprintf(stderr,"\tSnu: Covarianza entre las coordenadas north y up\n");
    fprintf(stderr,"\tSu2: Varianza de la coordenada up\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El formato de salida es el siguiente:\n\n");
    fprintf(stderr,"\t[cod] lat lon dx dy dz [Sdx2 Sdxdy Sdxdz Sdy2 Sdydz ");
    fprintf(stderr,"Sdz2]\n\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tlat: Latitud del punto estación. En grados ");
    fprintf(stderr,"sexagesimales, formato decimal\n");
    fprintf(stderr,"\tlon: Longitud del punto estación. En grados ");
    fprintf(stderr,"sexagesimales, formato decimal\n");
    fprintf(stderr,"\tdx: Incremento de coordenada X geocentrica\n");
    fprintf(stderr,"\tdy: Incremento de coordenada Y geocentrica\n");
    fprintf(stderr,"\tdz: Incremento de coordenada Z geocentrica\n");
    fprintf(stderr,"\tSdx2: Varianza del incremento de coordenada X\n");
    fprintf(stderr,"\tSdxdy: Covarianza entre los incrementos X e Y\n");
    fprintf(stderr,"\tSdxdz: Covarianza entre los incrementos X y Z\n");
    fprintf(stderr,"\tSdy2: Varianza del incremento de coordenada Y\n");
    fprintf(stderr,"\tSdydz: Covarianza entre los incrementos Y y Z\n");
    fprintf(stderr,"\tSdz2: Varianza del incremento de coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería solo\n");
    fprintf(stderr,"  debe pasarse el argumento %s, ninguno más\n",FLAGPIPE);
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la ");
    fprintf(stderr,"salida de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
