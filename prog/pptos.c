/**
\file pptos.c
\brief Programa para la extraccion de datos de posicion de un fichero de base de
       datos de puntos.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"ptos.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "pptos"
//numero minimo de argumentos de entrada
#define MINARG 3
//numero maximo de argumentos de entrada
#define MAXARG 9
//etiqueta de no existencia de bloque
#define NOBLOQUE 0
//etiqueta de existencia de bloque
#define SIBLOQUE 1
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
void ImprimeParametro(char argumento[],
                      posptos* posicion,
                      int nPunto);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //código de error
    int cError=ERRNOERROR;
    //variable para recorrer un bucle
    int i=0;
    //indicador de extraccion de parametros de error
    int extraeVarCov=0;
    //cadenas para almacenar el nombre de los argumentos de entrada
    char argv1[LONMAXNOMBREARG+2];
    char argv2[LONMAXNOMBREARG+2];
    char argv3[LONMAXNOMBREARG+2];
    char argv4[LONMAXNOMBREARG+2];
    char argv5[LONMAXNOMBREARG+2];
    char argv6[LONMAXNOMBREARG+2];
    char argv7[LONMAXNOMBREARG+2];
    char argv8[LONMAXNOMBREARG+2];
    //nombre del fichero de trabajo
    char fichero[LONMAXVALORARG+2];
    //nombre del bloque de trabajo
    char bloque[LONMAXVALORARG+2];
    //estructura posptos
    posptos posicion;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura posptos
    InicializaPosPtos(&posicion);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc<MINARG)||(argc>MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos los argumentos de entrada a valor vacio
    strcpy(argv1,FLAGVACIO);
    strcpy(argv2,FLAGVACIO);
    strcpy(argv3,FLAGVACIO);
    strcpy(argv4,FLAGVACIO);
    strcpy(argv5,FLAGVACIO);
    strcpy(argv6,FLAGVACIO);
    strcpy(argv7,FLAGVACIO);
    strcpy(argv8,FLAGVACIO);
    //extraemos el nombre del fichero de base de datos de puntos
    strcpy(argv1,"-f");
    strcpy(fichero,argv[1]);
    //extraemos el nombre del bloque de puntos de trabajo
    strcpy(argv2,"-b");
    strcpy(bloque,argv[2]);
    //extraccion del resto de argumentos de entrada
    if(argc==3)
    {
        //si solo se ha pasado el nombre del fichero y el bloque de trabajo la
        //salida son los códigos de las estaciones
        strcpy(argv3,FLAGCOD);
    }
    //vamos extrayendo el resto de argumentos si ha lugar
    if(argc>=4)
    {
        //extraemos el tercer argumentto de entrada
        strcpy(argv3,argv[3]);
        //comprobamos si hay mas argumentos
        if(argc>=5)
        {
            //extraemos el cuarto argumento de entrada
            strcpy(argv4,argv[4]);
            //comprobamos si hay mas argumentos
            if(argc>=6)
            {
                //extraemos el quinto argumento de entrada
                strcpy(argv5,argv[5]);
                //comprobamos si hay mas argumentos
                if(argc>=7)
                {
                    //extraemos el sexto argumento de entrada
                    strcpy(argv6,argv[6]);
                    //comprobamos si hay mas argumentos
                    if(argc>=8)
                    {
                        //extraemos el septimo argumento de entrada
                        strcpy(argv7,argv[7]);
                        if(argc==9)
                        {
                            //extraemos el octavo argumento de entrada
                            strcpy(argv8,argv[8]);
                        }
                    }
                }
            }
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si hay que extraer parametros de precision
    if((!strcmp(argv3,FLAGVCCOORPTOS))||(!strcmp(argv4,FLAGVCCOORPTOS))||
       (!strcmp(argv5,FLAGVCCOORPTOS))||(!strcmp(argv6,FLAGVCCOORPTOS))||
       (!strcmp(argv7,FLAGVCCOORPTOS))||(!strcmp(argv8,FLAGVCCOORPTOS)))
    {
        extraeVarCov = 1;
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos los puntos del fichero de trabajo
    cError = LeePosFicheroPuntos(fichero,bloque,extraeVarCov,&posicion);
    //comprobamos si se trabaja en modo test de existencia
    if((!strcmp(argv3,FLAGEX))||(!strcmp(argv4,FLAGEX))||
       (!strcmp(argv5,FLAGEX))||(!strcmp(argv6,FLAGEX))||
       (!strcmp(argv7,FLAGEX))||(!strcmp(argv8,FLAGEX)))
    {
        //comprobamos si el bloque existe
        if(cError==ERRNOERROR)
        {
            //imprimimos que existe
            fprintf(stdout,"%d\n",SIBLOQUE);
        }
        else if(cError==ERRNOBLOQUEFICH)
        {
            //imprimimos que no existe
            fprintf(stdout,"%d\n",NOBLOQUE);
        }
        //salimos de la función
        return 0;
    }
    //comprobamos el código de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaPosPtos(&posicion);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //imprimimos los resultados en la salida estandar
    for(i=0;i<posicion.nPuntos;i++)
    {
        ImprimeParametro(argv1,&posicion,i);
        ImprimeParametro(argv2,&posicion,i);
        ImprimeParametro(argv3,&posicion,i);
        ImprimeParametro(argv4,&posicion,i);
        ImprimeParametro(argv5,&posicion,i);
        ImprimeParametro(argv6,&posicion,i);
        ImprimeParametro(argv7,&posicion,i);
        fprintf(stdout,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaPosPtos(&posicion);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Extrae datos de posición de un fichero de base de datos\n");
    fprintf(stderr,"de puntos\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s fichPTOS BLOQUE [%s] [%s] [%s] [%s] [%s] [%s]\n\n",
            nombre,
            FLAGEX,FLAGCOD,FLAGSISTREF,FLAGEPOCA,FLAGCOORPTOS,FLAGVCCOORPTOS);
    fprintf(stderr,"\tfichPTOS: Nombre del fichero de base de datos de ");
    fprintf(stderr,"puntos de trabajo\n");
    fprintf(stderr,"\tBLOQUE: Identificador del bloque de puntos a extraer\n");
    fprintf(stderr,"\t        del fichero de base de datos de puntos\n");
    fprintf(stderr,"\t%s: Imprime el valor %d si el bloque BLOQUE existe en "
                   "fichPTOS y %d en caso contrario\n",FLAGEX,SIBLOQUE,
            NOBLOQUE);
    fprintf(stderr,"\t%s: Imprime el código de las estaciones\n",FLAGCOD);
    fprintf(stderr,"\t%s: Imprime el código del sistema de ",FLAGSISTREF);
    fprintf(stderr,"referencia al que esta anclado el punto\n");
    fprintf(stderr,"\t%s: Imprime la época asociada a las estaciones\n",
            FLAGEPOCA);
    fprintf(stderr,"\t%s: Imprime las coordenadas X Y Z de las estaciones\n",
            FLAGCOORPTOS);
    fprintf(stderr,"\t%s: Imprime matriz varianza-covarianza ",FLAGVCCOORPTOS);
    fprintf(stderr,"de las coordenadas\n\t     (Sx2 Sxy Sxz Sy2 Syz Sz2)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos fichPTOS y BLOQUE son obligatorios,\n");
    fprintf(stderr,"  mientras que el resto es opcional\n");
    fprintf(stderr,"**Si sólo se pasan fichPTOS y BLOQUE como argumentos de\n");
    fprintf(stderr,"  entrada, el programa devuelve el código de las\n");
    fprintf(stderr,"  estaciones contenidas en el fichero\n");
    fprintf(stderr,"**Si se pasa el argumento %s, todos los demás no se tienen "
                   "en cuenta\n",FLAGEX);
    fprintf(stderr,"**Los datos solicitados se imprimen en la salida ");
    fprintf(stderr,"estándar (stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeParametro(char argumento[],
                      posptos* posicion,
                      int nPunto)
{
    //formato de la presentacion en pantalla
    char formato[LONFORMATO+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //vamos comprobando el argumento a imprimir
    if(!strcmp(argumento,FLAGCOD))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMCODPTOS,SEPARADOR);
        //imprimo el código de la estacion
        fprintf(stdout,formato,posicion->puntos[nPunto]);
    }
    else if(!strcmp(argumento,FLAGSISTREF))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMSISTREF,SEPARADOR);
        //imprimo la epoca de observacion
        fprintf(stdout,formato,posicion->sistRef[nPunto]);
    }
    else if(!strcmp(argumento,FLAGEPOCA))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMEPOCAPTOS,SEPARADOR);
        //imprimo la epoca de observacion
        fprintf(stdout,formato,posicion->epocas[nPunto]);
    }
    else if(!strcmp(argumento,FLAGCOORPTOS))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s%s%s%s",FORMXGEOC,SEPARADOR,
                                       FORMYGEOC,SEPARADOR,
                                       FORMZGEOC,SEPARADOR);
        //imprimo las coordenadas de la estacion
        fprintf(stdout,formato,posicion->pos[nPunto][0],
                               posicion->pos[nPunto][1],
                               posicion->pos[nPunto][2]);
    }
    else if(!strcmp(argumento,FLAGVCCOORPTOS))
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s",FORMVC,SEPARADOR);
        //imprimo las coordenadas de la estacion
        fprintf(stdout,formato,posicion->vcpos[nPunto][0]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][1]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][2]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][3]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][4]);
        fprintf(stdout,formato,posicion->vcpos[nPunto][5]);
    }
    else if((!strcmp(argumento,FLAGVACIO))||(!strcmp(argumento,FLAGFICHERO))||
            (!strcmp(argumento,FLAGBLOQUE)))
    {
        //salimos de la funcion
        return;
    }
    else
    {
        fprintf(stderr,"\n");
        fprintf(stderr,"*****AVISO: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"            El argumento %s es incorrecto\n",argumento);
        fprintf(stderr,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
