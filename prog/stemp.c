/**
\file stemp.c
\brief Programa para el calculo de series temporales.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 15 de septiembre de 2009
\date 27 de febrero de 2010, reescritura casi completa
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include"errores.h"
#include"paramprg.h"
#include"ptos.h"
#include"series.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "stemp"
//numero minimo de argumentos de entrada
#define MINARG 6
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //codigo de error
    int cError=ERRNOERROR;
    //indice para recorrer bucles
    int i=0;
    //identificador de creación del script de GNU Octave
    char creaScript[LONMAXVALORARG+1];
    //sistemas de coordenadas
    char sistCoor[LONMAXVALORARG+1];
    //directorio para guardar las series temporales
    char dirSeries[LONMAXVALORARG+1];
    //nombre del fichero de puntos
    char nomPtos[LONMAXVALORARG+1];
    //identificador de fichero para escribir el script de GNU Octave
    FILE* idFich=NULL;
    //estructura posptos
    posptos posicion;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos la estructura posptos
    InicializaPosPtos(&posicion);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if(argc<MINARG)
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos el codigo de creación del script de GNU Octave
    strcpy(creaScript,argv[1]);
    //extraemos el codigo del sistema de coordenadas
    strcpy(sistCoor,argv[2]);
    //extraemos el nombre del directorio para almacenar las series temporales
    strcpy(dirSeries,argv[3]);
    //extraemos el nombre del fichero de base de datos de puntos
    strcpy(nomPtos,argv[4]);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //compruebo si los códigos de script y sistemas de coordenadas son correctos
    if((strcmp(creaScript,FLAGSOCTAVE))&&(strcmp(creaScript,FLAGNOCTAVE))&&
       (strcmp(sistCoor,FLAGTSERIEENU))&&(strcmp(sistCoor,FLAGTSERIEUTM))&&
       (strcmp(sistCoor,FLAGTSERIEGEOC))&&(strcmp(sistCoor,FLAGTSERIEPLANO))&&
       (strcmp(sistCoor,FLAGTSERIETODO)))
    {
        //lanzamos el mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaPosPtos(&posicion);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //extraemos los puntos del fichero de trabajo
    for(i=5;i<argc;i++)
    {
        cError = LeePosFicheroPuntos(nomPtos,argv[i],1,&posicion);
        //comprobamos el codigo de error devuelto
        if(cError!=ERRNOERROR)
        {
            //lanzamos el mensaje de error
            MensajeErrorPrograma(cError,PROGRAMA);
            //liberamos la memoria utilizada
            LiberaMemoriaPosPtos(&posicion);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //compruebo si hay que generar script de Octave
    if(!strcmp(creaScript,FLAGSOCTAVE))
    {
        //indicamos que se ha de escribir en la salida estándar
        idFich = stdout;
    }
    //calculamos las series temporales
    cError = SerieTempPtosOctave(&posicion,sistCoor,dirSeries,idFich,"stdout");
    //comprobamos el codigo de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //liberamos la memoria utilizada
        LiberaMemoriaPosPtos(&posicion);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //liberamos memoria
    LiberaMemoriaPosPtos(&posicion);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Calcula series temporales a partir de datos almacenados\n");
    fprintf(stderr,"en un fichero de base de datos de puntos y genera un\n");
    fprintf(stderr,"script de GNU Octave para dibujarlas\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s -f|-n -e|-u|-g|-p|-t dirSERIES fichPTOS "
                   "BLOQUE1 [BLOQUE2 [BLOQUE3 [...]]]\n\n",nombre);
    fprintf(stderr,"\t-f|-n: Identificador para generar o no script de Octave\n"
                   "\t       para crear los gráficos de velocidades. Varias\n"
                   "\t       opciones:\n"
                   "\t\t-f: Se crea el script\n"
                   "\t\t-n: No se crea el script\n"
                   "\t-e|-u|-g|-p|-t: Tipo de sistema de coordenadas al que\n"
                   "\t                vendrán referidas las series. Varias\n"
                   "\t                opciones:\n"
                   "\t\t-e: Sistema ENU\n"
                   "\t\t-u: Sistema UTM\n"
                   "\t\t-g: Sistema cartesiano tridimensional geocéntrico\n"
                   "\t\t-p: Sistemas ENU y UTM\n"
                   "\t\t-t: Sistemas ENU, UTM y tridimensional geocéntrico\n");
    fprintf(stderr,"\tdirSERIES: Nombre del directorio donde se almacenarán\n"
                   "\t           los ficheros con las series temporales\n");
    fprintf(stderr,"\tfichPTOS: Nombre del fichero de base de datos de ");
    fprintf(stderr,"puntos de trabajo\n");
    fprintf(stderr,"\tBLOQUE1: Identificador del un bloque de puntos a\n");
    fprintf(stderr,"\t         extraer del fichero de base de datos de");
    fprintf(stderr,"puntos\n");
    fprintf(stderr,"\tBLOQUE2,3,...: Identificador del otros bloques de\n");
    fprintf(stderr,"\t               puntos a extraer del fichero de base\n");
    fprintf(stderr,"\t               de datos de puntos\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Los datos solicitados se imprimen en la salida ");
    fprintf(stderr,"estándar (stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2009, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
