/**
\file esnx.c
\brief Programa para la extraccion de las estadisticas del ajuste de un fichero
       SINEX (Solution INdependent EXchange).
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"errores.h"
#include"paramprg.h"
#include"snx.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "esnx"
//numero minimo de argumentos de entrada
#define MINARG 2
//numero maximo de argumentos de entrada
#define MAXARG 3
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
void ImprimeTodo(statsnx* estadisticas);
void ImprimeParametro(char argumento[],
                      statsnx* estadisticas);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //código de error
    int cError=ERRNOERROR;
    //cadenas para almacenar el nombre del argumento de entrada
    char argv1[LONMAXNOMBREARG+2];
    //nombre del fichero de trabajo
    char fichero[LONMAXVALORARG+2];
    //estructura statsnx
    statsnx estadisticas;
    //indicador de impresion de la estimación de la varianza del observable de
    //peso unidad a posteriori si se ha pedido y hay un error
    int imprimeVarfact=0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc<MINARG)||(argc>MAXARG))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //inicializamos el argumento de entrada a valor vacio
    strcpy(argv1,FLAGVACIO);
    //extraemos el nombre del fichero SINEX
    strcpy(fichero,argv[1]);
    //extraccion del resto de argumentos de entrada
    if(argc==3)
    {
        //si solo se ha pasado el nombre del fichero la salida son los códigos
        //de las estaciones
        strcpy(argv1,argv[2]);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //indicador de impresion de la estimación de la varianza del observable de
    //peso unidad a posteriori si se ha pedido y hay un error
    if(!strcmp(argv1,FLAGVARFACT))
    {
        imprimeVarfact = 1;
    }
    //leemos las estadisticas
    cError = LeeEstadisticasFicheroSinex(fichero,imprimeVarfact,&estadisticas);
    //comprobamos el código de error devuelto
    if(cError!=ERRNOERROR)
    {
        //lanzamos el mensaje de error
        MensajeErrorPrograma(cError,PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //imprimimos los datos solicitados
    if(argc==2)
    {
        ImprimeTodo(&estadisticas);
    }
    else
    {
        ImprimeParametro(argv1,&estadisticas);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Extrae las estadísticas del ajuste de un fichero\n");
    fprintf(stderr,"SINEX (Solution INdependent EXchange)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s fichSINEX [%s|%s|%s|%s|%s|%s|%s|%s|%s]\n\n",nombre,
            FLAGNOBS,FLAGNUNK,FLAGSINT,FLAGSSR,FLAGPMS,FLAGCMS,FLAGNFREE,
            FLAGVARFACT,FLAGWSS);
    fprintf(stderr,"\tfichSINEX: Nombre del fichero SINEX de trabajo\n");
    fprintf(stderr,"\t%s: Imprime el número de observaciones\n",FLAGNOBS);
    fprintf(stderr,"\t%s: Imprime el número de incógnitas\n",FLAGNUNK);
    fprintf(stderr,"\t%s: Imprime el intervalo de muestreo en segundos\n",
            FLAGSINT);
    fprintf(stderr,"\t%s: Imprime el sumatorio del cuadrado de los ",FLAGSSR);
    fprintf(stderr,"residuos\n");
    fprintf(stderr,"\t%s: Imprime el parámetro sigma de la medida de fase\n",
            FLAGPMS);
    fprintf(stderr,"\t%s: Imprime el parámetro sigma de la medida de ",FLAGCMS);
    fprintf(stderr,"código\n");
    fprintf(stderr,"\t%s: Imprime el número de grados de libertad\n",FLAGNFREE);
    fprintf(stderr,"\t%s: Imprime la estimación de la varianza ",FLAGVARFACT);
    fprintf(stderr,"del observable\n\t          de peso unidad a posteriori\n");
    fprintf(stderr,"\t%s: Imprime el parámetro WEIGHTED SQUARE SUM OF O-C\n",
            FLAGWSS);
    fprintf(stderr,"\n");
    fprintf(stderr,"**El argumento fichSINEX es obligatorio, mientras que\n");
    fprintf(stderr,"  el resto es opcional\n");
    fprintf(stderr,"**Si sólo se pasa fichSINEX como argumento de entrada,\n");
    fprintf(stderr,"  el programa devuelve todo el texto del bloque %s\n",
            ETIQUETASOLSTAT);
    fprintf(stderr,"**Si se pasan dos argumentos de entrada el programa\n");
    fprintf(stderr,"  devuelve el valor del parámetro pedido\n");
    fprintf(stderr,"**Si se pide el parámetro varfact y este no existe, se\n");
    fprintf(stderr,"  devuelve el valor 1.0 y un mensaje de error\n");
    fprintf(stderr,"**Los datos solicitados se imprimen en la salida ");
    fprintf(stderr,"estandar (stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la salida ");
    fprintf(stderr,"de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Píblica General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeTodo(statsnx* estadisticas)
{
    //formato de la presentacion en pantalla
    char formato[LONFORMATO+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //imprimimos el parámetro numero de observaciones
    if(estadisticas->enobs)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMNOBS);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUNOBSSOLSTAT,estadisticas->nobs);
    }
    //imprimimos el parámetro numero de incognitas
    if(estadisticas->enunk)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMNUNK);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUNUNKSOLSTAT,estadisticas->nunk);
    }
    //imprimimos el parámetro intervalo de muestreo
    if(estadisticas->esint)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMSINT);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUSINTSOLSTAT,estadisticas->sint);
    }
    //imprimimos el parámetro sumatorio del cuadrado de los residuos
    if(estadisticas->essr)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMSSR);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUSSRSOLSTAT,estadisticas->ssr);
    }
    //imprimimos el parámetro sigma de la medida de fase
    if(estadisticas->epms)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMPMS);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUPMSSOLSTAT,estadisticas->pms);
    }
    //imprimimos el parámetro sigma de la medida de código
    if(estadisticas->ecms)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMCMS);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUCMSSOLSTAT,estadisticas->cms);
    }
    //imprimimos el parámetro grados de libertad
    if(estadisticas->enfree)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMNFREE);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUNFREESOLSTAT,estadisticas->nfree);
    }
    //imprimimos el parámetro estimación de la varianza del observable de peso
    //unidad a posteriori
    if(estadisticas->evarfact)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMVARFACT);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUVARFACTSOLSTAT,estadisticas->varfact);
    }
    //imprimimos el parámetro WEIGHTED SQUARE SUM OF O-C
    if(estadisticas->ewss)
    {
        //creamos la cadena de formato
        sprintf(formato,"%s%s%s\n",FORMNPARAM,SEPARADOR,FORMWSS);
        //imprimo el parámetro
        fprintf(stdout,formato,ETIQUWSSSOLSTAT,estadisticas->wss);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
void ImprimeParametro(char argumento[],
                      statsnx* estadisticas)
{
    //formato de la presentacion en pantalla
    char formato[LONFORMATO+2];
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //vamos comprobando el argumento a imprimir
    if(!strcmp(argumento,FLAGNOBS))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->enobs)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMNOBS);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->nobs);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'nobs'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGNUNK))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->enunk)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMNUNK);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->nunk);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'nunk'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGSINT))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->esint)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMSINT);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->sint);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'sint'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGSSR))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->essr)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMSSR);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->ssr);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'ssr'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGPMS))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->epms)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMPMS);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->pms);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'pms'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGCMS))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->ecms)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMCMS);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->cms);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'cms'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGNFREE))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->enfree)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMNFREE);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->nfree);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'nfree'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGVARFACT))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->evarfact)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMVARFACT);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->varfact);
        }
        else
        {
            //devolvemos el valor 1.0
            fprintf(stdout,"1.0\n");
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'varfact'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else if(!strcmp(argumento,FLAGWSS))
    {
        //imprimimos el parámetro si existe
        if(estadisticas->ewss)
        {
            //creamos la cadena de formato
            sprintf(formato,"%s\n",FORMWSS);
            //imprimo el código de la estacion
            fprintf(stdout,formato,estadisticas->wss);
        }
        else
        {
            //lanzamos el mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            En el bloque %s\n",ETIQUETASOLSTAT);
            fprintf(stderr,"            No existe el campo 'wss'\n");
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        fprintf(stderr,"\n");
        fprintf(stderr,"*****AVISO: En el programa %s\n",PROGRAMA);
        fprintf(stderr,"            El argumento %s es incorrecto\n",argumento);
        fprintf(stderr,"\n");
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
