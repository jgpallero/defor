/**
\file utmgeo.c
\brief Programa para la transformacion de coordenadas UTM en coordenadas
       geodesicas.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 28 de noviembre de 2010
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a traves
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"paramprg.h"
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "utmgeo"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadenas de formato para entrada y salidas incompleta y completa
    char fte[LONFORMATO+2],ftsi[LONFORMATO+2],ftsc[LONFORMATO+2];
    char ftsih[LONFORMATO+2],ftsch[LONFORMATO+2];
    //parametros de entrada por pipe
    char cod[LONCOD+2];
    int huso=0,hemis=0;
    double husoAux=0.0,hemisAux=0.0;
    double x=0.0,y=0.0,h=0.0;
    double varx=0.0,varxy=0.0,varxh=0.0,vary=0.0,varyh=0.0,varh=0.0;
    //parametros de salida
    double lat=0.0,lon=0.0;
    double varlat=0.0,varlatlon=0.0,varlon=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=2)&&(argc!=3)&&(argc!=4)&&(argc!=5)&&(argc!=6)&&(argc!=7)&&
       (argc!=8)&&(argc!=10)&&(argc!=12)&&(argc!=14))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos los formatos de salida incompleta
    sprintf(ftsi,"%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,FORMLAT,SEPARADOR,FORMLON);
    sprintf(ftsih,"%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,FORMLAT,SEPARADOR,FORMLON,SEPARADOR,FORMH);
    //creamos el formato de salida completa
    sprintf(ftsc,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,
            FORMLAT,SEPARADOR,FORMLON,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    sprintf(ftsch,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,
            FORMLAT,SEPARADOR,FORMLON,SEPARADOR,FORMH,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if((argc==2)||(argc==3))
    {
        //compruebo si el argumento indicador de entrada por pipe es correcto
        if(strcmp(argv[1],FLAGPIPE))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Argumento %s incorrecto\n",argv[1]);
            fprintf(stderr,"            El programa finalizara\n");
            fprintf(stderr,"\n");
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //construyo el formato de entrada con el maximo numero de argumentos
        sprintf(fte,
        "%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           cod,
                           &x,&y,&h,&husoAux,&hemisAux,
                           &varx,&varxy,&varxh,&vary,&varyh,&varh);
            //inicializamos todos los elementos opcionales a 0
            strcpy(cod,"");
            h = 0.0;
            varx = 0.0;
            varxy = 0.0;
            varxh = 0.0;
            vary = 0.0;
            varyh = 0.0;
            varh = 0.0;
            //dependiendo del numero de argumentos hago la lectura correcta
            if(nElem==4)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&y,&husoAux,&hemisAux);
                //convertimos en numero la cadena de texto leida como coordenda
                x = atof(cod);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==5)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&x,&y,&husoAux,&hemisAux);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
            }
            else if(nElem==5)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&y,&h,&husoAux,&hemisAux);
                //convertimos en numero la cadena de texto leida como coordenda
                x = atof(cod);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==6)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&x,&y,&h,&husoAux,&hemisAux);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
            }
            else if(nElem==7)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&y,&husoAux,&hemisAux,
                       &varx,&varxy,&vary);
                //convertimos en numero la cadena de texto leida como coordenda
                x = atof(cod);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==8)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&x,&y,&husoAux,&hemisAux,
                       &varx,&varxy,&vary);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
            }
            else if(nElem==11)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&y,&h,&husoAux,&hemisAux,
                       &varx,&varxy,&varxh,&vary,&varyh,&varh);
                //convertimos en numero la cadena de texto leida como coordenda
                x = atof(cod);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if((nElem==12)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD)))
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,cod,&x,&y,&h,&husoAux,&hemisAux,
                       &varx,&varxy,&varxh,&vary,&varyh,&varh);
                //pasamos el huso y hemisferio a numeros enteros
                huso = (int)husoAux;
                hemis = (int)hemisAux;
            }
            else
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //calculamos
            UtmGeoVC(x,y,varx,varxy,vary,huso,hemis,
                     SEMI_MAYOR,APL,
                     &lat,&lon,&varlat,&varlatlon,&varlon);
            //convertimos los angulos a grados sexagesimales en formato decimal
            lat *= RD;
            lon *= RD;
            varlat *= (RD*RD);
            varlatlon *= (RD*RD);
            varlon *= (RD*RD);
            //presentamos los resultados en la salida estandar
            if((nElem==4)||((nElem==5)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD))))
            {
                //imprimimos resultados
                fprintf(stdout,ftsi,cod,lat,lon);
            }
            else if((nElem==5)||
                    ((nElem==6)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD))))
            {
                //imprimimos resultados
                fprintf(stdout,ftsih,cod,lat,lon,h);
            }
            else if((nElem==7)||
                    ((nElem==8)&&(argc==3)&&(!strcmp(argv[2],FLAGCOD))))
            {
                //imprimimos resultados
                fprintf(stdout,ftsc,cod,lat,lon,varlat,varlatlon,varlon);
            }
            else
            {
                //imprimimos resultados
                fprintf(stdout,ftsch,cod,lat,lon,h,
                        varlat,varlatlon,0.0,varlon,0.0,varh);
            }
        }
    }
    else
    {
        //inicializamos todos los elementos opcionales a 0
        strcpy(cod,"");
        h = 0.0;
        varx = 0.0;
        varxy = 0.0;
        varxh = 0.0;
        vary = 0.0;
        varyh = 0.0;
        varh = 0.0;
        //asignamos los elementos de entrada dependiendo del numero
        if(argc==5)
        {
            //extraemos los datos de trabajo
            x = atof(argv[1]);
            y = atof(argv[2]);
            huso = atoi(argv[3]);
            hemis = atoi(argv[4]);
        }
        else if(argc==6)
        {
            //extraemos los datos de trabajo
            x = atof(argv[1]);
            y = atof(argv[2]);
            h = atof(argv[3]);
            huso = atoi(argv[4]);
            hemis = atoi(argv[5]);
        }
        else if((argc==7)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            x = atof(argv[3]);
            y = atof(argv[4]);
            huso = atoi(argv[5]);
            hemis = atoi(argv[6]);
        }
        else if((argc==8)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            x = atof(argv[3]);
            y = atof(argv[4]);
            h = atof(argv[5]);
            huso = atoi(argv[6]);
            hemis = atoi(argv[7]);
        }
        else if(argc==8)
        {
            //extraemos los datos de trabajo
            x = atof(argv[1]);
            y = atof(argv[2]);
            huso = atoi(argv[3]);
            hemis = atoi(argv[4]);
            varx = atof(argv[5]);
            varxy = atof(argv[6]);
            vary = atof(argv[7]);
        }
        else if((argc==10)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            x = atof(argv[3]);
            y = atof(argv[4]);
            huso = atoi(argv[5]);
            hemis = atoi(argv[6]);
            varx = atof(argv[7]);
            varxy = atof(argv[8]);
            vary = atof(argv[9]);
        }
        else if(argc==12)
        {
            //extraemos los datos de trabajo
            x = atof(argv[1]);
            y = atof(argv[2]);
            h = atof(argv[3]);
            huso = atoi(argv[4]);
            hemis = atoi(argv[5]);
            varx = atof(argv[6]);
            varxy = atof(argv[7]);
            varxh = atof(argv[8]);
            vary = atof(argv[9]);
            varyh = atof(argv[10]);
            varh = atof(argv[11]);
        }
        else if((argc==14)&&(!strcmp(argv[1],FLAGCOD)))
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[2]);
            x = atof(argv[3]);
            y = atof(argv[4]);
            h = atof(argv[5]);
            huso = atoi(argv[6]);
            hemis = atoi(argv[7]);
            varx = atof(argv[8]);
            varxy = atof(argv[9]);
            varxh = atof(argv[10]);
            vary = atof(argv[11]);
            varyh = atof(argv[12]);
            varh = atof(argv[13]);
        }
        else
        {
            //lanzamos un mensaje de ayuda
            MensajeAyuda(PROGRAMA);
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //calculamos
        UtmGeoVC(x,y,varx,varxy,vary,huso,hemis,
                 SEMI_MAYOR,APL,
                 &lat,&lon,&varlat,&varlatlon,&varlon);
        //convertimos los angulos a grados sexagesimales en formato decimal
        lat *= RD;
        lon *= RD;
        varlat *= (RD*RD);
        varlatlon *= (RD*RD);
        varlon *= (RD*RD);
        //presentamos los resultados en la salida estandar
        if((argc==5)||((argc==7)&&(!strcmp(argv[1],FLAGCOD))))
        {
            //imprimimos resultados
            fprintf(stdout,ftsi,cod,lat,lon);
        }
        else if((argc==6)||((argc==8)&&(!strcmp(argv[1],FLAGCOD))))
        {
            //imprimimos resultados
            fprintf(stdout,ftsih,cod,lat,lon,h);
        }
        else if((argc==8)||((argc==10)&&(!strcmp(argv[1],FLAGCOD))))
        {
            //imprimimos resultados
            fprintf(stdout,ftsi,cod,lat,lon,varlat,varlatlon,varlon);
        }
        else
        {
            //imprimimos resultados
            fprintf(stdout,ftsi,cod,lat,lon,h,
                    varlat,varlatlon,0.0,varlon,0.0,varh);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Pasa de coordenadas UTM a coordenadas geodesicas\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,
          "%s [%s] [%s [cod]] x y [h] Huso Hemis [Sx2 Sxy Sxh Sy2 Syh Sh2]\n\n",
            nombre,FLAGPIPE,FLAGCOD);
    fprintf(stderr,"\t%s: Indicador de que la entrada es a traves ",FLAGPIPE);
    fprintf(stderr,"de una tuberia (pipe)\n");
    fprintf(stderr,
            "\t%s: Indicador de que el primer dato es un codigo de punto\n",
            FLAGCOD);
    fprintf(stderr,"\tcod: Codigo de la estacion\n");
    fprintf(stderr,"\tx: Coordenada X UTM\n");
    fprintf(stderr,"\ty: Coordenada Y UTM\n");
    fprintf(stderr,"\th: Altitud elipsoidal\n");
    fprintf(stderr,"\tHuso: Huso UTM\n");
    fprintf(stderr,"\tHemis: Hemisferio en el que se encuentra el punto:\n");
    fprintf(stderr,"\t       - 1: Norte\n");
    fprintf(stderr,"\t       - 0: Sur\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxh: Covarianza entre las coordenadas X y h\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyh: Covarianza entre las coordenadas Y y h\n");
    fprintf(stderr,"\tSh2: Varianza de la coordenada h\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El formato de salida es el siguiente:\n\n");
    fprintf(stderr,"\t[cod] lat lon [h] [Slat2 Slatlon 0.0 Slon2 0.0 Sh2]\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"\tcod: Codigo de la estacion\n");
    fprintf(stderr,"\tlat: Latitud geodesica. En grados sexagesimales, ");
    fprintf(stderr,"formato decimal\n");
    fprintf(stderr,"\tlon: Longitud geodesica. En grados sexagesimales, ");
    fprintf(stderr,"formato decimal\n");
    fprintf(stderr,"\th: Altitud elipsoidal\n");
    fprintf(stderr,"\tSlat2: Varianza de la latitud\n");
    fprintf(stderr,"\tSlatlon: Covarianza entre la latitud y la longitud\n");
    fprintf(stderr,"\tSlon2: Varianza de la longitud\n");
    fprintf(stderr,"\tSh2: Varianza de la altitud elipsoidal\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a traves de una tuberia solo\n");
    fprintf(stderr,"  deben pasarse los argumentos %s (obligatorio)  y\n"
                   "  %s (opcional), ninguno mas\n",FLAGPIPE,FLAGCOD);
    fprintf(stderr,"**Las varianzas y covarianzas de las unidades angulares\n");
    fprintf(stderr,"  se expresan en grados sexagesimales elevadas a la\n");
    fprintf(stderr,"  potencia correspondiente\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estandar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la ");
    fprintf(stderr,"salida de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) Jose Luis Garcia Pallero, 2010, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Publica General ");
    fprintf(stderr,"GNU (GPL)\nen su version 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
