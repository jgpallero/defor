/**
\file trigeo.c
\brief Programa para la transformacion de coordenadas cartesianas
       tridimensionales geocéntricas en coordenadas geodésicas.
\author Jose Luis Garcia Pallero, jgpallero@gmail.com, jlgpallero@pdi.ucm.es
\date 25 de agosto de 2008
\version 1.0
\section Licencia Licencia
Este programa es software libre. Usted puede redistribuirlo y/o modificarlo bajo
los terminos de la Licencia Publica General GNU (GPL), en su version 3 o
posterior, publicada por la Free Software Foundation (FSF)
Puede obtener una copia de la GPL o ponerse en contacto con la FSF a través
de las direcciones: http://www.fsf.org o http://www.gnu.org

This program is free software. You can redistribute it and/or modify it under
the terms of the GNU General Public License (GPL) as published by the Free
Software Foundation (FSF), either version 3 of the License, or (at your option)
any later version.
You can obtain a copy of the GPL or contact with the FSF in: http://www.fsf.org
or http://www.gnu.org
*/
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include"paramprg.h"
#include"geodesia.h"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//nombre del programa
#define PROGRAMA "trigeo"
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//declaracion de funciones auxiliares
void MensajeAyuda(char nombre[]);
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
int main(int argc,char* argv[])
{
    //cadena para almacenar la entrada por pipe
    char entrada[LONPIPE+2];
    //numero de elementos leidos de la entrada por pipe
    int nElem=0;
    //cadenas de formato para entrada y salidas incompleta y completa
    char fte[LONFORMATO+2],ftsi[LONFORMATO+2],ftsc[LONFORMATO+2];
    //parametros de entrada por pipe
    char cod[LONCOD+2];
    double x=0.0,y=0.0,z=0.0;
    double varx=0.0,varxy=0.0,varxz=0.0,vary=0.0,varyz=0.0,varz=0.0;
    //parametros de salida
    double lat=0.0,lon=0.0,h=0.0;
    double varlat=0.0,varlatlon=0.0,varlath=0.0,varlon=0.0,varlonh=0.0,varh=0.0;
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //comprobamos si el numero de argumentos de entrada es correcto
    if((argc!=2)&&(argc!=4)&&(argc!=5)&&(argc!=10)&&(argc!=11))
    {
        //lanzamos un mensaje de ayuda
        MensajeAyuda(PROGRAMA);
        //salimos del programa
        exit(EXIT_FAILURE);
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //creamos el formato de salida incompleta
    sprintf(ftsi,"%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,
            FORMLAT,SEPARADOR,FORMLON,SEPARADOR,FORMH);
    //creamos el formato de salida completa
    sprintf(ftsc,"%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
            FORMCOD,SEPARADOR,
            FORMLAT,SEPARADOR,FORMLON,SEPARADOR,FORMH,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC,SEPARADOR,
            FORMVC,SEPARADOR,FORMVC,SEPARADOR,FORMVC);
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //distingo entre entrada por pipe o en la linea de argumentos
    if(argc==2)
    {
        //compruebo si el argumento indicador de entrada por pipe es correcto
        if(strcmp(argv[1],FLAGPIPE))
        {
            //lanzamos un mensaje de error
            fprintf(stderr,"\n");
            fprintf(stderr,"*****ERROR: En el programa %s\n",PROGRAMA);
            fprintf(stderr,"            Argumento %s incorrecto\n",argv[1]);
            fprintf(stderr,"            El programa finalizará\n");
            fprintf(stderr,"\n");
            //salimos del programa
            exit(EXIT_FAILURE);
        }
        //construyo el formato de entrada con el maximo numero de argumentos
        sprintf(fte,"%%s%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf%s%%lf",
                SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,SEPARADOR,
                SEPARADOR,SEPARADOR,SEPARADOR);
        //vamos leyendo la entrada del pipe
        while(fgets(entrada,LONPIPE+2,stdin))
        {
            //leo la entrada para contar el numero de argumentos
            nElem = sscanf(entrada,fte,
                           cod,
                           &x,&y,&z,
                           &varx,&varxy,&varxz,&vary,&varyz,&varz);
            //dependiendo del numero de argumentos hago la lectura correcta
            if(nElem==3)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,&y,&z);
                //convertimos en numero la cadena de texto leida como coordenda
                x = atof(cod);
                //asignamos 0.0 a los datos no pasados
                varx = 0.0;
                varxy = 0.0;
                varxz = 0.0;
                vary = 0.0;
                varyz = 0.0;
                varz = 0.0;
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if(nElem==4)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,
                       &x,&y,&z);
                //asignamos 0.0 a los datos no pasados
                varx = 0.0;
                varxy = 0.0;
                varxz = 0.0;
                vary = 0.0;
                varyz = 0.0;
                varz = 0.0;
            }
            else if(nElem==9)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,&y,&z,
                       &varx,&varxy,&varxz,&vary,&varyz,&varz);
                //convertimos en numero la cadena de texto leida como coordenda
                x = atof(cod);
                //asignamos al codigo una cadena vacia
                strcpy(cod,"");
            }
            else if(nElem==10)
            {
                //leemos los datos de entrada
                sscanf(entrada,fte,
                       cod,
                       &x,&y,&z,
                       &varx,&varxy,&varxz,&vary,&varyz,&varz);
            }
            else
            {
                //lanzamos un mensaje de ayuda
                MensajeAyuda(PROGRAMA);
                //salimos del programa
                exit(EXIT_FAILURE);
            }
            //calculamos
            TriGeoVC(SEMI_MAYOR,APL,
                     x,y,z,
                     varx,varxy,varxz,vary,varyz,varz,
                     &lat,&lon,&h,
                     &varlat,&varlatlon,&varlath,&varlon,&varlonh,&varh);
            //convertimos los angulos a grados sexagesimales en formato decimal
            lat *= RD;
            lon *= RD;
            varlat *= (RD*RD);
            varlatlon *= (RD*RD);
            varlath *= RD;
            varlon *= (RD*RD);
            varlonh *= RD;
            //presentamos los resultados en la salida estandar
            if((nElem==3)||(nElem==4))
            {
                //imprimimos resultados
                fprintf(stdout,ftsi,
                        cod,
                        lat,lon,h);
            }
            else
            {
                //imprimimos resultados
                fprintf(stdout,ftsc,
                        cod,
                        lat,lon,h,
                        varlat,varlatlon,varlath,varlon,varlonh,varh);
            }
        }
    }
    else
    {
        //asignamos los elementos de entrada dependiendo del numero
        if(argc==4)
        {
            //asignamos al codigo una cadena vacia
            strcpy(cod,"");
            //extraemos los datos de trabajo
            x = atof(argv[1]);
            y = atof(argv[2]);
            z = atof(argv[3]);
            //asignamos 0.0 a los datos no pasados
            varx = 0.0;
            varxy = 0.0;
            varxz = 0.0;
            vary = 0.0;
            varyz = 0.0;
            varz = 0.0;
        }
        else if(argc==5)
        {
            //extraemos los datos de trabajo
            strcpy(cod,argv[1]);
            x = atof(argv[2]);
            y = atof(argv[3]);
            z = atof(argv[4]);
            //asignamos 0.0 a los datos no pasados
            varx = 0.0;
            varxy = 0.0;
            varxz = 0.0;
            vary = 0.0;
            varyz = 0.0;
            varz = 0.0;
        }
        else if(argc==10)
        {
            //asignamos al codigo una cadena vacia
            strcpy(cod,"");
            //extraemos los datos de trabajo
            x = atof(argv[1]);
            y = atof(argv[2]);
            z = atof(argv[3]);
            varx = atof(argv[4]);
            varxy = atof(argv[5]);
            varxz = atof(argv[6]);
            vary = atof(argv[7]);
            varyz = atof(argv[8]);
            varz = atof(argv[9]);
        }
        else
        {
            //asignamos al codigo una cadena vacia
            strcpy(cod,argv[1]);
            x = atof(argv[2]);
            y = atof(argv[3]);
            z = atof(argv[4]);
            varx = atof(argv[5]);
            varxy = atof(argv[6]);
            varxz = atof(argv[7]);
            vary = atof(argv[8]);
            varyz = atof(argv[9]);
            varz = atof(argv[10]);
        }
        //calculamos
        TriGeoVC(SEMI_MAYOR,APL,
                 x,y,z,
                 varx,varxy,varxz,vary,varyz,varz,
                 &lat,&lon,&h,
                 &varlat,&varlatlon,&varlath,&varlon,&varlonh,&varh);
        //convertimos los angulos a grados sexagesimales en formato decimal
        lat *= RD;
        lon *= RD;
        varlat *= (RD*RD);
        varlatlon *= (RD*RD);
        varlath *= RD;
        varlon *= (RD*RD);
        varlonh *= RD;
        //presentamos los resultados en la salida estandar
        if((argc==4)||(argc==5))
        {
            //imprimimos resultados
            fprintf(stdout,ftsi,
                    cod,
                    lat,lon,h);
        }
        else
        {
            //imprimimos resultados
            fprintf(stdout,ftsc,
                    cod,
                    lat,lon,h,
                    varlat,varlatlon,varlath,varlon,varlonh,varh);
        }
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos del main
    return 0;
}
/******************************************************************************/
/******************************************************************************/
/******************************************************************************/
//DEFINICION DE FUNCIONES AUXILIARES
void MensajeAyuda(char nombre[])
{
    fprintf(stderr,"\n");
    fprintf(stderr,"Pasa de coordenadas cartesianas tridimensionales\n");
    fprintf(stderr,"geocéntricas a coordenadas geodésicas\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"Uso del programa %s:\n\n",nombre);
    fprintf(stderr,"%s [%s] [cod] x y z [Sx2 Sxy Sxz Sy2 Syz Sz2]\n\n",nombre,
            FLAGPIPE);
    fprintf(stderr,"\t%s: Indicador de que la entrada es a través ",FLAGPIPE);
    fprintf(stderr,"de una tubería (pipe)\n");
    fprintf(stderr,"\tcod: Codigo de la estación\n");
    fprintf(stderr,"\tx: Coordenada X geocéntrica\n");
    fprintf(stderr,"\ty: Coordenada Y geocéntrica\n");
    fprintf(stderr,"\tz: Coordenada Z geocéntrica\n");
    fprintf(stderr,"\tSx2: Varianza de la coordenada X\n");
    fprintf(stderr,"\tSxy: Covarianza entre las coordenadas X e Y\n");
    fprintf(stderr,"\tSxz: Covarianza entre las coordenadas X y Z\n");
    fprintf(stderr,"\tSy2: Varianza de la coordenada Y\n");
    fprintf(stderr,"\tSyz: Covarianza entre las coordenadas Y y Z\n");
    fprintf(stderr,"\tSz2: Varianza de la coordenada Z\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**El formato de salida es el siguiente:\n\n");
    fprintf(stderr,"\t[cod] lat lon h [Slat2 Slatlon Slath Slon2 Slonh Sh2]\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"\tcod: Código de la estación\n");
    fprintf(stderr,"\tlat: Latitud geodésica. En grados sexagesimales, ");
    fprintf(stderr,"formato decimal\n");
    fprintf(stderr,"\tlon: Longitud geodésica. En grados sexagesimales, ");
    fprintf(stderr,"formato decimal\n");
    fprintf(stderr,"\th: Altitud elipsoidal\n");
    fprintf(stderr,"\tSlat2: Varianza de la latitud\n");
    fprintf(stderr,"\tSlatlon: Covarianza entre la latitud y la longitud\n");
    fprintf(stderr,"\tSlath: Covarianza entre la latitud y la altitud\n");
    fprintf(stderr,"\tSlon2: Varianza de la longitud\n");
    fprintf(stderr,"\tSlonh: Covarianza entre la longitud y la altitud\n");
    fprintf(stderr,"\tSh2: Varianza de la altitud elipsoidal\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los argumentos entre corchetes son opcionales\n");
    fprintf(stderr,"**Si la entrada se pasa a través de una tubería sólo\n");
    fprintf(stderr,"  debe pasarse el argumento %s, ninguno más\n",FLAGPIPE);
    fprintf(stderr,"**Las varianzas y covarianzas de las unidades angulares\n");
    fprintf(stderr,"  se expresan en grados sexagesimales elevadas a la\n");
    fprintf(stderr,"  potencia correspondiente\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"**Los resultados se imprimen en la salida estándar ");
    fprintf(stderr,"(stdout)\n");
    fprintf(stderr,"**Los mensajes de aviso y error se imprimen en la ");
    fprintf(stderr,"salida de error (stderr)\n");
    fprintf(stderr,"\n");
    fprintf(stderr,"(C) José Luis García Pallero, 2008, jgpallero@gmail.com\n");
    fprintf(stderr,"Este programa se acoge a la Licencia Pública General ");
    fprintf(stderr,"GNU (GPL)\nen su versión 3 o posterior (www.fsf.org)\n");
    fprintf(stderr,"\n");
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    //salimos de la funcion
    return;
}
